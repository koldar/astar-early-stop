/*
 * cliUtils.cpp
 *
 *  Created on: Oct 10, 2018
 *      Author: koldar
 */

#include "cliUtils.h"
#include <exception>
#include "regex.hpp"
#include <DynamicPathFinding/log.h>
#include <regex>
#include <string>

double probabilityFromString(const char* str) {
	std::regex re{"^(\\d+(?:\\.\\d+)?)\\%$"};
	std::smatch matches;
	std::string strStr{str};
	bool canMatch = std::regex_match(strStr, matches, re);
	if (!canMatch) {
		throw std::invalid_argument{"regex couldn't be matched"};
	}

	double result = std::stod(matches[1], nullptr);
	if (result > 100) {
		result = 100;
	}
	if (result < 0) {
		result = 0;
	}

	return result/100.;
}

double parseNumber(const char* str, long total) {
	std::regex re{"^(\\d+(?:\\.\\d+)?)(\\%)?$"};
	std::smatch matches;
	std::string strStr{str};
	bool canMatch = std::regex_match(strStr, matches, re);
	if (!canMatch) {
		throw std::invalid_argument{"regex couldn't be matched"};
	}

	double result = std::stod(matches[1], nullptr);

	debug("number is", matches[1], " and percentage is", matches[2]);

	if (matches[2].str().size() > 0) {
		//percentage
		if (result > 100) {
			result = 100;
		}
		if (result < 0) {
			result = 0;
		}

		return (result/100.0)*total;
	} else {
		//actual number
		return result;
	}


}

bool parseIntegerListFromString(const char* str, std::vector<int>& list) {

	std::string subject{str};
	try {
	  std::regex re("[\\+\\-]?\\d+");
	  std::sregex_iterator next(subject.begin(), subject.end(), re);
	  std::sregex_iterator end;
	  while (next != end) {
	    std::smatch match = *next;
	    list.push_back(atoi(match.str().c_str()));
	    next++;
	  }
	} catch (std::regex_error& e) {
	  // Syntax error in the regular expression
		throw std::invalid_argument{"regex for parsing integer list from string failed"};
	}

	return true;
}

bool parseIntegerSetFromString(const char* str, std::unordered_set<int>& set) {
	std::vector<int> v;
	if (!parseIntegerListFromString(str, v)) {
		return false;
	}

	set = std::unordered_set<int>{v.begin(), v.end()};

	return true;
}

void parseAcceptableIntegerListFromString(const char* str, const std::vector<int>& input, std::vector<int>& output) {
	if (path_tester_utils::doesStringMatch(str, std::string{"ALL"})) {
		output = std::vector<int>{input};
	} else {
		std::vector<int> parsed;
		parseIntegerListFromString(str, parsed);
		for (int v : parsed) {
			if (v >= 0) {
				output.push_back(input[v]);
			} else {
				output.push_back(input[input.size() + v]);
			}

		}
	}

}

