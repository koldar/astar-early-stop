/*
 * regex.cpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#include "regex.hpp"
#include <regex>

namespace path_tester_utils {


bool doesStringMatch(const std::string& str, const std::string& pattern) {
	std::string actualPattern = "^";
	actualPattern.append(pattern);
	actualPattern.append("$");
	std::regex re{actualPattern};
	std::smatch matches;
	std::string strStr{str};
	return std::regex_match(strStr, matches, re);
}

bool doesStringMatch(const std::string& str, const std::string& pattern, std::vector<std::string>& groups) {
	std::string actualPattern = std::string{"^"};
	actualPattern.append(pattern);
	actualPattern.append("$");
	std::regex re{actualPattern};
	std::smatch matches;
	std::string strStr{str};
	bool canMatch = std::regex_match(strStr, matches, re);
	if (!canMatch) {
		return false;
	}

	bool first = true;
	for(std::sregex_iterator it = std::sregex_iterator(strStr.begin(), strStr.end(), re); it != std::sregex_iterator(); ++it) {
		if (first) {
			first = false;
			continue;
		}
		std::smatch m = *it;
		groups.push_back(m.str());
	}

	return true;
}

};

