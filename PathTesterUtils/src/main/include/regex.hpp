/*
 * regex.hpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#ifndef REGEX_HPP_
#define REGEX_HPP_

#include <string>
#include <unordered_map>
#include <vector>

namespace path_tester_utils {

bool doesStringMatch(const std::string& str, const std::string& pattern);

bool doesStringMatch(const std::string& str, const std::string& pattern, std::vector<std::string>& groups);

};



#endif /* REGEX_HPP_ */
