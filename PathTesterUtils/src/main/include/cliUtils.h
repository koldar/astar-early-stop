/*
 * cliUtils.h
 *
 *  Created on: Oct 10, 2018
 *      Author: koldar
 */

#ifndef CLIUTILS_H_
#define CLIUTILS_H_

#include <exception>
#include <regex>
#include <string>
#include <stdlib.h>
#include <random>
#include <vector>
#include <unordered_set>

double probabilityFromString(const char* str);

/**
 * Parse a list of comma separated integers from a string
 *
 * @pre
 *  @li no spaces are allowed;
 *
 * @param[in] str the string to parse. For example <tT>1,2,3,4,5,6</tt>.
 * @param[out] list the list that will be populated with the parsed numbers. Number will be placed as present in the string
 * @return
 *  @li always true
 */
bool parseIntegerListFromString(const char* str, std::vector<int>& list);

double parseNumber(const char* str, long total);

/**
 * Like ::parseIntegerListFromString but returns a set, not a vector
 *
 *
 */
bool parseIntegerSetFromString(const char* str, std::unordered_set<int>& set);

/**
 * Parse a list of comma separated list and generate the "acceptable" value in @c input
 *
 * what is "acceptable" depends on what you want to do with the list
 *
 * @code
 *  parseAcceptableIntegerListFromString("0,1,2", {1,2,3,4}, output); //output={1,2,3}
 *  parseAcceptableIntegerListFromString("ALL", {1,2,3,4}, output); //output={1,2,3,4}
 *  parseAcceptableIntegerListFromString("0,1,3", {1,2,3,4}, output); //output={1,2,4}
 *  parseAcceptableIntegerListFromString("0,1,-1", {1,2,3,4}, output); //output={1,2,4}
 *  parseAcceptableIntegerListFromString("", {1,2,3,4}, output); //output={}
 * @endcode
 *
 * @param[in] str the string to test containing the indexes of "acceptable" values in @c input
 * @param[in] input the initial vector you want to filter
 * @param[in] output the vector containing all the values in @c input which are acceptable
 */
void parseAcceptableIntegerListFromString(const char* str, const std::vector<int>& input, std::vector<int>& output);

/**
 * A mathematical range
 *
 * @note
 * We expect @c VAl to be an integer type
 */
template<typename VAL>
class range {
private:
	VAL lowerBound;
	VAL upperBound;
	bool lbIncluded;
	bool ubIncluded;
public:
	range(const VAL& lowerBound, const VAL& upperBound, bool lbIncluded, bool ubIncluded) : lowerBound{lowerBound}, upperBound{upperBound}, lbIncluded{lbIncluded}, ubIncluded{ubIncluded} {

	}
	range() : range(0, 0, true, true) {

	}
	~range() {

	}

	/**
	 * @return the lowerrbound you can put to a c++ rnadom distribution in order to get a random number
	 */
	VAL getLB() const {
		return lowerBound + (lbIncluded ? 0 : 1);
	}

	/**
	 * @return the upperbound you can put to a c++ rnadom distribution in order to get a random number
	 */
	VAL getUB() const {
		return upperBound - (ubIncluded ? 0 : 1);
	}

	/**
	 * generate a range from a mathematical range string
	 *
	 * @code
	 * fromMath("[5;7["); //lowerbound=5, upperbound=7, lbincluded=tue, ubincluded=false
	 * @endocde
	 *
	 * @param[in] fromMath the string to consider
	 * @return the range structure generated
	 */
	static range<int> fromMath(const char* mathRange) {
		std::regex re{"^([\\[\\]\\(])(\\d+),(\\d+)([\\[\\]\\)])$"};
		std::smatch matches;
		std::string mathRangeStr{mathRange};
		bool canMatch = std::regex_match(mathRangeStr, matches, re);
		if (!canMatch) {
			throw std::invalid_argument{"retgex didn't match"};
		}

		range<int> result{};

		if (matches[1].str() == "[") {
			result.lbIncluded = true;
		} else if (matches[1].str() == "]") {
			result.lbIncluded = false;
		} else if (matches[1].str() == "(") {
			result.lbIncluded = false;
		} else {
			throw std::invalid_argument{"incompatible lower bound parenthesis"};
		}

		result.lowerBound = atoi(matches[2].str().c_str());
		result.upperBound = atoi(matches[3].str().c_str());

		if (matches[4].str() == "]") {
			result.ubIncluded = true;
		} else if (matches[4].str() == "[") {
			result.ubIncluded = false;
		} else if (matches[4].str() == ")") {
			result.ubIncluded = false;
		} else {
			throw std::invalid_argument{"incompatible upper bound parenthesis"};
		}

		return result;
	}
};



#endif /* CLIUTILS_H_ */
