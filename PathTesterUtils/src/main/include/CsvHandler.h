/**
 * @file
 *
 * Simple library to write onto a CSV
 *
 * @code
 * {
 *	CsvHandler foo{"foo.csv", "id", "value", "description"};
 *	foo.writeRow(5, true, "cow"};
 *	foo.writeRow(6, true, "cow"};
 *	foo.writeRow(7, false, "horse"};
 * }
 * //csv is automatically closed when the CsvHandler is destructed
 * @endcode
 *
 * @date Oct 10, 2018
 * @author koldar
 */

#ifndef CSVHANDLER_H_
#define CSVHANDLER_H_

#include <iostream>
#include <cstdarg>

/**
 * Allows to easily write standard CSV files
 *
 * @code
 * CsvHandler h = CsvHandler{"ciao.csv", "ID", "VALUE"};
 * h.writeRow(5,"cow");
 * h.writeRow(6,"cat");
 * h.writeRow(7,"dog");
 * //the file will automatically close after the variable goes out of scope
 *
 * @endcode
 */
class CsvHandler {
public:
	template<typename ... ARGS>
	CsvHandler(const char* csvFileName, char separator, ARGS... headerNames) : csvStream{csvFileName}, colSeparator{separator} {
		this->writeRow(headerNames...);
	}
	template<typename ... ARGS>
	CsvHandler(const char* csvFileName, ARGS... headerNames) : csvStream{csvFileName}, colSeparator{','} {
		this->writeRow(headerNames...);
	}
	~CsvHandler() {
		csvStream.close();
	}
public:
	/**
	 * Write a row in the csv
	 *
	 * @param[in] firstValue the first value to write
	 * @param[in] values all the other values to write
	 */
	template<typename FIRST_TYPE, typename ... OTHER_TYPES>
	void writeRow(FIRST_TYPE firstValue, OTHER_TYPES... values) {
		csvStream << firstValue;
		if (sizeof...(values) > 0) {
			csvStream << this->colSeparator;
		}
		this->writeRow(values...);
	}

	void writeRow() {
		csvStream << "\n";
	}

	/**
	 * set fixed mode
	 *
	 * This will print number normally.
	 * @code
	 * 123567.34 //print 123567.34
	 * @endcode
	 */
	CsvHandler& setFixedMode() {
		this->csvStream << std::fixed;
		return *this;
	}

	/**
	 * set scientific mode
	 *
	 * This will print number normally.
	 * @code
	 * 123567.34 //print 1.2356734E+05
	 * @endcode
	 */
	CsvHandler& setScientificMode() {
		this->csvStream << std::scientific;
		return *this;
	}

	/**
	 * Set the precision of how to format floating point numbers like @c double or @c float
	 *
	 * This won't be used at all if you're printing a container storing floating point numbers in the csv
	 *
	 * @param[in] precision the precision to use to format plain floating point numbers
	 */
	CsvHandler& setPrecision(int precision) {
		this->csvStream.precision(precision);
		return *this;
	}

private:
	std::ofstream csvStream;
	char colSeparator;
};



#endif /* CSVHANDLER_H_ */
