General Information
===================

This repository contains the source code for implementing CPD-Search.

The related paper can be cited as follows:

```

```

Example of CPD-Search call (all commands have `--help` for further information):

```
# create a perturbated map where 10% of the connectsion have been perturbated by a multiplier
# between 3 and 10
MapPerturbator \
	--map_file="../maps/hrt201n.map" \
	--scenario_file="../maps/hrt201n.map.scen" \
	--map_perturbation="RANDOM" \
	--perturbation_mode="MULTIPLY" \
	--perturbation_range="[3,10]" \
	--output_template="hrt201n_perturbated" \
	--validate_changes \
	--perturbation_density="5%"

# test the algorithm on the perturbated map just created
DynamicPathFindingTester \
	--map_file="../maps/hrt201n.map" \
	--scenario_file="../maps/hrt201n.map.scen" \
	--output_template="hrt201n_performance" \
	--algorithm="A*" \
	--heuristic="CPD_WITH_CACHE" \
	--enable_earlystop \
	--validate_paths \
	--perturbated_map_filename_template="hrt201n_perturbated" \
	--h_weight=1

A file called hrt201n_performancetype#mainOutput|.csv will be present in the CWD containing
```


Preconditions
=============

In order to use the project, you need:

 * METIS;
 * C++11 compiler;
 * Python3.6;
 * Python3.6-tk;
 * Python3.6-venv;
 * cmake;
 * graphviz;

 You can install them as follows:

```
sudo apt-get install libmetis5 libmetis-doc libmetis-dev metis
sudo apt-get install libomp-dev
sudo apt-get install python3.6
sudo apt-get install python3.6-tk python3.6-venv python3-venv
sudo apt-get install cmake

sudo apt-get install graphviz
```

Quick build
==========

```
cd <git repo>
make
```

The command will sequentially compile and install on the system the program.
Make will automatically install the software in /usr/local/.

to Clean everything:

```
make clean
```

Compiling warthog library
-------------------------

You might also need to manually compile warthog library (Makefile `make` automatically handles this). 
This requires you to do some manual work. We need to do 2 things:

 * update the makefile;
 * solve some compilation errors;

The first one is easy: in `others` folder there is a file called "MakeFile": go in pathfinding/src and replace the "Makefile" file there with the updated one.
The new Makefile allows you to easily install and uninstall the library. To compile warthog just do:

```
cd pathfinding/src
make fast
```

As for the second, you need to solve some trivial compilation error:

```
jps/offline_jump_point_locator.cpp:102:7: error: ignoring return value of ‘size_t fread(void*, size_t, size_t, FILE*)’, declared with attribute warn_unused_result [-Werror=unused-result]
  fread(&dbsize_, sizeof(dbsize_), 1, f);
  ~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
jps/offline_jump_point_locator.cpp:106:7: error: ignoring return value of ‘size_t fread(void*, size_t, size_t, FILE*)’, declared with attribute warn_unused_result [-Werror=unused-result]
  fread(db_, sizeof(uint16_t), dbsize_, f);
  ~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
jps/offline_jump_point_locator2.cpp: In member function ‘bool warthog::offline_jump_point_locator2::load(const char*)’:
jps/offline_jump_point_locator2.cpp:113:7: error: ignoring return value of ‘size_t fread(void*, size_t, size_t, FILE*)’, declared with attribute warn_unused_result [-Werror=unused-result]
  fread(&dbsize_, sizeof(dbsize_), 1, f);
  ~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
jps/offline_jump_point_locator2.cpp:117:7: error: ignoring return value of ‘size_t fread(void*, size_t, size_t, FILE*)’, declared with attribute warn_unused_result [-Werror=unused-result]
  fread(db_, sizeof(uint16_t), dbsize_, f);
  ~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

You can solve it with this code:

* add `size:_t useless =` to the first fread call;
* add `useless +=` to tge second fread call;
* add `size:_t useless =` to the third fread call;
* add `useless +=` to tge fourth fread call;

then, just do:

```
make fast
sudo make install
```

Build projects
==============

All the projects in the repository uses `cmake` as build program. This is handled by `make` in the root project folder.
You can build one among DynamicPathFinding, DynamicPathFindingTester, PathTesterUtils, MapPerturbator with the classic `cmake`:

```
cd <project>
mkdir -p build/Release
# use mkdir -p build/Debug if you want to create the debug version
cmake ../..
make
sudo make install
sudo ldconfig
```
In order to automatically handle the dependencies of the project, I've provided a script that handles them correctly:

```
./install.bash
```

If successful, you should see "DONE!" at the end of the console

Useful Information
==================

This project uses:

* C++11 C plus plus version;
* OMP https://gcc.gnu.org/onlinedocs/libgomp/index.html#Top

Contents
========

* pathfinding: git repo from https://bitbucket.org/dharabor/pathfinding/
* FirstMoveCompression: a project implementing a Compressed Path Database (CPD) which appeared at the 2014 Grid-based Path Planning Competition;
* DynamicPathFinding: a project generating a library implementing CPD Search;
* DynamicPathFindingTester: testing utlility to efficiently test CPD search on a specified map/scenario;
* PathTesterUtils: a library containing some shared code between testing projects;
* MapPerturbator: a library generating perturbated maps starting from an original one;


Tested Environment
==================

* ubuntu;
* 64bit machine;
* g++ 8.3.0;

Coding Style
============

No specific code style used.
