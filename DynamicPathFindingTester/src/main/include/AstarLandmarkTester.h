/*
 * AstarLandmarkTester.h
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#ifndef ASTARLANDMARKTESTER_H_
#define ASTARLANDMARKTESTER_H_

#include "AbstractAstarTest.h"

class AstarLandmarkTester: public AbstractAstarTester {
public:
	AstarLandmarkTester(const AdjGraph& oldMap, const std::string mapName, const GridMap& map, const Mapper& mapper, PerturbatedMapFetcher fetcher, const std::string& heuristicChoice, int landmarkNumber, bool enableStatistics, double hScale, const std::string& algorithmToUse, bool useAnytime, double anytimeBound, const std::string& outputTemplate) :
		AbstractAstarTester{oldMap, mapName, map, mapper, fetcher, heuristicChoice, enableStatistics, hScale},
		algorithmToUse{algorithmToUse},
		useAnytime{useAnytime}, anytimeBound{anytimeBound}, outputTemplate{outputTemplate},
		aStar{nullptr}, landmarkNumber{landmarkNumber}, h{nullptr}, landmarkDatabase{nullptr}
		{

		}

		~AstarLandmarkTester();
public:
		virtual dpf::cost_t getPathCostFromTester(const AdjGraph& perturbatedMap) const;

		virtual void setup();

		virtual void prepareBeforePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap);

		virtual void runSingleTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap);

		virtual void generateExpandedNodesImage(const std::string& outputTemplate, int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap);

		virtual uint32_t getExpandedNodes(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) const;

		virtual unsigned long getHeuristicTime(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap);

		virtual void finalizePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap);
private:
		bool useAnytime;
		double anytimeBound;
		const std::string& outputTemplate;
		int landmarkNumber;
		LandMarkDatabase* landmarkDatabase;
		abstract_pathfinding_astar<AbstractHeuristic, GridMapExpander, AbstractStats>* aStar;
		AbstractHeuristic* h;
		const std::string& algorithmToUse;
};



#endif /* ASTARLANDMARKTESTER_H_ */
