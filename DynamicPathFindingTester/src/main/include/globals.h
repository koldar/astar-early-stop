/*
 * globals.h
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <string>
#include <vector>
#include <random>

extern std::default_random_engine global_randomEngine;

extern std::string global_mapFilename;
extern std::string global_scenarioFilename;
extern std::string global_outputTemplate;
extern std::string global_heuristicChoice;
extern bool global_enableFixedUpperBoundOptimization;
extern bool global_enableEarlyStopOptimization;
extern std::string global_perturbationDensityStr;
extern int global_randomSeed;
extern bool global_validatePaths;
extern std::string global_algorithmToTest;
extern std::string global_perturbatedMapFilenameTemplate;
extern bool global_drawExpandedMaps;
extern std::string global_generateImagesOnlyForStr;
extern std::vector<int> global_generateImagesOnlyFor;
extern int global_landmarkNumber;

extern bool global_useBounds;
extern double global_bounded;
extern double global_hWeight;
extern bool global_testBoundedAlgorithms;

extern bool global_testAnyTimeAlgorithms;
extern double global_anytimeBound;


#endif /* GLOBALS_H_ */
