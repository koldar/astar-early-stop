/*
 * AstarTester.h
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#ifndef ASTARCPDTESTER_H_
#define ASTARCPDTESTER_H_

#include "AbstractAstarTest.h"
#include <DynamicPathFinding/abstract_weighted_astar.hpp>

class AstarCPDTester: public AbstractAstarTester {
public:
	AstarCPDTester(const AdjGraph& oldMap, const std::string mapName, const GridMap& map, PerturbatedMapFetcher fetcher, const std::string& heuristicChoice, CpdHeuristic& h, bool enableFixedUpperBoundOptimization, bool enableEarlyStopOptimization, bool enableStatistics, bool enableBoundSearch, double bound, double hScale, const char* algorithm, bool enableAnytimeSearch, double anytimeBound, const char* outputTemplate) :
		AbstractAstarTester{oldMap, mapName, map, h.getMapper(), fetcher, heuristicChoice, enableStatistics, hScale},
		cpdHeuristic{nullptr}, h{h},
		aStar{nullptr},
		enableFixedUpperBoundOptimization{enableFixedUpperBoundOptimization},
		enableEarlyStopOptimization{enableEarlyStopOptimization},
		enableBoundSearch{enableBoundSearch},
		bound{bound},
		//TODO this algorithm variable should be moved to AbstractTester
		algorithm{algorithm},
		enableAnytimeSearch{enableAnytimeSearch}, anytimeBound{anytimeBound}, outputTemplate{outputTemplate}
		{

		}

		~AstarCPDTester();
public:

		virtual dpf::cost_t getPathCostFromTester(const AdjGraph& perturbatedMap) const;

		virtual void setup();

		virtual void prepareBeforePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

		virtual void runSingleTest(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

		virtual void generateExpandedNodesImage(const std::string& outputTemplate, int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

		virtual uint32_t getExpandedNodes(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) const;

		virtual unsigned long getHeuristicTime(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

		virtual void finalizePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap);
private:
		bool enableEarlyStopOptimization;
		bool enableFixedUpperBoundOptimization;
		bool enableBoundSearch;
		bool enableAnytimeSearch;
		double anytimeBound;
		double bound;
		const char* algorithm;
		//dynamic_pathfinding_astar<CpdHeuristic, AbstractStats>* aStar;
		abstract_pathfinding_astar<CpdHeuristic, GridMapExpander, AbstractStats>* aStar;
		CpdHeuristic& h;
		CpdHeuristic* cpdHeuristic;
		const char* outputTemplate;
};



#endif /* ASTARCPDTESTER_H_ */
