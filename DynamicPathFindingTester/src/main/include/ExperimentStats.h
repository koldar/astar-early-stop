/**
 * @file
 *
 * Module representin the statistics of an experiment of path planning (ask to go from one point to another in a map)
 *
 * @date Oct 11, 2018
 * @author koldar
 */

#ifndef EXPERIMENTSTATS_H_
#define EXPERIMENTSTATS_H_

#include <vector>
#include <DynamicPathFinding/xyLoc.h>
#include <numeric>
#include <algorithm>
#include <DynamicPathFinding/types.h>

class ExperimentOutcome {
public:
	ExperimentOutcome(int id, double microSeconds, std::vector<xyLoc> path, dpf::big_integer pathCost, dpf::big_integer pathLength, dpf::big_integer pathStepSize, dpf::big_integer expandedNodesNumber, unsigned long heuristicMicroSeconds, bool hasOptimalPathPerturbated) :
		id{id}, microSeconds{microSeconds}, path{path}, pathCost{pathCost}, pathLength{pathLength}, pathStepsSize{pathStepSize}, expandedNodes{expandedNodesNumber}, heuristicMicroSeconds{heuristicMicroSeconds}, hasOptimalPathPerturbated{hasOptimalPathPerturbated} {

		}
public:
	int id;
	double microSeconds;
	std::vector<xyLoc> path;
	/**
	 * sum of the arcs weights in the path
	 */
	dpf::big_integer pathCost;
	/**
	 * sum of the arcs lengths.
	 *
	 * horizontal/vertical arcs weights 1, diagonal arcs weights 1.14
	 */
	dpf::big_integer pathLength;
	/**
	 * number of locations you need to traverse to reach the goal
	 */
	dpf::big_integer pathStepsSize;
	/**
	 * Number of nodes (aka locations) expanded in some way by an algorithm
	 */
	dpf::big_integer expandedNodes;
	/**
	 * microseconds we took to compute the heuristic
	 */
	unsigned long heuristicMicroSeconds;
	/**
	 * If true, it means that the optimal path in the original map has been perturbated in the revised map
	 */
	bool hasOptimalPathPerturbated;

};

class ExperimentStats {
public:
	/**
	 * Adds a new experiment outcome in the statistics
	 */
	void addOutcome(int id, double microSeconds, const std::vector<xyLoc>& path, dpf::big_integer pathCost, dpf::big_integer pathLength, dpf::big_integer pathStepsSize, dpf::big_integer expandedNodes, unsigned long heuristicTime, bool hasOptimalPathPerturbated);
	/**
	 * @return sum of all the outcomes times
	 */
	double GetTotalTime() const;
	/**
	 * @return max of all the outcomes times
	 */
	double GetMaxTimestep() const;

	/**
	 * @return the time we spent ocmputing the heuristic during the search (in microseconds).
	 *
	 */
	unsigned long getHeuristicMicroSeconds() const;

	std::vector<ExperimentOutcome>::const_iterator begin() const;

	std::vector<ExperimentOutcome>::const_iterator end() const;
private:
	/**
	 * Each experiment performed
	 */
	std::vector<ExperimentOutcome> outcomes;

};



#endif /* EXPERIMENTSTATS_H_ */
