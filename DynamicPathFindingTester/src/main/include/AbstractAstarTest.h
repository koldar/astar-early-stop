/*
 * AbstarctAstarTest.cpp
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#ifndef ABSTARCTASTARTEST_CPP_
#define ABSTARCTASTARTEST_CPP_

#include <DynamicPathFinding/cpd_heuristic.h>
#include <DynamicPathFinding/cpd_cached_heuristic.h>
#include <DynamicPathFinding/dynamic_pathfinding_astar.h>
#include <DynamicPathFinding/quick_statistics.h>
#include <DynamicPathFinding/simple_pathfinding_astar.h>
#include <DynamicPathFinding/differential_heuristic.h>
#include <DynamicPathFinding/grid_map_expander.h>
#include <vector>
#include "AbstractTest.h"

class AbstractAstarTester: public AbstractTest {
public:
	AbstractAstarTester(const AdjGraph& oldMap, const std::string mapName, const GridMap& map, const Mapper& mapper, const PerturbatedMapFetcher& fetcher, const std::string& heuristicChoice, bool enableStatistics, double hScale) : 
		AbstractTest{mapName, map, mapper, oldMap, fetcher},
		heuristicChoice{heuristicChoice},
		gridMapExpander{nullptr},
		path{}, statistics{nullptr},
		enableStatistics{enableStatistics},
		hScale{hScale}
		{

		}

		virtual ~AbstractAstarTester();
public:

		virtual std::vector<xyLoc> getSingleTestPath(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

protected:
		const std::string& heuristicChoice;
		AbstractStats* statistics;
		GridMapExpander* gridMapExpander;
		std::vector<dpf::nodeid_t> path;
		bool enableStatistics;
		double hScale;
};



#endif /* ABSTARCTASTARTEST_CPP_ */
