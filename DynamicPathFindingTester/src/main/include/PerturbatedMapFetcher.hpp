/*
 * PerturbatedMapFetcher.hpp
 *
 *  Created on: 15 nov 2018
 *      Author: massimobono
 */

#ifndef PERTURBATEDMAPFETCHER_HPP_
#define PERTURBATEDMAPFETCHER_HPP_

#include <string>
#include <DynamicPathFinding/ScenarioLoader.h>
#include <DynamicPathFinding/adj_graph.h>

class PerturbatedMapFetcher {
public:
	PerturbatedMapFetcher();
	virtual ~PerturbatedMapFetcher();

	virtual AdjGraph fetchPerturbatedMap(const std::string& mapName, const std::string& scenarioName, int queryId, const ScenarioLoader& scenarioLoader, const AdjGraph* previousPerturbatedMap);
};


#endif /* PERTURBATEDMAPFETCHER_HPP_ */
