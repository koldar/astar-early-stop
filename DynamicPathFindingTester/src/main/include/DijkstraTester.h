/*
 * DijkstraTester.h
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#ifndef DIJKSTRATESTER_H_
#define DIJKSTRATESTER_H_


#include "AbstractTest.h"
#include <DynamicPathFinding/earlystop_dijkstra.h>

class DijkstraTester: public AbstractTest {
public:

	DijkstraTester(const AdjGraph& oldMap, const std::string& mapName, const GridMap& map, const Mapper& mapper, PerturbatedMapFetcher fetcher, bool enableStatistics) :
		AbstractTest{mapName, map, mapper, oldMap, fetcher},
		optimalMoves{}, earlyStopDijkstraAlgorithm{nullptr}, enableStatistics{enableStatistics} {

		}

		~DijkstraTester();

		virtual dpf::cost_t getPathCostFromTester(const AdjGraph& perturbatedMap) const;

		virtual void setup();

		virtual void prepareBeforePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

		virtual void runSingleTest(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

		virtual void generateExpandedNodesImage(const std::string& outputTemplate, int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

		virtual std::vector<xyLoc> getSingleTestPath(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

		virtual uint32_t getExpandedNodes(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) const;

		virtual unsigned long getHeuristicTime(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap);

		virtual void finalizePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap);
private:
		std::vector<dpf::nodeid_t> optimalMoves;
		EarlyStopDijkstra* earlyStopDijkstraAlgorithm;
		bool enableStatistics;
};


#endif /* DIJKSTRATESTER_H_ */
