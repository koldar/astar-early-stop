#ifndef _ANYTIMEEXPERIMENTSTATS_HEADER__
#define _ANYTIMEEXPERIMENTSTATS_HEADER__

#include <DynamicPathFinding/types.h>

class AnyTimeExperimentStats {
private:
    int solutionId;
    dpf::cost_t solutionCost;
    double microSeconds;
public:
    AnyTimeExperimentStats(int solutionId, dpf::cost_t solutionCost, double microSeconds) : solutionId{solutionId}, solutionCost{solutionCost}, microSeconds{microSeconds} {

    }

    int get_solutionId() const;
    dpf::cost_t get_solutionCost() const;
    double getMicroSeconds() const;
};




#endif