/*
 * AstarLandmarkTester.cpp
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#include "AstarLandmarkTester.h"
#include "globals.h"
#include <DynamicPathFinding/anytime_weight_astar.hpp>
#include <DynamicPathFinding/weight_astar.hpp>
#include <PathTesterUtils/CsvHandler.h>

AstarLandmarkTester::~AstarLandmarkTester() {
	DO_IF_NOT_NULL(h) { delete h; }
	DO_IF_NOT_NULL(statistics) { delete statistics; }
	DO_IF_NOT_NULL(aStar) { delete aStar; }
	DO_IF_NOT_NULL(landmarkDatabase) { delete landmarkDatabase; }
}



void AstarLandmarkTester::setup() {
	DifferentHeuristicAdvancePlacingLandmarkStrategy policy{this->landmarkNumber};
	std::string landmarkDatabaseFileName{global_outputTemplate};
	landmarkDatabaseFileName.append(".landmark.dat");
	this->landmarkDatabase = new LandMarkDatabase{this->oldMap, policy, landmarkDatabaseFileName};
	this->h = new DifferentialHeuristic{this->oldMap, mapper, *landmarkDatabase};

	statistics = new QuickStatistics{};
}

void AstarLandmarkTester::prepareBeforePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) {
	this->gridMapExpander = new GridMapExpander{map, mapper, perturbatedMap, true};
	
	if (this->useAnytime) {
		//anytime context
		if (this->algorithmToUse == std::string{"A*"}) {	
			throw std::domain_error{"A* is used only in optimal context!"};
		} else if (this->algorithmToUse == std::string{"WA*"}) {
			if (this->hScale <= 1) {
				throw std::domain_error{"AWA* needs to have w>1"};
			}
			this->aStar = new AnytimeWeightPathFindingAStar<AbstractHeuristic, GridMapExpander, AbstractStats>{
				h, gridMapExpander, statistics, false, this->hScale	
			};
		} else {
			throw std::domain_error{"invalid algorithm!"};
		}
	} else {
		//optimal test
		if (this->algorithmToUse == std::string{"A*"}) {
			this->aStar = new WeightPathFindingAStar<AbstractHeuristic, GridMapExpander, AbstractStats>{h, gridMapExpander, statistics, false, this->hScale};	
		} else if (this->algorithmToUse == std::string{"WA*"}) {
			throw std::domain_error{"WA* cannot be used in optimal context"};			
		} else {
			throw std::domain_error{"invalid algorithm!"};
		}
	}

	// if (this->algorithmToUse == std::string{"A*"}) {
	// 	if (this->useAnytime) {
	// 		throw std::domain_error{"A* is used only in optimal context!"};
	// 	}

	// 	this->aStar = new SimplePathFindingAStar<AbstractHeuristic, GridMapExpander, AbstractStats>{h, gridMapExpander, statistics, false, this->hScale};
	// } else if (this->algorithmToUse == std::string{"WA*"}) {

	// 	if (!this->useAnytime) {
	// 		throw std::domain_error{"WA* is used only in anytime context!"};
	// 	}
	// 	this->aStar = new AnytimeWeightPathFindingAStar<AbstractHeuristic, GridMapExpander, AbstractStats>{
	// 		h, gridMapExpander, statistics, false, this->hScale	
	// 	};
	// }
	
	aStar->set_verbose(false);
	path.clear();
}

dpf::cost_t AstarLandmarkTester::getPathCostFromTester(const AdjGraph& perturbatedMap) const {
	return 0;
}

void AstarLandmarkTester::finalizePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) {
	if (this->useAnytime) {
		critical("generating anytime csv!!!!");
		std::stringstream ss;
		ss << this->outputTemplate << "anytime:query#" << id << "@type#anytime|.csv";
		std::string csvName{ss.str()};
		CsvHandler writer{
			csvName.c_str(),
			',',
			"SOLUTION_ID", 
			"SOLUTION_COST", 
			"SOLUTION_TIME", 
			"IS_OPTIMAL"
		};

		if (this->aStar->getAnyTimeInfoVector().size() > 0) {

			long i=0;
			dpf::cost_t oldCost = this->aStar->getAnyTimeInfoVector().front().solutionCost;
			bool first = true;
			for (auto it=this->aStar->getAnyTimeInfoVector().begin(); it != this->aStar->getAnyTimeInfoVector().end(); ++it) {
				if (it->solutionCost == dpf::cost_t::getUpperBound()) {
					//solution is infinite
					continue;
				}
				if (!first) {
					if (it->solutionCost > oldCost) {
						error("new solution is ", it->solutionCost);
						error("old solution was", oldCost);
						error("whole vector is ", this->aStar->getAnyTimeInfoVector());
						throw domain_error{"old cost is less than new one!"};
					}
				}
				first = false;
				writer.writeRow(i, it->solutionCost, (long)it->microSeconds, it->isSolutionOptimal);
				oldCost = it->solutionCost;
				++i;
			}
		}
	}

	delete this->aStar;
	delete this->gridMapExpander;
	this->gridMapExpander = nullptr;
	this->aStar = nullptr;
}

void AstarLandmarkTester::runSingleTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) {
	this->path = aStar->get_path_as_vector(mapper(start), mapper(goal), true);
}

void AstarLandmarkTester::generateExpandedNodesImage(const std::string& outputTemplate, int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) {
	std::stringstream ss;
	ss << outputTemplate << "query#" << id << "@type#expandedNodes|";

	GridMapExpander ae = GridMapExpander{map, mapper, perturbatedMap, true};

	//landmakr cannot be used as an anytime techqnieu
	WeightPathFindingAStar<AbstractHeuristic, GridMapExpander, AbstractStats> aaStar{h, gridMapExpander, statistics, this->enableStatistics, this->hScale};
	aaStar.set_verbose(false);
	std::vector<dpf::nodeid_t> apath = aaStar.get_path_as_vector(mapper(start), mapper(goal), true);


	std::unordered_map<dpf::celltype_t, color_t> colorMap;

	colorMap[1000]=WHITE;
	colorMap[1500]=DARK_GREEN;
	colorMap[2000]=CYAN;
	colorMap[2500]=DARK_BLUE;

	PPMImage mapPrint{map.getImage(colorMap, BLACK)};

	PPMImage scaled{mapPrint.scale(4,4)};
	PPMImage image{aaStar.printExpandedNodesImage(mapper, map, mapper(start), mapper(goal), apath, nullptr)};
	PPMImage sum{scaled + image};

	//now we add the landmarks
	
	for(dpf::nodeid_t landmarkId: this->landmarkDatabase->getLandmarks()) {
		xyLoc landmark = mapper(landmarkId);

		//set pixels
		for (int y=1; y<4; ++y) {
			for (int x=1; x<4; ++x) {
				sum.setPixel(4*landmark.x+x, 4*landmark.y+y, DARK_RED);
			}
		}
		
	}

	sum.saveAndConvertIntoPNG(ss.str());
}

uint32_t AstarLandmarkTester::getExpandedNodes(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) const {
	debug("get nodes expanded are", aStar->get_nodes_expanded());
	return aStar->get_nodes_expanded();
}

unsigned long AstarLandmarkTester::getHeuristicTime(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) {
	return aStar->get_heuristic_time();
}
