/*
 * AstarTester.cpp
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */



#include "AstarCPDTester.h"
#include <DynamicPathFinding/log.h>
#include <cstdlib>
#include <PathTesterUtils/CsvHandler.h>
#include <DynamicPathFinding/anytime_weight_astar.hpp>
#include <DynamicPathFinding/weight_astar.hpp>
#include <DynamicPathFinding/cpd_astar_search.hpp>
#include <DynamicPathFinding/cpd_anytime_wa_search.hpp>
#include <DynamicPathFinding/bound_cpd_astar_search.hpp>

AstarCPDTester::~AstarCPDTester() {
	if (this->aStar != nullptr) {
		delete aStar;
	}
	delete statistics;

	if (std::string{heuristicChoice} == std::string{"CPD_NOCACHE"}) {
	} else if (std::string{heuristicChoice} == std::string{"CPD_WITH_CACHE"}) {
		if (this->cpdHeuristic != nullptr) {
			delete static_cast<CpdCacheHeuristic*>(cpdHeuristic);
		}
	} else {
		error("invalid heuristic choice", heuristicChoice);
		std::exit(1);
	}
}



void AstarCPDTester::setup() {
	statistics = new QuickStatistics{};
}

void AstarCPDTester::prepareBeforePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) {
	if (std::string{heuristicChoice} == std::string{"CPD_NOCACHE"}) {
		cpdHeuristic = &h;
	} else if (std::string{heuristicChoice} == std::string{"CPD_WITH_CACHE"}) {
		cpdHeuristic = new CpdCacheHeuristic{h, newMap};
	} else {
		throw std::invalid_argument{"heuristic choice invalid!"};
	}

	this->gridMapExpander = new GridMapExpander{map, mapper, newMap, true};

	//we ensure that WA* has use_bound set to false, if WA* is set this means that we do not want to run our algorithm!
	if (std::string{this->algorithm} == "WA*") {
		if (this->enableBoundSearch) {
			throw std::domain_error{"WA* can't have enableBound set!"};
		}
		if (enableEarlyStopOptimization) {
			throw std::domain_error{"WA* can't have early stop set!"};
		}
		if (enableFixedUpperBoundOptimization) {
			throw std::domain_error{"WA* can't have upperbound set!"};
		}
	}

	debug("early stop: ", this->enableEarlyStopOptimization);
	debug("fixed upperbound: ", this->enableFixedUpperBoundOptimization);
	debug("use bounds: ", this->enableBoundSearch);
	debug("bound set to", this->bound);
	critical("weight of h: ", this->hScale);

	if (this->enableAnytimeSearch) {
		//we want to run the anytime algorithms!
		if (std::string{this->algorithm} == std::string{"A*"}) {
			//this is anytime weighted A*
			//we want to run CPD search anytime weights needs to be 1
			critical("run our algorithm with scale", this->hScale);
			if (this->hScale != 1) {
				throw std::domain_error{"Anytime Weighted CPD search needs to have w=1"};
			}
			this->aStar = new CPDAnytimeWAStarSearch<CpdHeuristic, GridMapExpander, AbstractStats>{
				cpdHeuristic, gridMapExpander, statistics,
				enableFixedUpperBoundOptimization,
				enableEarlyStopOptimization,
				false,
				this->hScale,
			};
		} else if (std::string{this->algorithm} == std::string{"WA*"}) {
			//we want to run a normal WA*
			if (this->hScale <= 1) {
				throw std::domain_error{"AWA* needs to have w>1"};
			}
			this->aStar = new AnytimeWeightPathFindingAStar<CpdHeuristic, GridMapExpander, AbstractStats>{
				cpdHeuristic, gridMapExpander, statistics, false, this->hScale	
			};
		} else {
			throw std::domain_error{"invalid anytime algorithm!"};
		}
	} else if (this->enableBoundSearch) {

		if (std::string{this->algorithm} == std::string{"A*"}) {
			//we want to run our algorithm!
			this->aStar = new CPDBoundAStarSearch<CpdHeuristic, GridMapExpander, AbstractStats>{
				cpdHeuristic, gridMapExpander, statistics,
				enableFixedUpperBoundOptimization,
				enableEarlyStopOptimization,
				false,
				this->bound,
			};
		} else if (std::string{this->algorithm} == std::string{"WA*"}) {
			//we want to run a normal WA*
			//FIXME I've never test it this case because I didn't have enough time!
			this->aStar = new WeightPathFindingAStar<CpdHeuristic, GridMapExpander, AbstractStats>{
				cpdHeuristic, gridMapExpander, statistics, false, this->hScale
			};

		} else {
			throw std::domain_error{"invalid bounded algorithm!"};
		}
		
	} else {
		//optimal version
		if (std::string{this->algorithm} == "A*") {
			//we want an algorithm. It depends on enableFixedUpperBoundOptimization and enableEarlyStopOptimization
			this->aStar = new CPDAStarSearch<CpdHeuristic, GridMapExpander, AbstractStats>{
				cpdHeuristic, gridMapExpander, statistics,
				enableFixedUpperBoundOptimization,
				enableEarlyStopOptimization,
				false,
			};
		} else {
			throw std::domain_error{"invlaid algorithm for optimal case!"};
		}
	}

	// TODO REMOVE
	// if (std::string{this->algorithm} == "A*") {
	// 	//we want to run our algorithm!
	// 	this->aStar = new dynamic_pathfinding_astar<CpdHeuristic, AbstractStats>{
	// 		cpdHeuristic, gridMapExpander, statistics,
	// 		enableFixedUpperBoundOptimization,
	// 		enableEarlyStopOptimization,
	// 		false,
	// 		this->enableBoundSearch,
	// 		this->bound,
	// 		this->hScale,
	// 		this->enableAnytimeSearch,
	// 		this->anytimeBound
	// 	};
	// } else if (std::string{this->algorithm} == "WA*") {
	// 	if (!this->enableAnytimeSearch) {
	// 		//dynamic pathfinding is used both in bounded and optimal search
	// 		this->aStar = new dynamic_pathfinding_astar<CpdHeuristic, AbstractStats>{
	// 			cpdHeuristic, gridMapExpander, statistics,
	// 			enableFixedUpperBoundOptimization,
	// 			enableEarlyStopOptimization,
	// 			false,
	// 			this->enableBoundSearch,
	// 			this->bound,
	// 			this->hScale,
	// 			this->enableAnytimeSearch,
	// 			this->anytimeBound
	// 		};
	// 		((dynamic_pathfinding_astar<CpdHeuristic, AbstractStats>*)this->aStar)->resetImmediateSuccessors();
	// 	} else {
	// 		this->aStar = new AnytimeWeightPathFindingAStar<CpdHeuristic, GridMapExpander, AbstractStats>{
	// 			cpdHeuristic, gridMapExpander, statistics, false, this->hScale	
	// 		};
	// 	}
	// }
	aStar->set_verbose(false);
	path.clear();

}

//FIXME this is a huge workaround because anytime WA* generate the correct cost but the wrong path... who knows why
dpf::cost_t AstarCPDTester::getPathCostFromTester(const AdjGraph& perturbatedMap) const {
	return 0;
	// if (this->enableAnytimeSearch) {
	// 	return this->aStar->getAnyTimeInfoVector().back().solutionCost;
	// } else {
	// 	return 0;
	// }
}

void AstarCPDTester::finalizePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) {
	//write the information in the csv for the anytime algorithm
	critical("Ending A*!!!!");
	if (this->enableAnytimeSearch) {
		critical("generating anytime csv!!!!");

		std::stringstream ss;
		ss << this->outputTemplate << "anytime:query#" << id << "@type#anytime|.csv";
		std::string csvName{ss.str()};
		CsvHandler writer{
			csvName.c_str(),
			',',
			"SOLUTION_ID", 
			"SOLUTION_COST", 
			"SOLUTION_TIME", 
			"IS_OPTIMAL"
		};

		if (this->aStar->getAnyTimeInfoVector().size() > 0) {
			//the path is not empty

			long i=0;
			dpf::cost_t oldCost = this->aStar->getAnyTimeInfoVector().front().solutionCost;
			bool first = true;
			for (auto it=this->aStar->getAnyTimeInfoVector().begin(); it != this->aStar->getAnyTimeInfoVector().end(); ++it) {
				if (it->solutionCost == dpf::cost_t::getUpperBound()) {
					//solution is infinite
					continue;
				}
				if (!first) {
					if (it->solutionCost > oldCost) {
						error("new solution is ", it->solutionCost);
						error("old solution was", oldCost);
						error("whole vector is ", this->aStar->getAnyTimeInfoVector());
						throw domain_error{"old cost is less than new one!"};
					}
				}
				first = false;
				writer.writeRow(i, it->solutionCost, (long)it->microSeconds, it->isSolutionOptimal);
				oldCost = it->solutionCost;
				++i;
			}
		}
	}

	delete this->aStar;
	if (std::string{heuristicChoice} == std::string{"CPD_NOCACHE"}) {

	} else if (std::string{heuristicChoice} == std::string{"CPD_WITH_CACHE"}) {
		delete this->cpdHeuristic;
		this->cpdHeuristic = nullptr;
	} else {
		throw std::invalid_argument{"heuristic choice invalid!"};
	}

	delete this->gridMapExpander;

	this->aStar = nullptr;
	this->gridMapExpander = nullptr;
}

void AstarCPDTester::runSingleTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) {
	this->path = aStar->get_path_as_vector(mapper(start), mapper(goal), true);
}

void AstarCPDTester::generateExpandedNodesImage(const std::string& outputTemplate, int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) {
	std::stringstream ss;
	ss << outputTemplate << "query#" << id << "@type#expandedNodes|";

	CpdHeuristic* ah;
	if (std::string{heuristicChoice} == std::string{"CPD_NOCACHE"}) {
		ah = &h;
	} else if (std::string{heuristicChoice} == std::string{"CPD_WITH_CACHE"}) {
		ah = new CpdCacheHeuristic{h, newMap};
	} else {
		throw std::invalid_argument{"heuristic choice invalid!"};
	}
	GridMapExpander* ae = new GridMapExpander{map, mapper, newMap, true};
	CPDAStarSearch<CpdHeuristic, GridMapExpander, AbstractStats>* aaStar = new CPDAStarSearch<CpdHeuristic, GridMapExpander, AbstractStats>{
				ah, ae, statistics,
				enableFixedUpperBoundOptimization,
				enableEarlyStopOptimization,
				enableStatistics,
			};
	aaStar->set_verbose(false);

	std::vector<dpf::nodeid_t> apath= aaStar->get_path_as_vector(mapper(start), mapper(goal), true);

	std::unordered_map<dpf::celltype_t, color_t> colorMap;

	colorMap[1000]=WHITE;
	colorMap[1500]=DARK_GREEN;
	colorMap[2000]=CYAN;
	colorMap[2500]=DARK_BLUE;

	PPMImage mapPrint{map.getImage(colorMap, BLACK)};

	PPMImage scaled{mapPrint.scale(4,4)};
	PPMImage image{aaStar->printExpandedNodesImage(mapper, map, mapper(start), mapper(goal), apath, nullptr)};
	PPMImage sum{scaled + image};
	
	sum.saveAndConvertIntoPNG(ss.str());

	delete aaStar;
	if (std::string{heuristicChoice} == std::string{"CPD_NOCACHE"}) {
		//we don't need to deallocate the heuristic
	} else if (std::string{heuristicChoice} == std::string{"CPD_WITH_CACHE"}) {
		//we need to deallocate the heuriostic
		delete ah;
	} else {
		throw std::invalid_argument{"heuristic choice invalid!"};
	}
	delete ae;
}


uint32_t AstarCPDTester::getExpandedNodes(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) const {
	debug("get nodes expanded are", aStar->get_nodes_expanded());
	return aStar->get_nodes_expanded();
}

unsigned long AstarCPDTester::getHeuristicTime(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) {
	return aStar->get_heuristic_time();
}
