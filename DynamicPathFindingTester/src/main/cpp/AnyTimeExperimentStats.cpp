#include "AnyTimeExperimentStats.hpp"

int AnyTimeExperimentStats::get_solutionId() const {
    return this->solutionId;
}

dpf::cost_t AnyTimeExperimentStats::get_solutionCost() const {
    return this->solutionCost;
}

double AnyTimeExperimentStats::getMicroSeconds() const {
    return this->microSeconds;
}