/*
 * AbstractTest.cpp
 *
 *  Created on: Oct 30, 2018
 *      Author: koldar
 */

#include "AbstractTest.h"
#include "globals.h"
#include <DynamicPathFinding/ScenarioLoader.h>
#include <DynamicPathFinding/log.h>
#include <functional>
#include <DynamicPathFinding/pbmImage.h>
#include "DynamicPathFinding/earlystop_dijkstra.h"

const std::unordered_map<dpf::celltype_t, color_t> AbstractTest::GRIDMAP_COLOR_MAPPING = std::unordered_map<dpf::celltype_t, color_t>{
	{1000, WHITE},
	{1500, DARK_GREEN},
	{2000, CYAN},
	{2500, DARK_BLUE}
};

AbstractTest::~AbstractTest() noexcept(false){

}


void AbstractTest::runAllTest(const std::string& outputTemplate, const std::string& mapFilename, const std::string& scenarioFilename, bool validatePaths, bool generateExpandedNodesImage, const std::vector<int>& experimentsWhereTogenerateImages, bool checkOptimal) {
	ScenarioLoader scenario{scenarioFilename.c_str()};
	std::vector<xyLoc> thePath;

	std::vector<int> actualExperimentsWhereTogenerateImages{experimentsWhereTogenerateImages};

	for (auto i=0; i<actualExperimentsWhereTogenerateImages.size(); ++i) {
		if (actualExperimentsWhereTogenerateImages[i] < 0) {
			actualExperimentsWhereTogenerateImages[i] = scenario.GetNumExperiments() + actualExperimentsWhereTogenerateImages[i];
		}
	}
	critical("images to draw are", actualExperimentsWhereTogenerateImages);

	AdjGraph newMap;
	AdjGraph* newMapPtr = nullptr;

	this->setup();
	//loop over the experiments in the scenario
	for (int x = 0; x < scenario.GetNumExperiments(); x++) {
		critical("handling query", x+1, "of", scenario.GetNumExperiments());
		thePath.resize(0);

		xyLoc start, goal;
		start.x = scenario.GetNthExperiment(x).GetStartX();
		start.y = scenario.GetNthExperiment(x).GetStartY();
		goal.x = scenario.GetNthExperiment(x).GetGoalX();
		goal.y = scenario.GetNthExperiment(x).GetGoalY();

		newMap = this->fetcher.fetchPerturbatedMap(mapFilename, scenarioFilename, x, scenario, newMapPtr);
		newMapPtr = &newMap;

		bool hasOptimalPathPerturbated = this->checkIfOptimalPathHasBeenPerturbated(x, start, goal, newMap);

		this->prepareBeforePerformanceTest(x, start, goal, newMap);
		double microSecondsElapsed;
		PROFILE_TIME_CODE(microSecondsElapsed) {
			this->runSingleTest(x, start, goal, newMap);
		}

		thePath = this->getSingleTestPath(x, start, goal, newMap);
		uint32_t expandedNodes = this->getExpandedNodes(x, start, goal, newMap);
		unsigned long heuristicTime = this->getHeuristicTime(x, start, goal, newMap);

		critical("search took", microSecondsElapsed, "us");

		if (generateExpandedNodesImage && (std::find(actualExperimentsWhereTogenerateImages.begin(), actualExperimentsWhereTogenerateImages.end(), x) != actualExperimentsWhereTogenerateImages.end())) {
			this->generateExpandedNodesImage(outputTemplate, x, start, goal, newMap);
		}

		//printCPDSlice(mapData, width, height, cpdHeuristic->getCpdHelper(), goal, basename, DefaultLabelPrinter{}, ExpandedColorPrinter{});

		//ok, we compute the pathInformation
		dpf::cost_t pathCostFromTester = this->getPathCostFromTester(newMap);
		dpf::big_integer pathCost = static_cast<dpf::big_integer>(newMap.getCostOfPath(thePath, mapper));
		dpf::big_integer pathLength = static_cast<dpf::big_integer>(oldMap.getCostOfPath(thePath, mapper));
		dpf::big_integer pathStepSize = thePath.size();

		dpf::big_integer expandedNodesLong = static_cast<dpf::big_integer>(expandedNodes);
		experimentStats.addOutcome(
				x,
				microSecondsElapsed,
				thePath, pathCost, pathLength, pathStepSize,
				expandedNodesLong, heuristicTime,
				hasOptimalPathPerturbated
		);

		//validate path if the user has requested it
		debug("path is ", thePath);
		if (validatePaths && !this->validatePath(pathCostFromTester, thePath, map, start, goal, newMap, checkOptimal)) {
			error("the path generated for experiment", x, "didn't pass validation!", "path generated=", thePath, "start=", start, "goal=", goal);
			error("dumping adjGraph...");

			gridmap_path p = gridmap_path{thePath};
			xyLoc minPoint;
			xyLoc maxPoint;
			p.getRanges(minPoint, maxPoint);
			debug("points are", minPoint, maxPoint);

			std::function<bool(dpf::nodeid_t)> nodeLambda = [&](dpf::nodeid_t i) -> bool{return mapper(i).isInside(minPoint, maxPoint);};
			std::function<bool(dpf::nodeid_t, OutArc)> arcLambda = [&](dpf::nodeid_t i, OutArc a) -> bool{return mapper(i).isInside(minPoint, maxPoint) && mapper(a.target).isInside(minPoint, maxPoint);};
			newMap.print(mapper, std::string{"invalidMap.adjgraph"},
					&nodeLambda,
					&arcLambda
			);
			auto labelPrinter = DefaultLabelPrinter{};
			auto colorPrinter = DefaultColorPrinter{};

			printGridMap(map.getTraversableMask(), map.getWidth(), map.getHeight(), mapper, goal, std::string{"invalidPath"}, labelPrinter, colorPrinter, &minPoint, &maxPoint);

			throw std::domain_error{"the path is wrong!"};
		}

		this->finalizePerformanceTest(x, start, goal, newMap);
	}
}

bool AbstractTest::checkIfOptimalPathHasBeenPerturbated(int testId, const xyLoc& start, const xyLoc& goal, const AdjGraph& newMap) const {
	//we check the path length: it should be the optimal one
	EarlyStopDijkstra* earlyStopDijkstraAlgorithm = new EarlyStopDijkstra{oldMap, false};

	auto optimalMoves = earlyStopDijkstraAlgorithm->run(mapper(start), mapper(goal));
	std::vector<xyLoc> optimalPathForDijkstra = earlyStopDijkstraAlgorithm->getPathAsVector(mapper(start), mapper(goal), mapper, optimalMoves);

	//ok, now let's see what happens in the newMap

	dpf::cost_t oldCost = oldMap.getCostOfPath(optimalPathForDijkstra, mapper);
	dpf::cost_t newCost = newMap.getCostOfPath(optimalPathForDijkstra, mapper);

	delete earlyStopDijkstraAlgorithm;

	assert(oldCost <= newCost);
	return oldCost != newCost;
}

bool AbstractTest::validatePath(const dpf::cost_t pathCost, const std::vector<xyLoc>& path, const GridMap& map, const xyLoc& start, const xyLoc& goal, const AdjGraph& newMap, bool checkIfOptimal) const {
	if (path.size() == 0 && start == goal) {
		return true;
	}

	if (path.size() == 0 && start != goal) {
		//path does not exist!
		return true;
	}

	//we should begin from start
	if (start != path[0]) {
		error("first element of path is not start");
		return false;
	}

	//we should end in goal
	if (goal != path.back()) {
		error("last element of path is not goal");
		return false;
	}

	//check that every step in the path goes from a cell to a nearby one
	for (int x = 0; x < (int)path.size()-1; x++)
	{
		if (!path[x].isAdjacentTo(path[x+1])) {
			error("step", x, "is not adjacent to the next one");
			return false;
		}
		if (!map.isTraversable(path[x].x, path[x].y)) {
			error("the location in step", x, "is invalicable");
			return false;
		}
		if (!map.isTraversable(path[x+1].x, path[x+1].y)) {
			error("the location in step", x+1, "is invalicable");
			return false;
		}
		//diagonal movement
		if (path[x].x != path[x+1].x && path[x].y != path[x+1].y)
		{
			//
			if (!map.isTraversable(path[x].x, path[x+1].y)) {
				error("we were doing a diagonal movement in step", x, " but a untraversable corner is in the way!");
				return false;
			}
			if (!map.isTraversable(path[x+1].x, path[x].y)) {
				error("we were doing a diagonal movement in step", x, " but a untraversable corner is in the way!");
				return false;
			}
		}
	}

	//we check the path length: it should be the optimal one
	EarlyStopDijkstra* earlyStopDijkstraAlgorithm = new EarlyStopDijkstra{newMap, false};

	auto optimalMoves = earlyStopDijkstraAlgorithm->run(mapper(start), mapper(goal));
	std::vector<xyLoc> optimalPathForDijkstra = earlyStopDijkstraAlgorithm->getPathAsVector(mapper(start), mapper(goal), mapper, optimalMoves);

	delete earlyStopDijkstraAlgorithm;

	dpf::cost_t algorithmPathCost = newMap.getCostOfPath(path, mapper);
	//FIXME huge workaround becuase WA* cpd searhc anytime generate the correct solutio ncost but the wrong path -.-"
	// if (pathCost != 0) {
	// 	algorithmPathCost = pathCost;
	// }
	dpf::cost_t dijkstraPathCost = newMap.getCostOfPath(optimalPathForDijkstra, mapper);

	if (global_testBoundedAlgorithms) {
		critical("the bounded path steps: DIJKSTRA=", optimalPathForDijkstra.size(), global_algorithmToTest.c_str(), "(under test) =", path.size(), "We check the costs: DIJKSTRA=", dijkstraPathCost, "algorithm=", algorithmPathCost);
	} else if (global_testAnyTimeAlgorithms) {
		critical("the anytime path steps: DIJKSTRA=", optimalPathForDijkstra.size(), global_algorithmToTest.c_str(), "(under test) =", path.size(), "We check the costs: DIJKSTRA=", dijkstraPathCost, "algorithm=", algorithmPathCost);
	} else {
		critical("the optimal path steps: DIJKSTRA=", optimalPathForDijkstra.size(), global_algorithmToTest.c_str(), "(under test) =", path.size(), "We check the costs: DIJKSTRA=", dijkstraPathCost, "algorithm=", algorithmPathCost);
	}

	if ((!global_testBoundedAlgorithms) && (algorithmPathCost != dijkstraPathCost)) {
		error("newMap is perturbated? ", (newMap.hasBeenPerturbated() ? "yes": "no"));
		error("newMap weights: ", newMap.getWeights());
		error("the path computed by the tested algorithm has cost", algorithmPathCost, "(size", path.size(), ") but with dijkstra we obtain a cost of", dijkstraPathCost, "( size", optimalPathForDijkstra.size(), ")");
		error("############");
		error("path tested algorithm:  ", path);
		error("############");
		error("path Dikstra algorithm: ", optimalPathForDijkstra);
		error("dumping image showing both paths! It's called \"optimalPaths\"!");

		newMap.printPaths(mapper, "optimalPaths", std::vector<gridmap_path>{gridmap_path{path}, gridmap_path{optimalPathForDijkstra}});
		return false;
	} else if ((global_testBoundedAlgorithms) && (algorithmPathCost < dijkstraPathCost)) {
		error("we have found a solution which is less than the optimal one? What?");
		error("algorithm under test:", algorithmPathCost);
		error("DIJLSTRA optimal", dijkstraPathCost);
		error("dumping image showing both paths! It's called \"boundedPaths\"!");

		newMap.printPaths(mapper, "boundedPaths", std::vector<gridmap_path>{gridmap_path{path}, gridmap_path{optimalPathForDijkstra}});
		return false;
	}

	return true;

}

