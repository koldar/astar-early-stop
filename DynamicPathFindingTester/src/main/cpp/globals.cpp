/*
 * globals.cpp
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */


#include "globals.h"

std::default_random_engine global_randomEngine;

std::string global_mapFilename;
std::string global_scenarioFilename;
std::string global_outputTemplate;
std::string global_heuristicChoice;
bool global_enableFixedUpperBoundOptimization;
bool global_enableEarlyStopOptimization;
std::string global_perturbationDensityStr;
int global_randomSeed = 0;
bool global_validatePaths;
std::string global_algorithmToTest;
std::string global_perturbatedMapFilenameTemplate;
bool global_drawExpandedMaps;
std::string global_generateImagesOnlyForStr;
std::vector<int> global_generateImagesOnlyFor;
int global_landmarkNumber;

bool global_useBounds;
double global_bounded;
double global_hWeight;
bool global_testBoundedAlgorithms;

bool global_testAnyTimeAlgorithms;
double global_anytimeBound;