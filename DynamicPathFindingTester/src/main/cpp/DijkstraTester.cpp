/*
 * DijkstraTester.cpp
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */



#include "DijkstraTester.h"

DijkstraTester::~DijkstraTester() {
	DO_IF_NOT_NULL(earlyStopDijkstraAlgorithm) { delete earlyStopDijkstraAlgorithm; }
}

void DijkstraTester::setup() {

}

void DijkstraTester::prepareBeforePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) {
	earlyStopDijkstraAlgorithm = new EarlyStopDijkstra{newMap, false};
}

void DijkstraTester::finalizePerformanceTest(int id, xyLoc start, xyLoc goal, const AdjGraph& perturbatedMap) {
	delete earlyStopDijkstraAlgorithm;
	earlyStopDijkstraAlgorithm = nullptr;
}

dpf::cost_t DijkstraTester::getPathCostFromTester(const AdjGraph& perturbatedMap) const {
	return 0;
}

void DijkstraTester::runSingleTest(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) {
	optimalMoves = earlyStopDijkstraAlgorithm->run(mapper(start), mapper(goal));
}

void DijkstraTester::generateExpandedNodesImage(const std::string& outputTemplate, int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) {
	std::stringstream ss;
	ss << outputTemplate << "query#" << id << "@type#expandedNodes|";

	EarlyStopDijkstra adijkstra{newMap, enableStatistics};

	std::vector<dpf::nodeid_t> aoptimalMoves = adijkstra.run(mapper(start), mapper(goal));
	std::vector<dpf::nodeid_t> apath{mapper.convert(adijkstra.getPathAsVector(mapper(start), mapper(goal), mapper, aoptimalMoves))};

	std::unordered_map<dpf::celltype_t, color_t> colorMap;

	colorMap[1000]=WHITE;
	colorMap[1500]=DARK_GREEN;
	colorMap[2000]=CYAN;
	colorMap[2500]=DARK_BLUE;

	PPMImage mapPrint{map.getImage(colorMap, BLACK)};
	PPMImage scaled{mapPrint.scale(4,4)};
	PPMImage image{adijkstra.printExpandedNodesImage(ss.str(), mapper(start), mapper(goal), mapper, map, apath, nullptr)};
	PPMImage sum{scaled + image};
	sum.saveAndConvertIntoPNG(ss.str());
}

std::vector<xyLoc> DijkstraTester::getSingleTestPath(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) {
	return earlyStopDijkstraAlgorithm->getPathAsVector(mapper(start), mapper(goal), mapper, optimalMoves);
}

uint32_t DijkstraTester::getExpandedNodes(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) const {
	return static_cast<uint32_t>(earlyStopDijkstraAlgorithm->getExpandedNodesNumber());
}

unsigned long DijkstraTester::getHeuristicTime(int id, xyLoc start, xyLoc goal, const AdjGraph& newMap) {
	return 0;
}
