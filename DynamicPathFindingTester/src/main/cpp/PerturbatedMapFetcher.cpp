/*
 * PerturbatedMapFetcher.cpp
 *
 *  Created on: 15 nov 2018
 *      Author: massimobono
 */

#include "PerturbatedMapFetcher.hpp"
#include <sstream>
#include <iostream>
#include "globals.h"
#include <DynamicPathFinding/file_utils.h>
#include <cstdio>
#include <DynamicPathFinding/adj_graph.h>

PerturbatedMapFetcher::PerturbatedMapFetcher() {

}

PerturbatedMapFetcher::~PerturbatedMapFetcher() {

}

AdjGraph PerturbatedMapFetcher::fetchPerturbatedMap(const std::string& mapName, const std::string& scenarioName, int queryId, const ScenarioLoader& scenarioLoader, const AdjGraph* previousPerturbatedMap) {
	/**
	 * if there is a file named with the query id we pick that file. Otherwise we pick the previous one.
	 * If the previous one is not found, we load the graph from the file without citing the query id
	 */

	AdjGraph result;

	std::stringstream queryMapSS;
	queryMapSS << global_perturbatedMapFilenameTemplate << "query#" << queryId << "|.graph";

	std::stringstream mapSS;
	mapSS << global_perturbatedMapFilenameTemplate << ".graph";
	if (isFileExists(queryMapSS.str())) {
		//load the query file
		debug("reading from", queryMapSS.str());
		FILE* f = std::fopen(queryMapSS.str().c_str(), "rb");
		result = AdjGraph::load(f);
		fclose(f);
	} else if (previousPerturbatedMap != nullptr) {
		//previous perturbated map is available
		return *previousPerturbatedMap;
	} else if (isFileExists(mapSS.str())){
		//load the query file
		FILE* f = fopen(mapSS.str().c_str(), "rb");
		result = AdjGraph::load(f);
		fclose(f);
	} else {
		critical("can't load file ", queryMapSS.str(), " nor map ", mapSS.str());
		throw std::domain_error{"couldn't fetch map!"};
	}

	return result;

}
