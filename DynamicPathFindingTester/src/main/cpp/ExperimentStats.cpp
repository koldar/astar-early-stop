/*
 * ExperimentStats.cpp
 *
 *  Created on: Oct 11, 2018
 *      Author: koldar
 */

#include "ExperimentStats.h"
#include <DynamicPathFinding/log.h>

void ExperimentStats::addOutcome(int id, double microSeconds, const std::vector<xyLoc>& path, dpf::big_integer pathCost, dpf::big_integer pathLength, dpf::big_integer pathStepsSize, dpf::big_integer expandedNodes, unsigned long heuristicTime, bool hasOptimalPathPerturbated) {
	debug("add expandedNodes", expandedNodes, "converison into null is", static_cast<dpf::big_integer>(expandedNodes));
	outcomes.push_back(ExperimentOutcome{
		id,
		microSeconds,
		std::vector<xyLoc>{path},
		pathCost,
		pathLength,
		pathStepsSize,
		expandedNodes,
		heuristicTime,
		hasOptimalPathPerturbated
	});
}

double ExperimentStats::GetTotalTime() const {
	double result = 0;
	for (auto outcome : outcomes) {
		result += outcome.microSeconds;
	}
	return result;
}

double ExperimentStats::GetMaxTimestep() const {
	double result = 0;
	for (auto outcome : outcomes) {
		if (outcome.microSeconds > result) {
			result = outcome.microSeconds;
		}
	}
	return result;
}

std::vector<ExperimentOutcome>::const_iterator ExperimentStats::begin() const {
	return outcomes.begin();
}

std::vector<ExperimentOutcome>::const_iterator ExperimentStats::end() const {
	return outcomes.end();
}



