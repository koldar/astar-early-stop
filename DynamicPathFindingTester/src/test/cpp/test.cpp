/*
 * test.cpp
 *
 *  Created on: Oct 1, 2018
 *      Author: koldar
 */

#define CATCH_CONFIG_NO_POSIX_SIGNALS // https://github.com/catchorg/Catch2/issues/1295 avoid catch catching signals
#include "catch.hpp"

#include "map_loader.h"
#include "Entry.h"
#include "file_utils.h"
#include "log.h"
#include "vec_io.h"
#include "mapper.h"
#include "list_graph.h"
#include "adj_graph.h"
#include "dynamic_pathfinding_astar.h"
#include "cpd_heuristic.h"
#include "grid_map_expander.h"
#include "cpd_cached_heuristic.h"

#include <string>
#include <sstream>


static string getCPDFilename(const string& mapName) {
	std::stringstream ss;
	ss << "test" << "-" << mapName;
	return ss.str();
}



SCENARIO("Test the behaviour of some complex functions", "[normal]") {
	GIVEN("wanting to know about build_adj_array") {

		MapLoader mapLoader;
		const string mapName = "square02.map";
		int width, height;
		vector<bool> mapData;
		mapLoader.LoadMap(mapName.c_str(), mapData, width, height);
		Mapper mapper = Mapper{mapData, width, height};
		ListGraph g = extract_graph(mapper);

		std::vector<int>out_begin, out_dest;
		auto source_node = [&](int x){
			return g.arc[x].source;
		};

		auto target_node = [&](int x){
			return g.arc[x].target;
		};

		WHEN("") {

			g.print(mapper, "square02ListGraph");
			detail::build_begin_array(out_begin, g.node_count(), g.arc.size(), source_node);
			//			build_adj_array(
			//				out_begin, out_dest,
			//				g.node_count(), g.arc.size(),
			//				source_node, target_node
			//			);

			debug("out_begin is ", out_begin);

			THEN("") {
				REQUIRE(out_begin == std::vector<int>{0, 3, 8, 11, 16, 23, 26, 29, 32});
			}
		}
	}
}

SCENARIO("Tests previous code", "[normal]") {

	std::vector<bool> mapData;
	int width;
	int height;
	MapLoader mapLoader;
	string mapName;
	string cpdFilename;

	GIVEN("a normal map") {
		mapName = "square01.map";
		cpdFilename = getCPDFilename(mapName);

		mapLoader.LoadMap(mapName.c_str(), mapData, width, height);
		if (!isFileExists(cpdFilename.c_str())) {
			PreprocessMap(mapData, width, height, cpdFilename.c_str());
		}

		void* pathHelper = PrepareForSearch(mapData, width, height, cpdFilename.c_str());

		std::vector<xyLoc> path;
		bool result;
		WHEN("goal is on start") {
			result = GetPathWithCPD(pathHelper, xyLoc{0,0}, xyLoc{0,0}, path);
			debug("path is ", path);
			THEN("") {
				REQUIRE(result);
				REQUIRE(path.size() == 1);
				REQUIRE(path[0] == xyLoc{0, 0});
			}
		}

		WHEN("goal is immediately next to start") {
			result = GetPathWithCPD(pathHelper, xyLoc{0,0}, xyLoc{0,1}, path);
			THEN("") {
				REQUIRE(result);
				REQUIRE(path.size() == 2);
				REQUIRE(path == vector<xyLoc>{xyLoc{0, 0}, xyLoc{0,1}});
			}
		}

		WHEN("goal is in diagonal next to start") {
			result = GetPathWithCPD(pathHelper, xyLoc{0,0}, xyLoc{1,1}, path);
			THEN("") {
				debug("path is ", path);
				REQUIRE(result);
				REQUIRE(path.size() == 2);
				REQUIRE(path == vector<xyLoc>{xyLoc{0, 0}, xyLoc{1,1}});
			}
		}

		WHEN("goal is not near") {
			result = GetPathWithCPD(pathHelper, xyLoc{0,0}, xyLoc{0,2}, path);
			THEN("") {
				debug("path is ", path);
				REQUIRE(result);
				REQUIRE(path == vector<xyLoc>{xyLoc{0, 0}, xyLoc{0, 1}, xyLoc{0,2}});
			}
		}

		WHEN("path is long") {
			result = GetPathWithCPD(pathHelper, xyLoc{3,1}, xyLoc{3,3}, path);
			THEN("") {
				debug("path is ", path);

				drawMapWithPath(std::cerr, mapData, width, height, path);
				REQUIRE(result);
				REQUIRE(path == vector<xyLoc>{xyLoc{3, 1}, xyLoc{3,0}, xyLoc{2, 0}, xyLoc{1,0}, xyLoc{1,1}, xyLoc{1,2}, xyLoc{1,3}, xyLoc{1,4}, xyLoc{2,4}, xyLoc{3,4}, xyLoc{3,3}});
			}
		}

		WHEN("we alter a single arc which is not involved at all in the path") {
			//FIXME continue from here
		}

		DeleteHelperOfSearch(pathHelper);
	}
}

SCENARIO("testing support new feature for ICAPS2018", "[newfeature]") {

	MapLoader mapLoader;
	const string mapName = "square02.map";
	int width, height;
	vector<bool> mapData;
	mapLoader.LoadMap(mapName.c_str(), mapData, width, height);
	Mapper mapper = Mapper{mapData, width, height};
	ListGraph listGraph1 = extract_graph(mapper);

	string cpdFilename = getCPDFilename(mapName);
	if (!isFileExists(cpdFilename.c_str())) {
		PreprocessMap(mapData, width, height, cpdFilename.c_str());
	}

	GIVEN("a ListGraph") {

		WHEN("checking if 2 equal AdjGraph are the same") {
			AdjGraph g1 = AdjGraph{listGraph1};
			AdjGraph g2 = AdjGraph{listGraph1};

			REQUIRE(g1 == g2);
		}

		WHEN("checking if 2 AdjGraph are the same") {
			ListGraph listgraph2 = extract_graph(mapper);

			AdjGraph g1 = AdjGraph{listGraph1};
			AdjGraph g2 = AdjGraph{listgraph2};

			REQUIRE(g1 == g2);
		}

		WHEN("getting weight arcs") {
			AdjGraph g = AdjGraph{listGraph1};

			REQUIRE(g.getWeightOfArc(mapper({0,0}), mapper({0,1})) == 1000);
			REQUIRE(g.getWeightOfArc(mapper({0,0}), mapper({1,1})) == 1414);

			REQUIRE(g.getWeightOfArc(mapper({1,1}), mapper({1,2})) == 1000);
			REQUIRE(g.getWeightOfArc(mapper({2,0}), mapper({1,1})) == 1414);
		}

		WHEN("setting weight arcs") {
			AdjGraph g = AdjGraph{listGraph1};
			AdjGraph g2 = AdjGraph{listGraph1};

			g.changeWeightOfArc(mapper({0,0}), mapper({0,1}), 500);

			REQUIRE(g.getWeightOfArc(mapper({0,0}), mapper({0,1})) == 500);
			REQUIRE(g.getWeightOfArc(mapper({0,1}), mapper({0,0})) == 500);

			REQUIRE(g != g2);
		}

		WHEN("printing adjacency graph") {
			AdjGraph g = AdjGraph{listGraph1};

			g.print(mapper, "adjacencyMap");
		}
	}

	GIVEN("start and goal where we want to compute the optimal path with Massimo Bono's new abstract method") {
		void* pathHelper = PrepareForSearch(mapData, width, height, cpdFilename.c_str());
		int pathCost;
		std::vector<dpf::move_t> moves;
		std::vector<xyLoc> cells;
		bool result;

		WHEN("start=goal") {
			result = GetPathDataAllAtOnceWithCPD(pathHelper, {1,1}, {1,1}, pathCost, moves, cells);
			REQUIRE(result);
			REQUIRE(pathCost == 0);
			REQUIRE(moves == std::vector<dpf::move_t>{});
			REQUIRE(cells == std::vector<xyLoc>{{1,1}});
		}

		WHEN("start is just on the left of the goal") {
			result = GetPathDataAllAtOnceWithCPD(pathHelper, {1,1}, {2,1}, pathCost, moves, cells);
			REQUIRE(result);
			REQUIRE(pathCost == 1000);
			REQUIRE(moves == std::vector<dpf::move_t>{4});
			REQUIRE(cells == std::vector<xyLoc>{{1,1}, {2,1}});
		}

		WHEN("start is just on the diagonal of the goal") {
			result = GetPathDataAllAtOnceWithCPD(pathHelper, {1,1}, {0,0}, pathCost, moves, cells);
			REQUIRE(result);
			REQUIRE(pathCost == 1414);
			REQUIRE(moves == std::vector<dpf::move_t>{0});
			REQUIRE(cells == std::vector<xyLoc>{{1,1}, {0,0}});
		}

		WHEN("start is far from the goal") {
			result = GetPathDataAllAtOnceWithCPD(pathHelper, {0,0}, {2,0}, pathCost, moves, cells);
			REQUIRE(result);
			REQUIRE(pathCost == 2000);
			REQUIRE(moves == std::vector<dpf::move_t>{0, 1});
			REQUIRE(cells == std::vector<xyLoc>{{0,0}, {1,0}, {2,0}});
		}

		DeleteHelperOfSearch(pathHelper);
	}

	GIVEN("an A* implementation") {

		AdjGraph g = AdjGraph{listGraph1};
		string cpdFilename = getCPDFilename(mapName);
		auto h = CpdHeuristic{mapData, width, height, cpdFilename.c_str()};
		auto e = GridMapExpander{mapData, width, height, mapper, g};

		dynamic_pathfinding_astar aStar = dynamic_pathfinding_astar{&h, &e, false, false, false, 0};
		aStar.set_verbose(true);

		WHEN("start=goal") {
			REQUIRE(aStar.get_path_as_vector(mapper({0,0}), mapper({0,0})) == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0}))
			});
		}

		WHEN("goal just under start") {
			REQUIRE(aStar.get_path_as_vector(mapper({0,0}), mapper({0,1})) == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({0,1}))
			});
		}

		WHEN("goal is on the diagonal") {
			REQUIRE(aStar.get_path_as_vector(mapper({0,0}), mapper({1,1})) == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,1}))
			});
		}

		WHEN("goal on far on the right") {
			REQUIRE(h.h(h.getMapper()({0,0}), h.getMapper()({2,0})) == 2000);
			REQUIRE(h.h(h.getMapper()({1,0}), h.getMapper()({2,0})) == 1000);
			REQUIRE(h.h(h.getMapper()({1,1}), h.getMapper()({2,0})) == 1414);
			REQUIRE(h.h(h.getMapper()({2,0}), h.getMapper()({2,0})) == 0);
			REQUIRE(h.h(h.getMapper()({0,1}), h.getMapper()({2,0})) == (1414+1000));
			REQUIRE(aStar.get_path_as_vector(h.getMapper()({0,0}), h.getMapper()({2,0})) == std::vector<uint32_t>{
				static_cast<uint32_t>(h.getMapper()({0,0})),
						static_cast<uint32_t>(h.getMapper()({1,0})),
						static_cast<uint32_t>(h.getMapper()({2,0}))
			});
		}

		//		Massimo Bono: the code under first_move of CPD fails, so I assume it could never be done :\
		//		WHEN("looking for an unexisting path") {
		//			const string mapName3 = "square03.map";
		//			mapLoader.LoadMap(mapName3.c_str(), mapData, width, height);
		//			mapper = Mapper{mapData, width, height};
		//			listGraph1 = extract_graph(mapper);
		//			REQUIRE(aStar.get_path_as_vector(mapper({0,0}), mapper({4,4})) == std::vector<uint32_t>{});
		//		}

	}

}

SCENARIO("testing important new features for ICAPS2018", "[newfeature]") {

	MapLoader mapLoader;
	const string mapName = "square03.map";
	int width, height;
	vector<bool> mapData;
	mapLoader.LoadMap(mapName.c_str(), mapData, width, height);
	Mapper mapperTmp = Mapper{mapData, width, height};
	ListGraph listGraph1 = extract_graph(mapperTmp);


	const string cpdFilename = getCPDFilename(mapName);
	auto h = CpdHeuristic{mapData, width, height, cpdFilename.c_str()};
	AdjGraph graphWithAlteringArcs = AdjGraph{h.getGraph()};
	const Mapper& mapper = h.getMapper();
	auto e = GridMapExpander{mapData, width, height, h.getMapper(), graphWithAlteringArcs};

	GIVEN("A* with no optimizations") {
		dynamic_pathfinding_astar aStar = dynamic_pathfinding_astar{&h, &e, false, false, false, 0};
		aStar.set_verbose(true);

		WHEN("altering a weight of an arc changes A* costs but not CPDs") {
			REQUIRE(graphWithAlteringArcs.getWeightOfArc(0, 1) == 1000);
			REQUIRE(h.h(mapper({0,0}), mapper({1,0})) == 1000);

			graphWithAlteringArcs.changeWeightOfArc(0, 1, 2000);

			REQUIRE(graphWithAlteringArcs.getWeightOfArc(0, 1) == 2000);
			REQUIRE(h.h(mapper({0,0}), mapper({1,0})) == 1000);

			REQUIRE(h.hasHeuristicComputedAPath());
			REQUIRE(h.getCPDPathCost() == 1000);
			REQUIRE(h.getCPDPathCostInMap(graphWithAlteringArcs) == 2000);
			REQUIRE(!h.isCPDPathClearFromArcModifications(graphWithAlteringArcs));
		}

		WHEN("perturbation of single arc does not alter optimal path") {
			graphWithAlteringArcs.changeWeightOfArc(mapper({0,3}), mapper({0,4}), 5000);

			REQUIRE(aStar.get_path_as_vector(mapper({0,0}), mapper({4,0})) == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,0})),
						static_cast<uint32_t>(mapper({2,0})),
						static_cast<uint32_t>(mapper({3,0})),
						static_cast<uint32_t>(mapper({4,0}))
			});

			debug("nodes expanded are", aStar.getNodeExpandedList());
			REQUIRE(aStar.get_nodes_expanded() == 4);
			REQUIRE(aStar.get_nodes_generated() == 9);
			//{0,0} = 1
			//{0,0} plus all its successors (both expanded or not) = 3
			//{1,0} = 1
			//{1,0} plus all its successors (both expanded or not) = 5
			//{2,0} = 1
			//{2,0} plus all its successors (both expanded or not) = 5
			//{3,0} = 1
			//{3,0} plus all its successors (both expanded or not) = 5
			//{4,0} = 1
			//TOTAL = 23
			REQUIRE(aStar.get_nodes_touched() == 23);
		}

		WHEN("perturbation of a single arc does alter the optimal path") {
			graphWithAlteringArcs.changeWeightOfArc(mapper({1,0}), mapper({2,0}), 5000);

			std::vector<uint32_t> path = aStar.get_path_as_vector(mapper({0,0}), mapper({4,0}));
			debug("path is", path);
			REQUIRE(((path == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,0})),
						static_cast<uint32_t>(mapper({2,1})),
						static_cast<uint32_t>(mapper({3,0})),
						static_cast<uint32_t>(mapper({4,0}))
			}) ||
					(path == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,0})),
						static_cast<uint32_t>(mapper({2,1})),
						static_cast<uint32_t>(mapper({3,1})),
						static_cast<uint32_t>(mapper({4,0}))
			}) ||
				(path == std::vector<uint32_t>{
					static_cast<uint32_t>(mapper({0,0})),
							static_cast<uint32_t>(mapper({1,1})),
							static_cast<uint32_t>(mapper({2,0})),
							static_cast<uint32_t>(mapper({3,0})),
							static_cast<uint32_t>(mapper({4,0}))
				})
			));

			REQUIRE(aStar.get_nodes_expanded() == 4);
			REQUIRE(aStar.get_nodes_generated() == 9);
			REQUIRE(aStar.get_nodes_touched() == 23);
		}
	}

	GIVEN ("A* with early stop enabled") {
		dynamic_pathfinding_astar aStar = dynamic_pathfinding_astar{&h, &e, false, true, false, 0};
		aStar.set_verbose(true);

		WHEN("perturbation of single arc does not alter optimal path") {
			graphWithAlteringArcs.changeWeightOfArc(mapper({0,3}), mapper({0,4}), 5000);

			auto path = aStar.get_path_as_vector(mapper({0,0}), mapper({4,0}));
			REQUIRE(path == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,0})),
						static_cast<uint32_t>(mapper({2,0})),
						static_cast<uint32_t>(mapper({3,0})),
						static_cast<uint32_t>(mapper({4,0}))
			});

			REQUIRE(aStar.get_nodes_expanded() == 0);
			REQUIRE(aStar.get_nodes_generated() == 0);
			REQUIRE(aStar.get_nodes_touched() == 0);
		}

		WHEN("perturbation of a single arc does alter the optimal path") {
			graphWithAlteringArcs.changeWeightOfArc(mapper({1,0}), mapper({2,0}), 5000);

			auto path = aStar.get_path_as_vector(mapper({0,0}), mapper({4,0}));


			printCPD(mapName.c_str(), cpdFilename.c_str(), xyLoc{4, 0}, std::string{"cpd40"});

			debug("path obtained is ", path);
			REQUIRE(((path == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,0})),
						static_cast<uint32_t>(mapper({2,1})),
						static_cast<uint32_t>(mapper({3,0})),
						static_cast<uint32_t>(mapper({4,0}))
			}) ||
					(path == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,1})),
						static_cast<uint32_t>(mapper({2,0})),
						static_cast<uint32_t>(mapper({3,0})),
						static_cast<uint32_t>(mapper({4,0}))
			})));

			REQUIRE(aStar.get_nodes_expanded() == 1); //we pop the starting node and when we analyze {1,1} we stop
			REQUIRE(((aStar.get_nodes_generated() > 0) && (aStar.get_nodes_generated() < 4))); //we generate at least one and at most 3 nodes (depnding if we encouter first the new optimum)
			REQUIRE(((aStar.get_nodes_touched() > 1) && (aStar.get_nodes_touched() < 5))); //we generate at least one and at most 3 nodes (depnding if we encouter first the new optimum)
		}

		WHEN("perturbation of multiple arcs does alter the optimal path") {
			graphWithAlteringArcs.changeWeightOfArc(mapper({1,0}), mapper({2,0}), 5000);
			graphWithAlteringArcs.changeWeightOfArc(mapper({3,0}), mapper({4,0}), 5000);

			auto path = aStar.get_path_as_vector(mapper({0,0}), mapper({4,0}));

			printCPD(mapName.c_str(), cpdFilename.c_str(), xyLoc{4, 0}, std::string{"cpd40"});
			debug("expanded nodes", aStar.getNodeExpandedList());
			debug("generated nodes", aStar.getNodeGeneratedList());
			debug("touched nodes", aStar.getNodeTouchedList());

			debug("path obtained is ", path);
			REQUIRE(((path == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,0})),
						static_cast<uint32_t>(mapper({2,1})),
						static_cast<uint32_t>(mapper({3,1})),
						static_cast<uint32_t>(mapper({4,0}))
			})));

			REQUIRE(aStar.get_nodes_expanded() == 3); //we pop the starting node and when we analyze {1,1} we stop
			REQUIRE(((aStar.get_nodes_generated() >= 6) && (aStar.get_nodes_generated() <= 7)));
			REQUIRE(((aStar.get_nodes_touched() >= 13) && (aStar.get_nodes_touched() <= 18)));
		}
	}

	GIVEN ("A* with fixed upper bound enabled") {
		dynamic_pathfinding_astar aStar = dynamic_pathfinding_astar{&h, &e, true, false, false, 0};
		aStar.set_verbose(true);

		WHEN("perturbation of single arc does not alter optimal path") {
			graphWithAlteringArcs.changeWeightOfArc(mapper({0,3}), mapper({0,4}), 1100);

			//set the fix upperbound
			h.h(mapper({0,0}), mapper({4,0}));
			debug("cost of the optyimal path in the new map is", h.getCPDPathCostInMap(graphWithAlteringArcs));
			REQUIRE((h.getCPDPathCostInMap(graphWithAlteringArcs) == 4000));
			aStar.set_fixed_upperbound(h.getCPDPathCostInMap(graphWithAlteringArcs));
			//run a star
			auto path = aStar.get_path_as_vector(mapper({0,0}), mapper({4,0}));
			REQUIRE(path == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,0})),
						static_cast<uint32_t>(mapper({2,0})),
						static_cast<uint32_t>(mapper({3,0})),
						static_cast<uint32_t>(mapper({4,0}))
			});

			REQUIRE(aStar.getNodeExpandedList() == std::vector<uint32_t>{
				(uint32_t)mapper({0,0}), (uint32_t)mapper({1,0}), (uint32_t)mapper({2,0}), (uint32_t)mapper({3,0})
			});
			REQUIRE(aStar.get_nodes_generated() == 4); //all the other nodes in {x,1} are not even generated!
			REQUIRE(aStar.get_nodes_touched() == 23);
		}

		WHEN("perturbation arcs does alter the optimal path") {

			//cost by going through 1,0: original=4000 revised=5000
			graphWithAlteringArcs.changeWeightOfArc(mapper({0,0}), mapper({1,0}), 2000);
			//cost by going through 0,1: original=5414 revised=6414
			graphWithAlteringArcs.changeWeightOfArc(mapper({0,0}), mapper({0,1}), 2000);
			//cost by going through 1,1: original=5414 revised=6414
			graphWithAlteringArcs.changeWeightOfArc(mapper({0,0}), mapper({1,1}), 2414);

			//set the fix upperbound
			h.h(mapper({0,0}), mapper({0,4}));
			aStar.set_fixed_upperbound(h.getCPDPathCostInMap(graphWithAlteringArcs));
			//run a star
			auto path = aStar.get_path_as_vector(mapper({0,0}), mapper({4,0}));


			debug("expanded nodes", aStar.getNodeExpandedList());
			debug("generated nodes", aStar.getNodeGeneratedList());
			debug("touched nodes", aStar.getNodeTouchedList());

			debug("path obtained is ", path);
			//the path is unchanged
			REQUIRE(((path == std::vector<uint32_t>{
				static_cast<uint32_t>(mapper({0,0})),
						static_cast<uint32_t>(mapper({1,0})),
						static_cast<uint32_t>(mapper({2,0})),
						static_cast<uint32_t>(mapper({3,0})),
						static_cast<uint32_t>(mapper({4,0}))
			})));

			//BUT we generate less nodes!
			REQUIRE(aStar.get_nodes_expanded() == 4);
			REQUIRE(((aStar.get_nodes_generated() == 4)));
			REQUIRE(aStar.get_nodes_touched() == 23);
		}

	}

	GIVEN("testing CPD with caching") {
		AdjGraph newMap = AdjGraph{h.getGraph()};
		CpdCacheHeuristic h2 = CpdCacheHeuristic{h, newMap};
		dynamic_pathfinding_astar aStar = dynamic_pathfinding_astar{&h2, &e, true, true, false, 0};
		aStar.set_verbose(true);

		WHEN("searching a path with A*") {
			xyLoc start = xyLoc{0,0};
			xyLoc goal = xyLoc{4,4};
			uint32_t startid = mapper(start);
			uint32_t goalid = mapper(goal);

			printCPD(mapName.c_str(), cpdFilename.c_str(), goal, std::string{"to_44"});

			std::vector<uint32_t> path = aStar.get_path_as_vector(startid, goalid);

			h2.h(startid, goalid);

			debug("printing cost");
			//debug("cost is", h2.getOriginalCostOfGoingToGoalFrom(mapper(xyLoc{0,0})));
			REQUIRE(h2.isDataInCache(mapper(xyLoc{0,0})));
			REQUIRE(h2.isDataInCache(mapper(xyLoc{0,1})));
			REQUIRE(h2.isDataInCache(mapper(xyLoc{0,2})));
			REQUIRE(h2.isDataInCache(mapper(xyLoc{1,3})));
			REQUIRE(h2.isDataInCache(mapper(xyLoc{2,4})));
			REQUIRE(h2.isDataInCache(mapper(xyLoc{3,4})));
			REQUIRE(h2.isDataInCache(mapper(xyLoc{4,4})));
			REQUIRE(h2.getOriginalCostOfGoingToGoalFrom(mapper(xyLoc{0,0})) == 6828);
			REQUIRE(h2.getOriginalCostOfGoingToGoalFrom(mapper(xyLoc{0,1})) == 5828);
			REQUIRE(h2.getOriginalCostOfGoingToGoalFrom(mapper(xyLoc{0,2})) == 4828);
			REQUIRE(h2.getOriginalCostOfGoingToGoalFrom(mapper(xyLoc{1,3})) == 3414);
			REQUIRE(h2.getOriginalCostOfGoingToGoalFrom(mapper(xyLoc{2,4})) == 2000);
			REQUIRE(h2.getOriginalCostOfGoingToGoalFrom(mapper(xyLoc{3,4})) == 1000);
			REQUIRE(h2.getOriginalCostOfGoingToGoalFrom(mapper(xyLoc{4,4})) == 0000);
			//other cell not in the best path are not cached
			REQUIRE(!h2.isDataInCache(mapper(xyLoc{1,0})));
			REQUIRE(!h2.isDataInCache(mapper(xyLoc{2,0})));
			REQUIRE(!h2.isDataInCache(mapper(xyLoc{1,1})));
		}

	}

}


