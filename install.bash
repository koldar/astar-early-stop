#!/bin/bash

DEPLOY_NAME=Release
PERFORM_CLEAN="true"

#1 if a command is installed on the system; 0 otherwise
function hasCommand {
	echo `which $1 | wc -l`
}

#1 if the user has sudo, 0 othewise
function hasSudo {
	echo `groups | grep sudo | wc -l`
}

function makeProject {
	local projectName=$1
	local deployName=$2
	local performClean=$3
	local oldPwd=`pwd`
	mkdir -pv $projectName/build/$deployName
	cd $projectName/build/$deployName

	if test `hasSudo` -eq 1
	then
		cmake ../..
		if test $performClean = "true"
		then
			make clean
		fi
		make
		local result=$?
		if test $result -ne 0
		then
			echo "build of project $projectName failed!"
			exit 1
		fi
		sudo make install
		sudo ldconfig
	else
		LIBDIR=`echo ~/usr/lib`
		cmake -DU_INSTALL_DIRECTORY=~/usr -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS} -L${LIBDIR}" ../..
		make
		local result=$?
		make install
	fi
	cd $oldPwd
	echo "$result"
}


declare -a PROJECTS_TO_BUILD=("DynamicPathFinding" "PathTesterUtils" "MapPerturbator" "DynamicPathFindingTester" "DatabaseGenerator")

for project in "${PROJECTS_TO_BUILD[@]}"
do
	echo "building $project..."
	value=$(makeProject $project $DEPLOY_NAME $PERFORM_CLEAN)
done

for project in "${PROJECTS_TO_BUILD[@]}"
do
	echo "Succesfully built $project"
done
echo "DONE!"
