/*
 * gridmap.h
 *
 *  Created on: 05 nov 2018
 *      Author: massimobono
 */

#ifndef GRIDMAP_H_
#define GRIDMAP_H_

class GridMap;

#include <vector>
#include <unordered_map>
#include "types.h"
#include "pbmImage.h"
#include "xyLoc.h"
#include <random>
#include "adj_graph.h"

class GridMap {
private:
	std::vector<dpf::celltype_t> cells;
	int width;
	int height;
public:
	GridMap(const std::vector<bool>& bits, int width, int height, dpf::celltype_t typeIfSet);
	GridMap(const std::vector<dpf::celltype_t>& cells, int width, int height);
	GridMap(const GridMap& other);
	~GridMap();
	GridMap& operator =(const GridMap& other);
	AdjGraph getAdjGraph() const;

	std::vector<bool> getTraversableMask() const;

	inline int getWidth() const { return this->width; }
	inline int getHeight() const {return this->height; }
	inline bool isTraversable(dpf::coo2d_t x, dpf::coo2d_t y) const { return this->cells[y * width + x] > 0; }
	inline bool isUntraversable(dpf::coo2d_t x, dpf::coo2d_t y) const { return this->cells[y * width + x] == 0; }
	inline dpf::celltype_t getValue(dpf::coo2d_t x, dpf::coo2d_t y) const { return this->cells[y * width + x];}
	/**
	 * @return all the types inside this gridmap (only traversable cells are considered)
	 */
	std::unordered_set<dpf::celltype_t> getTraversableCellTypes() const;

	xyLoc pickRandomTraversableCell(std::default_random_engine randomEngine) const;

	PPMImage getImage(std::unordered_map<dpf::celltype_t, color_t> colorTraversableMapping, color_t untraversableColor) const;


};


#endif /* GRIDMAP_H_ */
