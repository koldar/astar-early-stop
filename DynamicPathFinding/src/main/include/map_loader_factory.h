/*
 * map_loader_factory.h
 *
 *  Created on: 05 nov 2018
 *      Author: massimobono
 */

#ifndef MAP_LOADER_FACTORY_H_
#define MAP_LOADER_FACTORY_H_

#include "map_loader.h"
#include "multi_map_loader.h"
#include <unordered_set>

/**
 * @brief class allowing you to load the correct map loader structure given a map name
 * 
 * 
 * During tests some maps need a certain way to load its data from a file (e.g. ignore "T" cells) while others
 * needs other ways to load the data from the file (e.g., loads all the symbols with different vlaues).
 * 
 * This factory allows you to agnosticly load a map without having to see the laoding mechanism at all.
 * 
 * 
 */
class MapLoaderFactory {
public:
	MapLoaderFactory();

	/**
	 * a new map loader
	 *
	 * @attention
	 * be sure to deallocate from memory the return value after use
	 *
	 */
	AbstractMapLoader* get(const char* filename) const;
private:
	std::unordered_set<char> symbolsInFile(const char* filename) const;
};


#endif /* MAP_LOADER_FACTORY_H_ */
