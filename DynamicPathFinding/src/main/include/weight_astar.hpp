#ifndef _WEIGHT_ASTAR_HEADER__
#define _WEIGHT_ASTAR_HEADER__

#include "abstract_weighted_astar.hpp"

/**
 * @brief A basic A* implementation
 * 
 * This implementation does NOT feature:
 *  - pruning;
 *  - early stop;
 *  - bounded search;
 * 
 * @tparam H 
 * @tparam E 
 * @tparam STATS 
 */
template <typename H, typename E, typename STATS>
class WeightPathFindingAStar : public AbstractWeightPathFindingAStar<H, E, STATS> {
public:
	WeightPathFindingAStar(H* heuristic, E* expander, STATS* stats, bool enableStatistics, double hscale) : 
        AbstractWeightPathFindingAStar<H, E, STATS>{heuristic, expander, stats, enableStatistics, hscale} {
	}
	
	virtual ~WeightPathFindingAStar() {

	}
protected:

    virtual void onGoalFound(const warthog::search_node* start, const warthog::search_node* goal, warthog::pqueue* openList, bool& keepGoing) {

    }

    virtual bool shouldWePruneNode(const warthog::search_node* start, const warthog::search_node* n, warthog::pqueue* openList, dpf::nodeid_t goalid) {
        return false;
    }

    virtual void onPrunedNode(const warthog::search_node* start, const warthog::search_node* n, warthog::pqueue* openList, dpf::nodeid_t goalid) {

    }

    virtual void onOpenListEmpty(const warthog::search_node* start, const warthog::search_node* goal, dpf::cost_t goalid) {

    }

    virtual void onBeforeStartSearching(const warthog::search_node* start, dpf::nodeid_t goalid) {

    }

    virtual void onExpandedNodeInClosedList(warthog::search_node* parent, warthog::search_node* child, dpf::cost_t cost_to_n) {
        throw std::domain_error{"expanding a node in closed list! this should never happen since we actively filter aweay expanded nodes!"};
    }

    virtual void onBeginNodeExpanded(const warthog::search_node* parent, const warthog::search_node* child, dpf::cost_t cost_to_n, bool& should_skip) {
        should_skip = child->get_expanded();
    }

};


#endif