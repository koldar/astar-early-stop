/*
 * path.h
 *
 *  Created on: Oct 22, 2018
 *      Author: koldar
 */

#ifndef GRIDMAP_PATH_H_
#define GRIDMAP_PATH_H_

class gridmap_path;

#include <vector>
#include "xyLoc.h"
#include "types.h"

/**
 * a vector representing a path of lcoations
 */
class gridmap_path : public std::vector<xyLoc> {
public:
	gridmap_path(const std::vector<xyLoc>& arg) : std::vector<xyLoc>{arg} {

	}

	/**
	 * Get a 2 points, @c min and @c max, representing respectively a recvtangle which fully contains the path
	 *
	 *@param[out] minPoint the top-left point such that no cell in the path has both x and y less than this point
	 *@param[out] axPoint the bottom-right point such that no cell in the path has both x and y greater than this point
	 */
	void getRanges(xyLoc& minPoint, xyLoc& maxPoint);

	/**
	 * @param[in] from a cell
	 * @param[in] to another cell
	 * @return
	 *  @li true if the path contains a move that **directly** goes from @c from to @c to
	 */
	bool contains(xyLoc from, xyLoc to) const;

	~gridmap_path() {

	}

};



#endif /* GRIDMAP_PATH_H_ */
