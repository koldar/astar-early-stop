/**
 * @file
 *
 * An algorithm of Dijkstra that will stop if reaches the optimal path fom a node to another one
 *
 *  Created on: Oct 18, 2018
 *      Author: koldar
 */

#ifndef EARLYSTOP_DIJKSTRA_H_
#define EARLYSTOP_DIJKSTRA_H_

#include "types.h"
#include "adj_graph.h"
#include <vector>
#include "heap.h"
#include "log.h"
#include "xyLoc.h"
#include "mapper.h"
#include <assert.h>
#include "pbmImage.h"

class EarlyStopDijkstra {
public:
	EarlyStopDijkstra(const AdjGraph& g, bool enableStatistics);

	~EarlyStopDijkstra();

    /**
     * Run dijkstra algorithm
     *
     * @param[in] start the node id where we start
     * @param[in] goal the node di we want to reach
     */
	std::vector<dpf::nodeid_t> run(dpf::nodeid_t start, dpf::nodeid_t goal);

	/**
	 * After calling dijkstra algorithm, use this function to determine the optimal path from a start node till a goal
	 *
	 * @code
	 * Dijkstra algorithm=Dijkstra{g};
	 * auto result = algorithm.run(5);
	 * std::vector<xyLoc> path = algorithm.getPathAsVector(5, 10, mapper, result);
	 * @endcode
	 *
	 * @param[in] startNode the same startNode as in the previous Dijkstra::run call,
	 * @param[in] goal the vertex we need to reach
	 * @param[in] mapper converter used to converting vertex id in locations
	 * @param[in] optimalMoves return value from the last Dijkstra::run call;
	 * @return a list of location representing the path we need to do in order to go from @c startNode till @c goal
	 */
	std::vector<xyLoc> getPathAsVector(dpf::nodeid_t start, dpf::nodeid_t goal, const Mapper& mapper, const std::vector<dpf::nodeid_t>& optimalMoves);

	/**
	 * @pre
	 *  @li run called
	 *  @li enableStatistics set to true
	 *
	 * @return the nodes expandeed in the algorithm
	 */
	const std::vector<dpf::nodeid_t>& getExpandedNodes() const;

	/**
	 * @pre
	 *  @ li run called
	 *
	 * @return the number fo nodes expanded in the algorithm
	 */
	dpf::big_integer getExpandedNodesNumber() const;

	/**
	 * generate a PBM image representing the expanded nodes of the last algorithm call
	 *
	 * @pre
	 *  @li the map given in the constructor of the object needs to represents the map @c rawMap
	 *
	 * @param[in] filename the filenam eof the PBM image (no extension!)
     * @param[in] startNode the node id where the last dijkstra algorith mstarted its search
     * @param[in] goalNode the node id where the last dijkstra algorithm ended its search
	 * @param[in] rawMap the map indicating the available cells
	 * @param[in] width the width of @c rawMap
	 * @param[in] height the height of @c rawMap
	 * @param[in] path an additional path to draw
	 * @param[in] imageToUpdate if not null, we will generate an image which is overlaid upon @c imageUpdate
	 */
	PPMImage printExpandedNodesImage(const std::string& filename, dpf::nodeid_t startNode, dpf::nodeid_t goalNode, const Mapper& mapper, const GridMap& map, const std::vector<dpf::nodeid_t>& path, const PPMImage* imageToUpdate) const;

private:
	void reach(dpf::nodeid_t v, dpf::cost_t d, dpf::nodeid_t bestPrevious);

	void clear();

private:
    /**
     * the graph where to apply dijkstra algorithm
     */
	const AdjGraph& g;
	/**
	 * A priority queue of vertices
	 */
	min_id_heap<dpf::cost_t> q;
	std::vector<dpf::cost_t> distancesFromSource;
	/**
	 * id represents the id we want to reach while the value is the nodewe need to arrive from
	 *
	 * th
	 */
	std::vector<dpf::nodeid_t> bestPreviousNode;
	/**
	 * If true, we will enable statistics
	 */
	bool enableStatistics;
	/**
	 * the nodes expandedin the last algorithm call
	 *
	 * used only if enableStatistics is true
	 */
	std::vector<dpf::nodeid_t> expandedNodes;
	/**
	 * the number of nodes expanded
	 *
	 * We don't use expandedNodes because that varaible can be turned off with enableStatistics
	 */
	dpf::big_integer expandedNodeNumber;
};



#endif /* EARLYSTOP_DIJKSTRA_H_ */
