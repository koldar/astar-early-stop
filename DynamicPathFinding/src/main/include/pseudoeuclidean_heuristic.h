/*
 * pseudoeuclidean_heuristic.h
 *
 *  Created on: Oct 11, 2018
 *      Author: koldar
 */

#ifndef PSEUDOEUCLIDEAN_HEURISTIC_H_
#define PSEUDOEUCLIDEAN_HEURISTIC_H_

#include "cpd_heuristic.h"

class PseudoEuclideanHeuristic : public AbstractHeuristic {
public:
	PseudoEuclideanHeuristic(const Mapper& mapper);

	virtual dpf::cost_t h(dpf::nodeid_t startid, dpf::nodeid_t goalid) = 0;
	virtual void clear() = 0;
private:
	const Mapper& mapper;
};



#endif /* PSEUDOEUCLIDEAN_HEURISTIC_H_ */
