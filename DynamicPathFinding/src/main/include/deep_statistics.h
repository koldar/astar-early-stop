/**
 * @file
 *
 * A statistic for A* which keeps track of the expanded/generated/touches nodes as well.
 *
 * Useful in debugging to grasp what A* is actually doing. Using this statistic will decrease performances.
 *
 *  Created on: Oct 17, 2018
 *      Author: koldar
 */

#ifndef DEEP_STATISTICS_H_
#define DEEP_STATISTICS_H_

#include "abstract_pathfinding_astar.h"
#include <vector>

class DeepStatistics : public AbstractStats {
public:
	DeepStatistics();
	~DeepStatistics();
public:
	/**
	 * reset the generated node statistic
	 */
	virtual void reset_nodes_generated();
	/**
	 * reset the expanded node statistic
	 */
	virtual void reset_nodes_expanded();
	/**
	 * reset the touched node statistic
	 */
	virtual void reset_nodes_touched();

	virtual void add_node_generated(const warthog::search_node& n);
	virtual void add_node_expanded(const warthog::search_node& n);
	virtual void add_node_touched(const warthog::search_node& n);
	virtual void reset_heuristic_time();
	virtual void add_to_heuristic_time(const warthog::search_node& n, unsigned long t);
	virtual unsigned long get_total_heuristic_time();

	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return Number of times we have expanded a node from the open list
	 */
	virtual int get_nodes_expanded() const;

	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return the number of nodes we have added inside the open list (initial node excluded)
	 */
	virtual int get_nodes_generated() const;

	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return the number that we have considered a node
	 */
	virtual int get_nodes_touched() const;

private:
	std::vector<dpf::nodeid_t> generatedNodeIds;
	std::vector<dpf::nodeid_t> expandedNodeIds;
	std::vector<dpf::nodeid_t> touchedNodeIds;
	/**
	 * the total time spent computing the heuristic (in microseconds)
	 */
	unsigned long totalheuristicTime;
};



#endif /* DEEP_STATISTICS_H_ */
