/**
 * @file
 *
 * A file where some exception used in the project are available
 *
 * @date Oct 1, 2018
 * @author koldar
 */

#ifndef ERRORS_H_
#define ERRORS_H_

#include <exception>
#include <string>

using namespace std;

struct CpdException: public exception
{

public:
	CpdException(const string& msg);
	CpdException(const string& fmt, ...);
	~CpdException();

	virtual const char* what() const throw();

private:
	string* _msg;

};

#endif /* ERRORS_H_ */
