/**
 * @file
 *
 * An AbstractExpander implementation for grid-based maps
 *
 * @attention
 * This expander assume the id of each search_node is also the id of a vertex in the AdjGraph representing the grid map
 *
 *
 *  Created on: Oct 2, 2018
 *      Author: koldar
 */

#ifndef GRID_MAP_EXPANDER_H_
#define GRID_MAP_EXPANDER_H_

#include <vector>
#include "abstract_pathfinding_astar.h"
#include "mapper.h"
#include "adj_graph.h"
#include <unordered_map>
#include "dynamic_search_node.h"

using namespace std;

/**
 * a hash function which simply return the given parameter
 *
 * @param[in] key the key to hash
 * @return the hashed key
 */
size_t identityHash(uint32_t key);

/**
 * The maximum number of object that may be put in a cpool
 */
constexpr int CPOOL_MAX_NUMBER = 100;

/**
 * @brief structure generating successors nodes for search algorithms when the map is a grid map
 * 
 * Suppose you're in a grid map and you are in (5,5). You needs to generate the successors.
 * You needs someone generating respectively (4,4), (5,4), (6,4), (4,5), (6,5), (4,6), (5,6), (6,6).
 * That someone also needs to check if the cells are traversable. If you have already generated one of those nodes,
 * that someone also needs to return the same node and not a copy of the old one.
 * 
 * This class is that "someone"
 * 
 */
class GridMapExpander : public AbstractExpander<DynamicSearchNode> {
public:
	/**
	 * @brief Construct a new Grid Map Expander object
	 * 
	 * @param map the grid map where to generate successors
	 * @param mapper function translating nodeid_t into xyLoc and viceversa
	 * @param adjGraph the underlying graph representing the map
	 * @param useVectroInsteadOfMap in order to check if a node has already been expanded or not, we can either use a map or a vector.
	 * 	Maps trades less memory but for more computational effort. Vector use more memory but the access time of the nodes is faster.
	 * 	Set this value to true if you want to use a vector, false if you want to use a map
	 */
	GridMapExpander(const GridMap& map, const Mapper& mapper, const AdjGraph& adjGraph, bool useVectroInsteadOfMap);

	virtual int mapwidth() const;
	virtual size_t mem() const;
	virtual DynamicSearchNode* generate(uint32_t startid);
	virtual void expand(const DynamicSearchNode* node, warthog::problem_instance* instance);
	virtual ~GridMapExpander();
	virtual void first(DynamicSearchNode*& n, dpf::cost_t& cost_to_n);
	virtual void next(DynamicSearchNode*& n, dpf::cost_t& cost_to_n);
	virtual void clear();

public:
	/**
	 * @return the map used by the expander to fetch new successors and costs
	 */
	const AdjGraph& get_map() const;
	/**
	 * For each element in the vector, it generates a search_node
	 *
	 * Each element of @c cells has parent the previous one. The first element has parent @c parent
	 *
	 * @param[in] moves the vector we need to iterate on
	 * @param[in] node the node where we apply the first move
	 * @param[out] pthCost the cost fo the path starting from @c n till the goal
	 * @return the search_node representing the last element of @c moves
	 */
	warthog::search_node& expandPath(const std::vector<dpf::move_t>& moves, const warthog::search_node& node, dpf::cost_t pathCost);
private:
	bool canBeOnOptimalPath(uint32_t id) const ;
private:
	//it's not the job of this class to change this resources; this is why I defined them as const

	const GridMap& map;
	const Mapper& mapper;
	const AdjGraph& adjGraph;

	bool useVectorInsteadOfMap;
	/**
	 * a list of nodes, indexed by their ids.
	 *
	 * We initially fill it with nullptr
	 */
	std::vector<DynamicSearchNode*> searchNodeVector;
	/**
	 * a map where the key contains the id of the nodes whilst the values contains the search node associated to them
	 */
	unordered_map<uint32_t, DynamicSearchNode&> searchNodeMap;

	/**
	 * Memory pool to avoid time consuming heap allocations
	 *
	 * This pool is used to allocate memory for the search_node in ::GridMapExpander::searchNodeMap
	 */
	warthog::mem::cpool* searchNodePool;
	/**
	 * the node whose successors we need to compute
	 *
	 * if noone has requested yet a expand routine, this field must be nullptr
	 */
	const warthog::search_node* parent;
	/**
	 * an index representing which element of GridMapExpander::nodeSuccessor the function GridMapExpander::next has to pick up
	 *
	 * UB if noone has requested yet a expand routine, this field must be nullptr
	 */
	int nodeSuccessorNextIndex;
	/**
	 * A list where successors of a node are temporarly put
	 *
	 * Each item is a pair. The first element is the arc that goes from the GridMapExpander::parent to the GridMapExpander::adjGraph
	 * vertex represented by the second element of the pair.
	 *
	 */
	vector<std::pair<const OutArc&, DynamicSearchNode&>> nodeSuccessors;


};



#endif /* GRID_MAP_EXPANDER_H_ */
