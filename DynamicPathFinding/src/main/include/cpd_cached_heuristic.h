/**
 *  @file
 *
 *  An heuristic equal to Cpdheuristic but it caches the heuristics computes by all the location found in an optimal path
 *
 * @date Oct 5, 2018
 * @author koldar
 */

#ifndef CPD_CACHED_HEURISTIC_H_
#define CPD_CACHED_HEURISTIC_H_

#include "cpd_heuristic.h"

namespace internal {
	struct cache_cell {
		/**
		 * the cost of the path in the original map
		 */
		dpf::cost_t originalMapCost;
		/**
		 * the cost fo the path in the weight modified map
		 */
		dpf::cost_t newMapCost;
	};
}

/**
 * An heuristic for path planning which caches intermediate path planning results
 */
class CpdCacheHeuristic : public CpdHeuristic {
public:

	CpdCacheHeuristic(const std::vector<bool>& rawMapData, int width, int height, const char* filename, const AdjGraph& newMap);
	CpdCacheHeuristic(const CpdHeuristic& h, const AdjGraph& newMap);
	virtual dpf::cost_t h(dpf::nodeid_t startid, dpf::nodeid_t goalid);
	dpf::cost_t getCPDPathCostInMap() const;
	virtual bool doesCPDPathHaveARevisedArcIn(const AdjGraph& newMap) const;
	~CpdCacheHeuristic();
public:
	inline dpf::cost_t getOriginalCostOfGoingToGoalFrom(dpf::nodeid_t nodeId) const {
		if (!this->hasVertexHasHeuristic(nodeId)) {
			throw CpdException{"node %u does not have an heuristic set!", nodeId};
		}
		return this->getCachedHeuristic(nodeId);
	}

	inline dpf::cost_t getNewCostOfGoingToGoalFrom(dpf::nodeid_t nodeId) const {
		if (!this->hasVertexHasHeuristic(nodeId)) {
			throw CpdException{"node %u does not have an cost set!", nodeId};
		}
		return this->getCachedCostInNewMap(nodeId);
	}
	inline bool isDataInCache(dpf::nodeid_t nodeId) const {
		return this->hasVertexHasHeuristic(nodeId);
	}

	virtual void clear() {
		CpdHeuristic::clear();
		std::fill(this->costs.begin(), this->costs.end(), internal::cache_cell{NO_HEURISTIC, NO_HEURISTIC});
	}

	virtual dpf::cost_t getCPDPathCostInMap(const AdjGraph& newMap) const;
	virtual dpf::cost_t getCPDPathCostInMap(const AdjGraph& newMap, dpf::nodeid_t n, dpf::nodeid_t goal);
private:
	/**
	 * @param[in] id the id of the vertex to check
	 * @return
	 *  @li true  if a given vertex has an heuristic cached;
	 *  @li false otherwise
	 */
	inline bool hasVertexHasHeuristic(dpf::nodeid_t id) const {
		return this->costs[id].originalMapCost != NO_HEURISTIC;
	}

	inline dpf::cost_t getCachedHeuristic(dpf::nodeid_t id) const {
		assert(this->hasVertexHasHeuristic(id));
		return this->costs[id].originalMapCost;
	}

	inline dpf::cost_t getCachedCostInNewMap(dpf::nodeid_t id) const {
		assert(this->hasVertexHasHeuristic(id));
		return this->costs[id].newMapCost;
	}

	inline void updateCachedHeuristic(dpf::nodeid_t id, dpf::cost_t originalMapCost, dpf::cost_t newMapPathCost) {
		debug("we're setting heuristics of", id, ": original cost to goal is", originalMapCost, "revised cost to goal is", newMapPathCost);
		this->costs[id].originalMapCost = originalMapCost;
		this->costs[id].newMapCost = newMapPathCost;
	}
private:
	/**
	 * A vector storing the heuristics of each node in the grid
	 *
	 * This is a cache of heuristics.
	 *
	 * index of this vector represents the id of the vertices in the AdjGraph representing the grid map.
	 * The value associated to an index represents the heuristic value we've previously computed for a particular vertex.
	 *
	 * The structure is just for one goal, so if you chnage goal locations all the heuristic value will stop being useful.
	 * If a vertex has no an heuristic yet, the associated value is CpdCacheHeuristic::NO_HEURISTIC
	 */
	std::vector<struct internal::cache_cell> costs;
	/**
	 * The map identical to the one insert in the constructor but some of its arcs weights have been increased.
	 */
	const AdjGraph& newMap;
	/**
	 * Value to be put in CpdCacheHeuristic::costs if no heuristic has been evaluated yet.
	 */
	static constexpr dpf::cost_t NO_HEURISTIC = -1;
};



#endif /* CPD_CACHED_HEURISTIC_H_ */
