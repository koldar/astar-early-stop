/*
 * differential_heuristic.h
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#ifndef DIFFERENTIAL_HEURISTIC_H_
#define DIFFERENTIAL_HEURISTIC_H_

#include "types.h"
#include <cstdint>
#include "abstract_pathfinding_astar.h"
#include "gridmap.h"
#include <vector>
#include "adj_graph.h"
#include <random>
#include <unordered_set>
#include <unordered_map>
#include "mapper.h"

struct farthest_node_t;

class AbstractLandmarkPlacingStrategy {
public:
	AbstractLandmarkPlacingStrategy(int landmarkToCreate);
	virtual ~AbstractLandmarkPlacingStrategy();

	virtual std::vector<dpf::nodeid_t> getLandmarks(const AdjGraph& graph, std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>>& getDistancesFromLandmarksToNodes) = 0;
	virtual void reset() = 0;
protected:
	const int landmarkToCreate;
};


class CanonicalAdvancePlacingLandmarkStrategy : public AbstractLandmarkPlacingStrategy {
	CanonicalAdvancePlacingLandmarkStrategy(int landmarkToCreate);
	virtual ~CanonicalAdvancePlacingLandmarkStrategy();

	virtual std::vector<dpf::nodeid_t> getLandmarks(const AdjGraph& graph, std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>>& getDistancesFromLandmarksToNodes);
	virtual void reset();
private:
	std::vector<farthest_node_t> getFarthestNodesFrom(const AdjGraph& graph, dpf::nodeid_t n) const;
	farthest_node_t breakTieInFarthestNodes(const std::vector<farthest_node_t>& nodes) const;
	bool checkNodesHaveSameDistance(const std::vector<farthest_node_t>& nodes) const;
	std::vector<dpf::cost_t> getDistancesFromNode(const AdjGraph& graph, dpf::nodeid_t n) const;
	dpf::nodeid_t getBestNodeByCostSumOfLandmarks(const AdjGraph& g, const std::unordered_map<dpf::nodeid_t, std::vector<dpf::cost_t>>& distancesFromLandmarks, dpf::nodeid_t a, dpf::nodeid_t b) const;
	bool isNodeUnMarked(dpf::nodeid_t n) const;
	dpf::nodeid_t getRandomUnMarkedNode(const AdjGraph& g) const;
	void markNode(dpf::nodeid_t n);
	void _markNodesInBreadthFirstSearch(const AdjGraph& g, dpf::nodeid_t n, safe_int statesToMark, std::vector<dpf::nodeid_t>& bfsQueue);
	void markNodesInBreadthFirstSearch(const AdjGraph& g, dpf::nodeid_t start, safe_int statesToMark);
private:
	/**
	 * true if the node has been marked, false othewise
	 */
	//std::vector<bool> markedNodes;
	std::vector<dpf::nodeid_t> unmarkedNodes;
};

class LandMarkDatabase {
public:
	/**
	 * @brief  generate the landmark database and allows you to retrieve information about the construction
	 * 
	 * @param graph graph used to operate the landmark with
	 * @param policy how do you want to select landmarks
	 * @param filename name of the file we will create containing the landmarks
	 * @param time time used to generate the landmarks. It is in micro seconds
	 * @param space number of bytes occupied by the file generated
	 * @return explitic 
	 */
	explicit LandMarkDatabase(const AdjGraph& graph, AbstractLandmarkPlacingStrategy& policy, const std::string& filename, double& time, size_t& space);
	explicit LandMarkDatabase(const AdjGraph& graph, AbstractLandmarkPlacingStrategy& policy, const std::string& filename);
	explicit LandMarkDatabase(const AdjGraph& graph, AbstractLandmarkPlacingStrategy& policy);
	explicit LandMarkDatabase(const std::vector<dpf::nodeid_t> landmarks, const std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>>& landMarksDistances);
	LandMarkDatabase(const LandMarkDatabase& other);
	virtual ~LandMarkDatabase();
	LandMarkDatabase& operator = (const LandMarkDatabase& other);
	const std::vector<dpf::nodeid_t> getLandmarks() const;
	dpf::big_integer getGraphSize() const;
	dpf::cost_t getDistanceFromLandmarkToNode(dpf::nodeid_t landmark, dpf::nodeid_t n) const;
	static LandMarkDatabase loadFromFile(const std::string& filename);
	void saveIntoFile(const std::string& filename);
	size_t getMemoryOccupied() const;
private:
	std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>> landMarksDistances;
	std::vector<dpf::nodeid_t> landmarks;
};

class DifferentHeuristicAdvancePlacingLandmarkStrategy : public AbstractLandmarkPlacingStrategy {
public:
	DifferentHeuristicAdvancePlacingLandmarkStrategy(int landmarkToCreate);
	virtual ~DifferentHeuristicAdvancePlacingLandmarkStrategy();

	virtual std::vector<dpf::nodeid_t> getLandmarks(const AdjGraph& graph, std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>>& getDistancesFromLandmarksToNodes);

	virtual void reset();
private:
	std::vector<uint32_t> convertCostTInto4Bytes(const std::vector<dpf::cost_t>& costVector) const;
	dpf::nodeid_t getRandomNode(const AdjGraph& g) const;
	std::vector<dpf::cost_t> getDistancesFrom(const AdjGraph& g, dpf::nodeid_t n) const;
	std::vector<dpf::nodeid_t> getFarthestNodes(const std::vector<dpf::cost_t>& distances, dpf::nodeid_t n, dpf::cost_t& maxDistance) const;
	dpf::nodeid_t breakFarthestNodesTie(const std::vector<dpf::nodeid_t>& farthestNodes) const;
	dpf::cost_t getMinimumDistanceFromLandmarks(const AdjGraph& g, dpf::nodeid_t n, const std::unordered_map<dpf::nodeid_t, std::vector<dpf::cost_t>>& distancesFromLandmarks) const;
};

/**
 * Represents a class which can be injected into ::abstract_pathfinding_astar in order to
 */
class DifferentialHeuristic : public AbstractHeuristic {

public:
	//TODO the mapper is here only for debuggin purposes... it shouldn't be present!
	DifferentialHeuristic(const AdjGraph& graph, const Mapper& mapper, const LandMarkDatabase& landmarkDatabase);
	DifferentialHeuristic(const DifferentialHeuristic& other);
//	DifferentialHeuristic& operator = (const DifferentialHeuristic& other);
	virtual ~DifferentialHeuristic();

	virtual dpf::cost_t h(uint32_t startid, uint32_t goalid);
	virtual void clear();
private:
	const LandMarkDatabase& landmarkDatabase;
	const AdjGraph& graph;
	const Mapper& mapper;
};



#endif /* DIFFERENTIAL_HEURISTIC_H_ */
