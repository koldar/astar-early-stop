/*
 * multi_map_loader.h
 *
 *  Created on: Nov 5, 2018
 *      Author: koldar
 */

#ifndef MULTI_MAP_LOADER_H_
#define MULTI_MAP_LOADER_H_

#include "map_loader.h"
#include <unordered_map>
#include <unordered_set>
#include "types.h"
#include "adj_graph.h"
#include "mapper.h"

/**
 * A map with several differente pixel color, each representinga  different terrain
 *
 * You can set the terrain cost and types as you wish
 */
class MultiColorMapLoader : public AbstractMapLoader {
private:
	/**
	 * Set of characters which are traversable.
	 *
	 * Each key is the symbol read from the map file while the value is the type of the terrain to associate
	 */
	std::unordered_map<char, dpf::celltype_t> traversableCharacters;
	/**
	 * A set of character in a map representing untraversable cells
	 */
	std::unordered_set<char> untraversableCharacters;
public:
	explicit MultiColorMapLoader() : traversableCharacters{}, untraversableCharacters{} {};

	virtual ~MultiColorMapLoader();

	MultiColorMapLoader& addUntraversableColor(char color);

	MultiColorMapLoader& addTraversableColor(char color, dpf::celltype_t t);
protected:
	virtual dpf::celltype_t handleCell(const std::vector<dpf::celltype_t>& map, int width, int height, dpf::coo2d_t x, dpf::coo2d_t y, char symbolRead);
};



#endif /* MULTI_MAP_LOADER_H_ */
