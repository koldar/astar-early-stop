#ifndef _CSV_WRITER_HEADER__
#define _CSV_WRITER_HEADER__

#include <vector>
#include <stdio.h>

//TODO in PathFindingTestUtils ther already is an identical class. Remove this and usde the other one
//this requires to create a "cpp utils project"
class CSVWriter {
/**
 * @brief a structure allowing you to print rows in a CSV.
 * 
 * In order to use this class, you need to exactly know the columns and the types of the csv
 * 
 */
public:
    CSVWriter(const std::string& filename, const std::vector<std::string>& header, char separator) : filename{filename}, header{header}, separator{separator} {
        this->f.open(filename);
        this->f << "sep=" << separator << std::endl;
        for (int i=0; i<header.size(); ++i) {
            this->f << header[i];
            if ((i+1) != header.size()) {
                this->f << this->separator;
            }
        }
        this->f << std::endl;
    }
    ~CSVWriter() {
        this->f.close();
    }
    /**
     * @brief print a row within the CSV
     * 
     * @code
     * CSVWriter<int,bool,int> csv{"hello.csv", {"a", "b", "c"}, ','};
     * csv.writeRow(4, true, 5);
     * @endcode
     * 
     * @param a the first value you need to add
     * @param other all the other values you need to add
     */
    template <typename FIRST, typename ... OTHER>
    void writeRow(const FIRST& a, OTHER... other) {
        this->_write(a, other...);
    }
    
protected:
    void _write() {
        this->f << std::endl;
    }
    template <typename FIRST, typename ... OTHER>
    void _write(const FIRST& a, OTHER... other) {
        this->f << a << this->separator;
        this->_write(other...);
    }
private:
    const std::string& filename;
    ofstream f;
    char separator;
    std::vector<std::string> header;
};

#endif