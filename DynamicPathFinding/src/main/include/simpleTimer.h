/**
 * @file
 *
 * Naive timer implementation
 *
 * @code
 * PROFILE_TIME_CODE(microseconds) {
 * 	printf("hello world!\n");
 * }
 * printf("job took %2.3f us\n", microseconds);
 * @endcode
 *
 * @date Oct 10, 2018
 * @author koldar
 */

#ifndef SIMPLETIMER_H_
#define SIMPLETIMER_H_

#include <sys/time.h>

class SimpleTimer {
public:
	/**
	 * generates a timer
	 *
	 * @param[in] microSecondsElapsed a variable where to store the number of microseconds used to perform a task
	 * pram[in] autoStart true fi we want to automatically start the timer. False otherwise
	 */
	SimpleTimer(double& microSecondsElapsed, bool autoStart=true);
	/**
	 * Destroy the timer
	 *
	 * @note
	 * This will automatically stop the timer if the timer is still going on
	 */
	~SimpleTimer();
public:
	/**
	 * Start the timer
	 */
	void StartTimer();
	/**
	 * Stop the timer
	 *
	 * @return the number of microseconds elapsed
	 */
	double EndTimer();
	/**
	 * @return the number of microseconds elapsed
	 */
	double GetElapsedTime_us() const;
	/**
	 * @return the number of milliseconds elapsed
	 */
	double GetElapsedTime_ms() const;
	/**
	 * @return the number of seconds elapsed
	 */
	double GetElapsedTime_s() const;
private:
	/**
	 * variable where to store the number of microseconds elapsed
	 */
	double& microSecondsElapsed;
	/**
	 * time when the last SimpleTimer::StartTimer happened
	 */
	struct timeval start;
};

/**
 * Main macro used to easily profile code
 *
 * @pre
 *  @li @c microSecondElapsed should already been declared
 *
 * @param[in] microSecondElapsed name of a variable idenfitier of type <tt>double</tt> already declared
 */
#define PROFILE_TIME_CODE(microSecondElapsed) \
for (bool _profile_time_code=true; _profile_time_code;) \
	for (SimpleTimer t = SimpleTimer{microSecondElapsed}; _profile_time_code; _profile_time_code=false)

#endif /* SIMPLETIMER_H_ */
