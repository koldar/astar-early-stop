/**
 * @file
 *
 * a wrapper of an integer number which automatically handles overflow for +, - and * operations
 *
 * When an overflow, the number is coerced to be the maximum value
 *
 *  Created on: Oct 17, 2018
 *      Author: koldar
 */

#ifndef SAFE_MATH_H_
#define SAFE_MATH_H_

#include <climits>
#include <cstdint>
#include <iostream>

/**
 * a simple "int" type which can be safely added, substracted or multiplied with other integer values
 */
struct safe_int {
	friend safe_int operator +(const safe_int& a, const safe_int& b);
	friend safe_int operator -(const safe_int& a, const safe_int& b);
	friend safe_int operator *(const safe_int& a, const safe_int& b);
	friend safe_int operator /(const safe_int& a, const safe_int& b);
	friend bool operator ==(const safe_int& a, const safe_int& b);
	friend bool operator !=(const safe_int& a, const safe_int& b);
	friend bool operator <=(const safe_int& a, const safe_int& b);
	friend bool operator <(const safe_int& a, const safe_int& b);
	friend bool operator >=(const safe_int& a, const safe_int& b);
	friend bool operator >(const safe_int& a, const safe_int& b);
	friend std::ostream& operator << (std::ostream& stream, const safe_int& a);
private:
	long var;
public:
	constexpr safe_int() : var{0} {

	}

	constexpr safe_int(int a) : var{a} {

	}

	constexpr safe_int(long a) : var{a} {

	}

	constexpr safe_int(uint32_t a) : var{static_cast<long>(a)} {

	}

	constexpr safe_int(float a) : var{static_cast<long>(a)} {

	}

	constexpr safe_int(double a) : var{static_cast<long>(a)} {

	}

	constexpr safe_int(long long a) : var{static_cast<long>(a)} {

	}

	safe_int(const safe_int& a);
	/**
	 * the minimum value this number can reach
	 */
	static constexpr safe_int getLowerBound() {
		return INT_MIN;
	}
	/**
	 * the maximum value this number can reach
	 */
	static constexpr safe_int getUpperBound() {
		return INT_MAX;
	}

public:
	safe_int& operator +=(const safe_int& a);
	safe_int& operator *=(const safe_int& a);
	safe_int& operator -=(const safe_int& a);
	safe_int& operator /=(const safe_int& a);

	safe_int& operator =(const safe_int& a);
	safe_int& operator = (int a);

	explicit operator int();
	explicit operator unsigned long long();
	explicit operator long long();
	explicit operator uint32_t();
	explicit operator uint32_t() const;
	explicit operator float();
	explicit operator double();
};

safe_int operator +(const safe_int& a, const safe_int& b);
safe_int operator +(const safe_int& a, const int& b);
safe_int operator +(const safe_int& a, const uint32_t& b);
safe_int operator +(const safe_int& a, const double& b);
safe_int operator -(const safe_int& a, const safe_int& b);
safe_int operator -(const safe_int& a, const int& b);
safe_int operator -(const safe_int& a, const uint32_t& b);
safe_int operator -(const safe_int& a, const double& b);
safe_int operator *(const safe_int& a, const safe_int& b);
safe_int operator *(const safe_int& a, const int& b);
safe_int operator *(const safe_int& a, const uint32_t& b);
safe_int operator *(const safe_int& a, const double& b);
safe_int operator /(const safe_int& a, const safe_int& b);
safe_int operator /(const safe_int& a, const int& b);
safe_int operator /(const safe_int& a, const uint32_t& b);
safe_int operator /(const safe_int& a, const double& b);

bool operator ==(const safe_int& a, const safe_int& b);
bool operator !=(const safe_int& a, const safe_int& b);
bool operator <=(const safe_int& a, const safe_int& b);
bool operator <(const safe_int& a, const safe_int& b);
bool operator >=(const safe_int& a, const safe_int& b);
bool operator >(const safe_int& a, const safe_int& b);

std::ostream& operator << (std::ostream& stream, const safe_int& a);


#endif /* SAFE_MATH_H_ */
