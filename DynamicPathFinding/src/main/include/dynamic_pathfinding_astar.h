/*
 * dynamic_apthfinding_astar.h
 *
 *  Created on: Oct 4, 2018
 *      Author: koldar
 */

#ifndef DYNAMIC_PATHFINDING_ASTAR_H_
#define DYNAMIC_PATHFINDING_ASTAR_H_


#include "abstract_pathfinding_astar.h"
#include "grid_map_expander.h"
#include "cpd_heuristic.h"
#include "log.h"
#include <cmath>

template <typename H, typename E, typename STATS>
class dynamic_pathfinding_astar : public abstract_pathfinding_astar<H, E,STATS>{

public:
	dynamic_pathfinding_astar(H* heuristic, E* expander, STATS* stats, bool upperBoundOptimization, bool earlyStopOptimization, bool enableStatistics, double hScale) :
		fixedUpperbound{INT_MAX},
		goal{nullptr},
		immediateSuccessorList{},
		abstract_pathfinding_astar<H, E, STATS>{heuristic, expander, stats, upperBoundOptimization, earlyStopOptimization, enableStatistics, hScale} {
		//abstract_pathfinding_astar<H, E, STATS>{heuristic, expander, stats, upperBoundOptimization, earlyStopOptimization, enableStatistics, enableBoundSearch, bound, hScale, useAnyBound, anyTimeAcceptedDelay} {
	}

	virtual ~dynamic_pathfinding_astar() {

	}

	/**
	 * set the new upperbound each expanded node should be less than
	 *
	 * nodes whose cost are higher than that are automatically pruned away
	 *
	 * @param[in] upperbound the new upperbound
	 */
	inline void set_fixed_upperbound(dpf::cost_t upperbound) {
		fixedUpperbound = upperbound;
	}

	/**
	 * @return the upperbound every expanded node should be less than
	 *
	 */
	inline int get_fixed_upperbound() const {
		return static_cast<int>((int)fixedUpperbound);
	}

	void resetImmediateSuccessors() {
		this->immediateSuccessorList.clear();
	}

	virtual bool canPruneNewGeneratedState(const warthog::search_node& childNode, const warthog::search_node& parent, dpf::cost_t childG, dpf::cost_t childH, const H& heuristic, const GridMapExpander& expander) {
		debug("checking if we can prune away the state", childNode.get_id(), " with g=", childG, "and h=", childH);
		return this->fixedUpperbound < (childG + childH);
	}

	virtual bool canEarlyStopAStar(const warthog::search_node& node, dpf::cost_t nodeG, dpf::cost_t nodeH, const H& heuristic, const GridMapExpander& expander) {
		return heuristic.isCPDPathClearFromArcModifications(expander.get_map());
	}

	//TODO remove
	// virtual void setup(const warthog::search_node* start, dpf::nodeid_t goalid) {
	// 	// if (this->useAnyBound) {
	// 	// 	dpf::cost_t solutionCost = this->heuristic_->getCPDPathCostInMap(this->expander_->get_map(), start->get_id(), goalid);
	// 	// 	this->baseSolutionCost = solutionCost;
	// 	// }
	// }

	virtual bool isNodeRepresentingAValidSolution(const warthog::search_node* n, dpf::nodeid_t goalid) {
		return true;
	}

	// virtual bool hasNodeImprovedCurrentSolution(const warthog::search_node* n, dpf::nodeid_t goalid, bool isOldSolutionPresent, dpf::cost_t oldSolutionCost) {
	// 	dpf::cost_t solutionCost = n->get_g() + this->heuristic_->getCPDPathCostInMap(this->expander_->get_map(), n->get_id(), goalid);
	// 	if (isOldSolutionPresent) {
	// 		return solutionCost < oldSolutionCost;
	// 	} else {
	// 		//this is the first VALID solution ever
	// 		return true;
	// 	}
	// }

	//TODO REMOVE
	// virtual bool getGoalIfWithinBound(const warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startSolution, dpf::cost_t bound, warthog::search_node*& goalFound) {
	// 	//we check the bound. The generated solutation may or may not be the actual goal.
	// 	//so we need to generate the immediate successors of the node
	// 	debug("BOUND condition: ");
	// 	debug("granualrity:", this->BOUND_GRANULARITY);
	// 	dpf::cost_t solutionCost = n->get_g() + this->heuristic_->getCPDPathCostInMap(this->expander_->get_map(), n->get_id(), goalid);
	// 	debug("actual solution of node:", solutionCost);
	// 	debug("f(n): ", n->get_f());
	// 	debug("bound: ", this->bound);
	// 	debug("check if ", this->BOUND_GRANULARITY * solutionCost, ", <= ", n->get_f() * (this->BOUND_GRANULARITY + this->bound));
	// 	if (this->BOUND_GRANULARITY * solutionCost <= n->get_f() * (this->BOUND_GRANULARITY + this->bound)) {
	// 		debug("the node we have found is within the bound! Returning solution!");
	// 		//quickly generate the goal
	// 		goalFound = &this->generateImmediateSuccessors(*n, this->instance);
	// 		return true;
	// 	}	
	// 	return false;
	// }

	warthog::search_node& generateSuccessorByFollowingHeuristic(const warthog::search_node& node, const warthog::problem_instance& instance) {
		void* cpdHelper = abstract_pathfinding_astar<H, GridMapExpander, STATS>::heuristic_->getCpdHelper();
		const Mapper& mapper = abstract_pathfinding_astar<H, GridMapExpander, STATS>::heuristic_->getMapper();
		dpf::cost_t originalPathCost;
		dpf::cost_t actualPathCost;
		std::vector<dpf::move_t> moves;
		//generate optimal path from node till goal
		GetPathDataCostAndMovesWithCPD(cpdHelper, mapper(node.get_id()), mapper(const_cast<warthog::problem_instance&>(instance).get_goal()), originalPathCost, moves);


		warthog::search_node& result =  this->expandPath(moves, node, actualPathCost);
		assert(const_cast<warthog::problem_instance&>(instance).get_goal() == result.get_id());

		//we set data of the result
		result.set_f(static_cast<warthog::cost_t>(node.get_g() + actualPathCost));
		result.set_g(static_cast<warthog::cost_t>(node.get_g() + actualPathCost));
		debug("the new node has f and g =", result.get_g());

		return result;
	}

	warthog::search_node& generateImmediateSuccessors(const warthog::search_node& node, const warthog::problem_instance& instance) {
		void* cpdHelper = abstract_pathfinding_astar<H, GridMapExpander, STATS>::heuristic_->getCpdHelper();
		const Mapper& mapper = abstract_pathfinding_astar<H, GridMapExpander, STATS>::heuristic_->getMapper();
		dpf::cost_t pathCost;
		std::vector<dpf::move_t> moves;
		//generate optimal path from node till goal
		debug("computing optimal moves from node", node.get_id());
		GetPathDataCostAndMovesWithCPD(cpdHelper, mapper(node.get_id()), mapper(const_cast<warthog::problem_instance&>(instance).get_goal()), pathCost, moves);

	//	std::vector<dpf::move_t> moves = heuristic_->getOptimalPath(
	//			this->heuristic_->getMapper()(node.get_id()),
	//			this->heuristic_->getMapper()(static_cast<int>(const_cast<warthog::problem_instance&>(instance).get_goal())) //TODO get_goal should be marked as const...
	//	);
	//	debug("moves is ", moves);


		warthog::search_node& result =  this->expandPath(moves, node, pathCost);
		assert(const_cast<warthog::problem_instance&>(instance).get_goal() == result.get_id());

		//we set data of the result
		result.set_f(static_cast<warthog::cost_t>(node.get_g() + pathCost));
		result.set_g(static_cast<warthog::cost_t>(node.get_g() + pathCost));
		debug("the new goal has f and g =", result.get_g());

		return result;
	}

	PPMImage printExpandedNodesImage(const Mapper& mapper, const GridMap& map, dpf::nodeid_t startNode, dpf::nodeid_t goalNode, const std::vector<dpf::nodeid_t>& path, const PPMImage* imageToUpdate) const {

		const AdjGraph& newMap = this->expander_->get_map();
		PPMImage result{newMap.getImageWith(mapper, map, startNode, goalNode, this->expandedNodes, DARK_YELLOW, GREEN, BLUE, RED, WHITE)};
		PPMImage tmp2{newMap.getImageWith(mapper, map, startNode, goalNode, this->immediateSuccessorList, YELLOW, GREEN, BLUE, RED, WHITE)};
		PPMImage tmp3{newMap.getImageWith(mapper, map, startNode, goalNode, path, PURPLE, GREEN, BLUE, RED, WHITE)};
		result += tmp2;
		result += tmp3;

		if (imageToUpdate != nullptr) {
			return result + *imageToUpdate;
		} else {
			return result;
		}

	}
protected:

	virtual void onOptimalGoalFound(warthog::search_node* start, warthog::search_node* goal, dpf::cost_t startH) = 0;

	virtual void onGoalFound(const warthog::search_node* start, const warthog::search_node* goal, bool& keepGoing) = 0;

    virtual bool shouldWePruneNode(const warthog::search_node* start, const warthog::search_node* n, dpf::nodeid_t goalid) = 0;

    virtual void onPrunedNode(const warthog::search_node* start, const warthog::search_node* n, dpf::nodeid_t goalid) = 0;

    virtual void onOpenListEmpty(const warthog::search_node* start, const warthog::search_node* goal, dpf::cost_t goalid) = 0;

	virtual void onBeforeComputingStartHeuristic(warthog::search_node* start, dpf::cost_t goalid) = 0;

    virtual void onBeforeStartSearching(const warthog::search_node* start, dpf::nodeid_t goalid) = 0;

    virtual void onExpandedNodeInClosedList(warthog::search_node* parent, warthog::search_node* child, dpf::cost_t cost_to_n) = 0;

    virtual void onBeginNodeExpanded(const warthog::search_node* parent, const warthog::search_node* child, dpf::cost_t cost_to_n, bool& should_skip) = 0;

	virtual void onValidNonGoalPeekedFromOpenList(warthog::search_node* n, warthog::search_node* start, warthog::search_node*& goal, dpf::nodeid_t goalid, bool& keepGoing) = 0;

	virtual void onValidNonLeadingToGoalPeekedFromOpenList(warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startH) = 0;

	//FIXME pure virtual!!!!!
	virtual void onEarlyStopOnSuccessor(warthog::search_node* parent, dpf::cost_t childG, dpf::cost_t childH, bool& skip) {
	}

	virtual warthog::search_node* search(dpf::nodeid_t startid, dpf::nodeid_t goalid) {

		// ****************** CLEAR METADATA *******************
		this->goal = nullptr;
		this->stats_->reset_nodes_generated();
		this->stats_->reset_nodes_expanded();
		this->stats_->reset_nodes_touched();
		this->stats_->reset_heuristic_time();
		
		if (this->enableStatistics) {
			this->expandedNodes.clear();
		}

		//**************** SETUP THE PROBLEM INSTANCE *************************
		abstract_pathfinding_astar<H,E,STATS>::searchid_ = 0;
		this->instance.set_goal(goalid);
		this->instance.set_start(startid);
		this->instance.set_searchid(abstract_pathfinding_astar<H,E,STATS>::searchid_);

		//TODO remove or refactor
		FILE* f = nullptr;
		DO_ON_DEBUG {
			f = fopen("AstarLog.csv", "w");
			fprintf(f, "NODE,G,H,W,F,NOTES\n");
			if (f == nullptr) {
				throw std::domain_error{"file is null!"};
			}
			critical("created file!!!");
		}

		//GENERATE THE START SEARCH NODE AND POPULATE F, G and H
		warthog::search_node* start = this->expander_->generate(startid);
		double heuristicTime;
		dpf::cost_t startH;

		this->onBeforeComputingStartHeuristic(start, goalid);

		PROFILE_TIME_CODE(heuristicTime) {
			startH = this->heuristic_->h(startid, goalid);
		}
		this->stats_->add_to_heuristic_time(*start, heuristicTime);
		start->reset(this->instance.get_searchid());
		start->set_g(0);
		((DynamicSearchNode*)start)->set_h(startH);
		start->set_f(static_cast<warthog::cost_t>(ceil((float)(startH * this->hscale_))));
		
		//perofrm specific setup code after the start node has been added to the open list
		//this->setup(start, goalid);
		this->onBeforeStartSearching(start, goalid);

		//WE CHECK IF WE CAN EARLY STOP
		if (this->enableOptimizationEarlyStop && this->canEarlyStopAStar(*start, 0, startH, *this->heuristic_, *this->expander_)) {
			info("early stop when computing initial state!");
			//quickly generate the goal
			this->goal = &this->generateImmediateSuccessors(*start, this->instance);
			this->onOptimalGoalFound(start, this->goal, startH);
			goto goal_found;
		}

		//PUT IT IN THE OPEN LIST
		this->open_->push(start);

		info("starting the search by looking open state");
		while(this->open_->size()) {

			warthog::search_node* incumbent = this->open_->peek();

			info("***************** NEW ITERATION *******************");
			info("id = ", incumbent->get_id(), "g = ", incumbent->get_g(), "h = ", ((DynamicSearchNode*)incumbent)->get_h(), "f = ", incumbent->get_f());
			this->stats_->add_node_touched(*incumbent);

			if (this->shouldWePruneNode(start, incumbent, goalid)) {
                info("pruning state ", incumbent->get_id(), "with g", incumbent->get_g(), "h=", ((DynamicSearchNode*)incumbent)->get_h());
                this->open_->pop();
                this->onPrunedNode(start, incumbent, goalid);

			DO_ON_DEBUG {
				fprintf(f, "%010u,%010u,%010u,%10.2f,%010u,%s\n",
					incumbent->get_id(),
					incumbent->get_g(),
					static_cast<unsigned int>(((DynamicSearchNode*)incumbent)->get_h()),
					this->hscale_,
					incumbent->get_f(),
					"popped but pruned"
				);
			}

                continue;
            }

			// *********************** IS THE NODE A GOAL? ***************************
			if (incumbent->get_id() == goalid) {
				//GOAL FOUND
				this->goal = incumbent;

				DO_ON_DEBUG {
					for(warthog::search_node* cur = this->goal; cur != nullptr;	cur = cur->get_parent()) {
						info("GOAL PATH GOAL FOUND", cur->get_id());
					}
				}

				DO_ON_DEBUG {
					fprintf(f, "%010u,%010u,%010u,%10.2f,%010u,%s\n",
						incumbent->get_id(),
						incumbent->get_g(),
						static_cast<unsigned int>(((DynamicSearchNode*)incumbent)->get_h()),
						this->hscale_,
						incumbent->get_f(),
						"popped and goal"
					);
				}

				bool keepGoing = false;
				this->onGoalFound(start, incumbent, keepGoing);
				if (!keepGoing) {
                    debug("we have decided to stop looking for better solutions");
                    goto goal_found;
                } else {
					//we mark it as expanded, otherwise it we happen to rencouter this node during the search
					//we may revise it value mistakenly!!!!
					this->goal->set_expanded(true);
					this->open_->pop();
					continue;
				}

				// if (this->useAnyBound && this->isNodeRepresentingAValidSolution(start, goalid)) {
				// 	auto us = anyTimeTimer.EndTimer();
				// 	debug("stop timer! we reached becausewe found the goal while searching!", incumbent->get_id(), start->get_id());
				// 	this->anyTimeInfos.push_back(this->getAnyTimeInfos(incumbent, goalid, startH, us, true, goal));
				// }
			}

			bool keepGoing = true;
			this->onValidNonGoalPeekedFromOpenList(incumbent, start, this->goal, goalid, keepGoing);
			if (!keepGoing) {
				goto goal_found;
			}
			
			// // ****************** check if we havce at least satisfied the bound ********************
			// if (this->enableBoundSearch) {
			// 	if (this->getGoalIfWithinBound(incumbent, goalid, start->get_f(), this->bound, goal)) {
			// 		goto goal_found;
			// 	}
			// }

			// ********************* FETCH THE NODE WITH MINIMUM COST RIGHT NOW *************************
			DynamicSearchNode* current = static_cast<DynamicSearchNode*>(incumbent);
			if (this->enableStatistics) {
				this->expandedNodes.push_back(current->get_id());
			}
			debug("fetched from the open list the node", current->get_id());

			// *********************** IS THE NODE AN EARLY STOP? ******************************
			dpf::cost_t hval = ((DynamicSearchNode*)incumbent)->get_h();
			if (this->enableOptimizationEarlyStop && current->is_leadToOptimalSolution()) {
				info("early stop when computing incumbent state", incumbent->get_id());
				//quickly generate the goal 
				this->goal = &this->generateImmediateSuccessors(*current, this->instance);
				current->set_expanded(true); //in this way for anytime variant we won't pick this node from a worse parent

				DO_ON_DEBUG {
					for(warthog::search_node* cur = this->goal; cur != nullptr;	cur = cur->get_parent()) {
						info("GOAL PATH ON EARLY STATE", cur->get_id());
					}
				}

				DO_ON_DEBUG {
					fprintf(f, "%010u,%010u,%010u,%10.2f,%010u,%s\n",
						current->get_id(),
						current->get_g(),
						static_cast<unsigned int>(((DynamicSearchNode*)incumbent)->get_h()),
						this->hscale_,
						current->get_f(),
						"poppoed and early stop"
					);
				}

				bool keepGoing = false;
				this->onGoalFound(start, this->goal, keepGoing);
				if (!keepGoing) {
                    debug("we have decided to stop looking for better solutions");
                    goto goal_found;
                } else {
					//we mark it as expanded, otherwise it we happen to rencouter this node during the search
					//we may revise it value mistakenly!!!!
					this->goal->set_expanded(true);
					this->open_->pop();

					continue;
				}

				

				// if (this->useAnyBound && this->isNodeRepresentingAValidSolution(start, goalid)) {
				// 	auto us = anyTimeTimer.EndTimer();
				// 	debug("stop timer! we reached the goial because we found a node which can point directly to the solution!");
				// 	//we do not push back a new solution since it was already done before for the best of the open list
				// 	this->anyTimeInfos.push_back(this->getAnyTimeInfos(incumbent, goalid, startH, us, true, goal));
				// }

				// goto goal_found;
			}

			this->onValidNonLeadingToGoalPeekedFromOpenList(incumbent, goalid, startH);

			//if AnyTime is enabled, we check if the solution has at least improved from the last time
			// if (this->useAnyBound 
			// 	&& this->isNodeRepresentingAValidSolution(incumbent, goalid) 
			// 	&& this->hasNodeImprovedCurrentSolution(incumbent, goalid, !this->anyTimeInfos.empty(), this->anyTimeInfos.empty() ? 0 : this->anyTimeInfos.back().solutionCost)) {
			// 	auto us = anyTimeTimer.EndTimer();
			// 	debug("stop timer! we reached a node which improved the previous bound!", incumbent->get_id(), start->get_id());
			// 	this->anyTimeInfos.push_back(this->getAnyTimeInfos(incumbent, goalid, startH, us, false, goal));
			// 	//we restart the timer
			// 	anyTimeTimer.StartTimer();
			// }

			this->stats_->add_node_expanded(*current);
			this->open_->pop();
			info("we are going to expand node", current->get_id(), "f=", current->get_f(), "g=", current->get_g(), "h=", hval);

			DO_ON_DEBUG {
				fprintf(f, "%010u,%010u,%010u,%10.2f,%010u,%s\n",
					current->get_id(),
					current->get_g(),
					static_cast<unsigned int>(((DynamicSearchNode*)current)->get_h()),
					this->hscale_,
					current->get_f(),
					"popped but not goal"
				);
			}

			// GENERATE NODE SUCCESSORS
			debug("marking node ", current->get_id(), "as expanded");
			current->set_expanded(true); // NB: set this before calling expander_
			assert(current->get_expanded());
			this->expander_->expand(current, &this->instance);

			DynamicSearchNode* n = nullptr;
			dpf::cost_t cost_to_n = dpf::cost_t::getUpperBound();
			bool should_skip = false;

			//HANDLE NODE SUCCESSORS
			for(this->expander_->first(n, cost_to_n);	n != 0;	this->expander_->next(n, cost_to_n)) {
				this->stats_->add_node_touched(*n);

				this->onBeginNodeExpanded(current, n, cost_to_n, should_skip);
				if (should_skip) {
					//skip neighbours
					continue;
				}

				// if(n->get_expanded()) {
				// 	// skip neighbours already expanded
				// 	continue;
				// }

				info("---------------- NEW EXPANSION", n->get_id(), " IN ITERATION OF PARENT ", current->get_id(), "-----------------");

				if(this->open_->contains(n))	{
					// update a node from the fringe
					dpf::cost_t gval = current->get_g() + cost_to_n;
					if(gval < n->get_g()) {
						//we have reached a node in the fringe via a more direct path in the parent. Update priority of node
						n->relax(static_cast<warthog::cost_t>(gval), current);
						this->open_->decrease_key(n);

						DO_ON_DEBUG {
							fprintf(f, "%010u,%010u,%010u,%10.2f,%010u,%s\n",
								n->get_id(),
								n->get_g(),
								static_cast<unsigned int>(n->get_h()),
								this->hscale_,
								n->get_f(),
								"revised children f in open"
							);
						}
					}
				} else if (n->get_expanded()) {
					//the node is in the closed list because it has already been expanded
					this->onExpandedNodeInClosedList(current, n, cost_to_n);
				} else {
					// add a new node to the fringe (it wasn't inside the open list)
					dpf::cost_t gval = current->get_g() + cost_to_n;
					dpf::cost_t hval;
					PROFILE_TIME_CODE(heuristicTime) {
						hval = this->heuristic_->h(n->get_id(), goalid);
					}
					this->stats_->add_to_heuristic_time(*n, heuristicTime);

					if (this->enableOptimizationUpperbound && this->canPruneNewGeneratedState(*n, *current, gval, hval, *this->heuristic_, *this->expander_)) {
						info("the state", n->get_id(), "can be pruned away!");
						//we mark it expanded: in this way if we happen to retrieve it, we won't bother by generating new memory space for it
						n->set_expanded(true);
						continue;
					}

					/*
					 * i don't put the early stop here because when we expand the children of a state, it is not garantueed that they are
					 * ordered by costs. So, if a children has an eraly stop, we are not garantueed to have the optimum one, only a feasible
					 * solution.
					 */
					//WE CHECK IF WE CAN EARLY STOP
					if (this->enableOptimizationEarlyStop && this->canEarlyStopAStar(*current, gval, hval, *this->heuristic_, *this->expander_)) {
						info("early stop when computing child state", n->get_id());
						n->set_leadToOptimalSolution(true);

						bool skip = false;
						this->onEarlyStopOnSuccessor(current, gval, hval, skip);
						if (skip) {
							continue;
						}
					}

					

					//we have generated a node but we don't add it in the open list
					this->stats_->add_node_generated(*n);

					n->set_h(hval);
					n->set_g(static_cast<warthog::cost_t>(gval));
					n->set_f(static_cast<warthog::cost_t>(ceil((float)(gval + ((float)hval) * this->hscale_))));
					n->set_parent(current);

					DO_ON_DEBUG {
						fprintf(f, "%010u,%010u,%010u,%10.2f,%010u,%s\n",
							n->get_id(),
							n->get_g(),
							static_cast<unsigned int>(n->get_h()),
							this->hscale_,
							n->get_f(),
							"children push to open"
						);
					}

					info("Add in Open list the children of node", current->get_id(), ", aka node ", n->get_id(), "f=", n->get_f(), "g=", gval, "h=", hval, "w=", this->hscale_);
					this->open_->push(n);


				}
			}

//			cslog(verbose_)
//			(dpf::coo2d_t x = current->get_id() % expander_->mapwidth(), y = (current->get_id() / expander_->mapwidth()))
//			("closing (", x, ", ", y, ")...", n);

		}

		DO_ON_DEBUG {
			for(warthog::search_node* cur = this->goal; cur != nullptr;	cur = cur->get_parent()) {
				debug("GOAL BEFOR EOPEN LIST EMPTY", cur->get_id());
			}
		}

		//open list empty
        this->onOpenListEmpty(start, this->goal, goalid);

		goal_found:;

		DO_ON_DEBUG {
			for(warthog::search_node* cur = this->goal; cur != nullptr;	cur = cur->get_parent()) {
				debug("GOAL PATH FINAL", cur->get_id());
			}
		}

		DO_ON_DEBUG {
			if (f != nullptr) {
				fclose(f);
			}
		}

		return this->goal;
	}
private:
	warthog::search_node& expandPath(const std::vector<dpf::move_t>& moves, const warthog::search_node& node, dpf::cost_t& pathCost) {
		pathCost = 0;
		warthog::search_node* parent = const_cast<warthog::search_node*>(&node);
		warthog::search_node* n = parent;
		const Mapper& mapper = abstract_pathfinding_astar<H, GridMapExpander, STATS>::heuristic_->getMapper();
		debug("moves is ", moves);
		for (auto it= moves.begin(); it != moves.end(); ++it) {
			/* In this path we can have a location whose search_node has already been generated, but wecan't have search_node already exapnded!
			 * For example: we start from "S" and we need to reach "G". "." cell are unamed. Suppose that the arc A->B has been modifieid an now
			 * its weight is large. We first expanded "A" and generated its successors (one of them being B). Since the arc between A and B weights
			 * a lot, we decided to expand D instead. There the optimal path goes to G via "B".
			 * This is an exmaple on how the optimal path can go to already GENERATED nodes, but not through EXPANDED nodes
			 *
			 * SABCG
			 * ..D..
			 *
			 * The below assertion checks exactly that
			 */

			int w = this->expander_->get_map().getIthOutArc(parent->get_id(),  *it).target;
			int weight = this->expander_->get_map().getIthOutArc(parent->get_id(),  *it).weight;
			pathCost += weight;
			debug("checking if cell ", w, "(", mapper(w), ")", "has already been generated...");

			//TODO reenter... it is a nice assertion! assert(canBeOnOptimalPath(w));

			n = this->expander_->generate(w);
			if (this->enableStatistics) {
				debug("added ", n->get_id(), "in expanded nodes");
				this->immediateSuccessorList.push_back(n->get_id());

			}
			assert(n->get_id() == w);
			n->set_parent(parent);
			debug("parent of node", n->get_id(), "is", n->get_parent()->get_id());
			parent = n;
		}

		debug("path generated! n=", n, "parent of n is", n->get_parent()->get_id());

		return *n;
	}


//	/**
//	 * clean up the internal resources from one search to another one
//	 */
//	virtual void cleanup() {
//		abstract_pathfinding_astar<H, GridMapExpander>::open_->clear();
//		abstract_pathfinding_astar<H, GridMapExpander>::expander_->clear();
//	}
protected:
	warthog::search_node* goal;
	/**
	 * the global upperbound of this search.
	 *
	 * If a node we're evaluating is evaluated with a value that is bigger than this one, it will
	 * automaitcally be pruned away. This because we have already a solution whose cost is this upperbound;
	 * hence any other solution with higer cost can be ignored.
	 */
	dpf::cost_t fixedUpperbound;
	/**
	 * A set of nodes generated during generateImmediateSuccessors routine.
	 *
	 * We use a different vector for such generated nodes because in this way we can determine which node was generated during A* and which one has been
	 * generated during the immediate successor procedure
	 *
	 * Used only if enableStatistics is set to true
	 */
	std::vector<dpf::nodeid_t> immediateSuccessorList;


};


#endif /* DYNAMIC_PATHFINDING_ASTAR_H_ */
