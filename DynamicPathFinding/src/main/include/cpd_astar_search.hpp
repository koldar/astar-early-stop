#ifndef _CDP_SEARCH_HEADER__
#define _CDP_SEARCH_HEADER__

#include "dynamic_pathfinding_astar.h"
#include <stdexcept>

/**
 * @brief optimal A* with our stop!
 * 
 * @tparam H 
 * @tparam STATS 
 */
template <typename H, typename E, typename STATS>
class CPDAStarSearch : public dynamic_pathfinding_astar<H, E, STATS>{
public:
    CPDAStarSearch(H* heuristic, E* expander, STATS* stats, bool upperBoundOptimization, bool earlyStopOptimization, bool enableStatistics) :
        dynamic_pathfinding_astar<H, E, STATS>(heuristic, expander, stats, upperBoundOptimization, earlyStopOptimization, enableStatistics, 1.0) {
	}

    ~CPDAStarSearch() {
        
    }
protected:

    virtual void onOptimalGoalFound(warthog::search_node* start, warthog::search_node* goal, dpf::cost_t startH) {

    }

	virtual void onGoalFound(const warthog::search_node* start, const warthog::search_node* goal, bool& keepGoing) {
        keepGoing = false;
    }

    virtual bool shouldWePruneNode(const warthog::search_node* start, const warthog::search_node* n, dpf::nodeid_t goalid) {
        return false;        
    }

    virtual void onPrunedNode(const warthog::search_node* start, const warthog::search_node* n, dpf::nodeid_t goalid) {

    }

    virtual void onValidNonGoalPeekedFromOpenList(warthog::search_node* n, warthog::search_node* start, warthog::search_node*& goal, dpf::nodeid_t goalid, bool& keepGoing) {

    }

    
    virtual void onValidNonLeadingToGoalPeekedFromOpenList(warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startH) {

    }        

    virtual void onOpenListEmpty(const warthog::search_node* start, const warthog::search_node* goal, dpf::cost_t goalid) {

    }

    virtual void onBeforeComputingStartHeuristic(warthog::search_node* start, dpf::cost_t goalid) {

    }

    virtual void onBeforeStartSearching(const warthog::search_node* start, dpf::nodeid_t goalid) {

    }

    virtual void onExpandedNodeInClosedList(warthog::search_node* parent, warthog::search_node* child, dpf::cost_t cost_to_n) {
        throw std::domain_error{"we're expanding a node which was in closed list! Impossible for A*!"};
    }

    virtual void onBeginNodeExpanded(const warthog::search_node* parent, const warthog::search_node* child, dpf::cost_t cost_to_n, bool& should_skip) {
        should_skip = child->get_expanded();
    }
};

#endif