#ifndef _BOUND_CDP_ASTAR_SEARCH_HEADER__
#define _BOUND_CDP_ASTAR_SEARCH_HEADER__

#include "dynamic_pathfinding_astar.h"

template <typename H, typename E, typename STATS>
class CPDBoundAStarSearch : public dynamic_pathfinding_astar<H, E, STATS>{
public:
    CPDBoundAStarSearch(H* heuristic, E* expander, STATS* stats, bool upperBoundOptimization, bool earlyStopOptimization, bool enableStatistics, double bound) :
        dynamic_pathfinding_astar<H, E, STATS>(heuristic, expander, stats, upperBoundOptimization, earlyStopOptimization, enableStatistics, 1.0),
        bound{static_cast<dpf::cost_t>(abstract_pathfinding_astar<H,E,STATS>::BOUND_GRANULARITY * bound)} {
	}

    ~CPDBoundAStarSearch() {

    }
protected:
    dpf::cost_t bound;
private:
    /**
	 * @brief check if the current node has a solution which is within a given bound
	 * 
	 * @param n the node to check
	 * @param goalid the id of the goal
	 * @param startSolution the cost of the relaxed solution. This can be interpreted as a lower bound of the optimal solution
	 * @param bound the bound
	 * @param goalFound the id of the goal we have found (if we have found any of those). 
	 * 	The value contains noise if the function returns false.
	 *  Otherwise it contains a well specific node whose parent represents the path to follow
	 * @return bool true if we have reached a goal in compliance with the bound. False otherwise
	 */
    virtual bool getGoalIfWithinBound(const warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startSolution, dpf::cost_t bound, warthog::search_node*& goalFound) {
		//we check the bound. The generated solutation may or may not be the actual goal.
		//so we need to generate the immediate successors of the node
		debug("BOUND condition: ");
		debug("granualrity:", this->BOUND_GRANULARITY);
		dpf::cost_t solutionCost = n->get_g() + this->heuristic_->getCPDPathCostInMap(this->expander_->get_map(), n->get_id(), goalid);
		debug("actual solution of node:", solutionCost);
		debug("f(n): ", n->get_f());
		debug("bound: ", this->bound);
		debug("check if ", this->BOUND_GRANULARITY * solutionCost, ", <= ", n->get_f() * (this->BOUND_GRANULARITY + this->bound));
		if (this->BOUND_GRANULARITY * solutionCost <= n->get_f() * (this->BOUND_GRANULARITY + this->bound)) {
			debug("the node we have found is within the bound! Returning solution!");
			//quickly generate the goal
			goalFound = &this->generateImmediateSuccessors(*n, this->instance);
			return true;
		}	
		return false;
	}
protected:

    virtual void onOptimalGoalFound(warthog::search_node* start, warthog::search_node* goal, dpf::cost_t startH) {

    }

	virtual void onGoalFound(const warthog::search_node* start, const warthog::search_node* goal, bool& keepGoing) {
        keepGoing = false;
    }

    virtual bool shouldWePruneNode(const warthog::search_node* start, const warthog::search_node* n, dpf::nodeid_t goalid) {
        return false;
    }

    virtual void onPrunedNode(const warthog::search_node* start, const warthog::search_node* n, dpf::nodeid_t goalid) {

    }

    virtual void onOpenListEmpty(const warthog::search_node* start, const warthog::search_node* goal, dpf::cost_t goalid) {

    }

	virtual void onBeforeComputingStartHeuristic(warthog::search_node* start, dpf::cost_t goalid) {
        
    }

    virtual void onBeforeStartSearching(const warthog::search_node* start, dpf::nodeid_t goalid) {

    }

    virtual void onExpandedNodeInClosedList(warthog::search_node* parent, warthog::search_node* child, dpf::cost_t cost_to_n) {
        throw std::domain_error{"It's impossible to expand a node in closed list in consistent A*!"};
    }

    virtual void onBeginNodeExpanded(const warthog::search_node* parent, const warthog::search_node* child, dpf::cost_t cost_to_n, bool& should_skip) {
        should_skip = child->get_expanded();
    }

	virtual void onValidNonGoalPeekedFromOpenList(warthog::search_node* n, warthog::search_node* start, warthog::search_node*& goal, dpf::nodeid_t goalid, bool& keepGoing) {
        if (this->getGoalIfWithinBound(n, goalid, start->get_f(), this->bound, goal)) {
            keepGoing = false;
        }
    }

	virtual void onValidNonLeadingToGoalPeekedFromOpenList(warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startH) {

    }
};

#endif