/*
 * dijkstra_algorithm.h
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#ifndef DIJKSTRA_ALGORITHM_H_
#define DIJKSTRA_ALGORITHM_H_

#include "adj_graph.h"
#include "heap.h"

/**
 * The classic dijkstra algorithm. No modification whatsoever
 */
class DijsktraAlgorithm {
public:
	DijsktraAlgorithm(const AdjGraph& g);
	virtual ~DijsktraAlgorithm();
	bool run(dpf::nodeid_t source);
	const std::vector<dpf::cost_t>& getDistances() const;
	void reset();
private:
	void reach(dpf::nodeid_t v, dpf::cost_t d, dpf::nodeid_t bestPrevious);
private:
	const AdjGraph& g;
	std::vector<dpf::cost_t> distances;
	min_id_heap<dpf::cost_t> q;
};


#endif /* DIJKSTRA_ALGORITHM_H_ */
