#ifndef _IMAP_HPP__
#define _IMAP_HPP__

#include <string>
#include "adj_graph.h"

/**
 * @brief enumeration representing all the types of maps avaialable
 * 
 */
enum class MapType {
    ///the map is a grid map
    GRIDMAP
};

/**
 * @brief an abstract class representing every possible map.
 * 
 * TODO This interface should be use in the future as base class for GridMap
 * 
 */
class IMap {

    /**
     * @brief the name of the map
     * 
     * @return const std::string&  the name of the map
     */
    virtual const std::string& getname() const = 0;

    /**
     * @brief the of the map
     * 
     * allowed returning values are
     * 
     * 
     * @return int 
     */
    virtual MapType getKind() const = 0;

    /**
     * @brief assign this map to another map
     * 
     * After this operation this map will be the same of other
     * 
     * @pre
     *  @li both this map and the other must have the same concrete implementation
     * 
     * @param other the other map
     * @return IMap& 
     */
    virtual IMap& operator =(const IMap& other) = 0;

    /**
     * @brief get the underlying adjacent graph representing the map
     * 
     * @return AdjGraph 
     */
	virtual AdjGraph getAdjGraph() const = 0;
};

#endif