#ifndef _ABSTRACT_WEIGHT_ASTAR_HEADER__
#define _ABSTRACT_WEIGHT_ASTAR_HEADER__

#include "abstract_pathfinding_astar.h"
#include <cmath>

/**
 * @brief A basic A* implementation
 * 
 * This implementation does NOT feature:
 *  - pruning;
 *  - early stop;
 *  - bounded search;
 * 
 * @tparam H 
 * @tparam E 
 * @tparam STATS 
 */
template <typename H, typename E, typename STATS>
class AbstractWeightPathFindingAStar : public abstract_pathfinding_astar<H, E, STATS> {
public:
	AbstractWeightPathFindingAStar(H* heuristic, E* expander, STATS* stats, bool enableStatistics, double hscale) : 
        abstract_pathfinding_astar<H, E, STATS>{heuristic, expander, stats, false, false, enableStatistics, hscale} {
	}
	
	virtual ~AbstractWeightPathFindingAStar() {

	}
public:

	//STANDARD ALGORITHM

	virtual bool canPruneNewGeneratedState(const warthog::search_node& childNode, const warthog::search_node& parent, dpf::cost_t childG, dpf::cost_t childH, const H& heuristic, const E& expander) {
		return false;
	}

	virtual bool canEarlyStopAStar(const warthog::search_node& node, dpf::cost_t nodeG, dpf::cost_t nodeH, const H& heuristic, const E& expander) {
		return false;
	}

	virtual warthog::search_node& generateImmediateSuccessors(const warthog::search_node& node, const warthog::problem_instance& instance) {
		throw CpdException{"this should never happen! @ %s", __func__};
	}

	//BOUNDED
    //TODO remove
	// virtual bool getGoalIfWithinBound(const warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startSolution, dpf::cost_t bound, warthog::search_node*& goalFoundId) {
	// 	return false;
	// }

	// ANYTIME

	virtual AnyTimeInfo getAnyTimeInfos(const warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startSolution , double microSeconds, bool isOptimalSolutionFound, const warthog::search_node* goal) {
		throw CpdException{"this should never happen! @ %s", __func__};
	}

	virtual bool hasNodeImprovedCurrentSolution(const warthog::search_node* n, dpf::nodeid_t goalid, bool isOldSolutionPresent, dpf::cost_t oldSolutionCost) {
		throw CpdException{"this should never happen! @ %s", __func__};
	}

	virtual bool isNodeRepresentingAValidSolution(const warthog::search_node* n, dpf::nodeid_t goalid) {
		throw CpdException{"this should never happen! @ %s", __func__};
	}

    //todo REMOVE   
	// virtual void setup(const warthog::search_node* n, dpf::nodeid_t goalid) {
	// }

	//TODO this is a copy of dynamic_pathfinding_astar. We should create a class containing only this method and then use
	//multi inheritance of C++ to make simplePathFindingAstar and dynamic_pathfinding astar inherit this method
	PPMImage printExpandedNodesImage(const Mapper& mapper, const GridMap& map, dpf::nodeid_t startNode, dpf::nodeid_t goalNode, const std::vector<dpf::nodeid_t>& path, const PPMImage* imageToUpdate) const {

		const AdjGraph& newMap = this->expander_->get_map();
		PPMImage result{newMap.getImageWith(mapper, map, startNode, goalNode, this->expandedNodes, DARK_YELLOW, GREEN, BLUE, RED, WHITE)};
		PPMImage tmp{newMap.getImageWith(mapper, map, startNode, goalNode, path, PURPLE, GREEN, BLUE, WHITE)};
		result += tmp;
		if (imageToUpdate != nullptr) {
			result += *imageToUpdate;
		}
		return result;

	}

protected:

    virtual void onGoalFound(const warthog::search_node* start, const warthog::search_node* goal, warthog::pqueue* openList, bool& keepGoing) = 0;

    virtual bool shouldWePruneNode(const warthog::search_node* start, const warthog::search_node* n, warthog::pqueue* openList, dpf::nodeid_t goalid) = 0;

    virtual void onPrunedNode(const warthog::search_node* start, const warthog::search_node* n, warthog::pqueue* openList, dpf::nodeid_t goalid) = 0;

    virtual void onOpenListEmpty(const warthog::search_node* start, const warthog::search_node* goal, dpf::cost_t goalid) = 0;

    virtual void onBeforeStartSearching(const warthog::search_node* start, dpf::nodeid_t goalid) = 0;

    virtual void onExpandedNodeInClosedList(warthog::search_node* parent, warthog::search_node* child, dpf::cost_t cost_to_n) = 0;

    virtual void onBeginNodeExpanded(const warthog::search_node* parent, const warthog::search_node* child, dpf::cost_t cost_to_n, bool& should_skip) = 0;

    virtual warthog::search_node* search(dpf::nodeid_t startid, dpf::nodeid_t goalid) {
        this->stats_->reset_nodes_generated();
        this->stats_->reset_nodes_expanded();
        this->stats_->reset_nodes_touched();
        this->stats_->reset_heuristic_time();
        
        if (this->enableStatistics) {
            this->expandedNodes.clear();
        }

        //**************** SETUP THE PROBLEM INSTANCE *************************
        this->instance.set_goal(goalid);
        this->instance.set_start(startid);
        this->instance.set_searchid(abstract_pathfinding_astar<H, E, STATS>::searchid_++);

        //GENERATE THE START SEARCH NODE AND POPULATE F, G and H
        warthog::search_node* goal = nullptr;
        warthog::search_node* start = this->expander_->generate(startid);
        double heuristicTime;
        dpf::cost_t startH;
        PROFILE_TIME_CODE(heuristicTime) {
            startH = this->heuristic_->h(startid, goalid);
        }
        this->stats_->add_to_heuristic_time(*start, heuristicTime);
        start->reset(this->instance.get_searchid());
        start->set_g(0);
        ((DynamicSearchNode*)start)->set_h(startH);
        start->set_f(static_cast<warthog::cost_t>(ceil(((float)startH) * this->hscale_)));

        //perofrm specific setup code after the start node has been added to the open list
        this->onBeforeStartSearching(start, goalid);

        //PUT IT IN THE OPEN LIST
        this->open_->push(start);
        while(this->open_->size()) {

            // if (goal != nullptr) {
            //     debug("goal is ", goal->get_id(), "g=", goal->get_g());
            // }

            warthog::search_node* incumbent = this->open_->peek();
            debug("***************** NEW ITERATION ", incumbent->get_id(), "g=", incumbent->get_g(), " h= ", ((DynamicSearchNode*)incumbent)->get_h(), "*******************");
            
            this->stats_->add_node_touched(*incumbent);

            // ********************** check if we should prune it ************************************

            if (this->shouldWePruneNode(start, incumbent, this->open_, goalid)) {
                debug("pruning state ", incumbent->get_id(), "with g", incumbent->get_g(), "h=", ((DynamicSearchNode*)incumbent)->get_h());
                this->open_->pop();
                this->onPrunedNode(start, incumbent, this->open_, goalid);
                continue;
            }

            // *********************** check if we have reached the goal ***************************
            if (incumbent->get_id() == goalid) {
                //GOAL FOUND
                goal = incumbent;
                bool keepGoing = false;
                debug("goal found with f ", goal->get_g() + ((DynamicSearchNode*)incumbent)->get_h());
                this->onGoalFound(start, incumbent, this->open_, keepGoing);
                if (!keepGoing) {
                    debug("we have decided to stop looking for better solutions");
                    goto goal_found;
                }
                //we mark it as expanded, otherwise it we happen to rencouter this node during the search
                //we may revise it value mistakenly!!!!
                goal->set_expanded(true);
                this->open_->pop();
                continue;
            }

            // ********************* FETCH THE NODE WITH MINIMUM COST RIGHT NOW *************************
            DynamicSearchNode* current = static_cast<DynamicSearchNode*>(incumbent);
            if (this->enableStatistics) {
                this->expandedNodes.push_back(current->get_id());
            }

            this->stats_->add_node_expanded(*current);
            this->open_->pop();

            debug("we are going to expand node", current->get_id(), "f=", current->get_f(), "g=", current->get_g(), "h=", ((DynamicSearchNode*)current)->get_h());

            // GENERATE NODE SUCCESSORS
            debug("marking node ", current->get_id(), "as expanded");
            current->set_expanded(true); // NB: set this before calling expander_
            assert(current->get_expanded());
            this->expander_->expand(current, &this->instance);

            DynamicSearchNode* n = 0;
            dpf::cost_t cost_to_n = dpf::cost_t::getUpperBound();
            bool should_skip = false;

            //HANDLE NODE SUCCESSORS
            for(this->expander_->first(n, cost_to_n);	n != 0;	this->expander_->next(n, cost_to_n)) {
                this->stats_->add_node_touched(*n);

                this->onBeginNodeExpanded(current, n, cost_to_n, should_skip);
                if (should_skip) {
                    //skip neighbours
                    continue;
                }

                debug("---------------- NEW EXPANSION", n->get_id(), " IN ITERATION OF PARENT ", current->get_id(), "-----------------");

                if(this->open_->contains(n))	{
                    // the successor is actuyally a node already in the open list!
                    //update a node from the fringe
                    dpf::cost_t gval = current->get_g() + cost_to_n;
                    if(gval < n->get_g()) {
                        //we have reached a node in the fringe via a more direct path in the parent. Update priority of node
                        n->relax(static_cast<warthog::cost_t>(gval), current);
                        this->open_->decrease_key(n);
                        debug("reached a node in open", n->get_id(), "which could be reached faster than before (now it will be ", n->get_g() + cost_to_n, ")!");
                    }
                } else if (n->get_expanded()) {
                    //the node is in the closed list because it has already been expanded
                    this->onExpandedNodeInClosedList(current, n, cost_to_n);
                } else {
                    // add a new node to the fringe (it wasn't inside the open list)
                    debug("generting g and h of a new child... g_current=", current->get_g(), " cost to add ", cost_to_n);
                    dpf::cost_t gval = current->get_g() + cost_to_n;

                    dpf::cost_t hval;
                    PROFILE_TIME_CODE(heuristicTime) {
                        hval = this->heuristic_->h(n->get_id(), goalid);
                    }
                    this->stats_->add_to_heuristic_time(*n, heuristicTime);

                    //we have generated a node but we don't add it in the open list
                    this->stats_->add_node_generated(*n);

                    n->set_h(hval);
                    n->set_g(static_cast<warthog::cost_t>(gval));
                    n->set_f(static_cast<warthog::cost_t>(ceil((float)(gval + ((float)hval) * this->hscale_))));
                    n->set_parent(current);

                    debug("Add in Open list the children of node", current->get_id(), ", aka node ", n->get_id(), " f=", n->get_f(), "g=", gval, "h=", hval);
                    this->open_->push(n);
                }
            }

        }
        debug("the open list was empty. finishing...");
        //open list empty
        this->onOpenListEmpty(start, goal, goalid);

        goal_found:;
        return goal;
    }

};


#endif