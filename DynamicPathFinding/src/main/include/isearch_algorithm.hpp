#ifndef _ISEARCH_ALGORITHM_HEADER__
#define _ISEARCH_ALGORITHM_HEADER__

#include "types.h"
#include <warthog/search_node.h>

class ISearchAlgorithm {

    virtual warthog::search_node* search(dpf::nodeid_t startid, dpf::nodeid_t goalid) = 0;

};

#endif