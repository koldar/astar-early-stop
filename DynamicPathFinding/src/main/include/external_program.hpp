/*
 * external_program.hpp
 *
 *  Created on: Dec 12, 2018
 *      Author: koldar
 */

#ifndef EXTERNAL_PROGRAM_HPP_
#define EXTERNAL_PROGRAM_HPP_

#include <string>
#include "macros.h"
#include <sstream>
#include <exception>
#include <stdexcept>

class ExternalProgramException: public std::runtime_error {
public:

	ExternalProgramException(const std::string& cmd, int exitcode) : runtime_error{"Failed to successfully call extenral program"}, cmd{cmd}, exitcode{exitcode}
	{}

	virtual const char* what() const throw() {
		std::stringstream ss;
		ss << runtime_error::what() << ": code=" << exitcode << " " << cmd;
		return ss.str().c_str();
	}

	inline const std::string& program() const {
		return cmd;
	}

	inline const int exit_code() const {
		return exitcode;
	}

private:
	const std::string cmd;
	const int exitcode;
};


#define execute_command(...) (_execute_command(COUNT_ARGS(__VA_ARGS__), __VA_ARGS__))

/**
 * execute an external program
 *
 * @param[in] a variadic string which, when concatenated, outputs the command to execute
 * @return the exit code returne dby the external program
 */
int _execute_command(int size, ...);



#endif /* EXTERNAL_PROGRAM_HPP_ */
