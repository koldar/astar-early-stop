/**
 * @file
 *
 * a search node which contains a flag indicating if the node lead to an optimal solution without any map perturbation
 *s
 *  Created on: Oct 22, 2018
 *      Author: koldar
 */

#ifndef DYNAMIC_SEARCH_NODE_H_
#define DYNAMIC_SEARCH_NODE_H_


#include <warthog/search_node.h>
#include "types.h"

class DynamicSearchNode : public warthog::search_node {
	friend std::ostream& operator<< (std::ostream& stream, const DynamicSearchNode& sn);
public:
	DynamicSearchNode(dpf::nodeid_t id) : warthog::search_node{id}, leadToOptimalSolution{false} {

	}
	~DynamicSearchNode() {

	}
	inline void set_leadToOptimalSolution(bool leadToOptimal) {
		this->leadToOptimalSolution = leadToOptimal;
	}

	inline bool is_leadToOptimalSolution() const {
		return this->leadToOptimalSolution;
	}
	inline void set_h(dpf::cost_t h) {
		this->h = h;
	}

	inline dpf::cost_t get_h() {
		return this->h;
	}
private:
	bool leadToOptimalSolution;
	dpf::cost_t h;
};

std::ostream& operator<< (std::ostream& stream, const DynamicSearchNode& sn);

#endif /* DYNAMIC_SEARCH_NODE_H_ */
