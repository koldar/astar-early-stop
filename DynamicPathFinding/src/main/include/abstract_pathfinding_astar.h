/**
 * @file
 *
 * Represents an A\* implementation which has the following properties
 *
 * @li **fixed upperbound**: if a node which we're evaluating has a cost (g+h) greater than this fixed upper bound
 * 	we can safely prune it from the search because it is still worse than the current solution; concretely the given upperbound is the cost
 * 	of the optimal path in the new revised map. Any node whose optimal path (in the original map) is greater than this value
 * 	can't possibly be an optimal solution, hence they can be safely pruned away
 *
 * @li **early stop*: if a node which we're evaluating generates an heuristic (i.e., cost of the path from the starting location till the goal in the original map)
 * 	which has the same cost of the path in the revised map, then we stopped our search because the path we've just found is not
 * 	altered by any arc revised and, hence, is optimal.
 *
 *
 * This current implementation of A\* does not include a close list.
 *
 * @attention
 * Note that this A\* implementation is not domain-independet. We assume there is a square map domain!
 *
 * @date Oct 1, 2018
 * @author koldar
 */

#ifndef ABSTRACT_PATHFINDING_ASTAR_H_
#define ABSTRACT_PATHFINDING_ASTAR_H_

#include <warthog/cpool.h>
#include <warthog/pqueue.h>
#include <warthog/problem_instance.h>
#include <warthog/search_node.h>
#include <warthog/timer.h>
#include "dynamic_search_node.h"
#include "simpleTimer.h"

#include <iostream>
#include <memory>
#include <stack>
#include <vector>

#include "log.h"
#include "types.h"

/**
 * @brief contains all the information regarding a given solution found by the anytime variant of this algorithm
 * 
 */
class AnyTimeInfo {
	friend std::ostream& operator<< (std::ostream& stream, const AnyTimeInfo& matrix);
public:
	/**
	 * @brief the cost of the solution we generated.
	 * 
	 * this is basically the cost of the  actual path from the start to the goal
	 * 
	 */
	dpf::cost_t solutionCost;
	/**
	 * @brief  value g of A* algorithm
	 * 
	 */
	dpf::cost_t g;
	/**
	 * @brief value h of A* algorithm
	 * 
	 */
	dpf::cost_t h;
	/**
	 * @brief time needed for the algorithm to fetch this solution.
	 * 
	 * The starting time is the last time we generated the last solution
	 * 
	 */
	double microSeconds;
	/**
	 * @brief  true if the solution is optimal, false otherwise
	 * 
	 */
	bool isSolutionOptimal;
public:

	AnyTimeInfo(dpf::cost_t solutionCost, dpf::cost_t g, dpf::cost_t h, double microSeconds, bool isSolutionOptimal) : solutionCost{solutionCost}, g{g}, h{h}, microSeconds{microSeconds}, isSolutionOptimal{isSolutionOptimal} {

	}
};


/**
 * A class which can be used as template to generate heuristics complaint with abstract_pathfinding_astar
 */
class AbstractHeuristic {
public:
	/**
	 * Computes the heuristic
	 *
	 * @param[in] startid id of the warthog::search_node represent the current state
	 * @param[in] goalid id of the warthog::search_node represent the current state
	 * @return the heuristic value of the state @c startid. Nothing is said about its admissability nor its consistency
	 */
	virtual dpf::cost_t h(dpf::nodeid_t startid, dpf::nodeid_t goalid) = 0;
	/**
	 * Clean up heuristic metadata after a search has been completed (regardless of the outcome)
	 */
	virtual void clear() = 0;
};

/**
 * A class which can be used as template to generate expander complaint with abstract_pathfinding_astar
 */
template <typename SEARCH_NODE>
class AbstractExpander {
public:
	virtual int mapwidth() const = 0;

	/**
	 * @return
	 * 	the number of bytes occupied by this expander
	 */
	virtual size_t mem() const = 0;

	/**
	 * generate a new node with the given id
	 *
	 * If the node with said id already exists, we simply return the already generated node
	 *
	 * @param[in] startid the id the new node has
	 * @return a pointer to the created node
	 */
	virtual SEARCH_NODE* generate(dpf::nodeid_t startid) = 0;

	/**
	 * generate the successors of a node
	 *
	 * The implementation should node care to set f,g,h or parent of the successors
	 *
	 * @post
	 *  @li after the call, use AbstractExpander::first and AbstractExpander::next to fetch the successors
	 *
	 * @param[in] node the node whose successors we need to compute
	 * @param[in] instance a structure reprresenting which problem we want to solve
	 */
	virtual void expand(const SEARCH_NODE* node, warthog::problem_instance* instance) = 0;



	/**
	 * get the first successor of the parent
	 *
	 * @pre
	 *  @li AbstractExpander::expand called
	 *
	 * @param[out] n the node which is the successor of the node input in AbstractExpander::expand function. nullptr
	 * 	if the parent has no successors;
	 * @param[out] cost_to_n the cost you need to pay for going from node <tt>parent fo n</tt> to @c n
	 */
	virtual void first(SEARCH_NODE*& n, dpf::cost_t& cost_to_n) = 0;

	/**
	 * get the next successor of the parent
	 *
	 * @code
	 * 	expand(parent) //expand call
	 * 	first() //call for fetching the first successor of "parent"
	 * 	next() //0 or more calls for fetching the next successors of "parent"
	 * @endcode
	 *
	 * @pre
	 *  @li AbstractExpander::first called
	 *
	 * @param[out] n the node which is the next successor of the node input in AbstractExpander::expand function. 0
	 * 	if the parent has no successors;
	 * @param[out] cost_to_n the cost you need to pay for going from node <tt>parent fo n</tt> to @c n
	 */
	virtual void next(SEARCH_NODE*& n, dpf::cost_t& cost_to_n) = 0;
	virtual void clear() = 0;
};

class AbstractStats {
public:
	virtual ~AbstractStats() { };
	/**
	 * reset the generated node statistic
	 */
	virtual void reset_nodes_generated() = 0;
	/**
	 * reset the expanded node statistic
	 */
	virtual void reset_nodes_expanded() = 0;
	/**
	 * reset the touched node statistic
	 */
	virtual void reset_nodes_touched() = 0;

	virtual void add_node_generated(const warthog::search_node& n) = 0;
	virtual void add_node_expanded(const warthog::search_node& n) = 0;
	virtual void add_node_touched(const warthog::search_node& n) = 0;

	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return Number of times we have expanded a node from the open list
	 */
	virtual int get_nodes_expanded() const = 0;

	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return the number of nodes we have added inside the open list (initial node excluded)
	 */
	virtual int get_nodes_generated() const = 0;

	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return the number that we have considered a node
	 */
	virtual int get_nodes_touched() const  = 0;

	/**
	 * Reset the time spent on computing the heuristic to 0 microseconds
	 */
	virtual void reset_heuristic_time() = 0;

	/**
	 * add to the total time spent in the heuristic the given time
	 *
	 * @param[in] n the node we computd the heuristic
	 * @param[in] t time (in microseconds) we ued to compute the heuristic
	 */
	virtual void add_to_heuristic_time(const warthog::search_node& n, unsigned long t) = 0;

	/**
	 * same as ::AbstractStats::add_to_heuristic_time but with double
	 *
	 * @param[in] n the node we computd the heuristic
	 * @param[in] t time (in microseconds) we ued to compute the heuristic
	 */
	virtual void add_to_heuristic_time(const warthog::search_node& n, double t) {
		this->add_to_heuristic_time(n, static_cast<unsigned long>(t));
	}

	/**
	 * @return
	 *  the total time we used to compute the heuristics (in microseconds)
	 */
	virtual unsigned long get_total_heuristic_time() = 0;
};


// H is a heuristic function
// E is an expansion policy
/**
 * An abstract class implementing an A* algorithm tuned for path finding
 *
 * @tparam H an heuristic object representing the heuristic to use. See AbstractHeuristic
 * @tparam E an object used to expand search nodes thus obtaining its successors. See AbstractExpander
 * @tparam STATS an object used to gather statistics upon algorithm performances. See AbstractStats
 */
template <typename H,typename E, typename STATS>
class abstract_pathfinding_astar {
public:
	/**
	 * @brief the bound of the algorithm is areal.
	 * 
	 * However, in order to make the compuation of the multiplication faster, the real values
	 * need to go away. This constant multiplies the bound, thus making the bound itself integer.
	 * 
	 * Note that this number should be an integer log of 10. This because in this way this also represents the last
	 * decimal which is considered by the algorithm.
	 * For example setting this to 1000 it means the bound can have the value at least 0.001
	 */
	static const int BOUND_GRANULARITY = 1000;
public:

	/**
	 * Create a new structure which perform A\* searches
	 *
	 * @param[in] heuristic an object allowing you to perform heuristics
	 * @param[in] expander an object allowing you to expand nodes
	 * @param[in] upperBoundOptimization true if you want to turn on the upper bound optimization
	 * @param[in] earlyStopOptimization true if you want to turn on the early stop optimization
	 * @param[in] useAnyBound if true, we will implement an AnyTmie algorithm. How is this different? First we generate
	 * 	a list containing solution cost, the time needed to obtain it and whether or not the solution is optimal. This
	 * 	list is available 
	 * @param[in] anyTimeAcceptedDelay a factor, related to the CPD initial heauristic value, representing the threshold where excessive delays are
	 * 	not considered as solution. For example, if the CPD says that from start to goal we need at least 100 and we say that
	 *  we do not consider solution for factor or 2; if during the search we find a node whose f is 201 we won't consider it as an anytime solution
	 * 	at all since 201 > 2*100.
	 */
	//TODO remove abstract_pathfinding_astar(H* heuristic, E* expander, STATS* stats, bool upperBoundOptimization, bool earlyStopOptimization, bool enableStatistics, bool enableBoundSearch, double bound, double hscale, bool useAnyBound, double anyTimeAcceptedDelay):
	abstract_pathfinding_astar(H* heuristic, E* expander, STATS* stats, bool upperBoundOptimization, bool earlyStopOptimization, bool enableStatistics, double hscale):
		heuristic_{heuristic}, expander_{expander}, stats_{stats},
		search_time_{0},
		enableStatistics{enableStatistics}, expandedNodes{},
		//enableBoundSearch{enableBoundSearch},
		//bound{static_cast<dpf::cost_t>(BOUND_GRANULARITY * bound)},
		enableOptimizationEarlyStop{earlyStopOptimization}, enableOptimizationUpperbound{upperBoundOptimization},
		hscale_{hscale},
		//useAnyBound{useAnyBound}, anyTimeAcceptedDelay{static_cast<dpf::cost_t>(BOUND_GRANULARITY * anyTimeAcceptedDelay)}, 
		anyTimeInfos{},
		verbose_{false} {
			open_ = new warthog::pqueue(1024, true);
		}

	~abstract_pathfinding_astar() {
		cleanup();
		delete open_;
	}

	/**
	 * Start an A\* search trying to find a path from a start node till a goal
	 *
	 * @param[in] startid
	 * @param[in] goalid
	 * @return
	 *  @li a stack containing ids of each state going from the start till the end
	 *  @li an empty stack if the goal cannot be reached or the start and the goal are the same state
	 */
	inline std::stack<dpf::nodeid_t> get_path(dpf::nodeid_t startid, dpf::nodeid_t goalid) {
		std::stack<dpf::nodeid_t> path;
		warthog::search_node* goal = search(startid, goalid);
		if (goal) {
			// follow backpointers to extract the path
			assert(goal->get_id() == goalid);
			for(warthog::search_node* cur = goal; cur != 0;	cur = cur->get_parent()) {
				path.push(cur->get_id());
			}
			assert(path.top() == startid);
		}
		cleanup();
		return path;
	}

	/**
	 * like abstract_pathfinding_astar::get_path but returns a vector
	 *
	 * @param[in] startid
	 * @param[in] goalid
	 * @param[in] performCleanup if true, we will cleanup akll search metadata after the completition. Set it false only when you want to inspect metadata for debugging purposes
	 * @return
	 *  @li a vector containing ids of each state going from the start till the end
	 *  @li an empty vector if the goal cannot be reached or the start and the goal are the same state
	 */
	std::vector<dpf::nodeid_t> get_path_as_vector(dpf::nodeid_t startid, dpf::nodeid_t goalid, bool performCleanup=true) {
		std::vector<dpf::nodeid_t> path;
		warthog::search_node* goal = search(startid, goalid);
		if (goal) {
			// follow backpointers to extract the path
			assert(goal->get_id() == goalid);
			for(warthog::search_node* cur = goal; cur != 0;	cur = cur->get_parent()) {
				debug("insertin node", cur->get_id(), " in thepath");
				path.insert(path.begin(), cur->get_id());
			}
			assert(path.front() == startid);
		}
		if (performCleanup) {
			this->cleanup();
		}
		return path;
	}

	/**
	 * Start an A\* search trying to find the length of the path from a start node till a goal
	 *
	 * @pre
	 *  @li action costs are 1
	 *
	 * @param[in] startid
	 * @param[in] goalid
	 * @return
	 *  the number of action to perform to reach @c goalid starting from @c startid
	 */
	double get_length(dpf::nodeid_t startid, dpf::nodeid_t goalid) {
		warthog::search_node* goal = search(startid, goalid);
		dpf::cost_t len = dpf::cost_t::getUpperBound();
		if (goal) {
			assert(goal->get_id() == goalid);
			len = goal->get_g();
		}

		DO_ON_DEBUG_IF(verbose_) {
			std::stack<warthog::search_node*> path;
			warthog::search_node* current = goal;
			while(current != 0)	{
				path.push(current);
				current = current->get_parent();
			}

			while(!path.empty()) {
				warthog::search_node* n = path.top();
				dpf::coo2d_t x, y;
				y = (n->get_id() / expander_->mapwidth());
				x = n->get_id() % expander_->mapwidth();
				std::cerr << "final path: ("<<x<<", "<<y<<")...";
				n->print(std::cerr);
				std::cerr << std::endl;
				path.pop();
			}
		}

		cleanup();
		return static_cast<double>(len / (double)warthog::ONE);
	}

	/**
	 *
	 * @return the number of bytes that the internal structures of A\* occupies
	 */
	inline size_t mem() {
		size_t bytes =
				// memory for the priority quete
				open_->mem() +
				// gridmap size and other stuff needed to expand nodes
				expander_->mem() +
				// misc
				sizeof(*this);
		return bytes;
	}



	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return number of microseconds took by A\* to complete the search
	 */
	inline double get_search_time() {
		return search_time_;
	}

	/**
	 * @return check if you want additional output while searching
	 */
	inline bool	get_verbose() {
		return verbose_;
	}

	/**
	 * set the logging level
	 *
	 * @param[in] verbose true if you want to enable the logging, false otherwise
	 */
	inline void	set_verbose(bool verbose) {
		verbose_ = verbose;
	}

	/**
	 *
	 *
	 * @return the weight fo the heuristic when computing f
	 */
	inline double get_hscale() {
		return hscale_;
	}

	/**
	 * @note
	 * use it to go from A\* to WA\* and viceversa
	 *
	 * @param[in] hscale the new multipler of the heuristic when computing f
	 */
	inline void set_hscale(double hscale) {
		hscale_ = hscale;
	}

	abstract_pathfinding_astar& operator=(const abstract_pathfinding_astar& other) {
		return *this;
	}

	inline int get_nodes_expanded() const {
		return this->stats_->get_nodes_expanded();
	}

	inline int get_nodes_generated() const {
		return this->stats_->get_nodes_generated();
	}

	inline int get_nodes_touched() const {
		return this->stats_->get_nodes_touched();
	}

	/**
	 * @return the time, in microseconds, it globally took to perform heuristics
	 */
	inline unsigned long get_heuristic_time() const {
		return this->stats_->get_total_heuristic_time();
	}

	inline const std::vector<dpf::nodeid_t>& getNodesExpanded() const {
		return this->expandedNodes;
	}

	inline const std::vector<AnyTimeInfo>& getAnyTimeInfoVector() const {
		return this->anyTimeInfos;
	}

public:
	/**
	 * tells if the newly generated state can be pruned away from the search
	 *
	 * @pre
	 *  @li abstract_pathfinding_astar::enableOptimizationUpperbound set;
	 *
	 * When a state (except the start one) is generated for the first time, before putting it in the open list
	 * we check if it can lead to a optimal solution. If not, the state is completely discarded. This function does that.
	 *
	 * @note
	 * The function is called immediately after the heuristic module has just computed the heuristic value of @c node
	 *
	 * @param[in] childNode the node just generated that we need to check
	 * @param[in] parent the node which has generated @c childNode
	 * @param[in] childG the cost of reaching @c childNode from the starting node of the search
	 * @param[in] childH the heuristic of @c childNode
	 * @param[in] heuristic the object representing the heuristic function
	 * @param[in] expander the object allowing you to expand nodes
	 * @return
	 *  @li true if this state can be pruned away;
	 *  @li false otherwise;
	 */
	virtual bool canPruneNewGeneratedState(const warthog::search_node& childNode, const warthog::search_node& parent, dpf::cost_t childG, dpf::cost_t childH, const H& heuristic, const E& expander) = 0;

	/**
	 * tells if we can immediately stop the A\* search because we have found a node which immediately lead us to the goal
	 *
	 * @pre
	 *  @li abstract_pathfinding_astar::enableOptimizationEarlyStop set;
	 *
	 * We can detect if a state can immediately lead us to the goal. note that a state which can immediately lead us to the goal
	 * is not a goal itself. This function allows you to detect whether such scenario happens.
	 *
	 * @note
	 * The function is called immediately after the heuristic module has just computed the heuristic value of @c node
	 *
	 * @param[in] node a node we need to check if it can immediately lead us to the goal
	 * @param[in] nodeG the cost of reaching @c node from the starting node of the search
	 * @param[in] nodeH the heuristic of @c node
	 * @param[in] heuristic the heuristic of this search
	 * @param[in] expander the expander of this seach
	 * @return
	 *  @li true if @c node can lead us to the goal immediately;
	 *  @li false otherwise;
	 */
	virtual bool canEarlyStopAStar(const warthog::search_node& node, dpf::cost_t nodeG, dpf::cost_t nodeH, const H& heuristic, const E& expander) = 0;

	/**
	 * Quickly generate the goal starting from a state which can lead us immediately to the goal
	 *
	 * The function should generate all the search_node from @c node till the goal as well
	 *
	 * @param[in] node the node where we can immediately compute the goal from
	 * @param[in] instance the problem instance we're trying to solve
	 * @return the search_node representing the goal of the problem
	 */
	virtual warthog::search_node& generateImmediateSuccessors(const warthog::search_node& node, const warthog::problem_instance& instance) = 0;

protected:
	/**
	 * An object implementing the heuristic
	 *
	 * The object has:
	 * @li a function pointer `_h` which given 2 id of states (the current one and the goal) generates the heuristic value (an int)
	 *
	 * @code
	 * heuristic_->h(startid, goalid)
	 * @endcode
	 */
	H* heuristic_;
	/**
	 * an object which has the following:
	 *
	 * @li a function pointer `generate` which given an state id generates a `warthog::search_node*`;
	 * @li a function pointer `mapwidth()` which return the width of the map invovled int he path planning
	 * @li a function pointer `expand(current, &instance)` which has the current `search_node*` and the instance fo the problem to update.
	 * @li a function pointer `first(n, cost_to_n)` which, after expand, generates in its output parameters
	 * 	the first successor of the node and the cost from reaching n starting from its
	 * 	parent (in other word, the action cost).
	 * @li a function pointer `next(n, cost_to_n)` which, after expand, generates the other successors other than the first.
	 * 	the semantic of this function is the same of `first` except that when there are no more successors, `n` is set to NULL
	 */
	E* expander_;
	/**
	 * An object whose job is to gather information about ythe algorothm performances.
	 */
	STATS* stats_;

	// /**
	//  * @brief change the algorithm behaviour
	//  * 
	//  * in the sense that the algorithm will stop as soon as it finds a node whose acutal solution is within
	//  * [O, O + B*O]
	//  * where O is the cost of the solution the CPD finds and B is a value greater than 0
	//  * 
	//  */
	// const bool enableBoundSearch;
	// /**
	//  * @brief if enableBoundSearch is set tyhis value represents the value B
	//  * 
	//  * Ignore if enableBoundSearch is disable.
	//  * Note that this value is not a double. This because in this way during the algorithm run time
	//  * the copmputation of the bound is faster.
	//  */
	// const dpf::cost_t bound;

	bool enableStatistics;
	std::vector<dpf::nodeid_t> expandedNodes;
	/**
	 * the open list fo A\*
	 */
	warthog::pqueue* open_;
	/**
	 * @brief the instance of the path planning problem we're trying to solve
	 * 
	 * this is automatically set during a search operation
	 */
	warthog::problem_instance instance;
	/**
	 * enable this if you want to have some debugging information while A\* is running
	 */
	bool verbose_;
	static uint32_t searchid_;
	/**
	 * number of microseconds took by A\* to complete the search
	 */
	double search_time_;
	/**
	 * heuristic scaling factor
	 *
	 * Used to implement WA\* if necessary
	 */
	double hscale_;

	/**
	 * Enable the optimization relative of the upperbound
	 *
	 */
	const bool enableOptimizationUpperbound;
	/**
	 * Enable the optimization relative to the early stop
	 */
	const bool enableOptimizationEarlyStop;

	/**
	 * @brief if you wish generate the data for the any time benchmark, this field is for you
	 * 
	 * It represents the list where we will put the data related to the anytime benchmark
	 * 
	 */
	std::vector<AnyTimeInfo> anyTimeInfos;
	// /**
	//  * @brief true if we want to run this algorithms as an anytime variant
	//  * 
	//  */
	// bool useAnyBound;
	// /**
	//  * @brief a factor, related to the best solution (CPD initial heauristic value),
	//  *  
	//  * representing the threshold where excessive delays are
	//  * not considered as solution. 
	//  * 
	//  * For example, if the CPD says that from start to goal we need at least 100 and we say that
	//  * we do not consider solution for factor or 2; if during the search we find a node whose f is 201 we won't consider it as an anytime solution
	//  * at all since 201 > 2*100.
	//  * 
	//  * Since this value is computed during runtime, we do not want it to be a double,
	//  * hence we have multiplied the double value with BOUND_GRANULARITY, to make it integer.
	//  * This value should always be greater (or equal) than 1 * BOUND_GRANULARITY.
	//  */
	// dpf::cost_t anyTimeAcceptedDelay;


	/**
	 * @brief check if the node is actually a goal
	 * 
	 * @param n the node to check
	 * @param goalid the id of the goal
	 * @return true the node is indeed a goal of our
	 * @return false otherwise
	 */
	virtual bool checkIfGoal(const warthog::search_node* n, dpf::nodeid_t goalid) const {
		//TODO use it
		return false;
	}

	/**
	 * @brief check if the current node has a solution which is within a given bound
	 * 
	 * @param n the node to check
	 * @param goalid the id of the goal
	 * @param startSolution the cost of the relaxed solution. This can be interpreted as a lower bound of the optimal solution
	 * @param bound the bound
	 * @param goalFound the id of the goal we have found (if we have found any of those). 
	 * 	The value contains noise if the function returns false.
	 *  Otherwise it contains a well specific node whose parent represents the path to follow
	 * @return bool true if we have reached a goal in compliance with the bound. False otherwise
	 */
	//TODO remove virtual bool getGoalIfWithinBound(const warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startSolution, dpf::cost_t bound, warthog::search_node*& goalFoundId) = 0;

	//TODO remove
	//virtual AnyTimeInfo getAnyTimeInfos(const warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startSolution , double microSeconds, bool isOptimalSolutionFound, const warthog::search_node* goal) = 0;

	virtual bool isNodeRepresentingAValidSolution(const warthog::search_node* n, dpf::nodeid_t goalid) = 0;

	//TODO remove
	// /**
	//  * @brief check if a new **valid solution** improves the an old **valid** solution
	//  * 
	//  * @pre
	//  *  @li oldSolutionCost is valid
	//  *  @li the new solution is valid
	//  * 
	//  * @param n the node representing a valid solution
	//  * @param goalid the id of the state representing the goal
	//  * @param isOldSolutionPresent true if thre's exist an old solution
	//  * @param oldSolutionCost if `isOldSolutionPresent` is true, the value of the old solution cost
	//  * @return true if the current solution represented by `n` improves the cost of the old one
	//  * @return false otherwise
	//  */
	// virtual bool hasNodeImprovedCurrentSolution(const warthog::search_node* n, dpf::nodeid_t goalid, bool isOldSolutionPresent, dpf::cost_t oldSolutionCost) = 0;

	// TODO remove virtual void setup(const warthog::search_node* n, dpf::nodeid_t goalid) = 0;

	/**
	 * start the A\* search
	 *
	 * Each search node is uniquely identified by an id
	 *
	 * @param[in] startid the id of the initial state
	 * @param[in] goalid the id of the final state to reach
	 * @return
	 *  @li the pointer of the goal we have found during search
	 *  @li NULL if the search didn't find anything
	 */
	virtual warthog::search_node* search(dpf::nodeid_t startid, dpf::nodeid_t goalid) = 0;
// 	virtual warthog::search_node* search(dpf::nodeid_t startid, dpf::nodeid_t goalid) {
// 		this->stats_->reset_nodes_generated();
// 		this->stats_->reset_nodes_expanded();
// 		this->stats_->reset_nodes_touched();
// 		this->stats_->reset_heuristic_time();
// 		if (this->useAnyBound) {
// 			this->anyTimeInfos.clear();
// 		}
// 		if (this->enableStatistics) {
// 			this->expandedNodes.clear();
// 		}

// 		search_time_ = 0;

// 		warthog::timer mytimer;
// 		mytimer.start();

// 		//variable for any time technique
// 		double anyTimeMicroseconds;
// 		SimpleTimer anyTimeTimer{anyTimeMicroseconds, false};
// 		if (this->useAnyBound) {
// 			anyTimeTimer.StartTimer();
// 		}

// 		clog(verbose_)("search: startid=", startid, " goalid=", goalid);

// 		//**************** SETUP THE PROBLEM INSTANCE *************************
// 		this->instance.set_goal(goalid);
// 		this->instance.set_start(startid);
// 		this->instance.set_searchid(searchid_++);

// 		//GENERATE THE START SEARCH NODE AND POPULATE F, G and H
// 		warthog::search_node* goal = 0;
// 		warthog::search_node* start = expander_->generate(startid);
// 		double heuristicTime;
// 		dpf::cost_t startH;
// 		PROFILE_TIME_CODE(heuristicTime) {
// 			startH = heuristic_->h(startid, goalid);
// 		}
// 		this->stats_->add_to_heuristic_time(*start, heuristicTime);
// 		start->reset(this->instance.get_searchid());
// 		start->set_g(0);
// 		start->set_f(static_cast<warthog::cost_t>((startH * hscale_)));
		

// 		//perofrm specific setup code after the start node has been added to the open list
// 		this->setup(start, goalid);

// 		//WE CHECK IF WE CAN EARLY STOP
// 		if (enableOptimizationEarlyStop && this->canEarlyStopAStar(*start, 0, startH, *heuristic_, *expander_)) {
// 			debug("early stop when computing initial state!");
// 			//quickly generate the goal
// 			goal = &this->generateImmediateSuccessors(*start, this->instance);

// 			//if anybound is involved, update its result
// 			if (this->useAnyBound && this->isNodeRepresentingAValidSolution(start, goalid)) {
// 				auto us = anyTimeTimer.EndTimer();
// 				critical("stop timer! we reached the without starting the search!");
// 				this->anyTimeInfos.push_back(this->getAnyTimeInfos(start, goalid, startH, us, true, goal));
// 			}
// 			//we retrieve the optimal path immediately

// 			goto goal_found;
// 		}

// 		//PUT IT IN THE OPEN LIST
// 		open_->push(start);

// 		while(open_->size()) {

// 			warthog::search_node* incumbent = open_->peek();

// 			debug("***************** NEW ITERATION *******************");
// 			this->stats_->add_node_touched(*incumbent);


// 			// *********************** check if we have reached the goal ***************************
// 			if (incumbent->get_id() == goalid) {
// 				//GOAL FOUND
// 				cslog(verbose_)
// 					(dpf::nodeid_t x = incumbent->get_id() / expander_->mapwidth(), y = incumbent->get_id() % expander_->mapwidth())
// 					("goal found (", x, ", ", y, ")...");

// 				goal = incumbent;

// 				if (this->useAnyBound && this->isNodeRepresentingAValidSolution(start, goalid)) {
// 					auto us = anyTimeTimer.EndTimer();
// 					debug("stop timer! we reached becausewe found the goal while searching!", incumbent->get_id(), start->get_id());
// 					this->anyTimeInfos.push_back(this->getAnyTimeInfos(incumbent, goalid, startH, us, true, goal));
// 				}


// 				goto goal_found;
// 			}

// 			// ****************** check if we havce at least satisfied the bound ********************
// 			if (this->enableBoundSearch) {
// 				if (this->getGoalIfWithinBound(incumbent, goalid, start->get_f(), this->bound, goal)) {
// 					goto goal_found;
// 				}
// 			}

// 			// ********************* FETCH THE NODE WITH MINIMUM COST RIGHT NOW *************************
// 			DynamicSearchNode* current = static_cast<DynamicSearchNode*>(incumbent);
// 			if (enableStatistics) {
// 				this->expandedNodes.push_back(current->get_id());
// 			}
// 			debug("fetched from the open list the node", current->get_id());

// 			//is the state we have popped an early stop?
// 			dpf::cost_t hval = (current->get_f() - current->get_g())/this->hscale_;
// 			if (enableOptimizationEarlyStop && current->is_leadToOptimalSolution()) {
// 				debug("early stop when computing state", incumbent->get_id());
// 				//quickly generate the goal 
// 				goal = &this->generateImmediateSuccessors(*current, this->instance);

// 				if (this->useAnyBound && this->isNodeRepresentingAValidSolution(start, goalid)) {
// 					auto us = anyTimeTimer.EndTimer();
// 					debug("stop timer! we reached the goial because we found a node which can point directly to the solution!");
// 					//we do not push back a new solution since it was already done before for the best of the open list
// 					this->anyTimeInfos.push_back(this->getAnyTimeInfos(incumbent, goalid, startH, us, true, goal));
// 				}

// 				goto goal_found;
// 			}

// 			//if AnyTime is enabled, we check if the solution has at least improved from the last time
// 			if (this->useAnyBound 
// 				&& this->isNodeRepresentingAValidSolution(incumbent, goalid) 
// 				&& this->hasNodeImprovedCurrentSolution(incumbent, goalid, !this->anyTimeInfos.empty(), this->anyTimeInfos.empty() ? 0 : this->anyTimeInfos.back().solutionCost)) {
// 				auto us = anyTimeTimer.EndTimer();
// 				debug("stop timer! we reached a node which improved the previous bound!", incumbent->get_id(), start->get_id());
// 				this->anyTimeInfos.push_back(this->getAnyTimeInfos(incumbent, goalid, startH, us, false, goal));
// 				//we restart the timer
// 				anyTimeTimer.StartTimer();
// 			}

// 			this->stats_->add_node_expanded(*current);
// 			open_->pop();

// 			debug("we are going to expand node", current->get_id(), "f=", current->get_f(), "g=", current->get_g(), "h=", (current->get_f() - current->get_g())/(this->hscale_));

// 			cslog(verbose_)(dpf::nodeid_t x = current->get_id() % expander_->mapwidth(), y = (current->get_id() / expander_->mapwidth()))
// 			("expanding (", x, ", ", y, ")...", current);

// 			// GENERATE NODE SUCCESSORS
// 			debug("marking node ", current->get_id(), "as expanded");
// 			current->set_expanded(true); // NB: set this before calling expander_
// 			assert(current->get_expanded());
// 			expander_->expand(current, &this->instance);

// 			DynamicSearchNode* n = 0;
// 			dpf::cost_t cost_to_n = dpf::cost_t::getUpperBound();

// 			//HANDLE NODE SUCCESSORS
// 			for(expander_->first(n, cost_to_n);	n != 0;	expander_->next(n, cost_to_n)) {
// 				this->stats_->add_node_touched(*n);
// 				if(n->get_expanded()) {
// 					// skip neighbours already expanded
// 					continue;
// 				}

// 				debug("---------------- NEW EXPANSION", n->get_id(), " IN ITERATION OF PARENT ", current->get_id(), "-----------------");

// 				if(open_->contains(n))	{
// 					// update a node from the fringe
// 					dpf::cost_t gval = current->get_g() + cost_to_n;
// 					if(gval < n->get_g()) {
// 						//we have reached a node in the fringe via a more direct path in the parent. Update priority of node
// 						n->relax(static_cast<warthog::cost_t>(gval), current);
// 						open_->decrease_key(n);

// 					} else {

// 					}
// 				} else {
// //					cslog(verbose_)(dpf::coo2d_t x = n->get_id() % expander_->mapwidth(), y = (n->get_id() / expander_->mapwidth()))
// //					("  generating (", x, ", ", y, ")...", n);

// 					// add a new node to the fringe (it wasn't inside the open list)
// 					dpf::cost_t gval = current->get_g() + cost_to_n;

// 					dpf::cost_t hval;
// 					PROFILE_TIME_CODE(heuristicTime) {
// 						hval = heuristic_->h(n->get_id(), goalid);
// 					}
// 					this->stats_->add_to_heuristic_time(*n, heuristicTime);

// 					if (enableOptimizationUpperbound && this->canPruneNewGeneratedState(*n, *current, gval, hval, *heuristic_, *expander_)) {
// 						debug("the state", n->get_id(), "can be pruned away!");
// 						//we mark it expanded: in this way if we happen to retrieve it, we won't bother by generating new memory space for it
// 						n->set_expanded(true);
// 						continue;
// 					}

// 					/*
// 					 * i don't put the early stop here because when we expand the children of a state, it is not garantueed that they are
// 					 * ordered by costs. So, if a children has an eraly stop, we are not garantueed to have the optimum one, only a feasible
// 					 * solution.
// 					 */
// 					//WE CHECK IF WE CAN EARLY STOP
// 					if (enableOptimizationEarlyStop && this->canEarlyStopAStar(*current, gval, hval, *heuristic_, *expander_)) {
// 						debug("early stop when computing state", n->get_id());
// 						n->set_leadToOptimalSolution(true);
// 					}

// 					//we have generated a node but we don't add it in the open list
// 					this->stats_->add_node_generated(*n);

// 					n->set_g(static_cast<warthog::cost_t>(gval));
// 					n->set_f(static_cast<warthog::cost_t>(gval + hval * hscale_));
// 					n->set_parent(current);

// 					debug("Add in Open list the children of node", current->get_id(), ", aka node ", n->get_id(), "f=", n->get_f(), "g=", gval, "h=", hval);
// 					open_->push(n);


// 				}
// 			}

// //			cslog(verbose_)
// //			(dpf::coo2d_t x = current->get_id() % expander_->mapwidth(), y = (current->get_id() / expander_->mapwidth()))
// //			("closing (", x, ", ", y, ")...", n);

// 		}

// 		goal_found:;
// 		mytimer.stop();
// 		search_time_ = mytimer.elapsed_time_micro();
// 		return goal;
// 	}

	/**
	 * clean up the internal resources from one search to another one
	 */
	virtual void cleanup() {
		open_->clear();
		if (this->heuristic_ != nullptr) {
			heuristic_->clear();
		}
		if (this->expander_ != nullptr) {
			expander_->clear();
		}
	}

};

template <class H, class E, typename STATS>
uint32_t abstract_pathfinding_astar<H, E, STATS>::searchid_ = 0;


#endif /* ABSTRACT_PATHFINDING_ASTAR_H_ */
