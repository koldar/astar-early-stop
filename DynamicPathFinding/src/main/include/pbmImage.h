/**
 * @file
 *
 * A module allowing to manipulate a PPM image.
 *
 * PPM images are useful because are easy to write and can be used to be converted into regular images files (like convert) to pngs
 *
 *  Created on: Oct 27, 2018
 *      Author: koldar
 */

#ifndef PBMIMAGE_H_
#define PBMIMAGE_H_

#include <string>
#include <iostream>
#include "types.h"
#include "vec_io.h"

struct safe_uchar {
    friend std::ostream& operator <<(std::ostream& stream, const safe_uchar& c);
    friend safe_uchar operator + (const safe_uchar& a, const safe_uchar& b);
    friend safe_uchar operator - (const safe_uchar& a, const safe_uchar& b);
    friend safe_uchar operator / (const safe_uchar& a, const safe_uchar& b);
    friend safe_uchar operator & (const safe_uchar& a, const safe_uchar& b);
    friend safe_uchar operator | (const safe_uchar& a, const safe_uchar& b);
    friend bool operator == (const safe_uchar& a, const safe_uchar& b);
    friend bool operator != (const safe_uchar& a, const safe_uchar& b);
public:
    safe_uchar();
    explicit safe_uchar(unsigned char c);
    explicit safe_uchar(int c);
    safe_uchar(const safe_uchar& other);
    safe_uchar mean(const safe_uchar& other) const;
    ~safe_uchar();
public:
    safe_uchar& operator = (const safe_uchar& other);
    operator unsigned char() const;
    safe_uchar& operator += (const safe_uchar& other);
    safe_uchar& operator += (unsigned char other);
    safe_uchar& operator -= (const safe_uchar& other);
    safe_uchar& operator -= (unsigned char other);
    safe_uchar& operator /= (const safe_uchar& other);
    safe_uchar& operator /= (unsigned char other);
    safe_uchar& operator &= (const safe_uchar& other);
	safe_uchar& operator &= (unsigned char other);
	safe_uchar& operator |= (const safe_uchar& other);
	safe_uchar& operator |= (unsigned char other);
    safe_uchar operator ~() const;
private:
    unsigned char value;
};

std::ostream& operator <<(std::ostream& stream, const safe_uchar& c);
safe_uchar operator + (const safe_uchar& a, const safe_uchar& b);
safe_uchar operator - (const safe_uchar& a, const safe_uchar& b);
safe_uchar operator / (const safe_uchar& a, const safe_uchar& b);
safe_uchar operator & (const safe_uchar& a, const safe_uchar& b);
safe_uchar operator | (const safe_uchar& a, const safe_uchar& b);
bool operator == (const safe_uchar& a, const safe_uchar& b);
bool operator != (const safe_uchar& a, const safe_uchar& b);

struct color_t {
    friend color_t operator + (const color_t& a, const color_t& b);
    friend color_t operator - (const color_t& a, const color_t& b);
    friend color_t operator / (const color_t& a, const int& b);
    friend color_t operator ~(const color_t& a);
    friend color_t operator &(const color_t& a, const color_t& other);
    friend color_t operator |(const color_t& a, const color_t& other);
    friend bool operator ==(const color_t& a, const color_t& other);
    friend bool operator !=(const color_t& a, const color_t& other);
    friend std::ostream& operator <<(std::ostream& stream, const color_t& c);
public:
    color_t(unsigned char r, unsigned char g, unsigned char b);
    color_t(const color_t& other);
    color_t();

    ~color_t();
    safe_uchar getRed() const;
    safe_uchar getGreen() const;
    safe_uchar getBlue() const;

    color_t lerp(const color_t& other) const;
public:
    color_t& operator +=(const color_t& other);
    color_t& operator -=(const color_t& other);
    color_t& operator =(const color_t& other);
    color_t& operator /(const int& i);

private:
    safe_uchar r;
    safe_uchar g;
    safe_uchar b;
};

extern const color_t DARK_RED;
extern const color_t RED;
extern const color_t DARK_ORANGE;
extern const color_t ORANGE;
extern const color_t DARK_YELLOW;
extern const color_t YELLOW;
extern const color_t DARK_GREEN;
extern const color_t GREEN;

extern const color_t DARK_CYAN;
extern const color_t CYAN;

extern const color_t DARK_BLUE;
extern const color_t BLUE;

extern const color_t PURPLE;

extern const color_t DARK_VIOLET;
extern const color_t VIOLET;

extern const color_t GRAY;
extern const color_t WHITE;
extern const color_t BLACK;

color_t operator + (const color_t& a, const color_t& b);
color_t operator - (const color_t& a, const color_t& b);
color_t operator / (const color_t& a, const int& b);
color_t operator ~(const color_t& a);
color_t operator &(const color_t& a, const color_t& other);
color_t operator |(const color_t& a, const color_t& other);
bool operator ==(const color_t& a, const color_t& other);
bool operator !=(const color_t& a, const color_t& other);
std::ostream& operator <<(std::ostream& stream, const color_t& c);


class PPMImage {
    friend PPMImage operator + (const PPMImage& a, const PPMImage& b);
    friend PPMImage operator - (const PPMImage& a, const PPMImage& b);
public:
    PPMImage(dpf::coo2d_t width, dpf::coo2d_t height);
    PPMImage(dpf::coo2d_t width, dpf::coo2d_t height, const color_t& color);
    PPMImage(const PPMImage& other);
    ~PPMImage();

    /**
     * save the PBM image ina file
     *
     * @param[in] filename the name of file to save (extension excluded!);
     */
    void save(const std::string& filename) const;
    /**
     * like ::PPMImage::save but we will convert the ppm image into a jpeg, to avoid wasting filesystem space
     *
     * @pre
     *  @li @c convert command is expected to be installed on tyhe system
     *
     * @param[in] filename the name of file to save (extension excluded!);
     */
    void saveAndConvertIntoJPEG(const std::string& filename) const;

    /**
	 * like ::PPMImage::save but we will convert the ppm image into a png, to avoid wasting filesystem space
	 *
	 * @pre
	 *  @li @c convert command is expected to be installed on the system
	 *
	 * @param[in] filename the name of file to save (extension excluded!);
	 */
    void saveAndConvertIntoPNG(const std::string& filename) const;

    void setPixel(dpf::coo2d_t x, dpf::coo2d_t y, const color_t& color);
    color_t& getPixel(dpf::coo2d_t x, dpf::coo2d_t y);
    color_t getPixel(dpf::coo2d_t x, dpf::coo2d_t y) const;
    void fillImageWith(const color_t& color);
    bool areSameSizes(const PPMImage& other) const;
    PPMImage scale(int scaleX, int scaleY) const;
public:
    PPMImage& operator +=(const PPMImage& other);
    PPMImage& operator -=(const PPMImage& other);
private:
    unsigned long long getIdFromCoordinated(dpf::coo2d_t x, dpf::coo2d_t y) const;
    void saveAndConvertInto(const std::string& filename, const std::string& extension) const;

private:
    const dpf::coo2d_t width;
    const dpf::coo2d_t height;
    /**
     * matrix of pixels.
     *
     * allocated memory goes to width*height pixels. they are in form of matrix,
     * starting from topleft (0) till bottomright (width*height -1). Matrix goes row by row
     */
    color_t* matrix;
};

PPMImage operator + (const PPMImage& a, const PPMImage& b);
PPMImage operator - (const PPMImage& a, const PPMImage& b);

#endif /* PBMIMAGE_H_ */
