#ifndef _CPD_ANYTIME_WA_SEARCH_HEADER__
#define _CPD_ANYTIME_WA_SEARCH_HEADER__

#include "dynamic_pathfinding_astar.h"

template <typename H, typename E, typename STATS>
class CPDAnytimeWAStarSearch : public dynamic_pathfinding_astar<H,E, STATS>{
private:
    double anyTimeMicroSeconds;
    /**
     * @brief tghe timer keeping track of the new anytime findings
     * 
     */
    SimpleTimer anyTimeTimer;
    /**
     * @brief the upperbound for the solution, computed from the last fetched solution
     * 
     */
    dpf::cost_t upperBound;
public:

    public:
    CPDAnytimeWAStarSearch(H* heuristic, E* expander, STATS* stats, bool upperBoundOptimization, bool earlyStopOptimization, bool enableStatistics, double hScale) :
        dynamic_pathfinding_astar<H, E, STATS>(heuristic, expander, stats, upperBoundOptimization, earlyStopOptimization, enableStatistics, hScale),
        upperBound{safe_int::getUpperBound()},
        anyTimeMicroSeconds{0}, anyTimeTimer{anyTimeMicroSeconds, false} {
	}

    ~CPDAnytimeWAStarSearch() {

    }

protected:

    virtual bool hasNodeImprovedCurrentSolution(const warthog::search_node* n, dpf::nodeid_t goalid, bool isOldSolutionPresent, dpf::cost_t oldSolutionCost) {
		dpf::cost_t solutionCost = n->get_g() + this->heuristic_->getCPDPathCostInMap(this->expander_->get_map(), n->get_id(), goalid);
		if (isOldSolutionPresent) {
			return solutionCost < oldSolutionCost;
		} else {
			//this is the first VALID solution ever
			return true;
		}
	}

    virtual AnyTimeInfo getAnyTimeInfos(const warthog::search_node* n, dpf::nodeid_t goalid, double microSeconds, bool isOptimalSolutionFound) {
        dpf::cost_t solutionCost = n->get_g() + this->heuristic_->getCPDPathCostInMap(this->expander_->get_map(), n->get_id(), goalid);
        debug("solution is ", solutionCost);
		dpf::cost_t h = ((DynamicSearchNode*)n)->get_h(); //static_cast<dpf::cost_t>((n->get_f() - n->get_g())/this->hscale_);
		return AnyTimeInfo{solutionCost, n->get_g(), h, microSeconds, isOptimalSolutionFound};
	}

    virtual void onOptimalGoalFound(warthog::search_node* start, warthog::search_node* goal, dpf::cost_t startH) {
        //remember: a timer started after evaluating the initial state.
        //if we reached this function, it means we need to stop the timer and change the last vlaue to "optimal"
		auto us = this->anyTimeTimer.EndTimer();
		debug("stop timer! we reached the without starting the search!");
        this->anyTimeInfos.back().isSolutionOptimal = true;
    }

	virtual void onGoalFound(const warthog::search_node* start, const warthog::search_node* goal, bool& keepGoing) {
        //the goal may happen multiple times. we clear immediateSuccessorList
        if (this->enableStatistics) {
            this->immediateSuccessorList.clear();
        }

        keepGoing = true;

        /*
         * there is one scenario where onGoalFound is called when goal is not less than the upperbound.
         * It is when the goal found si directy computed from the start. In that the case the costs are the same.
         * We check against such case
         */
        if (this->upperBound <= goal->get_g()) {
            return;
        }

        //the g of the new solution is the new upperbound.
        //the upperbound is NOT just f, because in f=g+wh. We need to set the upper bound to g!
        info("old upperbound was ", this->upperBound, " but new upper bound is ", goal->get_g());
        this->upperBound = goal->get_g();
        auto us = anyTimeTimer.EndTimer();
        dpf::cost_t h = ((DynamicSearchNode*)goal)->get_h(); //static_cast<dpf::cost_t>((goal->get_f() - goal->get_g())/this->hscale_);
        info("stop timer! we reached goal ", "id:", goal->get_id(), "cost:", h);
        auto infos = this->getAnyTimeInfos(goal, this->instance.get_goal(), us, false);
        this->anyTimeInfos.push_back(infos);
        this->anyTimeTimer.StartTimer();
    }

    virtual bool shouldWePruneNode(const warthog::search_node* start, const warthog::search_node* n, dpf::nodeid_t goalid) {
        //we can't use f to compare against the upperbound because f=g+wh! We need to use f=g+h
        if (this->goal == nullptr) {
            //if we haven't found an actual goal yet we don't want to prune anything.
            //without this check the start node would be pruned away because h_start >= h_start!
            // dpf::cost_t h = static_cast<dpf::cost_t>((n->get_f() - n->get_g())/this->hscale_);
            // return (n->get_g() + h) > this->upperBound;
            //FIXME hen you disable enable early stop this should become return (n->get_g() + h) > this->upperBound; 
            return false;
        } else {
            dpf::cost_t h = ((DynamicSearchNode*)n)->get_h(); //static_cast<dpf::cost_t>((n->get_f() - n->get_g())/this->hscale_);
            info("checking if g+h >= upperbound: ", n->get_g() + h, ">=", this->upperBound);
            return (n->get_g() + h) >= this->upperBound; 
        }
        
    }

    virtual void onPrunedNode(const warthog::search_node* start, const warthog::search_node* n, dpf::nodeid_t goalid) {
        debug("state ", n->get_id(), "has been pruned away because it is less than", this->upperBound);
    }

    virtual void onValidNonGoalPeekedFromOpenList(warthog::search_node* n, warthog::search_node* start, warthog::search_node*& goal, dpf::nodeid_t goalid, bool& keepGoing) {

    }

    virtual void onValidNonLeadingToGoalPeekedFromOpenList(warthog::search_node* n, dpf::nodeid_t goalid, dpf::cost_t startH) {
		if (this->hasNodeImprovedCurrentSolution(n, goalid, !this->anyTimeInfos.empty(), this->anyTimeInfos.empty() ? 0 : this->anyTimeInfos.back().solutionCost)) {
            auto us = anyTimeTimer.EndTimer();
            this->upperBound = n->get_g() + this->heuristic_->getCPDPathCostInMap(this->expander_->get_map(), n->get_id(), goalid);
            //we need to generate the new solution
            //in order to generate the solution, we need to expand the path from the node till the goal
            //note that the path from this node and the goal does not have be absent from perturbation!
            if (this->hscale_ > 1.0) {
                this->goal = &this->generateSuccessorByFollowingHeuristic(*n, this->instance);
            }

            DO_ON_DEBUG {
					for(warthog::search_node* cur = this->goal; cur != nullptr;	cur = cur->get_parent()) {
						info("GOAL PATH ON NON-GOAL FOUND", cur->get_id());
					}
				}

            info("stop timer! we reached a node which improved the previous bound!", "id:", n->get_id(), "cost:", ((DynamicSearchNode*)n)->get_h());
            this->anyTimeInfos.push_back(this->getAnyTimeInfos(n, goalid, us, false));
            //we restart the timer
            anyTimeTimer.StartTimer();
        }
    }        

    virtual void onOpenListEmpty(const warthog::search_node* start, const warthog::search_node* goal, dpf::cost_t goalid) {
        info("the open list is empty!");
        //list is empty. stop the timer but not add any solutio0n
        this->anyTimeTimer.EndTimer();
        //the last entry in anyTimeInfos is also the optimal path!
        if (!this->anyTimeInfos.empty()) {
            this->anyTimeInfos.back().isSolutionOptimal = true;
        }
    }

    virtual void onBeforeComputingStartHeuristic(warthog::search_node* start, dpf::cost_t goalid) {
        this->anyTimeInfos.clear();
        this->anyTimeTimer.StartTimer();
    }

    virtual void onBeforeStartSearching(const warthog::search_node* start, dpf::nodeid_t goalid) {
        auto us = this->anyTimeTimer.EndTimer();
        dpf::cost_t h = ((DynamicSearchNode*)start)->get_h(); //static_cast<dpf::cost_t>((start->get_f() - start->get_g())/this->hscale_);
		auto infos = this->getAnyTimeInfos(start, goalid, us, false);
        info("stop timer! the start goal is an upper bound!", "id:", start->get_id(), "cost:", start->get_g() + ((DynamicSearchNode*)start)->get_h());
        this->anyTimeInfos.push_back(infos);
        this->upperBound = 0 + this->heuristic_->getCPDPathCostInMap(this->expander_->get_map(), start->get_id(), goalid);
        info("new upper bound is ", this->upperBound);
        this->anyTimeTimer.StartTimer();
        
    }

    virtual void onEarlyStopOnSuccessor(warthog::search_node* parent, dpf::cost_t childG, dpf::cost_t childH, bool& skip) {
        skip = this->upperBound < (childG + childH);
    }
						

    virtual void onExpandedNodeInClosedList(warthog::search_node* parent, warthog::search_node* child, dpf::cost_t cost_to_n) {
        dpf::cost_t gval = parent->get_g() + cost_to_n;

        if(gval < child->get_g()) {
            //only because gval is decreased it does not mean that it is lower than the upper bound! we need to check it
            dpf::cost_t h = ((DynamicSearchNode*)child)->get_h(); //static_cast<dpf::cost_t>((child->get_f() - child->get_g())/this->hscale_);
            if ((gval + h) >= this->upperBound) {
                //we still need to prune away the node
                return;
            }

            //we have reached a node in the fringe via a more direct path in the parent. Update priority of node
            child->relax(static_cast<warthog::cost_t>(gval), parent);
            //we need to remove it from the closed list and readd it to the open list
            child->set_expanded(false);
            this->open_->push(child);
            info("readded a node ", child->get_id(), "in closed list to open list! g=", gval, "h=", h, "f=", gval+this->hscale_ * h);
        }
    }

    virtual void onBeginNodeExpanded(const warthog::search_node* parent, const warthog::search_node* child, dpf::cost_t cost_to_n, bool& should_skip) {
        should_skip = false;
    }

};

#endif