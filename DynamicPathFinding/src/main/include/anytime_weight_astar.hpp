#ifndef _ANYTIME_WEIGHT_ASTAR_HEADER__
#define _ANYTIME_WEIGHT_ASTAR_HEADER__

#include "log.h"
#include "abstract_weighted_astar.hpp"

/**
 * @brief A basic A* implementation
 * 
 * This implementation does NOT feature:
 *  - pruning;
 *  - early stop;
 *  - bounded search;
 * 
 * @tparam H 
 * @tparam E 
 * @tparam STATS 
 */
template <typename H, typename E, typename STATS>
class AnytimeWeightPathFindingAStar : public AbstractWeightPathFindingAStar<H, E, STATS> {
public:
	AnytimeWeightPathFindingAStar(H* heuristic, E* expander, STATS* stats, bool enableStatistics, double hscale) : 
        AbstractWeightPathFindingAStar<H, E, STATS>{heuristic, expander, stats, enableStatistics, hscale},
        isUpperBoundPresent{false},
        upperBound{0},
        microSeconds{0}, anyTimeTimer{microSeconds, false} {
	}
	
	virtual ~AnytimeWeightPathFindingAStar() {

	}

protected:
    virtual void onGoalFound(const warthog::search_node* start, const warthog::search_node* goal, warthog::pqueue* openList, bool& keepGoing) {
        debug("setting keep going to true!");
        keepGoing = true;
        //we update the upperbound        
        this->isUpperBoundPresent = true;
        //the g of the new solution is the new upperbound.
        //the upperbound is NOT just f, because in f=g+wh. We need to set the upper bound to g!
        this->upperBound = goal->get_g();
        
        auto us = this->anyTimeTimer.EndTimer();

        dpf::cost_t h = ((DynamicSearchNode*)goal)->get_h(); //static_cast<dpf::cost_t>((goal->get_f() - goal->get_g())/this->hscale_);
		auto infos = AnyTimeInfo{
            goal->get_g(), 
            goal->get_g(), 
            h, 
            us, 
            false //here the goal is never optimal. We set the last bound as optimal when hte list becomes empty
        };
        this->anyTimeInfos.push_back(infos);
        this->anyTimeTimer.StartTimer();
    }

    virtual bool shouldWePruneNode(const warthog::search_node* start, const warthog::search_node* n, warthog::pqueue* openList, dpf::nodeid_t goalid) {
        if (!this->isUpperBoundPresent) {
            return false;
        }
        //we can't use f to compare against the upperbound because f=g+wh! We need to use f=g+h
        dpf::cost_t h = ((DynamicSearchNode*)n)->get_h(); //static_cast<dpf::cost_t>((n->get_f() - n->get_g())/this->hscale_);
        return (n->get_g() + h) >= this->upperBound;
    }

    virtual void onPrunedNode(const warthog::search_node* start, const warthog::search_node* n, warthog::pqueue* openList, dpf::nodeid_t goalid) {

    }

    virtual void onBeforeStartSearching(const warthog::search_node* start, dpf::nodeid_t goalid) {
        this->anyTimeInfos.clear();
        this->anyTimeTimer.StartTimer();
    }

    virtual void onOpenListEmpty(const warthog::search_node* start, const warthog::search_node* goal, dpf::cost_t goalid) {
        //list is empty. stop the timer but not add any solutio0n
        this->anyTimeTimer.EndTimer();
        //the last entry in anyTimeInfos is also the optimal path!
        if (!this->anyTimeInfos.empty()) {
            this->anyTimeInfos.back().isSolutionOptimal = true;
        }
    }

    virtual void onExpandedNodeInClosedList(warthog::search_node* parent, warthog::search_node* child, dpf::cost_t cost_to_n) {
        dpf::cost_t gval = parent->get_g() + cost_to_n;

        if(gval < child->get_g()) {
            //only because gval is decreased it does not mean that it is lower than the upper bound! we need to check it
            dpf::cost_t h = ((DynamicSearchNode*)child)->get_h(); //static_cast<dpf::cost_t>((child->get_f() - child->get_g())/this->hscale_);
            if (this->isUpperBoundPresent) {
                if ((gval + h) >= this->upperBound) {
                    //we still need to prune away the node
                    return;
                }
            }

            //we have reached a node in the fringe via a more direct path in the parent. Update priority of node
            child->relax(static_cast<warthog::cost_t>(gval), parent);
            //we need to remove it from the closed list and readd it to the open list
            child->set_expanded(false);
            this->open_->push(child);
            debug("readded a node ", child->get_id(), "in closed list to open list! g=", gval, "h=", h, "f=", gval+h);
        }

    }

    virtual void onBeginNodeExpanded(const warthog::search_node* parent, const warthog::search_node* child, dpf::cost_t cost_to_n, bool& should_skip) {
        should_skip = false;
    }

private:
    double microSeconds;
    SimpleTimer anyTimeTimer;
    bool isUpperBoundPresent;
    dpf::cost_t upperBound;
};

#endif