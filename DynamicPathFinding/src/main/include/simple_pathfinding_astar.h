/**
 * @file
 *
 * A standard implementation of A star
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#ifndef SIMPLE_PATHFINDING_ASTAR_H_
#define SIMPLE_PATHFINDING_ASTAR_H_

#include "weight_astar.hpp"

/**
 * @brief A basic A* implementation
 * 
 * This implementation does NOT feature:
 *  - pruning;
 *  - early stop;
 *  - bounded search;
 *  - weight on h;
 * 
 * @tparam H 
 * @tparam E 
 * @tparam STATS 
 */
template <typename H, typename E, typename STATS>
class SimplePathFindingAStar : public WeightPathFindingAStar<H, E, STATS> {
public:
	SimplePathFindingAStar(H* heuristic, E* expander, STATS* stats, bool enableStatistics) : 
		WeightPathFindingAStar<H, E, STATS>(heuristic, expander, stats, enableStatistics, 1.0) {
	}
	
	virtual ~SimplePathFindingAStar() {

	}
};

#endif /* SIMPLE_PATHFINDING_ASTAR_H_ */
