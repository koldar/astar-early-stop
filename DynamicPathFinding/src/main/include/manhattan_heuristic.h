/*
 * mangattan_heuristic.h
 *
 *  Created on: Oct 11, 2018
 *      Author: koldar
 */

#ifndef MANHATTAN_HEURISTIC_H_
#define MANHATTAN_HEURISTIC_H_

#include "cpd_heuristic.h"

class MahnattanHeuristic : public AbstractHeuristic {
public:
	MahnattanHeuristic(const Mapper& mapper);

	virtual dpf::cost_t h(dpf::nodeid_t startid, dpf::nodeid_t goalid) = 0;
	virtual void clear() = 0;
private:
	const Mapper& mapper;
};


#endif /* MANHATTAN_HEURISTIC_H_ */
