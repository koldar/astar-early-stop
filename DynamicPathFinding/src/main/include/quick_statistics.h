/**
 * @file
 *
 * Implementation of AbstractStats to be compliant with the A* implementation available in warthog library
 *
 *  Created on: Oct 17, 2018
 *      Author: koldar
 */

#ifndef QUICK_STATISTICS_H_
#define QUICK_STATISTICS_H_

#include "abstract_pathfinding_astar.h"

/**
 * A statistics which keeps track only of the number of nodes generated, touched or expanded
 */
class QuickStatistics : public AbstractStats {
public:
	QuickStatistics();
	~QuickStatistics();
public:
	/**
	 * reset the generated node statistic
	 */
	virtual void reset_nodes_generated();
	/**
	 * reset the expanded node statistic
	 */
	virtual void reset_nodes_expanded();
	/**
	 * reset the touched node statistic
	 */
	virtual void reset_nodes_touched();

	virtual void add_node_generated(const warthog::search_node& n);
	virtual void add_node_expanded(const warthog::search_node& n);
	virtual void add_node_touched(const warthog::search_node& n);
	virtual void reset_heuristic_time();
	virtual void add_to_heuristic_time(const warthog::search_node& n, unsigned long t);
	virtual unsigned long get_total_heuristic_time();

	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return Number of times we have expanded a node from the open list
	 */
	virtual int get_nodes_expanded() const;

	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return the number of nodes we have added inside the open list (initial node excluded)
	 */
	virtual int get_nodes_generated() const;

	/**
	 * @pre
	 *  @li you have run get_path or get_length
	 *
	 * @return the number that we have considered a node
	 */
	virtual int get_nodes_touched() const;
private:
	/**
	 * Number of times we have have decided to expand (i.e., compute successors) a node from the open list
	 */
	int nodes_expanded_;
	/**
	 * the number of nodes we have added inside the open list (initial node excluded)
	 */
	int nodes_generated_;
	/**
	 * the number that we have considered a node
	 *
	 * Every time we have performed some operation on a node, we increment the count
	 */
	int nodes_touched_;
	/**
	 * the total time spent computing the heuristic (in microseconds)
	 */
	unsigned long totalheuristicTime;
};



#endif /* QUICK_STATISTICS_H_ */
