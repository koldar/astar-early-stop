/**
 * @file
 *
 * A Datasource manager allowing you to load map data from an external `.map` file
 *
 * @date Oct 1, 2018
 * @author koldar
 */

#ifndef MAP_LOADER_H_
#define MAP_LOADER_H_

#include <vector>
#include <stdio.h>
#include <ctype.h>
#include "errors.h"
#include "types.h"
#include "gridmap.h"

using namespace std;


class AbstractMapLoader {
public:
	AbstractMapLoader() {};
	virtual ~AbstractMapLoader() {};

	/**
	 * Load a map
	 *
	 * @includedoc mapLayout.dox
	 *
	 * @param[in] fname the name fo the file <tt>.map</tt> to load
	 * @param[out] map value that will be populated with the data fetched from the file
	 * @param[out] width number of cell each row of the map is populated with
	 * @param[out] height number of cell each column of the map is populated with
	 */
	virtual GridMap LoadMap(const char* name);
protected:
	virtual dpf::celltype_t handleCell(const std::vector<dpf::celltype_t>& map, int width, int height, dpf::coo2d_t x, dpf::coo2d_t y, char symbolRead) = 0;
};

/**
 * class used to fetch data from an extenral map file.
 *
 * The file has only 2 cell types: traversible or untraversable.
 *
 * Any character not "." is automatically set to "untraversable"
 */
class MapLoader : public AbstractMapLoader {

public:

	/**
	 * Empty constructor
	 */
	MapLoader() {};
	/**
	 * Empty destructor
	 */
	virtual ~MapLoader() {};
protected:
	virtual dpf::celltype_t handleCell(const vector<dpf::celltype_t>& map, int width, int height, dpf::coo2d_t x, dpf::coo2d_t y, char symbolRead);

};


#endif /* MAP_LOADER_H_ */
