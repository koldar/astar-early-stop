#ifndef ENTRY_H
#define ENTRY_H

#include <string>
#include "xyLoc.h"
#include "mapper.h"
#include "adj_graph.h"
#include <warthog/constants.h>
#include "types.h"
#include "gridmap.h"

/**
 * preprocess a map
 *
 * @opst
 *  @li a file named @c filename will appear in the CWD
 *
 * @param[in] bits the map you need to preprocess
 * @param[in] width the width of the map
 * @param[in] height the height of the map
 * @param[in] filename the name of the file that will represent the CPD serialized version
 */
void PreprocessMap(std::vector<bool> &bits, int width, int height, const char *filename);
void PreprocessMap(const GridMap& map, const char *filename);
void *PrepareForSearch(std::vector<bool> &bits, int width, int height, const char *filename);
void *PrepareForSearch(const GridMap& map, const char *filename);

/**
 * A strategy allowing you to encdode GridMap in ListGraph by adjusting costs of terrains
 * depending on the neighbours
 */
int multiTerrainStrategy(const GridMap&, const Mapper&, xyLoc, xyLoc);

int singleTerrainStrategy(const GridMap&, const Mapper&, xyLoc, xyLoc);

const Mapper& GetMapper(const void* helper);

const AdjGraph& GetGraph(const void* helper);

/**
 * Remove from memory the pointer previosuly created with ::PrepareForSearch
 *
 * @param[in] helper the helper to remove from memory
 */
void DeleteHelperOfSearch(const void* helper);

/**
 * get the first optimal move to perform if you want to reach @c t starting from @c s
 *
 * @attention
 * The behaviour of this function is heavily influenced by the macro ::EXTRACT_ALL_AT_ONCE,
 * See @ref buildingMacros for further information
 *
 *
 * @param[in] data a pointer of type Start
 * @param[in] s the starting location
 * @param[in] t the goal we need to reach
 * @param[inout] an initialized vector that, after this call, will contain the first move.
 * 	The first move will be appended at the tail of the vector. The path is just a sequence of locations.
 * 	At index `i` is present where are we in the timestamp `i`
 * @return
 *  @li true if we have reached the goal;
 *  @li true if there is no path from @c s to @c t
 *  @li false if we have to perform other step other than the first one in order to reach @c t
 */
bool GetPath(void *data, xyLoc s, xyLoc g, std::vector<xyLoc> &path);

/**
 * Generate an optimal path using the CPD.
 *
 * @note
 * this function is a wrapper to the function GetPath which is compilation sensitive
 *
 * @param[inout] cpdHelper the helper created with ::PrepareForSearch
 * @param[in] start the initial position of the agent
 * @param[in] goal the final destination of the agent
 * @param[inout] path the built path. At index `i` the vector contains the cell where we need to go in order to reach the location @c goal
 * @return
 *  @li true if we ere able to find the optional path (thus @c path is altered);
 *  @li false if no path could be obtained (thus @c path is unchanged);
 */
bool GetPathWithCPD(void *cpdHelper, xyLoc start, xyLoc goal, std::vector<xyLoc> &path);

/**
 * like ::GetPathWithCPD but generates the cost of the path in the CPD as well
 *
 * @param[inout] cpdHelper the helper created with ::PrepareForSearch
 * @param[in] start the initial position of the agent
 * @param[in] goal the final destination of the agent
 * @param[out] pathCost the cost of the path @c path.
 * @param[out] moves the built path. Each index `i` represents the action the CPD told us to perform. Each action is the i-th OutArc we need to select
 * 	in the map we're in. So it's the second parameter of AdjGraph::getIthOutArc;
 * @param[inout] path the built path. At index `i` the vector contains the cell where we need to go in order to reach the location @c goal
 * @return
 *  @li true if we were able to find the optional path (thus @c path is altered);
 *  @li false if no path could be obtained (thus @c path is unchanged);
 */
bool GetPathDataAllAtOnceWithCPD(void* cpdHelper, xyLoc start, xyLoc goal, dpf::cost_t& pathCost, std::vector<dpf::move_t>& moves, std::vector<xyLoc>& path);

/**
 * like ::GetPathDataAllAtOnceWithCPD but generates the cost of the path in the CPD as well
 *
 * @param[inout] cpdHelper the helper created with ::PrepareForSearch
 * @param[in] start the initial position of the agent
 * @param[in] goal the final destination of the agent
 * @param[out] pathCost the cost of the path @c path.
 * @param[out] moves the built path. Each index `i` represents the action the CPD told us to perform. Each action is the i-th OutArc we need to select
 * 	in the map we're in. So it's the second parameter of AdjGraph::getIthOutArc;
 * @return
 *  @li true if we were able to find the optional path (thus @c path is altered);
 *  @li false if no path could be obtained (thus @c path is unchanged);
 */
bool GetPathDataCostAndMovesWithCPD(void* cpdHelper, xyLoc start, xyLoc goal, dpf::cost_t& pathCost, std::vector<dpf::move_t>& moves);

/**
 * Retrieve all the information about the next optimal move through the CPD
 *
 * This function is sucpposed to be run in a loop by altering @c start each time with the last position @c cells has
 *
 * @param[in] cpdHelper a structure aiding you in the first move computation
 * @param[in] start the position where the agent currently is
 * @param[in] goal the position where the agent would like to be
 * @param[inout] pathCost an accumulator variable used to store the cost fo the path we're generating
 * @param[inout] moves an accumulator storing all the action we're doing in order to go from @c start till @c goal
 * @param[out] nextCellId id of the next position where the CPD told us to go
 * @param[out] isPathTerminated true if no more calls of this function should be performed, false otheriwsE;
 * @return
 *  @li true if we've reached the @c goal;
 *  @li false otherwise
 */
bool GetPathInformationNextMoveWithCPD(void* cpdHelper, xyLoc start, xyLoc goal, dpf::cost_t& pathCost, std::vector<dpf::move_t>& moves, uint32_t& nextCellId, bool& isPathTerminated);

/**
 * retrieves the cost of the optimal path after determine the next move of the path itself
 *
 * The function computes what is the cost of the **first move** of the optimal path from going from @c start till @c goal.
 *
 * @note
 * in general the cost fo the path and the length of the plan are different things.
 *
 * @param[inout] cpdHelper a pointer used to interact with the cpd
 * @param[in] start the starting location where the agent is
 * @param[in] goal the final destination of the agent
 * @param[out] pathCost place where to put the cost of the first move of the path generated by the CPD
 * @param[out] isPathTerminated true if no more actions can be perform in order to reach the goal; false otherwise
 * @return
 *  @li true if we have reached the goal
 *  @li false if otherwise;
 */
bool GetPathLengthNextMoveWithCPD(void* cpdHelper, xyLoc start, xyLoc goal, dpf::cost_t& pathCost, bool& isPathTerminated);

/**
 * like ::GetPathLengthNextMoveWithCPD but generates the whole path in one sweep
 *
 * @param[inout] cpdHelper a pointer used to interact with the cpd
 * @param[in] start the starting location where the agent is
 * @param[in] goal the final destination of the agent
 * @param[out] pathCost place where to put the cost of the first move of the path generated by the CPD
 * @return
 *  @li true if it's possible to reach @c goal from @c start
 *  @li false otherwise
 */
bool getPathLengthAllAtOnceWithCPD(void* cpdHelper, xyLoc start, xyLoc goal, dpf::cost_t& pathCost);

const char *GetName();

/**
 * Draw a representation of a map with a path embedded into it
 *
 * @param[inout] str the stream where to write the data on
 * @param[in] map the map where to write the path on
 * @param[in] width the number of columns in the map
 * @param[in] height the number of rows in the map
 * @param[in] path the path to write
 * @param[in] pathSymbol the character to use to print the elements in the path (start and goal locations are excluded though)
 */
void drawMapWithPath(std::ostream& str, const std::vector<bool>& map, int width, int height, const std::vector<xyLoc>& path, char pathSymbol='*');

/**
 * generate an image representing the first best move of the cpd
 *
 * @post
 *  @li an image is present in CWD
 *
 * @param[in] mapName the filename containing the map to print
 * @param[in] cpdFilename a filename specifying which is the file containing the cpd associated to @c mapName
 * @param[in] goal the target the agent aims to reach
 * @param[in] basename the base name of the image to generate
 */
void printCPD(const char* mapName, const char* cpdFilename, xyLoc goal, const std::string& basename);

#endif
