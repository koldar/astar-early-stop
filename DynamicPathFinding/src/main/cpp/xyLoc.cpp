/*
 * xyLoc.cpp
 *
 *  Created on: Oct 8, 2018
 *      Author: koldar
 */


#include "xyLoc.h"
#include "errors.h"
#include <cmath>

const char* getLabel(const Direction& dir) {
	switch (dir) {
	case Direction::NORTHWEST: { return "NW"; }
	case Direction::NORTH: { return "N"; }
	case Direction::NORTHEAST: { return "NE";}
	case Direction::WEST: { return "W"; }
	case Direction::EAST: { return "E"; }
	case Direction::SOUTHWEST: { return "SW"; }
	case Direction::SOUTH: { return "S"; }
	case Direction::SOUTHEAST: { return "SE"; }
	default:
		throw new CpdException{"invalid direction ", dir};
	}
}

Direction xyLoc::getDirectionTo(const xyLoc& to) const {
	return xyLoc::getDirection(*this, to);
}

Direction xyLoc::getDirection(const xyLoc& from, const xyLoc& to) {
	if ((from.x < to.x) && (from.y < to.y)) {
		return Direction::SOUTHEAST;
	} else if ((from.x == to.x) && (from.y < to.y)) {
		return Direction::SOUTH;
	} else if ((from.x > to.x) && (from.y < to.y)) {
		return Direction::SOUTHWEST;
	} else if ((from.x < to.x) && (from.y == to.y)) {
		return Direction::EAST;
	} else if ((from.x > to.x) && (from.y == to.y)) {
		return Direction::WEST;
	} else if ((from.x < to.x) && (from.y > to.y)) {
		return Direction::NORTHEAST;
	} else if ((from.x == to.x) && (from.y > to.y)) {
		return Direction::NORTH;
	} else if ((from.x > to.x) && (from.y > to.y)) {
		return Direction::NORTHWEST;
	}
	throw new CpdException{"invalid cell locations", from, to};
}

bool xyLoc::isAdjacentTo(const xyLoc& other) const {
	//we check both because xyLoc has unsigned integer coordinates, hence if pos1=(4,6) and pos2=(5,6) if we do pos1.x-pos2.x we obtain -1
	//but being unsigned we obtain UINT_MAX-1 (a very big number)

	return ((std::abs(static_cast<int>(this->x - other.x)) <= 1) || (std::abs(static_cast<int>(other.x - this->x)) <= 1)) &&
			((std::abs(static_cast<int>(this->y - other.y)) <= 1) || (std::abs(static_cast<int>(other.y - this->y)) <= 1));
}

