#include "abstract_pathfinding_astar.h"

std::ostream& operator<< (std::ostream& stream, const AnyTimeInfo& ati) {
    stream << "{ us=" << ati.microSeconds <<" cost=" << ati.solutionCost << " optimal=" << ati.isSolutionOptimal << " }";
    return stream;
}