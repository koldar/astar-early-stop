/*
 * gridmap_astar_supports.cpp
 *
 *  Created on: Oct 2, 2018
 *      Author: koldar
 */

#include "cpd_heuristic.h"
#include "Entry.h"
#include "file_utils.h"
#include "log.h"
#include "simpleTimer.h"

CpdHeuristic::CpdHeuristic(const GridMap& map, const char* filename) : map{map}, filename{filename}, moves{}, pathCost{0}, pathGenerated{false},
searchHelper{nullptr}, destroySearchHelper{true} {
	//TODO add const to first parameter of PrepareForSearch: it doesn't alter it!

	//we create the CPD if not already done

	if (!isFileExists(filename)) {
		debug("generating the CPD for the heuristic... width=", map.getWidth(), ", height=", map.getHeight(), ", mapName=", filename);
		double microSeconds = 0;
		PROFILE_TIME_CODE(microSeconds) {
			PreprocessMap(map, filename);
		}
		critical("we used ", microSeconds, "to generate the CPD! Storing it in ", filename);
	}
	searchHelper = PrepareForSearch(map, filename);
}

CpdHeuristic::CpdHeuristic(const std::vector<bool>& rawMapData, int width, int height, const char* filename) :
		CpdHeuristic{GridMap{rawMapData, width, height, 1000}, filename} {
}

CpdHeuristic::CpdHeuristic(const CpdHeuristic& h) :
start{h.start}, goal{h.goal},
pathCost{h.pathCost}, moves{h.moves}, pathGenerated{h.pathGenerated}, searchHelper{h.searchHelper},
filename{h.filename}, map{h.map}, destroySearchHelper{false} {

}

CpdHeuristic::~CpdHeuristic() {
	if (destroySearchHelper) {
		if (searchHelper != nullptr) {
			DeleteHelperOfSearch(searchHelper);
		}
	}
}

dpf::cost_t CpdHeuristic::h(uint32_t startid, uint32_t goalid) {

	this->start = this->getMapper()(startid);
	this->goal = this->getMapper()(goalid);
	this->pathCost = 0;
	this->moves.clear();

	this->pathGenerated = GetPathDataCostAndMovesWithCPD(
			this->searchHelper,
			this->start, this->goal,
			this->pathCost,
			this->moves
	);

	if (!this->pathGenerated) {
		this->pathCost = dpf::cost_t::getUpperBound();
		this->moves.clear();
	}

	return this->pathCost;
}


bool CpdHeuristic::hasHeuristicComputedAPath() const {
	return this->pathGenerated;
}

bool CpdHeuristic::isCPDPathClearFromArcModifications(const AdjGraph& newMap) const {
	return !this->doesCPDPathHaveARevisedArcIn(newMap);
//	dpf::cost_t pathCostNewMap = getCPDPathCostInMap(newMap);
//	debug("optimal path cost from ", this->start, "to", this->goal, "is", this->pathCost, " while optimal path cost in the revised map is", pathCostNewMap);
//	return this->pathCost == pathCostNewMap;
}


dpf::cost_t CpdHeuristic::getCPDPathCost() const {
	return this->pathCost;
}

bool CpdHeuristic::doesCPDPathHaveARevisedArcIn(const AdjGraph& newMap) const {
	if (!this->hasHeuristicComputedAPath()) {
		return false;
	}

	if (this->moves.size() == 0) {
		//path was generated by with no cell movement
		return 0;
	}

	dpf::nodeid_t vertexId = this->getMapper()(this->start);
	debug("path is ");
	for (auto it=this->moves.begin(); it != this->moves.end(); ++it) {
		assert(newMap.getIthOutArc(vertexId, *it).weight >= this->getGraph().getIthOutArc(vertexId, *it).weight);
		if (newMap.getIthOutArc(vertexId, *it).weight != this->getGraph().getIthOutArc(vertexId, *it).weight) {
			return true;
		}
		vertexId = newMap.getIthOutArc(vertexId, *it).target;
	}
	debug(" -> ", "vertex", vertexId, this->getMapper()(vertexId));
	assert(this->getMapper()(vertexId) == this->goal);

	return false;
}


dpf::cost_t CpdHeuristic::getCPDPathCostInMap(const AdjGraph& newMap) const {
	debug("calling getCPDPathCostInMap in heuristic");
	if (!this->hasHeuristicComputedAPath()) {
		return dpf::cost_t::getUpperBound();
	}

	if (this->moves.size() == 0) {
		//path was generated by with no cell movement
		return 0;
	}

	dpf::nodeid_t vertexId = this->getMapper()(this->start);
	dpf::cost_t result = 0;
	debug("path is ");
	for (auto it=this->moves.begin(); it != this->moves.end(); ++it) {
		//debug("going from", vertexId, "(", this->getMapper()(*it), ") to", newMap.getIthOutArc(vertexId, *it).target, "(", this->getMapper()(newMap.getIthOutArc(vertexId, *it).target), ")");
		result += newMap.getIthOutArc(vertexId, *it).weight;
		debug(" -> ", "vertex", vertexId, this->getMapper()(vertexId), "move weight", newMap.getIthOutArc(vertexId, *it).weight, "move original weight", this->getGraph().getIthOutArc(vertexId, *it).weight);
		vertexId = newMap.getIthOutArc(vertexId, *it).target;
	}
	debug(" -> ", "vertex", vertexId, this->getMapper()(vertexId));
	assert(this->getMapper()(vertexId) == this->goal);

	return result;
}

dpf::cost_t CpdHeuristic::getCPDPathCostInMap(const AdjGraph& newMap, dpf::nodeid_t n, dpf::nodeid_t goal) {
	auto oldMoves{this->moves};
	auto oldStart{this->start};
	auto oldGoal{this->goal};
	auto oldPathCost{this->pathCost};
	auto oldPathGenerated{this->pathGenerated};
	
	dpf::cost_t h = this->h(n, goal);
	auto result = this->getCPDPathCostInMap(newMap);

	this->moves = oldMoves;
	this->start = oldStart;
	this->goal = oldGoal;
	this->pathCost = oldPathCost;
	this->pathGenerated = oldPathGenerated;

	return result;
}

std::vector<dpf::move_t> CpdHeuristic::getOptimalPath(xyLoc start, xyLoc goal) const {
	std::vector<dpf::move_t> result;
	dpf::cost_t pathCost;
	GetPathDataCostAndMovesWithCPD(
			this->searchHelper,
			start, goal,
			pathCost,
			result
	);

	debug("resul is", result);

	return result;
}






