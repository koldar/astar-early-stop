/*
 * pseudoeuclidean_heuristic.cpp
 *
 *  Created on: Oct 11, 2018
 *      Author: koldar
 */


#include "pseudoeuclidean_heuristic.h"
#include "xyLoc.h"
#include <cmath>

PseudoEuclideanHeuristic::PseudoEuclideanHeuristic(const Mapper& mapper) : mapper{mapper} {

}

dpf::cost_t PseudoEuclideanHeuristic::h(dpf::nodeid_t startid, dpf::nodeid_t goalid) {

	xyLoc start = mapper(startid);
	xyLoc goal = mapper(goalid);

	dpf::coo2d_t width = std::abs(static_cast<double>(start.x - goal.x));
	dpf::coo2d_t height = std::abs(static_cast<double>(start.y - goal.y));

	if (width == height) {
		return width*std::sqrt(2);
	}

	dpf::coo2d_t minSide = std::min(width, height);
	dpf::coo2d_t maxSide = std::max(width, height);

	return minSide * std::sqrt(2) + (maxSide - minSide);
}

void PseudoEuclideanHeuristic::clear() {

}
