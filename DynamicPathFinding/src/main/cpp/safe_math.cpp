/*
 * safe_math.cpp
 *
 *  Created on: Oct 17, 2018
 *      Author: koldar
 */


#include "safe_math.h"

safe_int::safe_int(const safe_int& a) : var{a.var} {

}

safe_int& safe_int::operator+=(const safe_int& a) {
	*this = *this + a;
	return *this;
}

safe_int& safe_int::operator-=(const safe_int& a) {
	*this = *this - a;
	return *this;
}

safe_int& safe_int::operator*=(const safe_int& a) {
	*this = *this * a;
	return *this;
}

safe_int& safe_int::operator/=(const safe_int& a) {
	*this = *this / a;
	return *this;
}

safe_int& safe_int::operator=(const safe_int& a) {
	this->var = a.var;
	return *this;
}

safe_int& safe_int::operator = (int a) {
	this->var = static_cast<long>(a);
	return *this;
}

safe_int::operator int() {
	return static_cast<int>(this->var);
}

safe_int::operator unsigned long long() {
	return static_cast<unsigned long long>(this->var);
}

safe_int::operator long long() {
	return static_cast<long long>(this->var);
}

safe_int::operator uint32_t() {
	return static_cast<uint32_t>(this->var);
}

safe_int::operator uint32_t() const {
	return static_cast<uint32_t>(this->var);
}

safe_int::operator float() {
	return static_cast<float>(this->var);
}

safe_int::operator double() {
	return static_cast<double>(this->var);
}

safe_int operator+(const safe_int& a, const safe_int& b) {
	if ((a.var + b.var) > safe_int::getUpperBound()) {
		return safe_int::getUpperBound();
	}
	if ((a.var + b.var) < safe_int::getLowerBound()) {
		return safe_int::getLowerBound();
	}

	return safe_int{a.var + b.var};

//	//FIXME Note: this is compiler dependent! It is supported starting from GCC 5
//	safe_int result;
//	if (__builtin_add_overflow(a.var, b.var, &result.var)) {
//		//overflow detected
//		result.var = a.var > 0 ? INT_MAX : INT_MIN;
//	}
//
//	return result;
}

safe_int operator +(const safe_int& a, const int& b) {
	return a + safe_int{b};
}

safe_int operator +(const safe_int& a, const uint32_t& b) {
	return a + safe_int{b};
}

safe_int operator +(const safe_int& a, const double& b) {
	return a + safe_int{b};
}

safe_int operator-(const safe_int& a, const safe_int& b) {
	if ((a.var - b.var) > safe_int::getUpperBound()) {
		return safe_int::getUpperBound();
	}
	if ((a.var - b.var) < safe_int::getLowerBound()) {
		return safe_int::getLowerBound();
	}

	return safe_int{a.var - b.var};

//	//FIXME Note: this is compiler dependent! It is supported starting from GCC 5
//	safe_int result;
//	if (__builtin_sub_overflow (a.var, b.var, &result.var)) {
//		//overflow detected
//		result.var = a.var < 0 ? INT_MAX : INT_MIN;
//	}
//
//	return result;
}

safe_int operator -(const safe_int& a, const int& b) {
	return a - safe_int{b};
}

safe_int operator -(const safe_int& a, const uint32_t& b) {
	return a - safe_int{b};
}

safe_int operator -(const safe_int& a, const double& b) {
	return a - safe_int{b};
}

safe_int operator*(const safe_int& a, const safe_int& b) {
	if ((a.var * b.var) > safe_int::getUpperBound()) {
		return safe_int::getUpperBound();
	}
	if ((a.var * b.var) < safe_int::getLowerBound()) {
		return safe_int::getLowerBound();
	}

	return safe_int{a.var * b.var};

//	//FIXME Note: this is compiler dependent! It is supported starting from GCC 5
//	safe_int result;
//	if (__builtin_mul_overflow (a.var, b.var, &result.var)) {
//		//overflow detected
//		if ((a.var > 0 && b.var > 0) || (a.var < 0 && b.var < 0)) {
//			result.var = INT_MAX;
//		} else {
//			result.var = INT_MIN;
//		}
//	}
//
//	return result;
}

safe_int operator *(const safe_int& a, const int& b) {
	return a * safe_int{b};
}

safe_int operator *(const safe_int& a, const uint32_t& b) {
	return a * safe_int{b};
}

safe_int operator *(const safe_int& a, const double& b) {
	return a * safe_int{b};
}

safe_int operator /(const safe_int& a, const safe_int& b) {
	//division between integer cannot lead to under/over flow
	return safe_int{a.var / b.var};
}

safe_int operator /(const safe_int& a, const int& b) {
	return a / safe_int{b};
}

safe_int operator /(const safe_int& a, const uint32_t& b) {
	return a / safe_int{b};
}

safe_int operator /(const safe_int& a, const double& b) {
	return a / safe_int{b};
}

bool operator==(const safe_int& a, const safe_int& b) {
	return a.var == b.var;
}

bool operator!=(const safe_int& a, const safe_int& b) {
	return a.var != b.var;
}

bool operator<=(const safe_int& a, const safe_int& b) {
	return a.var <= b.var;
}

bool operator<(const safe_int& a, const safe_int& b) {
	return a.var < b.var;
}

bool operator>=(const safe_int& a, const safe_int& b) {
	return a.var >= b.var;
}

bool operator>(const safe_int& a, const safe_int& b) {
	return a.var > b.var;
}

std::ostream& operator << (std::ostream& stream, const safe_int& a) {
	stream << a.var;
	return stream;
}
