/*
 * dijkstra_algorithm.cpp
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */


#include "dijkstra_algorithm.h"

DijsktraAlgorithm::DijsktraAlgorithm(const AdjGraph& g) : g{g}, distances{}, q{static_cast<int>(g.node_count())} {
	this->reset();
}

DijsktraAlgorithm::~DijsktraAlgorithm() {

}

void DijsktraAlgorithm::reset() {
	distances.resize(g.node_count());
	q.clear();
	std::fill(distances.begin(), distances.end(), dpf::cost_t::getUpperBound());
}

bool DijsktraAlgorithm::run(dpf::nodeid_t source) {
	this->reset();

	this->distances[source] = 0;

	for(int i=0; i<g.out_deg(source); ++i)
	{
		OutArc a = g.getIthOutArc(source, i);
		reach(static_cast<dpf::nodeid_t>(a.target), 0 + a.weight, source);
	}

	while(!q.empty())
	{
		dpf::nodeid_t x = q.pop();
		//debug("popping out", x);

		for(auto a : g.out(x))
		{
			reach(a.target, distances[x] + a.weight, x);
		}
	}

	DO_ON_DEBUG
	{
		for(int u=0; u<g.node_count(); ++u)
		{
			for(auto uv : g.out(u))
			{
				int v = uv.target;
				assert(distances[u] >= (distances[v] - uv.weight));
			}
		}
	}

	return true;
}

const std::vector<dpf::cost_t>& DijsktraAlgorithm::getDistances() const {
	return this->distances;
}

void DijsktraAlgorithm::reach(dpf::nodeid_t v, dpf::cost_t d, dpf::nodeid_t bestPrevious) {
    if(d < distances[v])
    {
        q.push_or_decrease_key(static_cast<int>(v), d);
        distances[v] = d;
    }
}
