/*
 * map_loader_factory.cpp
 *
 *  Created on: 05 nov 2018
 *      Author: massimobono
 */


#include "map_loader_factory.h"
#include "multi_map_loader.h"
#include "types.h"

MapLoaderFactory::MapLoaderFactory() {

}

AbstractMapLoader* MapLoaderFactory::get(const char* filename) const {
	std::unordered_set<char> symbolsInFile = this->symbolsInFile(filename);

	bool containsDot = symbolsInFile.find('.') != symbolsInFile.end();
	bool containsAt = symbolsInFile.find('@') != symbolsInFile.end();
	bool containsT = symbolsInFile.find('T') != symbolsInFile.end();
	bool containsS = symbolsInFile.find('S') != symbolsInFile.end();
	bool containsW = symbolsInFile.find('W') != symbolsInFile.end();

	debug(filename, "contains: .=", containsDot, "@=", containsAt, "T=", containsT, "S=", containsS, "W=", containsW);

	std::string filenamStr{filename};

	if (containsDot && containsAt && containsT && containsS && containsW) {
		MultiColorMapLoader* loader = new MultiColorMapLoader{};
		debug("picking MultiColorMapLoader!");

		loader->addTraversableColor('.', dpf::celltype_t{1000})
		.addTraversableColor('T', dpf::celltype_t{1500})
		.addTraversableColor('S', dpf::celltype_t{2000})
		.addTraversableColor('W', dpf::celltype_t{2500})
		.addUntraversableColor('@')
		;
		return loader;
	} else if (filenamStr.find("hrt201n") != std::string::npos) {
		//we tested hrt201n considering green cells... technically all this ifs should be replaed ti the MultiColorMapLoader
		return new MapLoader{};
	} else {
		//by deault we enable the multi terrain
		MultiColorMapLoader* loader = new MultiColorMapLoader{};
		debug("picking MultiColorMapLoader!");

		loader->addTraversableColor('.', dpf::celltype_t{1000})
		.addTraversableColor('T', dpf::celltype_t{1500})
		.addTraversableColor('S', dpf::celltype_t{2000})
		.addTraversableColor('W', dpf::celltype_t{2500})
		.addUntraversableColor('@')
		;
		return loader;
	}


	throw CpdException{"cannot establish the correct map loader! we're in function %s", __func__};
}

std::unordered_set<char> MapLoaderFactory::symbolsInFile(const char* filename) const {
	FILE *f;
	int useless;
	int width;
	int height;
	std::unordered_set<char> result;

	f = fopen(filename, "r");
	if (f == nullptr) {
		throw CpdException{"file %s not found!", filename};
	}

	useless = fscanf(f, "type octile\nheight %d\nwidth %d\nmap\n", &height, &width);
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			char c;
			do {
				useless = fscanf(f, "%c", &c);
			} while (isspace(c));

			result.insert(c);
		}
	}
	fclose(f);
	return result;
}

