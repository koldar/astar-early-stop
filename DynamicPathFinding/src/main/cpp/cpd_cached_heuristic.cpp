/*
 * cpd_cached_heuristic.cpp
 *
 *  Created on: Oct 5, 2018
 *      Author: koldar
 */

#include "cpd_cached_heuristic.h"
#include <stack>

constexpr dpf::cost_t CpdCacheHeuristic::NO_HEURISTIC;


CpdCacheHeuristic::CpdCacheHeuristic(const std::vector<bool>& rawMapData, int width, int height, const char* filename, const AdjGraph& newMap) :
CpdHeuristic{rawMapData, width, height, filename},
costs{},
newMap{newMap} {
	this->costs = std::vector<internal::cache_cell>{(unsigned int)this->getMapper().node_count(), internal::cache_cell{NO_HEURISTIC, NO_HEURISTIC}};
}

CpdCacheHeuristic::CpdCacheHeuristic(const CpdHeuristic& h, const AdjGraph& newMap) : CpdHeuristic{h},
		newMap{newMap}, costs{(unsigned int)this->getMapper().node_count(), internal::cache_cell{NO_HEURISTIC, NO_HEURISTIC}} {
}

CpdCacheHeuristic::~CpdCacheHeuristic() {
}


dpf::cost_t CpdCacheHeuristic::h(dpf::nodeid_t startid, dpf::nodeid_t goalid) {

	this->start = this->getMapper()(startid);
	this->goal = this->getMapper()(goalid);

	//update goal costs
	this->updateCachedHeuristic(goalid, 0, 0);

	//we already have cache the value. We can simply retrieve it!
	if (this->hasVertexHasHeuristic(startid)) {
		debug(startid, "has already an heuristic associated!");
		this->pathCost = this->getCachedHeuristic(startid);
		return this->pathCost;
	}

	this->pathCost = 0;
	this->moves.clear();
	bool isPathTerminated;
	std::stack<std::pair<dpf::nodeid_t, internal::cache_cell>> nodeCosts;
	dpf::nodeid_t tmpStartid = startid;
	dpf::nodeid_t nextPositionId;

	//variable used to represents the original and the perturbated cost of the node which has a cached heuristic/revised cost
	dpf::cost_t originalCumulativePathCost = 0;
	dpf::cost_t newCumulativePathCost = 0;

	while (true) {

		if (this->hasVertexHasHeuristic(tmpStartid)) {
			debug(tmpStartid, "has already an heuristic associated which is", this->getCachedHeuristic(tmpStartid), "(the cost in the revised map is ", this->getCachedCostInNewMap(tmpStartid), "instead)");
			//we reached a location with an heuristic cached! We've finished!
			this->pathCost += this->getCachedHeuristic(tmpStartid);

			//ok, we need to update the cache of every heuristic in nodeCosts
			originalCumulativePathCost = this->getCachedHeuristic(tmpStartid);
			newCumulativePathCost = this->getCachedCostInNewMap(tmpStartid);
			goto cache_heuristic_found;

		} else {
			//we don't have the heuristic value in cache. We need to compute it!
			debug(tmpStartid, "does not have an heuristic associated. Compute it!");
			dpf::cost_t originalCost = 0;
			//generate the heuristic value
			bool isGoalReached = GetPathInformationNextMoveWithCPD(
				this->searchHelper,
				this->getMapper()(tmpStartid), this->goal,
				originalCost, this->moves, nextPositionId,
				isPathTerminated
			);
			//keep track of the values in newMap as well
			this->pathCost += originalCost;


			if (!isGoalReached && isPathTerminated) {
				//there is no solution

				this->pathCost = dpf::cost_t::getUpperBound();
				this->pathGenerated = false;

				return this->pathCost;
			} else if (!isGoalReached && !isPathTerminated) {
				debug("pippo");
				dpf::cost_t newCost = this->newMap.getWeightOfIthArc(tmpStartid, this->moves.back());
				//we haven't reached the goal yet, but we can continue polling GetPathInformatioNextMoveWithCPD
				debug("the CPD tells that we need to move from", tmpStartid, "to ", this->newMap.getIthOutArc(tmpStartid, this->moves.back()).target, "(which is", this->getMapper()(this->newMap.getIthOutArc(tmpStartid, this->moves.back()).target), ")");
				debug("pushing to stack the node", tmpStartid, ". original=", originalCost, "newCost=", newCost);
				debug("nextPositionId is", nextPositionId);
				nodeCosts.push(std::pair<uint32_t, internal::cache_cell>{
					tmpStartid, internal::cache_cell{
						originalCost,
						newCost
					}
				});
				tmpStartid = nextPositionId;
			} else if (isGoalReached) {
				//we reached the goal before reaching a location with a cache heuristic
				assert(isPathTerminated);

				//the heuristic cost can be easily found
				originalCumulativePathCost = 0;
				newCumulativePathCost = 0;
				goto cache_heuristic_found;
			}

		}


	}

	cache_heuristic_found:;
	debug("found a node with a cached heuristic set. Now we update all the vertices with no heuristic found up until now");
	//For every location in nodeCosts, we update the cache

	/*
	 * now we can use nodeCosts to update our cache, since they represents the optimal paths cost.
	 * For example suppose that the path is : A -> 10 -> B -> 5 -> C -> 7 -> GOAL.
	 * On the heuristicValues will be (in popping order) 7,5,10.
	 * So the heuristic of:
	 * - C->GOAL is 7 (7);
	 * - B->GOAL is 12 (7+5);
	 * - A->GOAL is 22 (7+5+10);
	 *
	 */


	while (!nodeCosts.empty()) {
		auto tmp = nodeCosts.top();
		nodeCosts.pop();

		originalCumulativePathCost += tmp.second.originalMapCost;
		newCumulativePathCost += tmp.second.newMapCost;
		this->updateCachedHeuristic(tmp.first, originalCumulativePathCost, newCumulativePathCost);
	}

	this->pathGenerated = true;

	return this->pathCost;
}

dpf::cost_t CpdCacheHeuristic::getCPDPathCostInMap(const AdjGraph& newMap) const {
	debug("calling getCPDPathCostInMap in cache heuristic");
	assert(this->hasVertexHasHeuristic(this->getMapper()(this->start)));
	return this->costs[this->getMapper()(this->start)].newMapCost;
}

dpf::cost_t CpdCacheHeuristic::getCPDPathCostInMap(const AdjGraph& newMap, dpf::nodeid_t n, dpf::nodeid_t goal) {
	if (goal != this->getMapper()(this->goal)) {
		error("goal given:", goal, "goal of the map", this->getMapper()(this->goal));
		throw std::domain_error{"goal id not the one the mapper think it is!"};
	}

	debug("calling getCPDPathCostInMap in cache heuristic");
	assert(this->hasVertexHasHeuristic(n));
	return this->costs[n].newMapCost;
}

bool CpdCacheHeuristic::doesCPDPathHaveARevisedArcIn(const AdjGraph& newMap) const {
	debug("in cpd cache heuristic of does a cpd path have a revised arc?");
	assert(this->hasVertexHasHeuristic(this->getMapper()(this->start)));
	debug("the outcome is: new cost:", this->costs[this->getMapper()(this->start)].newMapCost, "original:", this->costs[this->getMapper()(this->start)].originalMapCost);
	return this->costs[this->getMapper()(this->start)].newMapCost > this->costs[this->getMapper()(this->start)].originalMapCost;
}
