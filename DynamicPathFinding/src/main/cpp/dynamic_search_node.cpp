/*
 * dynamic_search_node.cpp
 *
 *  Created on: Oct 22, 2018
 *      Author: koldar
 */

#include "dynamic_search_node.h"

std::ostream& operator<< (std::ostream& stream, const DynamicSearchNode& sn) {
	stream << warthog::search_node(sn).get_id() << "lead to optimal solution: " << sn.leadToOptimalSolution;
	return stream;
}


