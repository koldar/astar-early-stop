#include "earlystop_dijkstra.h"
#include "pbmImage.h"
#include <functional>

EarlyStopDijkstra::EarlyStopDijkstra(const AdjGraph& g, bool enableStatistics) : g{g}, q{static_cast<int>(g.node_count())}, distancesFromSource{}, bestPreviousNode{}, enableStatistics{enableStatistics}, expandedNodes{}, expandedNodeNumber{0}
{
    distancesFromSource.resize(g.node_count());
    bestPreviousNode.resize(g.node_count());

    //debug("distance from sources", distancesFromSource);
}

EarlyStopDijkstra::~EarlyStopDijkstra()
{

}

std::vector<dpf::nodeid_t> EarlyStopDijkstra::run(dpf::nodeid_t start, dpf::nodeid_t goal)
{

    this->clear();

    if (this->enableStatistics)
    {
        this->expandedNodes = std::vector<dpf::nodeid_t> {};
    }

    this->expandedNodeNumber = 0;
    this->distancesFromSource[start] = 0;
    this->bestPreviousNode[start] = 0;

    if (start == goal)
    {
        return this->bestPreviousNode;
    }

    for(int i=0; i<g.out_deg(start); ++i)
    {
        OutArc a = g.out(start, i);
        reach(static_cast<dpf::nodeid_t>(a.target), 0 + a.weight, start);
    }

    while(!q.empty())
    {
        dpf::nodeid_t x = q.pop();
        //debug("popping out", x);

        if (this->enableStatistics)
        {
            this->expandedNodes.push_back(x);
        }
        expandedNodeNumber += 1;

        if (x == goal)
        {
            return this->bestPreviousNode;
        }

        for(auto a : g.out(x))
        {
            reach(a.target, distancesFromSource[x] + a.weight, x);
        }
    }

    DO_ON_DEBUG
    {
        for(int u=0; u<g.node_count(); ++u)
        {
            for(auto uv : g.out(u))
            {
                int v = uv.target;
                assert(distancesFromSource[u] >= (distancesFromSource[v] - uv.weight));
            }
        }
    }

    return bestPreviousNode;
}

std::vector<xyLoc> EarlyStopDijkstra::getPathAsVector(dpf::nodeid_t start, dpf::nodeid_t goal, const Mapper& mapper, const std::vector<dpf::nodeid_t>& optimalMoves)
{
    std::vector<xyLoc> result{};
    dpf::nodeid_t tmp = goal;

    result.push_back(mapper(tmp));

    //debug("start!!!!");
    while (tmp != start)
    {
        //debug("tmp is", tmp, "aka", mapper(tmp), ". the optimal node is", optimalMoves[tmp], "aka", mapper(optimalMoves[tmp]));
        dpf::nodeid_t prev = optimalMoves[tmp];
        DO_ON_DEBUG {
            if (prev == tmp) {
                error("prev ", prev, "is the same of ", tmp);
            }
        }
        assert(prev != tmp);
        
        //debug("prev is", prev, "aka ", mapper(prev));
        result.push_back(mapper(prev));
        tmp = prev;
    }
    //debug("done!");

    std::reverse(result.begin(), result.end());
    return result;
}

const std::vector<dpf::nodeid_t>& EarlyStopDijkstra::getExpandedNodes() const
{
	if (!this->enableStatistics) {
		throw std::domain_error{"enableStatistics is turned off! You can't use this variable!"};
	}
    return this->expandedNodes;
}

dpf::big_integer EarlyStopDijkstra::getExpandedNodesNumber() const {
	return this->expandedNodeNumber;
}


PPMImage EarlyStopDijkstra::printExpandedNodesImage(const std::string& filename, dpf::nodeid_t startNode, dpf::nodeid_t goalNode, const Mapper& mapper, const GridMap& map, const std::vector<dpf::nodeid_t>& path, const PPMImage* imageToUpdate) const
{
    if (!this->enableStatistics) {
        throw std::invalid_argument{"you need to enable statistics in Dijkstra in order to print the image!"};
    }
    PPMImage result{this->g.getImageWith(mapper, map, startNode, goalNode, expandedNodes, color_t{150,150,0}, GREEN, BLUE)};
    //draw path
    PPMImage tmp1{this->g.getImageWith(mapper, map, startNode, goalNode, path, PURPLE, GREEN, BLUE)};
    result += tmp1;
    if (imageToUpdate != nullptr) {
        result += *imageToUpdate;
    }
    return result;

}


/**
 * @brief 
 * 
 * @param v sink of an edge
 * @param d least cost you need to pay to reach `v` by going through the edge starting from the start vertex
 * @param bestPrevious source of the edge. It also represents the best way know to reach optimally `v`.
 */
void EarlyStopDijkstra::reach(dpf::nodeid_t v, dpf::cost_t d, dpf::nodeid_t bestPrevious)
{
    //debug("v=", v, "d=", d, "bestPrevious=", bestPrevious);
    if(d < distancesFromSource[v])
    {
        q.push_or_decrease_key(static_cast<int>(v), d);
        //debug("data related to", v, "has changed! now distance is", d, "while previous is", bestPrevious);
        distancesFromSource[v] = d;
        bestPreviousNode[v] = bestPrevious;
    }
}

void EarlyStopDijkstra::clear()
{
    q.clear();

//		for (int i=0; i<distancesFromSource.size(); ++i) {
//			distancesFromSource[i] = dpf::cost_t::getUpperBound();
//		}
    std::fill(distancesFromSource.begin(), distancesFromSource.end(), dpf::cost_t::getUpperBound());
    std::fill(bestPreviousNode.begin(), bestPreviousNode.end(), 0);
}
