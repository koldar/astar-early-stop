/*
 * map_loader.cpp
 *
 *  Created on: Oct 1, 2018
 *      Author: koldar
 */

#include "map_loader.h"

#include <stdio.h>
#include <ctype.h>
#include "errors.h"
#include "log.h"
#include <stdio.h>
#include <stdlib.h>

GridMap AbstractMapLoader::LoadMap(const char* name) {
	FILE *f;
	int useless;
	int width;
	int height;
	std::vector<dpf::celltype_t> map;

	f = fopen(name, "r");
	if (f == nullptr) {
		throw CpdException{"file %s not found!", name};
	}

	useless = fscanf(f, "type octile\nheight %d\nwidth %d\nmap\n", &height, &width);
	map.resize(height*width);
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			char c;
			do {
				useless = fscanf(f, "%c", &c);
			} while (isspace(c));

			map[y*width+x] = this->handleCell(map, width, height, x, y, c);
		}
	}
	fclose(f);
	return GridMap{map, width, height};
}

dpf::celltype_t MapLoader::handleCell(const std::vector<dpf::celltype_t>& map, int width, int height, dpf::coo2d_t x, dpf::coo2d_t y, char symbolRead) {
	if ((symbolRead == '.' || symbolRead == 'G' || symbolRead == 'S')) {
		return dpf::celltype_t{1000};
	} else {
		return dpf::celltype_t{0};
	}
}

//TODO remove
//void MapLoader::LoadMap(const char *fname, std::vector<dpf::celltype_t> &map, int &width, int &height)
//{
//	FILE *f;
//	int useless;
//	f = fopen(fname, "r");
//	if (f == NULL) {
//		throw CpdException{"file", fname, "not found!"};
//	}
//
//	useless = fscanf(f, "type octile\nheight %d\nwidth %d\nmap\n", &height, &width);
//	map.resize(height*width);
//	for (int y = 0; y < height; y++)
//	{
//		for (int x = 0; x < width; x++)
//		{
//			char c;
//			do {
//				useless = fscanf(f, "%c", &c);
//			} while (isspace(c));
//			map[y*width+x] = (c == '.' || c == 'G' || c == 'S');
//			//printf("%c", c);
//		}
//		//printf("\n");
//	}
//	fclose(f);
//
//}


