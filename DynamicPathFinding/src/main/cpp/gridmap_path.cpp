/*
 * gridmap_path.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: koldar
 */

#include "gridmap_path.h"


void gridmap_path::getRanges(xyLoc& minPoint, xyLoc& maxPoint) {
	minPoint.x = INT_MAX; //todo put max coordinates
	minPoint.y = INT_MAX; //todo put max coordinates

	maxPoint.x = 0;
	maxPoint.y = 0;
	for (xyLoc loc : *this) {
		if (loc.x < minPoint.x) {
			minPoint.x = loc.x;
		}
		if (loc.y < minPoint.y) {
			minPoint.y = loc.y;
		}
		if (loc.x > maxPoint.x) {
			maxPoint.x = loc.x;
		}
		if (loc.y > maxPoint.y) {
			maxPoint.y = loc.y;
		}
	}
}


bool gridmap_path::contains(xyLoc from, xyLoc to) const {
	for (int i=1; i<this->size(); ++i) {
		if ((this->at(i-1) == from) && (this->at(i) == to)) {
			return true;
		}
	}

	return false;
}
