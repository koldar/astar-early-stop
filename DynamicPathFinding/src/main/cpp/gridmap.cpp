/*
 * gridmap.cpp
 *
 *  Created on: 05 nov 2018
 *      Author: massimobono
 */


#include "gridmap.h"
#include "errors.h"
#include "log.h"
#include "list_graph.h"
#include "mapper.h"

GridMap::GridMap(const std::vector<bool>& bits, int width, int height, dpf::celltype_t typeIfSet) : cells{}, width{width}, height{height} {
	this->cells.resize(width * height);

	for (int i=0; i<bits.size(); ++i) {
		this->cells[i] = bits[i] ? typeIfSet : 0;
	}
}

GridMap::GridMap(const std::vector<dpf::celltype_t>& cells, int width, int height) : cells{cells}, width{width}, height{height} {

}

GridMap::GridMap(const GridMap& other) : cells{other.cells}, width{other.width}, height{other.height} {

}

GridMap::~GridMap() {

}

GridMap& GridMap::operator =(const GridMap& other) {
	this->cells = other.cells;
	this->width = other.width;
	this->height = other.height;
	return *this;
}

AdjGraph GridMap::getAdjGraph() const {
	Mapper mapper = Mapper{*this};
	ListGraph listGraph = extract_graph(mapper);
	AdjGraph result{listGraph};
	return result;
}

std::vector<bool> GridMap::getTraversableMask() const {
	std::vector<bool> result{};

	result.resize(this->width * this->height);

	for (auto y=0; y<this->height; ++y) {
		for (auto x=0; x<this->width; ++x) {
			result[y*this->width + x] = this->isTraversable(x, y);
		}
	}

	return result;
}

xyLoc GridMap::pickRandomTraversableCell(std::default_random_engine randomEngine) const {
	std::uniform_int_distribution<int> xdistribution(0,this->width -1);
	std::uniform_int_distribution<int> ydistribution(0,this->height -1);

	xyLoc result;
	while (true) {
		result.x = xdistribution(randomEngine);
		result.y = ydistribution(randomEngine);

		if (this->isTraversable(result.x, result.y)) {
			return result;
		}
	}


}



PPMImage GridMap::getImage(std::unordered_map<dpf::celltype_t, color_t> colorTraversableMapping, color_t untraversableColor) const {
	PPMImage result{static_cast<dpf::coo2d_t>(this->width), static_cast<dpf::coo2d_t>(this->height), WHITE};

	for (dpf::coo2d_t y=0; y<this->height; ++y) {
		for (dpf::coo2d_t x=0; x<this->width; ++x) {
			color_t c;
			if (!this->isTraversable(x, y)) {
				debug("is untraversable!");
				c = untraversableColor;
			} else if (colorTraversableMapping.find(this->getValue(x,y)) != colorTraversableMapping.end()) {
				debug("is traversable!", colorTraversableMapping[this->getValue(x, y)], "value is", this->getValue(x, y));
				debug("mapping is", colorTraversableMapping);
				c = colorTraversableMapping[this->getValue(x, y)];
			} else {
				throw CpdException{"cell %ld,%ld of has an invalid value of %ld", x, y, c};
			}

			debug("color is", c);
			result.setPixel(x, y, c);
		}
	}

	return result;

}

std::unordered_set<dpf::celltype_t> GridMap::getTraversableCellTypes() const {
	std::unordered_set<dpf::celltype_t> result;

	for (auto y=0; y<this->height; ++y) {
		for (auto x=0; x<this->width; ++x) {
			if (!this->isTraversable(x, y)) {
				continue;
			}
			dpf::celltype_t t = this->getValue(x, y);

			if (result.find(t) == result.end()) {
				result.insert(t);
			}
		}

	}

	return result;
}
