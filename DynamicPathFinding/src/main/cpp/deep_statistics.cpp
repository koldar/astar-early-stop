/*
 * deep_statistics.cpp
 *
 *  Created on: Oct 17, 2018
 *      Author: koldar
 */


#include "deep_statistics.h"

DeepStatistics::DeepStatistics() : generatedNodeIds{}, expandedNodeIds{}, touchedNodeIds{}, totalheuristicTime{0} {

}

DeepStatistics::~DeepStatistics() {

}

void DeepStatistics::reset_nodes_generated() {
	this->generatedNodeIds.clear();
}

void DeepStatistics::reset_nodes_expanded() {
	this->expandedNodeIds.clear();
}

void DeepStatistics::reset_nodes_touched() {
	this->touchedNodeIds.clear();
}

void DeepStatistics::add_node_generated(const warthog::search_node& n) {
	this->generatedNodeIds.push_back(n.get_id());
}
void DeepStatistics::add_node_expanded(const warthog::search_node& n) {
	this->expandedNodeIds.push_back(n.get_id());
}
void DeepStatistics::add_node_touched(const warthog::search_node& n) {
	this->touchedNodeIds.push_back(n.get_id());
}

void DeepStatistics::reset_heuristic_time() {
	this->totalheuristicTime = 0;
}

void DeepStatistics::add_to_heuristic_time(const warthog::search_node& n, unsigned long t) {
	this->totalheuristicTime += t;
}

unsigned long DeepStatistics::get_total_heuristic_time() {
	return this->totalheuristicTime;
}

int DeepStatistics::get_nodes_expanded() const {
	return this->expandedNodeIds.size();
}


int DeepStatistics::get_nodes_generated() const {
	return this->generatedNodeIds.size();
}


int DeepStatistics::get_nodes_touched() const {
	return this->touchedNodeIds.size();
}

