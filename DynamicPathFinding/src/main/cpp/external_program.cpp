/*
 * external_program.cpp
 *
 *  Created on: Dec 12, 2018
 *      Author: koldar
 */

#include "external_program.hpp"
#include <cstdarg>

int _execute_command(int size, ...) {
	std::stringstream ss;
	va_list args;
	va_start(args, size);

	for (int i=0; i<size; ++i) {
		const char* s = va_arg(args, const char*);
		ss << s;
	}
	va_end(args);
	int exit_code = system(ss.str().c_str());
	if (exit_code != 0) {
		throw ExternalProgramException{ss.str().c_str(), exit_code};
	}

	return exit_code;
}

