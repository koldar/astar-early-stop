/*
 * quick_statistics.cpp
 *
 *  Created on: Oct 17, 2018
 *      Author: koldar
 */


#include "quick_statistics.h"

QuickStatistics::QuickStatistics() : nodes_expanded_{0}, nodes_generated_{0}, nodes_touched_{0}, totalheuristicTime{0} {

}

QuickStatistics::~QuickStatistics() {

}

void QuickStatistics::reset_nodes_generated() {
	this->nodes_generated_ = 0;
}

void QuickStatistics::reset_nodes_expanded() {
	this->nodes_expanded_ = 0;
}

void QuickStatistics::reset_nodes_touched() {
	this->nodes_touched_ = 0;
}

void QuickStatistics::add_node_generated(const warthog::search_node& n) {
	this->nodes_generated_ += 1;
}
void QuickStatistics::add_node_expanded(const warthog::search_node& n) {
	this->nodes_expanded_ += 1;
}
void QuickStatistics::add_node_touched(const warthog::search_node& n) {
	this->nodes_touched_ += 1;
}

void QuickStatistics::reset_heuristic_time() {
	this->totalheuristicTime = 0;
}

void QuickStatistics::add_to_heuristic_time(const warthog::search_node& n, unsigned long t) {
	this->totalheuristicTime += t;
}

unsigned long QuickStatistics::get_total_heuristic_time() {
	return this->totalheuristicTime;
}


int QuickStatistics::get_nodes_expanded() const {
	return this->nodes_expanded_;
}


int QuickStatistics::get_nodes_generated() const {
	return this->nodes_generated_;
}


int QuickStatistics::get_nodes_touched() const {
	return this->nodes_touched_;
}

