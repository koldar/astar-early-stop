/*
 * simpleTimer.cpp
 *
 *  Created on: Oct 10, 2018
 *      Author: koldar
 */

#include "simpleTimer.h"

#include <sys/time.h>
#include <stdint.h>

SimpleTimer::SimpleTimer(double& microSecondsElapsed, bool autoStart) : microSecondsElapsed{microSecondsElapsed}, start{} {
	if (autoStart) {
		this->StartTimer();
	}
}

SimpleTimer::~SimpleTimer() {
	this->EndTimer();
}

void SimpleTimer::StartTimer() {
	this->microSecondsElapsed = 0;
	gettimeofday( &start, nullptr);
}

double SimpleTimer::EndTimer(){
	struct timeval stopTime;

	gettimeofday(&stopTime, nullptr );
	uint64_t microsecs = stopTime.tv_sec - start.tv_sec;
	microsecs = microsecs * 1000000 + stopTime.tv_usec - start.tv_usec;
	this->microSecondsElapsed = microsecs;
	return this->microSecondsElapsed;
}

double SimpleTimer::GetElapsedTime_us() const {
	return this->microSecondsElapsed;
}

double SimpleTimer::GetElapsedTime_ms() const {
	return this->microSecondsElapsed / 1000.0;
}

double SimpleTimer::GetElapsedTime_s() const {
	return this->microSecondsElapsed / 1000000.0;
}
