/*
 * multi_map_loader.cpp
 *
 *  Created on: Nov 5, 2018
 *      Author: koldar
 */

#include "multi_map_loader.h"
#include "errors.h"


MultiColorMapLoader::~MultiColorMapLoader() {

}

MultiColorMapLoader& MultiColorMapLoader::addUntraversableColor(char color) {
	this->untraversableCharacters.insert(color);
	return *this;
}

MultiColorMapLoader& MultiColorMapLoader::addTraversableColor(char color, dpf::celltype_t t) {
	this->traversableCharacters[color] = t;
	return *this;
}

dpf::celltype_t MultiColorMapLoader::handleCell(const std::vector<dpf::celltype_t>& map, int width, int height, dpf::coo2d_t x, dpf::coo2d_t y, char symbolRead) {
	if (this->untraversableCharacters.find(symbolRead) != this->untraversableCharacters.end()) {
		return dpf::celltype_t{0}; //untraversable
	}
	if (this->traversableCharacters.find(symbolRead) != this->traversableCharacters.end()) {
		return this->traversableCharacters[symbolRead];
	}
	throw CpdException{"we have read character '%c' in map file, but it is associated with no terrain type!", symbolRead};
}

