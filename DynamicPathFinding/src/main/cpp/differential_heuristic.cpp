/*
 * differential_heuristic.cpp
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */


#include "differential_heuristic.h"
#include "dijkstra_algorithm.h"
#include <unordered_map>
#include "vec_io.h"

#include <cstdint>
#include <climits>

struct nodedistances_t {
	dpf::nodeid_t node;
	dpf::cost_t distnace;
};

AbstractLandmarkPlacingStrategy::AbstractLandmarkPlacingStrategy(int landmarkToCreate) : landmarkToCreate{landmarkToCreate} {
}

AbstractLandmarkPlacingStrategy::~AbstractLandmarkPlacingStrategy() {

}

CanonicalAdvancePlacingLandmarkStrategy::CanonicalAdvancePlacingLandmarkStrategy(int landmarkToCreate) : AbstractLandmarkPlacingStrategy{landmarkToCreate}, unmarkedNodes{} {

}

CanonicalAdvancePlacingLandmarkStrategy::~CanonicalAdvancePlacingLandmarkStrategy() {

}

void CanonicalAdvancePlacingLandmarkStrategy::reset() {
	//std::fill(markedNodes.begin(), markedNodes.end(), false);
	this->unmarkedNodes.clear();
}

std::vector<dpf::nodeid_t> CanonicalAdvancePlacingLandmarkStrategy::getLandmarks(const AdjGraph& graph, std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>>& getDistancesFromLandmarksToNodes) {
	debug("computing landmarks of graph");

	std::vector<dpf::nodeid_t> result;

	//set all nodes as unmarked
	for (dpf::nodeid_t n=0; n<graph.node_count(); ++n) {
		this->unmarkedNodes.push_back(n);
	}

	int landmarkCreated = 0;
	do {
		if (this->unmarkedNodes.size() == 0) {
			warning("there are left no states!");
			throw CpdException{"no states left for creating %ld landmarks!", this->landmarkToCreate};
		}

		//pick a start vertex
		dpf::nodeid_t startNode = this->getRandomUnMarkedNode(graph);
		//do the BFS
		(graph, startNode, graph.node_count()/this->landmarkToCreate);
		//the new landmark is the seed of the BFS
		landmarkCreated += 1;
		result.push_back(startNode);
	} while ((landmarkCreated + 0.0)/(0.0 + this->landmarkToCreate) <= 0.9); //we're within the 10% of the intended landmarkToCreate

	getDistancesFromLandmarksToNodes = std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>>{};

	return result;
}

bool CanonicalAdvancePlacingLandmarkStrategy::isNodeUnMarked(dpf::nodeid_t n) const {
	return std::find(this->unmarkedNodes.begin(), this->unmarkedNodes.end(), n) != this->unmarkedNodes.end();
}

void CanonicalAdvancePlacingLandmarkStrategy::markNode(dpf::nodeid_t n) {
	assert(this->isNodeUnMarked(n));

	auto it = std::find(this->unmarkedNodes.begin(), this->unmarkedNodes.end(), n);
	assert(it !=this->unmarkedNodes.end());

	this->unmarkedNodes.erase(it);
}

dpf::nodeid_t CanonicalAdvancePlacingLandmarkStrategy::getRandomUnMarkedNode(const AdjGraph& g) const {
	default_random_engine eng;
	std::uniform_int_distribution<dpf::nodeid_t> startVertexDistribution(0, this->unmarkedNodes.size()-1);

	return startVertexDistribution(eng);
}

void CanonicalAdvancePlacingLandmarkStrategy::markNodesInBreadthFirstSearch(const AdjGraph& g, dpf::nodeid_t start, safe_int statesToMark) {
	std::vector<dpf::nodeid_t> bfsQueue;
	this->_markNodesInBreadthFirstSearch(g, start, statesToMark, bfsQueue);
}

void CanonicalAdvancePlacingLandmarkStrategy::_markNodesInBreadthFirstSearch(const AdjGraph& g, dpf::nodeid_t n, safe_int statesToMark, std::vector<dpf::nodeid_t>& bfsQueue) {
	if (statesToMark == 0) {
		return;
	}

	this->markNode(n);

	for (int i=0; i<g.out_deg(n); ++i) {
		bfsQueue.push_back(static_cast<dpf::nodeid_t>(g.getIthOutArc(n, i).target));
	}

	//pop first element of the vector
	dpf::nodeid_t head = bfsQueue.front();
	bfsQueue.erase(bfsQueue.begin());

	//recursive call
	this->_markNodesInBreadthFirstSearch(g, head, statesToMark - 1, bfsQueue);
}


DifferentHeuristicAdvancePlacingLandmarkStrategy::DifferentHeuristicAdvancePlacingLandmarkStrategy(int landmarkToCreate) : AbstractLandmarkPlacingStrategy{landmarkToCreate} {
}

DifferentHeuristicAdvancePlacingLandmarkStrategy::~DifferentHeuristicAdvancePlacingLandmarkStrategy() {
}

std::vector<dpf::nodeid_t> DifferentHeuristicAdvancePlacingLandmarkStrategy::getLandmarks(const AdjGraph& graph, std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>>& getDistancesFromLandmarksToNodes) {
	debug("generating landmarks via DifferentHeuristicAdvancePlacingLandmarkStrategy!");
	std::vector<dpf::nodeid_t> result;

	//get random state
	dpf::nodeid_t randomState = this->getRandomNode(graph);
	//get first canonical state by getting the farthest node from randomState
	std::vector<dpf::cost_t> distances = this->getDistancesFrom(graph, randomState);
	dpf::nodeid_t newLandmark;
	dpf::cost_t maxDistance;
	/**
	 * key: landmarks
	 * value: vector representing the optimal distance from the landmark till the i-th node
	 */
	std::unordered_map<dpf::nodeid_t, std::vector<dpf::cost_t>> distancesFromLandmarks;

	debug("picked random node", randomState);
	newLandmark = this->breakFarthestNodesTie(this->getFarthestNodes(distances, randomState, maxDistance));
	distancesFromLandmarks[newLandmark] = this->getDistancesFrom(graph, newLandmark);
	result.push_back(newLandmark);

	//get the nodes which maximize the minimum distance from landmarks
	for (int landmark_id=1; landmark_id<this->landmarkToCreate; ++landmark_id) {
		debug("finding landmark #", landmark_id, "/", this->landmarkToCreate);
		debug("the current landmarks are", result);

		dpf::cost_t maximum = dpf::cost_t::getLowerBound();
		std::vector<dpf::nodeid_t> possibleLandmarks;
		for (dpf::nodeid_t source=0; source<graph.node_count(); ++source) {
			debug("********** considering source", source, "... what is the minimum?");
			dpf::cost_t value = this->getMinimumDistanceFromLandmarks(graph, source, distancesFromLandmarks);
			if (value > maximum) {
				debug("new maximum! source=", source, "value", value);
				maximum = value;
				possibleLandmarks.clear();
				possibleLandmarks.push_back(source);
			} else if (value == maximum) {
				possibleLandmarks.push_back(source);
			}
		}

		newLandmark = this->breakFarthestNodesTie(possibleLandmarks);
		distancesFromLandmarks[newLandmark] = this->getDistancesFrom(graph, newLandmark);
		debug("the new landmark is", newLandmark);

		result.push_back(newLandmark);

	}

	//update the input-output parameter
	getDistancesFromLandmarksToNodes = std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>>{};
	for (auto it=distancesFromLandmarks.begin(); it!= distancesFromLandmarks.end(); ++it) {
		getDistancesFromLandmarksToNodes[it->first] = this->convertCostTInto4Bytes(it->second);
	}
	//getDistancesFromLandmarksToNodes = this->convertCostTInto4Bytes(distancesFromLandmarks);
	return result;
}

dpf::cost_t DifferentHeuristicAdvancePlacingLandmarkStrategy::getMinimumDistanceFromLandmarks(const AdjGraph& g, dpf::nodeid_t n, const std::unordered_map<dpf::nodeid_t, std::vector<dpf::cost_t>>& distancesFromLandmarks) const {
	dpf::cost_t result = dpf::cost_t::getUpperBound();

	for (auto it=distancesFromLandmarks.begin(); it!=distancesFromLandmarks.end(); ++it) {
		dpf::nodeid_t landmark = it->first;

		dpf::cost_t distanceFromLandmark = it->second[n];
		debug("distance is ", distanceFromLandmark);
		if (distanceFromLandmark < result) {
			debug("new minimum! source=", n, "value=", distanceFromLandmark);
			result = distanceFromLandmark;
		}
	}

	return result;
}

dpf::nodeid_t DifferentHeuristicAdvancePlacingLandmarkStrategy::getRandomNode(const AdjGraph& g) const {
	default_random_engine eng;
	std::uniform_int_distribution<dpf::nodeid_t> startVertexDistribution(0, g.node_count()-1);

	return startVertexDistribution(eng);
}

std::vector<dpf::nodeid_t> DifferentHeuristicAdvancePlacingLandmarkStrategy::getFarthestNodes(const std::vector<dpf::cost_t>& distances, dpf::nodeid_t n, dpf::cost_t& maxDistance) const {
	std::vector<dpf::nodeid_t> result;

	maxDistance = dpf::cost_t::getLowerBound();
	for (dpf::nodeid_t i=0; i<distances.size(); ++i) {
		if (distances[i] > maxDistance) {
			maxDistance = distances[i];
			result.clear();
			result.push_back(i);
		} else if (distances[i] == maxDistance) {
			result.push_back(i);
		}
	}

	return result;
}

dpf::nodeid_t DifferentHeuristicAdvancePlacingLandmarkStrategy::breakFarthestNodesTie(const std::vector<dpf::nodeid_t>& farthestNodes) const {
	return farthestNodes[0];
}

std::vector<dpf::cost_t> DifferentHeuristicAdvancePlacingLandmarkStrategy::getDistancesFrom(const AdjGraph& g, dpf::nodeid_t n) const {
	DijsktraAlgorithm dijkstra{g};
	dijkstra.run(n);
	return dijkstra.getDistances();
}

std::vector<uint32_t> DifferentHeuristicAdvancePlacingLandmarkStrategy::convertCostTInto4Bytes(const std::vector<dpf::cost_t>& costVector) const {
	std::vector<uint32_t> result{};
	for (auto i=0; i<costVector.size(); ++i) {
		if (costVector[i] > static_cast<dpf::cost_t>(UINT32_MAX)) {
			throw std::domain_error{"trying to compress a number into a 32bit which will generate overflow!"};
		}
		result.push_back(static_cast<uint32_t>(costVector.at(i)));
	}
	return result;
}

void DifferentHeuristicAdvancePlacingLandmarkStrategy::reset() {
}

LandMarkDatabase::LandMarkDatabase(const AdjGraph& graph, AbstractLandmarkPlacingStrategy& policy, const std::string& filename) {
	debug("checking if ",filename, " exists..");
	if (isFileExists(filename)) {
		debug("it exists!");
		*this = LandMarkDatabase::loadFromFile(filename);
	} else {
		debug("nope!");
		*this = LandMarkDatabase{graph, policy};
		this->saveIntoFile(filename);
	}
}

LandMarkDatabase::LandMarkDatabase(const AdjGraph& graph, AbstractLandmarkPlacingStrategy& policy, const std::string& filename, double& time, size_t& space) {
	debug("checking if ",filename, " exists..");
	if (isFileExists(filename)) {
		debug("it exists!");
		*this = LandMarkDatabase::loadFromFile(filename);
		time = 0;
	} else {
		debug("nope!");
		PROFILE_TIME_CODE(time) {
			this->landmarks = policy.getLandmarks(graph, this->landMarksDistances);
		}
		critical("in order to generate the landmarks of the graph we took ", time, "us!");

		if (this->landMarksDistances.size() == 0) {
			throw CpdException{"landmarks distances is empty! policy is %p", &policy};
		}
		this->saveIntoFile(filename);
	}
	space = getBytesOfFile(filename);
}

LandMarkDatabase::LandMarkDatabase(const AdjGraph& graph, AbstractLandmarkPlacingStrategy& policy) {
	double microSecondsElapsed;
	PROFILE_TIME_CODE(microSecondsElapsed) {
		this->landmarks = policy.getLandmarks(graph, this->landMarksDistances);
	}
	critical("in order to generate the landmarks of the graph we took ", microSecondsElapsed, "us!");

	if (this->landMarksDistances.size() == 0) {
		throw CpdException{"landmarks distances is empty! policy is %p", &policy};
	}

	DO_ON_DEBUG {
		int expectedSize = this->landMarksDistances[this->landmarks[0]].size();
		for (auto it=this->landMarksDistances.begin(); it!=this->landMarksDistances.end(); ++it) {
			if (it->second.size() != expectedSize) {
				error("sizes don't match!");
				assert(false);
			}
		}
	}
}

LandMarkDatabase::LandMarkDatabase(const std::vector<dpf::nodeid_t> landmarks, const std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>>& landMarksDistances) : landmarks{landmarks}, landMarksDistances{landMarksDistances} {

}

LandMarkDatabase::~LandMarkDatabase() {
}

LandMarkDatabase::LandMarkDatabase(const LandMarkDatabase& other) : landMarksDistances{other.landMarksDistances}, landmarks{other.landmarks} {

}

LandMarkDatabase& LandMarkDatabase::operator = (const LandMarkDatabase& other) {
	this->landmarks = other.landmarks;
	this->landMarksDistances = other.landMarksDistances;
	return *this;
}

const std::vector<dpf::nodeid_t> LandMarkDatabase::getLandmarks() const {
	return this->landmarks;
}

dpf::big_integer LandMarkDatabase::getGraphSize() const {
	return this->landMarksDistances.at(this->landmarks.at(0)).size();
}

dpf::cost_t LandMarkDatabase::getDistanceFromLandmarkToNode(dpf::nodeid_t landmark, dpf::nodeid_t n) const {
	return this->landMarksDistances.at(landmark)[n];
}

LandMarkDatabase LandMarkDatabase::loadFromFile(const std::string& filename) {
	std::FILE* f = fopen(filename.c_str(), "rb");

	if (f == nullptr) {
		throw CpdException{"cannot open file %s!", filename.c_str()};
	}


	std::unordered_map<dpf::nodeid_t, std::vector<uint32_t>> landmarksDistances;
	std::vector<dpf::nodeid_t> landmarks = load_vector<dpf::nodeid_t>(f);
	for (int i=0; i<landmarks.size(); ++i) {
		dpf::nodeid_t landmark = landmarks[i];
		std::vector<uint32_t> loaded = load_vector<uint32_t>(f);
		landmarksDistances[landmark] = loaded;
	}

	fclose(f);


	debug("landmarks are", landmarks);
	LandMarkDatabase result{landmarks, landmarksDistances};

	return result;
}

void LandMarkDatabase::saveIntoFile(const std::string& filename) {
	FILE* f = fopen(filename.c_str(), "wb");

	if (f == NULL) {
		throw CpdException{"cannot open file %s!", filename.c_str()};
	}

	save_vector<dpf::nodeid_t>(f, this->landmarks);
	for (int i=0; i<this->landmarks.size(); ++i) {
		dpf::nodeid_t landmark = this->landmarks[i];
		//we don't save the dpf::cost_t by itself, but we save only the first 4 bytes of it
		save_vector<uint32_t>(f, this->landMarksDistances.at(landmark));
	}

	fclose(f);
}

size_t LandMarkDatabase::getMemoryOccupied() const {

	size_t result = 0;

	for (auto it=this->landMarksDistances.begin(); it != this->landMarksDistances.end(); ++it) {
		result += it->second.capacity() * sizeof(it->second.front());
	}

	result += sizeof(this->landmarks.front()) * this->landmarks.capacity();
	result += sizeof(*this);

	return result;
}


DifferentialHeuristic::DifferentialHeuristic(const AdjGraph& graph, const Mapper& mapper, const LandMarkDatabase& landmarkDatabase) : graph{graph}, landmarkDatabase{landmarkDatabase}, mapper{mapper} {
}

//DifferentialHeuristic& DifferentialHeuristic::operator = (const DifferentialHeuristic& other) {
//	this->landmarkDatabase = other.landmarkDatabase;
//	this->graph = other.graph;
//	return *this;
//}

DifferentialHeuristic::DifferentialHeuristic(const DifferentialHeuristic& other) : graph{other.graph}, landmarkDatabase{other.landmarkDatabase}, mapper{other.mapper} {

}

DifferentialHeuristic::~DifferentialHeuristic() {

}

dpf::cost_t DifferentialHeuristic::h(uint32_t startid, uint32_t goalid) {
	dpf::cost_t result = dpf::cost_t::getLowerBound();

	debug("evaluating state", mapper(startid));
	for (dpf::nodeid_t landmark : this->landmarkDatabase.getLandmarks()) {
		debug("considering landmark", mapper(landmark));

		dpf::cost_t a = this->landmarkDatabase.getDistanceFromLandmarkToNode(landmark, startid);
		dpf::cost_t b = this->landmarkDatabase.getDistanceFromLandmarkToNode(landmark, goalid);

		debug(mapper(startid), "->", mapper(landmark), ":", a);
		debug(mapper(goalid), "->", mapper(landmark), ":", b);
		dpf::big_sint diff = static_cast<dpf::big_sint>(a) - static_cast<dpf::big_sint>(b);

		dpf::cost_t value = dpf::cost_t{diff > 0 ? diff : -diff};

		debug("value=", value, "max=",result);

		if (value > result) {
			result = value;
		}
	}

	return result;
}

void DifferentialHeuristic::clear() {

}

