/*
 * grid_map_expander.cpp
 *
 *  Created on: Oct 2, 2018
 *      Author: koldar
 */

#include "grid_map_expander.h"
#include "cpd_slice_printer.h"

size_t identityHash(uint32_t key) {
	return key;
}

GridMapExpander::GridMapExpander(const GridMap& map, const Mapper& mapper, const AdjGraph& adjGraph, bool useVectorInsteadOfMap) :
	map{map}, mapper{mapper}, adjGraph{adjGraph},
	nodeSuccessors{}, searchNodeMap{}, searchNodePool{nullptr}, searchNodeVector{}, useVectorInsteadOfMap{useVectorInsteadOfMap},
	parent{nullptr}, nodeSuccessorNextIndex{0} {

	if (useVectorInsteadOfMap) {
		this->searchNodeVector.resize(adjGraph.node_count(), nullptr);
	}
	this->searchNodePool = new warthog::mem::cpool{sizeof(DynamicSearchNode), CPOOL_MAX_NUMBER};
}

int GridMapExpander::mapwidth() const {
	return map.getWidth();
}


size_t GridMapExpander::mem() const {
	return
			sizeof(searchNodeVector) +
			sizeof(searchNodeMap) +
			sizeof(*this);
}

DynamicSearchNode* GridMapExpander::generate(uint32_t startid) {
	DynamicSearchNode* newNode = nullptr;

	if (this->useVectorInsteadOfMap) {
		if (this->searchNodeVector[startid] != nullptr) {
			return this->searchNodeVector[startid];
		}
		newNode = new (searchNodePool->allocate()) DynamicSearchNode(startid);
		this->searchNodeVector[startid] = newNode;

	} else {
		auto oldSearchNodeIterator = searchNodeMap.find(startid);
		if(oldSearchNodeIterator != searchNodeMap.end()) {
			//the node is already present in the expander
			return &oldSearchNodeIterator->second;
		}

		//generate new search node
		newNode = new (searchNodePool->allocate()) DynamicSearchNode(startid);
		debug("creating node", startid, "(", this->mapper(startid), ") in the searchNodeMap");
		searchNodeMap.insert(std::pair<uint32_t,DynamicSearchNode&>{startid, *newNode});
	}

	return newNode;
}

void GridMapExpander::expand(const DynamicSearchNode* node, warthog::problem_instance* instance) {
	//in the adjacency graph, we need to generate all the successors of this node

	//remove the elements, but not from the heap
	this->nodeSuccessors.clear();
	uint32_t id = node->get_id();
	for (auto it=adjGraph.arcBegin(id); it != adjGraph.arcEnd(id); ++it) {
		debug("a sucessor of ", node->get_id(), " is ", static_cast<uint32_t>(it->target));
		if (it->weight < dpf::cost_t::getUpperBound()) {
			this->nodeSuccessors.push_back(std::pair<const OutArc&, DynamicSearchNode&>{
				*it,
				*(this->generate(static_cast<uint32_t>(it->target)))
			});
		} else {
			debug("we won't follow this arc because the cost is infinity!");
		}
	}

	this->parent = node;
	this->nodeSuccessorNextIndex = 0;
}


GridMapExpander::~GridMapExpander() {
	//delete all the search_node in the map
	this->clear();
	delete this->searchNodePool;
}

void GridMapExpander::first(DynamicSearchNode*& n, dpf::cost_t& cost_to_n) {
	assert(this->parent != nullptr);

	if (this->nodeSuccessors.size() == 0) {
		n = 0; //TODO nullptr
		cost_to_n = dpf::cost_t::getUpperBound();
	} else {
		n = &this->nodeSuccessors.front().second;
		cost_to_n = static_cast<dpf::cost_t>(this->nodeSuccessors.front().first.weight);
		this->nodeSuccessorNextIndex = 1;
	}

}

void GridMapExpander::next(DynamicSearchNode*& n, dpf::cost_t& cost_to_n) {
	assert(this->parent != nullptr);

	if (this->nodeSuccessorNextIndex >= this->nodeSuccessors.size()) {
		n = 0; //TODO nullptr
		cost_to_n = dpf::cost_t::getUpperBound();
	} else {
		n = &this->nodeSuccessors[this->nodeSuccessorNextIndex].second;
		cost_to_n = static_cast<dpf::cost_t>(this->nodeSuccessors[this->nodeSuccessorNextIndex].first.weight);
		this->nodeSuccessorNextIndex += 1;
	}

}

void GridMapExpander::clear() {
	this->parent = nullptr;
	this->nodeSuccessors.clear();
	this->nodeSuccessorNextIndex = 0;

	std::fill(this->searchNodeVector.begin(), this->searchNodeVector.end(), nullptr);
	this->searchNodeMap.clear();
	this->searchNodePool->reclaim();
}

const AdjGraph& GridMapExpander::get_map() const {
	return adjGraph;
}

//TODO not used anymore
//warthog::search_node& GridMapExpander::expandPath(const std::vector<dpf::move_t>& moves, const warthog::search_node& node) {
//	warthog::search_node* parent = const_cast<warthog::search_node*>(&node);
//	warthog::search_node* n = parent;
//	debug("moves is ", moves);
//	for (auto it= moves.begin(); it != moves.end(); ++it) {
//		/* In this path we can have a location whose search_node has already been generated, but wecan't have search_node already exapnded!
//		 * For example: we start from "S" and we need to reach "G". "." cell are unamed. Suppose that the arc A->B has been modifieid an now
//		 * its weight is large. We first expanded "A" and generated its successors (one of them being B). Since the arc between A and B weights
//		 * a lot, we decided to expand D instead. There the optimal path goes to G via "B".
//		 * This is an exmaple on how the optimal path can go to already GENERATED nodes, but not through EXPANDED nodes
//		 *
//		 * SABCG
//		 * ..D..
//		 *
//		 * The below assertion checks exactly that
//		 */
//
//		int w = this->adjGraph.getIthOutArc(parent->get_id(),  *it).target;
//		debug("checking if cell ", w, "(", this->mapper(w), ")", "has already been generated...");
//
//		assert(canBeOnOptimalPath(w));
//
//		n = this->generate(w);
//		assert(n->get_id() == w);
//		n->set_parent(parent);
//		debug("parent of node", n->get_id(), "is", n->get_parent()->get_id());
//		parent = n;
//	}
//
//	debug("path generated! n=", n, "parent of n is", n->get_parent()->get_id());
//
//	return *n;
//}

bool GridMapExpander::canBeOnOptimalPath(uint32_t id) const {
	if (this->useVectorInsteadOfMap) {
		if (this->searchNodeVector[id] == nullptr) {
			return true;
		}

		DynamicSearchNode* n = this->searchNodeVector[id];
		return !n->get_expanded();
	} else {
		if (searchNodeMap.find(id) == searchNodeMap.end()) {
			return true;
		}
		DynamicSearchNode& n = searchNodeMap.find(id)->second;

		if (!n.get_expanded()) {
			return true;
		}

	//	auto labelPrinter = DefaultLabelPrinter{};
	//	auto colorPrinter = ExpandedColorPrinter{searchNodeMap};
	//	printGridMap(this->rawMap, width, height, mapper, xyLoc{0,0}, "canBeOptimal", labelPrinter, colorPrinter);

		return false;
	}
}
