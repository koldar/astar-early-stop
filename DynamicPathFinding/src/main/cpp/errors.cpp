/*
 * errors.cpp
 *
 *  Created on: Oct 1, 2018
 *      Author: koldar
 */

#include "errors.h"
#include <sstream>
#include <stdio.h>
#include <stdarg.h>
#include "log.h"

CpdException::CpdException(const string& msg) : _msg{nullptr}
{
	_msg = new string{msg};
};

CpdException::CpdException(const string& fmt, ...) : _msg{nullptr} {
	va_list args;

	char buffer[100];
	va_start(args, fmt);
	vsnprintf(buffer, static_cast<size_t>(100), fmt.c_str(), args);
	va_end(args);
	debug("exception  text is ", buffer);
	_msg = new string{buffer};
	debug("string text is ", _msg);
}

CpdException::~CpdException() {
	delete _msg;
}


const char* CpdException::what() const throw() {
	return _msg->c_str();
}

