/*
 * manhattan_heuristic.cpp
 *
 *  Created on: Oct 11, 2018
 *      Author: koldar
 */


#include "manhattan_heuristic.h"
#include "xyLoc.h"
#include <cmath>

MahnattanHeuristic::MahnattanHeuristic(const Mapper& mapper) : mapper{mapper} {

}

dpf::cost_t MahnattanHeuristic::h(dpf::nodeid_t startid, dpf::nodeid_t goalid) {
	xyLoc start = mapper(startid);
	xyLoc goal = mapper(goalid);

	return std::abs(static_cast<double>(start.x - goal.x)) + std::abs(static_cast<double>(start.y - goal.y));
}

void MahnattanHeuristic::clear() {

}

