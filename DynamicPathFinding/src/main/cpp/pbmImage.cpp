#include "pbmImage.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include "errors.h"
#include "log.h"
#include "external_program.hpp"
#include "file_utils.h"


const color_t DARK_RED = color_t{150, 0, 0};
const color_t RED = color_t{255, 0, 0};

const color_t DARK_ORANGE = color_t{150, 20, 0};
const color_t ORANGE = color_t{255, 128, 0};

const color_t DARK_YELLOW = color_t{150,150,0};
const color_t YELLOW = color_t{255, 255, 0};

const color_t DARK_GREEN = color_t{0, 150,0};
const color_t GREEN = color_t{0, 255,0};

const color_t DARK_CYAN = color_t{0, 150, 150};
const color_t CYAN = color_t{0, 255, 255};

const color_t DARK_BLUE = color_t{0, 0,150};
const color_t BLUE = color_t{0, 0,255};

const color_t PURPLE = color_t{102, 0, 102};

const color_t DARK_VIOLET = color_t{150, 0, 150};
const color_t VIOLET = color_t{255, 0, 255};

const color_t GRAY = color_t{128, 128, 128};
const color_t WHITE = color_t{255, 255,255};
const color_t BLACK = color_t{0, 0,0};


safe_uchar::safe_uchar() : value{0}
{
}

safe_uchar::safe_uchar(unsigned char c) : value{c}
{
}

safe_uchar::safe_uchar(int c) : value { static_cast<unsigned char>(c)} {
}

safe_uchar::safe_uchar(const safe_uchar& other) : value{other.value}
{
}

safe_uchar safe_uchar::mean(const safe_uchar& other) const {
	return safe_uchar{static_cast<unsigned char>((static_cast<int>(this->value) + static_cast<int>(other.value))/2)};
}

safe_uchar::~safe_uchar()
{
}

safe_uchar& safe_uchar::operator = (const safe_uchar& other)
{
	this->value = other.value;
	return *this;
}

safe_uchar::operator unsigned char() const
{
	return this->value;
}

safe_uchar& safe_uchar::operator += (const safe_uchar& other)
				{
	*this = *this + other;
	return *this;
				}

safe_uchar& safe_uchar::operator += (unsigned char other)
				{
	*this = *this + safe_uchar{other};
	return *this;
				}

safe_uchar& safe_uchar::operator -= (const safe_uchar& other)
				{
	*this = *this - other;
	return *this;
				}

safe_uchar& safe_uchar::operator -= (unsigned char other)
				{
	*this = *this - safe_uchar{other};
	return *this;
				}

safe_uchar& safe_uchar::operator /= (const safe_uchar& other)
				{
	*this = *this / other;
	return *this;
				}

safe_uchar& safe_uchar::operator /= (unsigned char other)
				{
	*this = *this / safe_uchar{other};
	return *this;
				}

safe_uchar& safe_uchar::operator &= (const safe_uchar& other) {
	*this = *this & other;
	return *this;
}

safe_uchar& safe_uchar::operator &= (unsigned char other) {
	*this = *this & safe_uchar{other};
	return *this;
}

safe_uchar& safe_uchar::operator |= (const safe_uchar& other) {
	*this = *this | other;
	return *this;
}

safe_uchar& safe_uchar::operator |= (unsigned char other) {
	*this = *this | safe_uchar{other};
	return *this;
}

safe_uchar safe_uchar::operator ~() const {
	return safe_uchar{~this->value};
}

std::ostream& operator <<(std::ostream& stream, const safe_uchar& c)
{
	stream << c.value;
	return stream;
}

safe_uchar operator + (const safe_uchar& a, const safe_uchar& b)
{
	int valueA = a.value;
	int valueB = b.value;

	int result = valueA + valueB;
	if (result > 255)
	{
		result = 255;
	}

	return safe_uchar{static_cast<unsigned char>(result)};
}

safe_uchar operator - (const safe_uchar& a, const safe_uchar& b)
{
	int valueA = a.value;
	int valueB = b.value;

	int result = valueA - valueB;
	if (result < 0)
	{
		result = 0;
	}

	return safe_uchar{static_cast<unsigned char>(result)};
}

safe_uchar operator / (const safe_uchar& a, const safe_uchar& b)
				{
	return safe_uchar{static_cast<unsigned char>(a.value/b.value)};
				}

safe_uchar operator & (const safe_uchar& a, const safe_uchar& b) {
	return safe_uchar{static_cast<unsigned char>(a.value & b.value)};
}

safe_uchar operator | (const safe_uchar& a, const safe_uchar& b) {
	return safe_uchar{static_cast<unsigned char>(a.value | b.value)};
}

bool operator == (const safe_uchar& a, const safe_uchar& b) {
	return a.value == b.value;
}
bool operator != (const safe_uchar& a, const safe_uchar& b) {
	return a.value != b.value;
}

color_t::color_t(unsigned char r, unsigned char g, unsigned char b) : r{safe_uchar{r}}, g{safe_uchar{g}}, b{safe_uchar{b}}
{
}

color_t::color_t(const color_t& other) : r{other.r}, g{other.g}, b{other.b}
{
}

color_t::color_t() : r{safe_uchar{0}}, g{safe_uchar{0}}, b{safe_uchar{0}}
{
}

color_t::~color_t()
{
}

safe_uchar color_t::getRed() const
{
	return this->r;
}

safe_uchar color_t::getGreen() const
{
	return this->g;
}

safe_uchar color_t::getBlue() const
{
	return this->b;
}

color_t color_t::lerp(const color_t& other) const {
	return color_t{
		this->r.mean(other.r),
		this->g.mean(other.g),
		this->b.mean(other.b)
	};
}


color_t& color_t::operator +=(const color_t& other)
				{
	this->r += other.r;
	this->g += other.g;
	this->b += other.b;

	return *this;
				}

color_t& color_t::operator -=(const color_t& other)
				{
	this->r -= other.r;
	this->g -= other.g;
	this->b -= other.b;

	return *this;
				}

color_t& color_t::operator =(const color_t& other)
{
	this->r = other.r;
	this->g = other.g;
	this->b = other.b;

	return *this;
}

color_t operator + (const color_t& a, const color_t& b)
{
	return color_t{a.r + b.r, a.g + a.g, a.b + b.b};
}

color_t operator - (const color_t& a, const color_t& b)
{
	return color_t{a.r - b.r, a.g - a.g, a.b - b.b};
}

color_t operator / (const color_t& a, const int& b) {
	safe_uchar _b = safe_uchar{b};
	return color_t{a.r / _b, a.g / _b, a.b / _b};
}

color_t operator ~(const color_t& a) {
	return color_t{
		static_cast<unsigned char>(~a.r),
		static_cast<unsigned char>(~a.g),
		static_cast<unsigned char>(~a.b)
	};
}

color_t operator &(const color_t& a, const color_t& other) {
	return color_t{
		static_cast<unsigned char>(a.r & other.r),
		static_cast<unsigned char>(a.g & other.g),
		static_cast<unsigned char>(a.b & other.b)
	};
}
color_t operator |(const color_t& a, const color_t& other) {
	return color_t{
		static_cast<unsigned char>(a.r | other.r),
		static_cast<unsigned char>(a.g | other.g),
		static_cast<unsigned char>(a.b | other.b)
	};
}

bool operator ==(const color_t& a, const color_t& other) {
	return (a.r == other.r) && (a.g == other.g) && (a.b == other.b);
}
bool operator !=(const color_t& a, const color_t& other) {
	return (a.r != other.r) || (a.g != other.g) || (a.b != other.b);
}

std::ostream& operator <<(std::ostream& stream, const color_t& c)
{
	stream << "{R=" << c.r << " G=" << c.g << " B=" << c.b << "}";
	return stream;
}



PPMImage::PPMImage(dpf::coo2d_t width, dpf::coo2d_t height) : width{width}, height{height}, matrix{nullptr}
{
	this->matrix = new color_t[width*height];
}

PPMImage::PPMImage(dpf::coo2d_t width, dpf::coo2d_t height, const color_t& color) : width{width}, height{height}
{
	this->matrix = new color_t[width*height];
	this->fillImageWith(color);
}

PPMImage::PPMImage(const PPMImage& other) : width{other.width}, height{other.height}
{
	this->matrix = new color_t[width*height];
	for (auto y=0; y<this->height; ++y)
	{
		for (auto x=0; x<this->width; ++x)
		{
			this->setPixel(x, y, other.getPixel(x, y));
		}
	}
}

PPMImage::~PPMImage()
{
	if (this->matrix != nullptr) {
		delete[] this->matrix;
	}
}

void PPMImage::save(const std::string& filename) const
{
	std::ofstream f;
	std::stringstream ss;
	ss << filename.c_str() << ".ppm";
	f.open(ss.str());
	// HEADER
	f << "P3" << "\n";
	// WIDTH
	f << this->width << "\n";
	// HEIGHT
	f << this->height << "\n";
	// Maximum value for each color
	f << 255 << "\n";
	//LIST OF RGB TRIPLETS: from top left to bottom right, going by image row
	for (auto y=0; y<this->height; ++y)
	{
		for (auto x=0; x<this->width; ++x)
		{
			f << this->getPixel(x,y).getRed() << " " << this->getPixel(x,y).getGreen() << " " << this->getPixel(x,y).getBlue();
			if ((x+1)<this->width)
			{
				f << " ";
			}
		}
		f << "\n";
	}
	f.close();
}

void PPMImage::saveAndConvertInto(const std::string& filename, const std::string& extension) const {
	this->save(filename);
	std::ofstream f;

	debug("filename is \"", filename.c_str(), "\"");
	const char* bn = getBaseName(filename.c_str());
	debug("basename is \"", bn, "\"");
	std::string basename = std::string{bn};

	//we put the ppm into /tmp folder. This will avoid possible "paths tool long" errors
	execute_command("mv \"", filename.c_str(), ".ppm\"", " \"/tmp/", basename.c_str(), ".ppm\"");

	try {
		//convert ciao.DijkstraEarlyStop.expandedNodes.0.ppm result.png
		execute_command("convert \"/tmp/", basename.c_str(), ".ppm\" \"/tmp/", basename.c_str(), ".", extension.c_str(), "\"");
	} catch (ExternalProgramException& e) {
		error("error during convert utility (exit code", e.exit_code(), ". Program executed was:", e.program());
		throw std::domain_error{"we couldn't convert PPM into JPEG! Are you sure you have installed 'convert' utility?"};
	}

	//copy the image into the image
	execute_command("mv \"/tmp/", basename.c_str(), ".", extension.c_str(), "\" \"", filename.c_str(), ".", extension.c_str(), "\"");
	//remove the ppm image in tmp
	execute_command("rm \"/tmp/", basename.c_str(), ".ppm\"");
}

void PPMImage::saveAndConvertIntoJPEG(const std::string& filename) const {
	saveAndConvertInto(filename, std::string{"jpeg"});
}

void PPMImage::saveAndConvertIntoPNG(const std::string& filename) const {
	saveAndConvertInto(filename, std::string{"png"});
}

void PPMImage::setPixel(dpf::coo2d_t x, dpf::coo2d_t y, const color_t& color)
{
	this->matrix[this->getIdFromCoordinated(x, y)] = color;
}

color_t& PPMImage::getPixel(dpf::coo2d_t x, dpf::coo2d_t y)
{
	return this->matrix[this->getIdFromCoordinated(x, y)];
}

color_t PPMImage::getPixel(dpf::coo2d_t x, dpf::coo2d_t y) const
{
	return this->matrix[this->getIdFromCoordinated(x, y)];
}

void PPMImage::fillImageWith(const color_t& color)
{
	for (auto y=0; y<this->height; ++y)
	{
		for (auto x=0; x<this->width; ++x)
		{
			this->setPixel(x, y, color);
		}
	}
}

bool PPMImage::areSameSizes(const PPMImage& other) const {
	return (this->width == other.width) && (this->height == other.height);
}

PPMImage PPMImage::scale(int scaleX, int scaleY) const {
	PPMImage result{this->width*scaleX, this->height*scaleY, WHITE};

	debug("old image:", this->width, this->height, "new image:", result.width, result.height);
	for (int y=0; y<this->height; ++y) {
		for (int x=0; x<this->width; ++x) {
			color_t c = this->getPixel(x, y);

			debug("scaling cell", x, ",", y, "vs", this->width, this->height);
			//fill the result
			for (int suby=0; suby<scaleY; ++suby) {
				for (int subx=0; subx<scaleX; ++subx) {
					result.setPixel(scaleX * x + subx, scaleY * y + suby, c);
				}
			}

		}
	}

	return result;
}

PPMImage& PPMImage::operator +=(const PPMImage& other) {
	if (!this->areSameSizes(other)) {
		throw std::invalid_argument{"you've tried to add update 2 images with different width and/or heights"};
	}
	//TODO we could use move constructor to increase performances
	PPMImage sum{*this + other};
	delete[] this->matrix;
	this->matrix = sum.matrix;
	sum.matrix = nullptr;

	return *this;
}

PPMImage& PPMImage::operator -=(const PPMImage& other) {
	if (!this->areSameSizes(other)) {
		throw std::invalid_argument{"you've tried to subtract update 2 images with different width and/or heights"};
	}
	//TODO we could use move constructor to increase performances
	PPMImage sum{*this - other};
	delete[] this->matrix;
	this->matrix = sum.matrix;
	sum.matrix = nullptr;

	return *this;
}

unsigned long long PPMImage::getIdFromCoordinated(dpf::coo2d_t x, dpf::coo2d_t y) const
{
	return y * this->width + x;
}

PPMImage operator + (const PPMImage& a, const PPMImage& b) {
	if (!a.areSameSizes(b)) {
		throw CpdException{"you've tried sum images with different width/height! a= %ld x %ld b= %ld x %ld", a.width, a.height, b.width, b.height};
	}

	PPMImage result{a.width, a.height, color_t{0,0,0}};
	for (auto y=0; y<a.height; ++y) {
		for (auto x=0; x<a.width; ++x) {
			//white color is special: it's like the default background. We ignore it if a pixel is white
			if (a.getPixel(x,y) == WHITE) {
				result.setPixel(x, y, b.getPixel(x,y));
			} else if (b.getPixel(x,y) == WHITE) {
				result.setPixel(x, y, a.getPixel(x,y));
			} else {
				result.setPixel(x, y, a.getPixel(x, y).lerp(b.getPixel(x, y)));
			}
		}
	}

	return result;
}
PPMImage operator - (const PPMImage& a, const PPMImage& b) {
	if (!a.areSameSizes(b)) {
		throw std::invalid_argument{"you've tried to subtract one image to another one with different width and/or height!"};
	}

	PPMImage result{a.width, a.height, color_t{0,0,0}};
	for (auto y=0; y<a.height; ++y) {
		for (auto x=0; x<a.width; ++x) {
			result.setPixel(x, y, a.getPixel(x, y) - b.getPixel(x, y));
		}
	}

	return result;
}
