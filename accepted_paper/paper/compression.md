CPD file size vs landmakr file size
===================================

|map             |cpd    |landmark 6   |landmark 12   |landmark 18   |
|----------------|-------|-------------|--------------|--------------|
|hrt201n         |3.1M   |555k         |1.1M          |1.7M          |
|dustwallowkeys  |542M   |5.5M         |11M           |17M           |
|16room_003      |164M   |5.4M         |11M           |16M           |
|maze512-1-4     |3M     |3.1M         |6.1M          |9.1M          |

