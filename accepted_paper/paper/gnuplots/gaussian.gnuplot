#!/usr/bin/gnuplot

# PARAMETERS
output_filename = 'gaussian.eps'
x_label = "Edge distance from n (#)"
y_label = "Edge Weight multipler (#)"

# SCRIPT

#load 'style2.gnu'
#set terminal postscript color eps enhanced font "Helvetica, 20"
set terminal epslatex color
set out "gaussian.tex"
set size 1, 1
set output output_filename
set xrange [0: 16]
set yrange [0: 5]
set grid

f(x) = 1 + 3*exp(-(x*x)/125)
# Plot
plot f(x) title "$1+3e^{-\frac{x^2}{125}}$" with lines linestyle 1

set out