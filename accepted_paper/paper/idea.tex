\section{Searching with CPD Heuristics}

%\begin{itemize}
%\item Description of the idea
%\item Pseudocode
%\item Properties (bounds, early termination)
%\end{itemize}

\ignore
{
The principal idea we explore in this paper is the use of CPD as a lower bound heuristic for 
path planning in graphs with dynamic edge-costs.  
Suppose we are given as input a graph $G = (N,E)$ and a corresponding weight function $w$, 
which we refer to as the \emph{original} edge weights.
We construct a CPD for $G$ given $w$ as usual and then proceed to modify edge weights 
in order to derive a new \emph{perturbed} weight function $w'$ where, for any edge $e \in E$
we have $w'(e) \geq w(e)$. The following two results are easy to see and also appear in 
%~\cite{dw-lbrdg-07}:
\begin{itemize}
\item The cost of any $s$ to $t$ path given $w$, as indicated by the notation $c(P,w)$,
is necessarily a lower bound on $c(P,w')$; i.e. the cost of the same path given $w'$.
\item The cost of an optimal path $P$, from $s$ to $t$ with respect to $w$,
is necessarily a lower bound on the cost of $P'$, an optimal path from $s$ to $t$
with respect to $w'$. To see this notice that $c(P, w) \leq c(P', w) \leq c(P', w')$. 
\end{itemize}
}

The principal idea we explore in this paper is the use of CPD as a lower bound heuristic for path planning.
Under the perturbation scenario we consider, the cost of the shortest path recorded in the CPD
using original weights $w$ is a lower bound on the actual shortest path in the graph using new weights $w'$.
Clearly since $w'(s,t) \geq w(s,t)$ for all $(s,t) \in E$ we have that
$c(P,w') \geq c(P,w)$ for all paths $P$ in $G$.
Hence the CPD defines an \emph{admissible heuristic} for A* search.


Furthermore, since the path $P$ recorded in the CPD can be quickly recovered,
we can calculate its cost $c(P,w')$ using the new weights $w'$.
This allows us to track an incumbent solution which is the shortest such path we have found.
Given that we have an upper bound on the solution,
and the $f$ costs of nodes represent a lower bound of the solution we can then
adjust the A* algorithm to return a \emph{bounded suboptimal path} whose
cost is not more than $\epsilon$ times the shortest path, where $\epsilon \geq 1$.
This may allow us to explore much less of the graph than if we demand to find an optimal path.

\ignore{
Given a graph $G = (N,E)$ with original weight function
$w$ we can construct a CPD for $G$ with $w$ as usual.
Then when path planning in the graph $G$ with altered weights $w'$ where for any edge $e \in E$
$w'(e) \geq w(e)$ the cost of the shortest path in $G$ given $w$, $wSP(s,t,w)$,
is necessarily a lower bound on the length of the shortest path in $G$ given $w'$, $wSP(s,t,w')$.



We can lookup a shortest path from $s$ to $t$ given $w$, using the CPD, as $P$ = \textsf{CPD}($s$,$t$).
We can use this to calculate its length, $wSP(s,t,w) = W(P)$,
thus giving us an admissible heuristic for
use in our path planning algorithm.

Unlike other heuristics we can also calculate $w'(P)$, the length of the path returned
by the CPD heuristic given the hew weights.  This gives us an incumbent solution to the problem
and an \emph{upper bound} on the shortest path from $s$ to $t$ in $G$ with $w'$.
This means we have the capacity to
relax our search for the shortest path to only look for one within $\epsilon$
of the shortest path
}


Algorithm 1 is a modification of A* as follows.
For simplicity, we store with each node $n$
in the open list a (current) shortest path $p[n]$ from $s$ to $n$;
in practice this is stored by back pointers.
We keep track of a shortest incumbent path $I$ and its cost $u$ (an upper bound
on the cost of the shortest possible path). The incumbent $I$ is stored as a node $m$ which
encodes the concatenation of the current path from $s$ to $m$ with the CPD path from $m$ to $t$.

When we select a node $n$ from the $open$ set (line~\ref{l:sn}),
if it is the target we have found an optimal
path and we return it (line~\ref{l:rn}).
If $\epsilon$ times the $f$ value is not less than $u$,
then the incumbent path $I$ is no worse than factor $\epsilon$ from optimal, and 
we return the incumbent (line~\ref{l:u})
(function \textsf{SP-CPD}($I,t)$ returns the path recorded by the CPD from $I$ to $t$).
%
Note that the algorithm will always \emph{terminate early}
if it expands a node $n$, where \textsf{wCPD}($n$,$t$) indicates that $h = h'$.
In this case, when $n$ was added to $open$ it also became the incumbent $I$
and set $u = f[n] = g[n] + h$, and hence the test on line~\ref{l:u} succeeds.

Otherwise we investigate the neighbours $m$ of $n$ where the path from $s$ to $m$ via $n$
is shorter than any previously found path.
We call \textsf{wCPD}($m$,$t$) which returns  the cost $h = c(P,w)$ according to $w$
of the path $P$ recorded in the CPD from $m$ to $t$, as well as the cost $h' = c(P,w')$
according to $w'$.
We update the $f$ cost of $m$ using the original cost $h$ of this path (line~\ref{l:f}).
%We also check if the cost of the path to $m$ followed by $P$, \emph{using the current weights $w'$}
We also check if the cost to $m$ followed by $P$, \emph{using the current weights $w'$}
is shorter than any previous path (line~\ref{l:ub}).
If so we update the upper bound $u$ and the incumbent $I$.

\input alg_astar
\input alg_wcpd

Algorithm 2 simply returns the cost of the CPD path from $s$ to $t$
both using the original weights $w$ and the new weights $w'$.
The path can always be reconstructed using the CPD itself.
The pseudo-code use $co[s]$ and $cn[s]$ to cache the cost of the shortest path from
$s$ to $t$ using original weights $w$, and the cost of the same path,
not necessarily shortest for these weights, using weights $w'$.
We assume the cache is initialised to $\infty$ for each new \textsf{CPD-Search} call. 

Note that because of caching of calls to \textsf{wCPD}, the CPD is only called for
any node $m$ for current target $t$ at most once.
Hence the CPD heuristic is amortised ${\cal O}(1)$ cost to compute. 

\begin{proposition}
  Let $P$ be the path returned by $\textsf{CPD-Search}(s,t,w,w',\epsilon)$
  then $c(P,w') \leq \epsilon c(S,w')$
  for all paths $S$ in $G$ from $s$ to $t$.
\end{proposition}
\begin{proof}
  Since the CPD heuristic is admissible, we know that $f[n]$ is no greater
  than the cost of the shortest path from $s$ to $t$ via $n$.  When the node
  $n$ in {\it open} with minimum $f[n]$ is such that $\epsilon f[n] \geq u$,
  where $u$ is the cost of the incumbent, then we have that no path
  from $s$ to $t$ via $n$ can cost less than $u$.
%  and hence the result holds. 
\end{proof}
  

