#ifndef _MAPPERTURBATOR_EXCEPTIONS_HPP__
#define _MAPPERTURBATOR_EXCEPTIONS_HPP__

#include <exception>

namespace map_perturbator {

/**
 * @brief exception to throw if RatioQueryPerturbationPositionStrategy::choosePosition can establishg a correct position
 * 
 */
class PerturbationLocationNotFoundException: public std::exception {
public:
    PerturbationLocationNotFoundException(int queryId);
    const char* what () const throw ();
private:
    int queryId;
};

}

#endif 