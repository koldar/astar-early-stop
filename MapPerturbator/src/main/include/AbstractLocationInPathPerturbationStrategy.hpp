/**
 * @file
 *
 * A policy to perturb the map
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#ifndef ABSTRACTLOCATIONINPATHPERTURBATIONSTRATEGY_HPP_
#define ABSTRACTLOCATIONINPATHPERTURBATIONSTRATEGY_HPP_

#include <DynamicPathFinding/adj_graph.h>
#include <DynamicPathFinding/xyLoc.h>
#include <vector>

namespace map_perturbator {

class AbstractLocationInPathPerturbationStrategy {
public:
	AbstractLocationInPathPerturbationStrategy();
	virtual ~AbstractLocationInPathPerturbationStrategy();
	/**
	 * @param[in] queryId the query to alter
	 * @param[in] graph the graph to alter
	 * @param[in] location the location to perturb
	 * @param[in] locationId the id of the location @c location
	 * @param[in] paths locations (start and goal included) representing the optimal path of the query to alter
	 * @return the number of arcs altered
	 */
	virtual int perturbateLocation(int queryId, AdjGraph& graphToPerturbate, xyLoc location, dpf::nodeid_t locationId, const std::vector<dpf::nodeid_t>& paths) = 0;
};

}

#endif /* ABSTRACTLOCATIONINPATHPERTURBATIONSTRATEGY_HPP_ */
