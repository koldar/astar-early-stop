/*
 * Utils.hpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <DynamicPathFinding/adj_graph.h>
#include <DynamicPathFinding/types.h>
#include <vector>
#include <DynamicPathFinding/types.h>

namespace map_perturbator {

std::vector<xyLoc> getOptimalPathAsXyLoc(const AdjGraph& g, const Mapper& mapper, dpf::nodeid_t start, dpf::nodeid_t goal);
std::vector<dpf::nodeid_t> getOptimalPathAsIds(const AdjGraph& graph, const Mapper& mapper, dpf::nodeid_t start, dpf::nodeid_t goal);
std::vector<Arc> getArcsFromLocations(const AdjGraph& map, const Mapper& mapper, const std::vector<xyLoc>& locations);

}



#endif /* UTILS_HPP_ */
