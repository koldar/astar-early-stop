/**
 * @file
 *
 * tells where in the path we want to perturbate
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#ifndef ABSTRACTQUERYPERTURBATIONPOSITIONSTRATEGY_HPP_
#define ABSTRACTQUERYPERTURBATIONPOSITIONSTRATEGY_HPP_

#include <DynamicPathFinding/types.h>
#include <DynamicPathFinding/adj_graph.h>
#include <vector>

namespace map_perturbator {

class AbstractQueryPerturbationPositionStrategy {
public:
	AbstractQueryPerturbationPositionStrategy();
	~AbstractQueryPerturbationPositionStrategy();
	/**
	 * @brief choose a location given a particula query
	 * 
	 * @param queryId the id of the query involved
	 * @param graphToPerturbate the map where the query is located
	 * @param mapper mapper allowing you to transform node ids into xylocs
	 * @param start start of the query
	 * @param goal end of thew query
	 * @return dpf::nodeid_t a node id which is the epicenter of the perturbation we want to apply to @c graphToPerturbate
	 */
	virtual dpf::nodeid_t choosePosition(int queryId, const AdjGraph& graphToPerturbate, const Mapper& mapper, dpf::nodeid_t start, dpf::nodeid_t goal) = 0;
	virtual void reset() = 0;
};

}

#endif /* ABSTRACTQUERYPERTURBATIONPOSITIONSTRATEGY_HPP_ */
