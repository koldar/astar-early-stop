/*
 * OptimalPathContiguousSubSetPerturbationStrategy.hpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#ifndef OPTIMALPATHCONTIGUOUSSUBSETPERTURBATIONSTRATEGY_HPP_
#define OPTIMALPATHCONTIGUOUSSUBSETPERTURBATIONSTRATEGY_HPP_

#include <PathTesterUtils/cliUtils.h>
#include <vector>
#include <DynamicPathFinding/mapper.h>
#include "AbstractLocationInPathPerturbationStrategy.hpp"

namespace map_perturbator {

class OptimalPathContiguousSubSetPerturbationStrategy : public AbstractLocationInPathPerturbationStrategy {
public:
	OptimalPathContiguousSubSetPerturbationStrategy(const Mapper& mapper, range<int> sequenceLength, range<int> perturbationRange, const std::string& mode);
	virtual ~OptimalPathContiguousSubSetPerturbationStrategy();
	/**
	 * @param[in] graph the graph to alter
	 * @param[in] location the location to perturb
	 * @param[in] locationId the id of the location @c location
	 * @return the number of arcs altered
	 */
	virtual int perturbateLocation(int quetyId, AdjGraph& graphToPerturbate, xyLoc location, dpf::nodeid_t locationId, const std::vector<dpf::nodeid_t>& path);
private:
	range<int> sequenceLength;
	const std::string& mode;
	range<int> perturbationRange;
	const Mapper& mapper;
};

}

#endif /* OPTIMALPATHCONTIGUOUSSUBSETPERTURBATIONSTRATEGY_HPP_ */
