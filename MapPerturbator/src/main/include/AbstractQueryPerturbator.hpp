/*
 * AbstractQueryPerturbator.hpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#ifndef ABSTRACTQUERYPERTURBATOR_HPP_
#define ABSTRACTQUERYPERTURBATOR_HPP_

#include "AbstractPerturbator.h"
#include <DynamicPathFinding/ScenarioLoader.h>

namespace map_perturbator {

/**
 * perturbator which generates a different perturbate map per query
 */
class AbstractQueryPerturbator {
public:
	AbstractQueryPerturbator();
	virtual ~AbstractQueryPerturbator();
	virtual AdjGraph perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map, int queryId, const ScenarioLoader& loader) = 0;
	virtual void reset() = 0;
};

}

#endif /* ABSTRACTQUERYPERTURBATOR_HPP_ */
