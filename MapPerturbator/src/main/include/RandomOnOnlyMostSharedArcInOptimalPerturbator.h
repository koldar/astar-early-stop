///*
// * RandomOnEveryOptimalPerturbator.h
// *
// *  Created on: Nov 2, 2018
// *      Author: koldar
// */
//
//#ifndef RANDOMONONLYMOSTSHAREDARCINOPTIMALPERTURBATOR_H_
//#define RANDOMONONLYMOSTSHAREDARCINOPTIMALPERTURBATOR_H_
//
//#include "AbstractPerturbator.h"
//#include <PathTesterUtils/cliUtils.h>
//#include <DynamicPathFinding/ScenarioLoader.h>
//
//namespace map_perturbator {
//
///**
// * A perturbator which perturbate arcs only in the current optimal paths of the queries
// *
// * The perturbators firstly get all the optimal paths of all the queries, computes what are the shared arcs between the optimal paths
// * and then proceeds to alter the arcs from the most shared one till tghe least one.
// *
// * The perturbator consider only the optimal paths in the original path. In other words if the perturbation alter the optimal path of a query,
// * this poerturbator won't touch the new optimal path!
// */
//class RandomOnOnlyMostSharedArcInOptimalPerturbator : public AbstractPerturbator {
//public:
//	RandomOnOnlyMostSharedArcInOptimalPerturbator(double probability, const range<int>& range, const std::string& mode, const std::string& scenarioFilename) : AbstractPerturbator{probability, range, mode}, scenarioFilename{scenarioFilename} {
//
//	}
//	~RandomOnOnlyMostSharedArcInOptimalPerturbator() {
//
//	}
//	virtual AdjGraph perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map);
//protected:
//	/**
//	 * choose an arc to alter in a path
//	 *
//	 * @param[in] map the map where the path is located on
//	 * @param[in] mapper a structure allowing to translate node ids into cell locations
//	 * @param[in] start the beginning of the path
//	 * @param[in] goal the end of the pat
//	 * @param[in] path the path consider
//	 * @param[out] source id of the node of the arc chosen
//	 * @param[out] sink id of the node of the arc chosen
//	 * @return
//	 *  @li true if we were able to find an unchanged arc;
//	 *  @li false otherwise
//	 */
//	virtual bool pickArcInOptimalPath(const AdjGraph& map, const Mapper& mapper, const xyLoc& start, const xyLoc& goal, const std::vector<xyLoc>& path, dpf::nodeid_t& source, dpf::nodeid_t& sink);
//	virtual int pickQuery(const ScenarioLoader& scenarioLoader);
//private:
//	/**
//	 * @param[in] scenarioLoader a structure containing the queries fetched from the scenario
//	 * @param[in] g the graph where we want to compute the optimal paths on
//	 * @param[in] mapper a structure allowing to convert node id and location and viceversa
//	 * @return a vector with the same size of the queries in @c scenarioLoader containing, for each query, the optimal path over the map
//	 */
//	std::vector<gridmap_path> getAllOptimalPaths(const ScenarioLoader& scenarioLoader, const AdjGraph& g, const Mapper& mapper);
//	/**
//	 * @param[in] scenarioLoader the scenario involved
//	 * @param[in] graph the graph where all the path are laid down
//	 * @param[in] mapper structure allowing you to convert nodeid to locations and viceversa
//	 * @return
//	 *  an **ordered list** of arcs representing, from the most used till the least one, the arcs involved in the optimal paths of the queries in the @c scenarioLoader.
//	 *  Arcs near the head of the lists are shared among lots of optimal paths
//	 */
//	std::vector<Arc> getMostUsedArcsForOptimalPaths(const ScenarioLoader& scenarioLoader, const AdjGraph& graph, const Mapper& mapper);
//private:
//	const std::string scenarioFilename;
//	double perturbationProbability;
//	range<int> perturbationRange;
//	const std::string& perturbationMode;
//};
//
//
//}
//
//
//#endif /* RANDOMONONLYMOSTSHAREDARCINOPTIMALPERTURBATOR_H_ */
