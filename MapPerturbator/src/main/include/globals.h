/*
 * cliOptions.h
 *
 *  Created on: Oct 25, 2018
 *      Author: koldar
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <string>
#include <random>

namespace map_perturbator {

extern std::string global_mapFilename;
extern std::string global_scenarioFilename;
extern std::string global_perturbationModeStr;
extern std::string global_perturbationRangeStr;
extern std::string global_perturbationDensityStr;
extern int global_randomSeed;
extern std::string global_outputTemplate;
extern std::string global_mapPerturbationStr;
extern bool global_validateChanges;
extern std::string global_perturbationAreaRadiusStr;
extern std::string global_terrainToPerturbateSetStr;
extern int global_terranToPerturbateNumber;
extern std::string global_perturbationSequenceStr;
extern bool global_generatePerturbationsPerQuery;
extern std::string global_locationChooserStr;
extern std::string global_optimalPathRatioStr;

extern const std::string MAP_PERTURBATION_RANDOM;
extern const std::string MAP_PERTURBATION_RANDOM_ON_OPTIMAL;
extern const std::string MAP_PERTURBATION_RANDOM_ON_MOSTSHAREDARCS;
extern const std::string MAP_PERTURBATION_RANDOM_ON_AREA;
extern const std::string MAP_PERTURBATION_RANDOM_TERRAIN;
extern const std::string MAP_PERTURBATION_PATH_RANDOM_ON_OPTIMAL;

extern const std::string QUERY_PERTURBATION_RANDOM_ON_AREA;
extern const std::string QUERY_PERTURBATION_RANDOM_ON_OPTIMALPATH;

extern const std::string QUERY_LOCATION_RATIO_ON_OPTIMAL_PATH;

extern std::default_random_engine global_randomEngine;

}



#endif /* GLOBALS_H_ */
