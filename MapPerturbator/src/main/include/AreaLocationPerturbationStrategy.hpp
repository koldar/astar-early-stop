/*
 * AreaLocationPerturbationStrategy.hpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#ifndef AREALOCATIONPERTURBATIONSTRATEGY_HPP_
#define AREALOCATIONPERTURBATIONSTRATEGY_HPP_

#include <PathTesterUtils/cliUtils.h>
#include <unordered_set>
#include "AbstractLocationInPathPerturbationStrategy.hpp"

namespace map_perturbator {

struct node_info;

/**
 * @brief exception to throw when you fetch the start node of a query and you didn't want it
 * 
 */
class StartNodeInterceptedException : std::exception {
	const char* what() throw();
};

/**
 * @brief exception to throw when you fetch the goal node of a query and you didn't want it
 * 
 */
class GoalNodeInterceptedException : std::exception {
	const char* what() throw();
};

class AreaLocationPerturbationStrategy: public AbstractLocationInPathPerturbationStrategy {
public:
	/**
	 * @brief Construct a new Area Location Perturbation Strategy object
	 * 
	 * @param radius 
	 * @param perturbationRange 
	 * @param mode 
	 * @param ensureStartGoalAreNotAffected if true we will throw error if we're trying to change an edge hose endpoint is either the start/end of the query
	 */
	AreaLocationPerturbationStrategy(range<int> radius, range<int> perturbationRange, const std::string& mode, bool ensureStartGoalAreNotAffected);
	virtual ~AreaLocationPerturbationStrategy();
	virtual int perturbateLocation(int queryId, AdjGraph& graphToPerturbate, xyLoc location, dpf::nodeid_t locationId, const std::vector<dpf::nodeid_t>& paths);
private:
	/**
	 * @brief Get the New Weight In Area object
	 * 
	 * Generates the following gaussian:
	 * 
	 * M*e^(-lambda * (1/2)*(x/stddev)^2) + k
	 * 
	 * This gaussian useful because at the beginning is (M+k) and after stddev is k
	 * 
	 * @param x x value of the gaussian
	 * @param M the maximum value the gaussian can have
	 * @param stddev  standard deviation of the gaussian
	 * @param k a value to add to the gaussian
	 * @param lambda a standard deviation multiplier. If greater than 1 it will compress the gaussian. If less than 1 it will stretch it. Negative values (or 0) will lead to UB
	 * 	This can be tuned to ensure that at stddev  the gaussian is k. Default value to 10
	 * @return double the y value of the gaussian
	 */
	double getNewWeightInArea(double x, double M, double stddev, double k, double lambda=10.0) const;
	void performDFS(const AdjGraph& graph, dpf::nodeid_t start, int maxDepth, std::vector<node_info>& nodes, dpf::nodeid_t startPath, dpf::nodeid_t endPath) const;
	/**
	 * @brief Get a correct location
	 * 
	 * Assume you don't want to include the end into the area perturbation.
	 * A naive approach would be not to choose as a location some xy near the endpoint along the optimal path
	 * However, it maybe that the optimal path does a sort of "U" at the end of the path: hence even if you choose
	 * a location which is far from the goal along the optimal path it may be that, considering only cells,
	 * the chosen location is not so distantr from it.
	 * 
	 * This function just moves the "epicenter location" far away from the goal up until a DFS don't include the goal no more.
	 * 
	 * 
	 * 
	 * @param graphToPerturbate the graph involved
	 * @param location the location chosen by a "AAbstractQueryPerturbationPositionStrategy"
	 * @param locationId the location chosen by a "AAbstractQueryPerturbationPositionStrategy"
	 * @param paths the optimal path involved
	 * @param areaRadius the radius of the areas to perturbate
	 * @return dpf::nodeid_t 
	 */
	dpf::nodeid_t getCompliantEpicenter(int queryId, AdjGraph& graphToPerturbate, xyLoc location, dpf::nodeid_t locationId, const std::vector<dpf::nodeid_t>& paths, int areaRadius);
private:
	range<int> radius;
	const std::string& mode;
	range<int> perturbationRange;
	bool ensureStartGoalAreNotAffected;
};

}

#endif /* AREALOCATIONPERTURBATIONSTRATEGY_HPP_ */
