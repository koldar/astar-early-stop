/*
 * RandomPerturbator.h
 *
 *  Created on: Oct 25, 2018
 *      Author: koldar
 */

#ifndef RANDOMPERTURBATOR_H_
#define RANDOMPERTURBATOR_H_

#include "AbstractPerturbator.h"
#include <PathTesterUtils/cliUtils.h>
#include <DynamicPathFinding/types.h>

namespace map_perturbator {

class RandomPerturbator : public AbstractPerturbator {
public:
	RandomPerturbator(dpf::big_integer edgesToAlter, const range<int>& range, const std::string& mode) : edgesToAlter{edgesToAlter}, perturbationRange{range}, perturbationMode{mode} {

	}
	~RandomPerturbator() {

	}
	virtual AdjGraph perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map);
protected:
	dpf::big_integer edgesToAlter;
	range<int> perturbationRange;
	const std::string& perturbationMode;
};

}


#endif /* RANDOMPERTURBATOR_H_ */
