/**
 * @file
 *
 * perturbator where we perturbate all the arcs inside a particular cell type
 *
 *  Created on: Nov 10, 2018
 *      Author: koldar
 */

#ifndef RANDOMTERRAINPERTURBATOR_H_
#define RANDOMTERRAINPERTURBATOR_H_

#include "AbstractPerturbator.h"
#include <DynamicPathFinding/types.h>
#include <unordered_set>

namespace map_perturbator {

class RandomTerrainPerturbator : public AbstractPerturbator {
public:
	RandomTerrainPerturbator(const std::unordered_set<dpf::celltype_t>& possibleCellTypesToPerturbate, int numberOfCellTypeToPerturbate, const range<int>& range, const std::string& mode);
	virtual ~RandomTerrainPerturbator();
	virtual AdjGraph perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map);
private:
	void perturbateWholeTerrain(dpf::celltype_t terrainToAlter, AdjGraph& buildingMap, const Mapper& mapper, const GridMap& map);
private:
	range<int> perturbationRange;
	const std::string& perturbationMode;
	const std::unordered_set<dpf::celltype_t>& possibleCellTypesToPerturbate;
	const int numberOfCellTypeToPerturbate;
};

}

#endif /* RANDOMTERRAINPERTURBATOR_H_ */
