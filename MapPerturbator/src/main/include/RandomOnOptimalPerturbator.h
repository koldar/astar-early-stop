/*
 * RandomOnOptimalPerturbator.h
 *
 *  Created on: Oct 25, 2018
 *      Author: koldar
 */

#ifndef RANDOMONOPTIMALPERTURBATOR_H_
#define RANDOMONOPTIMALPERTURBATOR_H_

#include "AbstractPerturbator.h"
#include <PathTesterUtils/cliUtils.h>
#include <DynamicPathFinding/types.h>

namespace map_perturbator {

class RandomOnOptimalPerturbator : public AbstractPerturbator {
public:
	RandomOnOptimalPerturbator(dpf::big_integer edgesToAlter, const range<int>& range, const std::string& mode, const std::string& scenarioFilename) : edgesToAlter{edgesToAlter}, perturbationRange{range}, perturbationMode{mode}, scenarioFilename{scenarioFilename} {

	}
	~RandomOnOptimalPerturbator() {

	}
	virtual AdjGraph perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map);
protected:
	/**
	 * choose an arc to alter in a path
	 *
	 * @param[in] map the map where the path is located on
	 * @param[in] mapper a structure allowing to translate node ids into cell locations
	 * @param[in] start the beginning of the path
	 * @param[in] goal the end of the pat
	 * @param[in] path the path consider
	 * @param[out] source id of the node of the arc chosen
	 * @param[out] sink id of the node of the arc chosen
	 * @return
	 *  @li true if we were able to find an unchanged arc;
	 *  @li false otherwise
	 */
	virtual bool pickArcInOptimalPath(const AdjGraph& map, const Mapper& mapper, const xyLoc& start, const xyLoc& goal, const std::vector<xyLoc>& path, dpf::nodeid_t& source, dpf::nodeid_t& sink);
private:
	const std::string scenarioFilename;
	dpf::big_integer edgesToAlter;
	range<int> perturbationRange;
	const std::string& perturbationMode;
};

}


#endif /* RANDOMONOPTIMALPERTURBATOR_H_ */
