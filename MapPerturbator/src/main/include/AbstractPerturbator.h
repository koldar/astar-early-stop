/*
 * AbstractPerturbator.hpp
 *
 *  Created on: Oct 25, 2018
 *      Author: koldar
 */

#ifndef ABSTRACTPERTURBATOR_H_
#define ABSTRACTPERTURBATOR_H_

#include <vector>
#include <DynamicPathFinding/adj_graph.h>
#include <DynamicPathFinding/mapper.h>
#include <DynamicPathFinding/gridmap.h>
#include <PathTesterUtils/cliUtils.h>

namespace map_perturbator {

class AbstractPerturbator {
public:
	AbstractPerturbator();

	virtual ~AbstractPerturbator() {

	}

	virtual AdjGraph perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map) = 0;
	virtual void reset();
	int getEdgeChanged() const;
protected:
	virtual void perturbateArc(const Mapper& mapper, AdjGraph& graph, dpf::nodeid_t source, dpf::nodeid_t sink, int rangeLowerBound, int rangeUpperBound, const std::string& perturbationMode, std::default_random_engine randomEngine);
	virtual std::vector<xyLoc> getOptimalPath(const AdjGraph& graph, const Mapper& mapper, const xyLoc& start, const xyLoc& goal) const;
protected:
	int edgeChanged;
};

}

#endif /* ABSTRACTPERTURBATOR_H_ */
