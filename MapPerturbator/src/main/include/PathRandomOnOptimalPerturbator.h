/**
 * @file
 *
 * Perturbator perturbation optimal path coming from the original map.
 *
 * So it alters arcs only involved in the first optimal paths.
 * It also perturbate the optimal paths by altering contiguous arcs in the optimal paths
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */

#ifndef PATHRANDOMONOPTIMALPERTURBATOR_H_
#define PATHRANDOMONOPTIMALPERTURBATOR_H_

#include "AbstractPerturbator.h"
#include <vector>
#include <DynamicPathFinding/types.h>

namespace map_perturbator {

class PathRandomOnOptimalPerturbator : public AbstractPerturbator {
public:
	PathRandomOnOptimalPerturbator(range<int> sequentialPerturbationInPath, dpf::big_integer edgesToAlter, const range<int>& range, const std::string& mode, const std::string& scenarioFilename);
	virtual AdjGraph perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map);
protected:
	std::vector<Arc> pickArcsInOptimalPath(const AdjGraph& map, const Mapper& mapper, const xyLoc& start, const xyLoc& goal, const std::vector<xyLoc>& path, int sequenceLength);
	std::vector<Arc> getArcsFromLocations(const AdjGraph& map, const Mapper& mapper, const std::vector<xyLoc>& locations) const;
private:
	range<int> sequentialPerturbationInPath;
	const std::string scenarioFilename;
	dpf::big_integer edgesToAlter;
	range<int> perturbationRange;
	const std::string& perturbationMode;
};

}

#endif /* PATHRANDOMONOPTIMALPERTURBATOR_H_ */
