/**
 * @file
 *
 * We choose where to perturb a query at a certain distance in the path
 *
 *
 * For example if the path is long 100 tiles and we specified "0.25" it means we want o inject a perturbation at the 25%
 * of the optimal path
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#ifndef RATIOQUERYPERTURBATIONPOSITIONSTRATEGY_HPP_
#define RATIOQUERYPERTURBATIONPOSITIONSTRATEGY_HPP_

#include "AbstractQueryPerturbationPositionStrategy.hpp"
#include <PathTesterUtils/cliUtils.h> 

namespace map_perturbator {

enum class PathTooShortPolicy {
	AVOID_GENERATE_PERTURBATION,
	THROW_ERROR
};

class RatioQueryPerturbationPositionStrategy : public AbstractQueryPerturbationPositionStrategy {
public:
	/**
	 * @brief Construct a new Ratio Query Perturbation Position Strategy object
	 * 
	 * @param ratio the range (in percentage) where the location is allowed to be chosen (e.g., [25,75] means between 25% and 75% of optimal path)
	 * @param ensureLocationIsFarFromStartGoal if true we will ewnsure that the location chosen respect the limit of numberOfCellsFarFromStartGoal. Ignored otherwise
	 * @param numberOfCellsFarFromStartGoal number of "locations" the result needs to be distant fom both start and goal.
	 * @param[in] policy what to do if the path we need to compute is too short
	 * @throws PerturbationLocationNotFoundException if a location cannot be establish properly. This doesn't eman that
	 * an error was found during processing, but rather that we establish a location cannot be found
	 */
	RatioQueryPerturbationPositionStrategy(range<int> ratio, bool ensureLocationIsFarFromStartGoal, int numberOfCellsFarFromStartGoal, PathTooShortPolicy policy);
	virtual ~RatioQueryPerturbationPositionStrategy();
	/**
	 * @brief computew the position where to perturbate the map
	 * 
	 * @param queryId the id of the query to perturbate
	 * @param graphToPerturbate the graph to perturvate
	 * @param mapper mapper allowing you to convert node ids into xyloc
	 * @param start start of the query
	 * @param goal end of the query
	 * @return dpf::nodeid_t 
	 */
	virtual dpf::nodeid_t choosePosition(int queryId, const AdjGraph& graphToPerturbate, const Mapper& mapper, dpf::nodeid_t start, dpf::nodeid_t goal);
	virtual void reset();
private:
	/**
	 * lower and  upperbound of a percentage of where the perturbation should be positioned
	 */
	range<int> ratio;
	bool ensureLocationIsFarFromStartGoal;
	int numberOfCellsFarFromStartGoal;
	PathTooShortPolicy pathTooShortPolicy;
};

}

#endif /* RATIOQUERYPERTURBATIONPOSITIONSTRATEGY_HPP_ */
