/*
 * Utils.cpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */




#include "Utils.hpp"
#include <DynamicPathFinding/earlystop_dijkstra.h>
#include <vector>

namespace map_perturbator {

std::vector<xyLoc> getOptimalPathAsXyLoc(const AdjGraph& graph, const Mapper& mapper, dpf::nodeid_t start, dpf::nodeid_t goal) {
	EarlyStopDijkstra earlyStopDijkstra = EarlyStopDijkstra{graph, false};
	std::vector<dpf::nodeid_t> optimalMoves = earlyStopDijkstra.run(start, goal);
	return earlyStopDijkstra.getPathAsVector(start, goal, mapper, optimalMoves);
}

std::vector<dpf::nodeid_t> getOptimalPathAsIds(const AdjGraph& graph, const Mapper& mapper, dpf::nodeid_t start, dpf::nodeid_t goal) {
	EarlyStopDijkstra earlyStopDijkstra = EarlyStopDijkstra{graph, false};
	std::vector<dpf::nodeid_t> optimalMoves = earlyStopDijkstra.run(start, goal);
	std::vector<xyLoc> locs = earlyStopDijkstra.getPathAsVector(start, goal, mapper, optimalMoves);
	return mapper.convert(locs);
}


std::vector<Arc> getArcsFromLocations(const AdjGraph& map, const Mapper& mapper, const std::vector<xyLoc>& locations) {
	std::vector<Arc> result;

	xyLoc tmp = locations[0];
	for (auto i=1; i<locations.size(); ++i) {
		dpf::nodeid_t source = mapper(tmp);
		dpf::nodeid_t sink = mapper(locations[i]);
		result.push_back(Arc{static_cast<int>(source), static_cast<int>(sink), 0}); //i don't care about the weight
		tmp = locations[i];
	}

	return result;
}

}
