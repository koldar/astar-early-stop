/*
 * MapPerturbator.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: koldar
 */

#include <string>
#include "CLI11.hpp"
#include <DynamicPathFinding/log.h>
#include <DynamicPathFinding/map_loader.h>
#include <DynamicPathFinding/adj_graph.h>
#include <random>
#include "AbstractPerturbator.h"
#include "RandomPerturbator.h"
#include "RandomOnOptimalPerturbator.h"
#include "RandomOnOnlyMostSharedArcInOptimalPerturbator.h"
#include "RandomTerrainPerturbator.h"
#include "PathRandomOnOptimalPerturbator.h"
#include "globals.h"
#include <DynamicPathFinding/cpd_heuristic.h>
#include <DynamicPathFinding/cpd_cached_heuristic.h>
#include <DynamicPathFinding/map_loader_factory.h>
#include <DynamicPathFinding/ScenarioLoader.h>
#include "Utils.hpp"

#include "AbstractLocationInPathPerturbationStrategy.hpp"
#include "AbstractQueryPerturbator.hpp"
#include "AbstractLocationInPathPerturbationStrategy.hpp"
#include "AreaLocationPerturbationStrategy.hpp"
#include "OptimalPathContiguousSubSetPerturbationStrategy.hpp"
#include "AbstractQueryPerturbationPositionStrategy.hpp"
#include "RatioQueryPerturbationPositionStrategy.hpp"
#include "exceptions.hpp"


static string getCpdFilename(const char* filename);
static const char* getBasename(const char* path, char* output);
static void generateInfoFile(const std::string& outputTemplate, const AdjGraph& g, dpf::big_integer edgeChanged);

using namespace map_perturbator;

int main(int argc, const char** args) {
	// ********************************************************************
	// *********************** CLI PARSING ********************************
	// ********************************************************************

	CLI::App app{"DynamicPathFindingTester"};

	global_perturbationAreaRadiusStr=std::string{"[3,3]"};
	global_perturbationDensityStr=std::string{};


	app.add_option("-m,--map_file", global_mapFilename,
			"The file where it is contained the map"
	)->required();
	app.add_option("-s,--scenario_file", global_scenarioFilename,
			"The file where are contained several queries"
	)->required();
	app.add_option("-p,--map_perturbation", global_mapPerturbationStr,
		"Tells how you want to perturbate the map. Possible values are RANDOM (generate random perturbations); "
		"RANDOM_ON_OPTIMAL (generate random perturbations on optimal paths of scenario); "
		"RANDOM_ON_MOSTSHAREDARCS (generate random perturbation only on most shared arcs among orignal map optimal paths); "
		"RANDOM_ON_AREA (generate random perturbation in circled-shaped sector of the map, each of them with the same amount of perturbations): you need to specify -R; "
		"RANDOM_TERRAIN (perturbate the whole terrain types. Does not use -d, but uses)"
		"PATH_RANDOM_ON_OPTIMAL (perturbate contiguous optimal paths ONLY in the original map)"
		"QUERY_RANDOM_OPTIMAL_PATH: generate perturbation in a contiguous subset of the optimal path. Works only with --generate_perturbations_per_query enabled"
		"QUERY_RANDOM_AREA: generate perturbation in area. Works only with --generate_perturbations_per_query enabled"
		""
	)->required();

	app.add_option("--location_chooser", global_locationChooserStr,
			"Tells the program how we choosen a plcae to perturbate. Possible values are:"
			"RATIO_ON_OPTIMAL_PATH: we choose a point which is at a given point in an optimal path. If so use --optiomal_path_ratio to choose the value"
	);

	app.add_option("-P,--perturbation_mode", global_perturbationModeStr,
			"Tells how we need to interpret -r value. If this option is 'ADD' we will add to edges the value in -p; if the option is 'MULTIPLY' we treat the value returned by -p as a multiplier of the current weight arc. if the option is 'ASSIGN' the value of -p will replace the value of the arc"
	);
	app.add_option("-r,--perturbation_range", global_perturbationRangeStr,
			"If -p is enabled, the range of the perturbation of the weights arcs. This is a math range (e.g., [4,100[ or (4,10) )"
	);
	app.add_option("-d,--perturbation_density", global_perturbationDensityStr,
			"if -p is enable, represents the probability to alter the weight of an arc. values goes from '0%' (none) to '100%' (every arc)."
			"If the percentage is not present, it represents the number of edges we're going to alter."
	);
	app.add_option("-R,--perturbation_radius_range", global_perturbationAreaRadiusStr,
			"If RANDOM_ON_AREA is set, specify the radius of the cicle-shaped perturbation area"
	);
	app.add_option("-u,--random_seed", global_randomSeed,
			"the seed for any pseudo random generator. Default to 0"
	);
	app.add_option("-o,--output_template", global_outputTemplate,
			"Prefix every output file will have"
	)->required();
	app.add_flag("-V,--validate_changes", global_validateChanges,
			"If present we will validate that the changes have been made correctly"
	);
	app.add_option("-T,--terrain_to_perturbate_set", global_terrainToPerturbateSetStr,
			"Used only for RANDOM_ON_TERRAIN: comma separated list of base terrain costs to perturbate. E.g., \"1000,2000\""
	);
	app.add_option("-t,--terrain_to_perturbate_number", global_terranToPerturbateNumber,
			"Used only for RANDOM_ON_TERRAIN: number of terrains to perturbate."
	);
	app.add_option("--perturbation_sequence", global_perturbationSequenceStr,
			"If PATH_RANDOM_ON_OPTIMAL is selected, represent sthe length of the pertrubation to alter in optimal paths"
	);
	app.add_flag("--generate_perturbations_per_query", global_generatePerturbationsPerQuery,
			"Instead of generating a perturbation map, we will generate a batch of perturbation maps, one for every query in the scenario."
			"Map perturbations in this case ends with query.<query_id>.graph"
	);
	app.add_option("--optimal_path_ratio", global_optimalPathRatioStr,
			"A percentage of where we want to put a perturbation. E.g., '[10, 15]'. Used only in RATIO_ON_OPTIMAL_PATH"
	);

	CLI11_PARSE(app, argc, args);

	global_randomEngine = std::default_random_engine{global_randomSeed};

	debug("parsing perturbation range...", global_perturbationRangeStr);
	range<int> perturbationRange = range<int>::fromMath(global_perturbationRangeStr.c_str());

	std::unordered_set<int> terrainToPerturbate;
	parseIntegerSetFromString(global_terrainToPerturbateSetStr.c_str(), terrainToPerturbate);

	MapLoaderFactory mapLoaderFactory{};
	AbstractMapLoader* mapLoader = mapLoaderFactory.get(global_mapFilename.c_str());

	GridMap map = mapLoader->LoadMap(global_mapFilename.c_str());

	const string cpdFilename = getCpdFilename(global_mapFilename.c_str());
	CpdHeuristic h = CpdHeuristic{map, cpdFilename.c_str()};

	// ********************************************************************
	// *********************** GENERATE PERTURBATIONS *********************
	// ********************************************************************

	if (global_generatePerturbationsPerQuery) {
		critical("we need to perturbate the map foreach query!");
		AbstractQueryPerturbationPositionStrategy* queryPositionPerturbatorStrategy = nullptr;
		AbstractLocationInPathPerturbationStrategy* locationInPathPerturbationStrategy = nullptr;

		if (global_mapPerturbationStr == QUERY_PERTURBATION_RANDOM_ON_AREA) {
			range<int> perturbationAreaRadius = range<int>::fromMath(global_perturbationAreaRadiusStr.c_str());
			locationInPathPerturbationStrategy = new AreaLocationPerturbationStrategy{perturbationAreaRadius, perturbationRange, global_perturbationModeStr, true};
		} else if (global_mapPerturbationStr == QUERY_PERTURBATION_RANDOM_ON_OPTIMALPATH){
			range<int> perturbationSequenceLength = range<int>::fromMath(global_perturbationSequenceStr.c_str());
			locationInPathPerturbationStrategy = new OptimalPathContiguousSubSetPerturbationStrategy{h.getMapper(), perturbationSequenceLength, perturbationRange, global_perturbationModeStr};
		} else {
			throw std::invalid_argument{"invlaid map perturbation option!"};
		}

		if (global_locationChooserStr == QUERY_LOCATION_RATIO_ON_OPTIMAL_PATH) {
			range<int> ratio = range<int>::fromMath(global_optimalPathRatioStr.c_str());
			if (global_mapPerturbationStr != QUERY_PERTURBATION_RANDOM_ON_AREA) {
				//this if should be considered temporary!
				throw std::domain_error{"I've temporarly disabled location ratio for everything which is not random on area"};
			}

			range<int> perturbationAreaRadius = range<int>::fromMath(global_perturbationAreaRadiusStr.c_str());
			queryPositionPerturbatorStrategy = new RatioQueryPerturbationPositionStrategy{
				ratio, 
				true, 
				perturbationAreaRadius.getUB(), //FIXME this should be removed! normally we want to affect both start and end too!
				PathTooShortPolicy::AVOID_GENERATE_PERTURBATION,
			};
		} else {
			throw std::invalid_argument{"invalid location chooser option!"};
		}

		//ok, now for each query we apply the query perturbator

		ScenarioLoader scenarioLoader{global_scenarioFilename.c_str()};

		for (int queryId=0; queryId<scenarioLoader.GetNumExperiments(); ++queryId) {
			critical("generating the perturbate map for the query ", queryId, "/", scenarioLoader.GetNumExperiments());
			//perturbated map filename
			std::stringstream ss;
			ss << global_outputTemplate << "query#" << queryId <<"|.graph";

			if (isFileExists(ss.str())) {
				continue;
			}

			Experiment query = scenarioLoader.GetNthExperiment(queryId);
			//generate the graph to perturbate
			AdjGraph graphToPerturbate{h.getGraph()};
			dpf::nodeid_t start =  h.getMapper()(query.getStart());
			dpf::nodeid_t goal = h.getMapper()(query.getGoal());
			int numberOfArcsAltered = 0;

			//find location to perturbate
			info("generating graph for query", queryId);
			try {
				dpf::nodeid_t positionToPerturbate = queryPositionPerturbatorStrategy->choosePosition(queryId, h.getGraph(), h.getMapper(), start, goal);

				//generate the optimal path of the query
				std::vector<xyLoc> optimalPath = map_perturbator::getOptimalPathAsXyLoc(
						graphToPerturbate,
						h.getMapper(),
						start, goal
				);

				xyLoc positionToPerturbateLoc = h.getMapper()(positionToPerturbate);
				numberOfArcsAltered = locationInPathPerturbationStrategy->perturbateLocation(
					queryId, 
					graphToPerturbate,
					positionToPerturbateLoc, positionToPerturbate,
					h.getMapper().convert(optimalPath)
				);

			} catch (PerturbationLocationNotFoundException e) {
				warning("couldn't generate perturbated graph! graph will remain 'as-is'");
				//we write the the graph file without perturbations
				numberOfArcsAltered = 0;
			}

			//write the file
			FILE* graphFile = fopen(ss.str().c_str(), "wb");
			if (graphFile == nullptr) {
				error("couldn't open file in wb ", ss.str().c_str());
			}
			graphToPerturbate.save(graphFile);
			fclose(graphFile);

			//write the file
			std::stringstream ssinfo;
			ssinfo << global_outputTemplate << "query#" << queryId;
			generateInfoFile(ssinfo.str(), graphToPerturbate, numberOfArcsAltered);

		}

	} else {
		critical("we need to perturbate the whole map");
		std::stringstream ss;
		ss << global_outputTemplate << ".graph";

		if (!isFileExists(ss.str())) {
			AbstractPerturbator* perturbator = nullptr;
			if (global_mapPerturbationStr == MAP_PERTURBATION_RANDOM) {
				dpf::big_integer edgesToAlter = static_cast<dpf::big_integer>(parseNumber(global_perturbationDensityStr.c_str(), h.getGraph().getTotalNumberOfArcs()));
				debug("edges to perturbate: ", edgesToAlter, " node total:", h.getGraph().getTotalNumberOfArcs());
				perturbator = new RandomPerturbator{edgesToAlter, perturbationRange, global_perturbationModeStr};

			} else if (global_mapPerturbationStr == MAP_PERTURBATION_RANDOM_ON_OPTIMAL) {
				dpf::big_integer edgesToAlter = static_cast<dpf::big_integer>(parseNumber(global_perturbationDensityStr.c_str(), h.getGraph().getTotalNumberOfArcs()));
				perturbator = new RandomOnOptimalPerturbator{edgesToAlter, perturbationRange, global_perturbationModeStr, global_scenarioFilename};

			} else if (global_mapPerturbationStr == MAP_PERTURBATION_RANDOM_TERRAIN) {
				perturbator = new RandomTerrainPerturbator{terrainToPerturbate, global_terranToPerturbateNumber, perturbationRange, global_perturbationModeStr};

			} else if (global_mapPerturbationStr == MAP_PERTURBATION_PATH_RANDOM_ON_OPTIMAL) {
				range<int> perturbationSequence = range<int>::fromMath(global_perturbationSequenceStr.c_str());
				dpf::big_integer edgesToAlter = static_cast<dpf::big_integer>(parseNumber(global_perturbationDensityStr.c_str(), h.getGraph().getTotalNumberOfArcs()));
				perturbator = new PathRandomOnOptimalPerturbator{perturbationSequence, edgesToAlter, perturbationRange, global_perturbationModeStr, global_scenarioFilename};

			} else {
				throw std::invalid_argument{"invalid map perturbation option!"};
			}


			critical("start perturbing map!");
			AdjGraph result = perturbator->perturbateMap(h.getGraph(), h.getMapper(), map);


			critical("perturbated map statistics:");
			
			critical(" - weights:", result.getWeights());
			critical(" - is perturbated:", result.hasBeenPerturbated());


			FILE* graphFile = fopen(ss.str().c_str(), "wb");
			result.save(graphFile);
			fclose(graphFile);

			//write the file
			std::stringstream ssinfo;
			ssinfo << global_outputTemplate;
			generateInfoFile(ssinfo.str(), result, perturbator->getEdgeChanged());
			critical("we have perturbated ", perturbator->getEdgeChanged(), " edges");

			delete perturbator;
		}


	}



	critical("DONE");
	//AdjGraph newMap = loadPreviousMap(h.getGraph(), perturbationProbability, perturbationRange, perturbationMode, mapData, width, height);

}

static string getCpdFilename(const char* filename) {
	char buffer[500];
	std::stringstream ss;
	ss << getBasename(filename, buffer) << ".cpd";
	return ss.str();
}

static const char* getBasename(const char* path, char* output) {
	char* tmp = strdup(path);
	char* result = basename(tmp);
	strcpy(output, result);
	free(tmp);

	return output;
}

void generateInfoFile(const std::string& outputTemplate, const AdjGraph& g, dpf::big_integer edgeChanged) {
	dpf::big_integer edgeTotal = g.getTotalNumberOfArcs();
	debug("changed", edgeChanged, "of", edgeTotal, "(aka ", (edgeChanged/(edgeTotal + 0.0))*100, "%)");

	std::ofstream perturbationFile;
	std::stringstream ss;
	ss << outputTemplate << ".graph.info.txt";
	perturbationFile.open(ss.str().c_str());

	perturbationFile.precision(2);

	perturbationFile << std::fixed;
	perturbationFile << "changed edges: " << edgeChanged << "\n";
	perturbationFile << "total edges: " << edgeTotal << "\n";
	perturbationFile << "percentage edges: " << (edgeChanged/(edgeTotal + 0.0))*100 << "%\n";

	perturbationFile.close();
}


