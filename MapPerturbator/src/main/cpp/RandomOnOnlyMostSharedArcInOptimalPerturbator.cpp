///*
// * RadnomOnEveryOptimalPerturbator.cpp
// *
// *  Created on: Nov 2, 2018
// *      Author: koldar
// */
//
//#include "RandomOnOnlyMostSharedArcInOptimalPerturbator.h"
//
//#include <random>
//#include <DynamicPathFinding/log.h>
//#include <vector>
//#include <DynamicPathFinding/types.h>
//#include <DynamicPathFinding/earlystop_dijkstra.h>
//#include <DynamicPathFinding/ScenarioLoader.h>
//#include <iostream>
//#include <sstream>
//#include <fstream>
//#include "globals.h"
//#include <unordered_map>
//
//namespace map_perturbator {
//
//AdjGraph RandomOnOnlyMostSharedArcInOptimalPerturbator::perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map) {
//	AdjGraph result = AdjGraph{originalMap};
//	ScenarioLoader scenarioLoader{this->scenarioFilename.c_str()};
//
//	dpf::big_integer loop = 0;
//	dpf::big_integer edgeTotal = originalMap.getTotalNumberOfArcs();
//
//	// first we need to ensure that we have enough paths to be perturbated
//	std::vector<Arc> arcs = this->getMostUsedArcsForOptimalPaths(scenarioLoader, originalMap, mapper);
//	dpf::big_integer edgeNumberToPerturbate = originalMap.getNumberOfArcsFromRatio(perturbationProbability);
//	if (edgeNumberToPerturbate > arcs.size()) {
//		//these are all the arcs used in all optimal paths... we do not have enough arcs to use!
//		throw CpdException{"We do not have enough shared arcs to fill! Required: %d; Actual: %d", edgeNumberToPerturbate, arcs.size()};
//	}
//
//	int nextArc = 0;
//	while(true) {
//		if ((edgeChanged/(edgeTotal + 0.0)) >= this->perturbationProbability) {
//			break;
//		}
//
//		//PICK THE MOST USED ARC
//		Arc arcToAlter = arcs.at(nextArc);
//
//		//ALTER SAID ARC
//		this->perturbateArc(mapper, result, arcToAlter.source, arcToAlter.target, this->perturbationRange.getLB(), this->perturbationRange.getUB(), perturbationMode, global_randomEngine);
//	}
//
//	this->generateInfoFile(global_outputTemplate, result);
//
//	return result;
//}
//
//std::vector<Arc> RandomOnOnlyMostSharedArcInOptimalPerturbator::getMostUsedArcsForOptimalPaths(const ScenarioLoader& scenarioLoader, const AdjGraph& graph, const Mapper& mapper) {
//	std::vector<gridmap_path> optimalPathsOfQuery = this->getAllOptimalPaths(scenarioLoader, graph, mapper);
//
//	//structure containing, for how many times an arc si used optimal paths, a list of arcs used said times in optimal paths
//	//e.g., arc a is used 4 times in optimal paths, so at key "arc a" there value is 4
//
//	std::unordered_map<Arc, int> arcOccurence{};
//	std::vector<Arc> availableArcs{};
//
//	for (gridmap_path p : optimalPathsOfQuery) {
//		for (Arc arc: graph.getArcsOverMap(p, mapper)) {
//			if (arcOccurence.find(arc) == arcOccurence.end()) {
//				arcOccurence[arc] = 0;
//				availableArcs.push_back(arc);
//			}
//
//			arcOccurence[arc] += 1;
//		}
//	}
//
//	//now we generate the vector from the most used arc till the least used. availableArcs contain a list of all the arcs we need to sort
//	std::sort(availableArcs.begin(), availableArcs.end(), [&](const Arc& a, const Arc&b) -> bool {
//		return arcOccurence[a] < arcOccurence[b];
//	});
//
//	return availableArcs;
//}
//
//std::vector<gridmap_path> RandomOnOnlyMostSharedArcInOptimalPerturbator::getAllOptimalPaths(const ScenarioLoader& scenarioLoader, const AdjGraph& g, const Mapper& mapper) {
//	std::vector<gridmap_path> result{};
//
//	for (int i=0; i<scenarioLoader.GetNumExperiments(); ++i) {
//		Experiment experiment = scenarioLoader.GetNthExperiment(i);
//
//		//GENERATE OPTIMAL PATH
//		EarlyStopDijkstra earlyStopDijkstra = EarlyStopDijkstra{g, false};
//		dpf::nodeid_t start = mapper(experiment.getStart());
//		dpf::nodeid_t goal = mapper(experiment.getGoal());
//		std::vector<dpf::nodeid_t> optimalMoves = earlyStopDijkstra.run(start, goal);
//		std::vector<xyLoc> optimalPath = earlyStopDijkstra.getPathAsVector(start, goal, mapper, optimalMoves);
//
//
//		gridmap_path path{optimalPath};
//		result.push_back(path);
//	}
//
//	return result;
//}
//
//int RandomOnOnlyMostSharedArcInOptimalPerturbator::pickQuery(const ScenarioLoader& scenarioLoader) {
//	std::uniform_int_distribution<int> experimentDistribution{0, scenarioLoader.GetNumExperiments()-1};
//	int randomQueryId = experimentDistribution(global_randomEngine);
//	debug("we have picked experiment #", randomQueryId, " on a grand total of", scenarioLoader.GetNumExperiments());
//	return randomQueryId;
//}
//
//bool RandomOnOnlyMostSharedArcInOptimalPerturbator::pickArcInOptimalPath(const AdjGraph& map, const Mapper& mapper, const xyLoc& start, const xyLoc& goal, const std::vector<xyLoc>& path, dpf::nodeid_t& source, dpf::nodeid_t& sink) {
//	//distrubtion genreate [a,b] but we want [a,b)
//	std::uniform_int_distribution<int> experimentDistribution{0, static_cast<int>(path.size())-2};
//	//we try 10 times before switching to a different path
//	for (auto i=0; i<10; ++i) {
//        int randomLocationInPath = experimentDistribution(global_randomEngine);
//
//        source = mapper(path.at(randomLocationInPath));
//        sink = mapper(path.at(randomLocationInPath + 1));
//
//        if (!map.hasArcBeenPerturbated(source, sink)) {
//            return true;
//        }
//	}
//    return false;
//
//}
//
//}
