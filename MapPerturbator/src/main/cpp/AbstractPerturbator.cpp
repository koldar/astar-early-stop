/*
 * AbstractPerturbator.cpp
 *
 *  Created on: Nov 2, 2018
 *      Author: koldar
 */

#include "AbstractPerturbator.h"
#include <DynamicPathFinding/log.h>
#include <DynamicPathFinding/adj_graph.h>
#include <DynamicPathFinding/types.h>
#include <DynamicPathFinding/earlystop_dijkstra.h>
#include <random>
#include <vector>
#include <string>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>

namespace map_perturbator {

AbstractPerturbator::AbstractPerturbator() : edgeChanged{0} {

}


void AbstractPerturbator::perturbateArc(const Mapper& mapper, AdjGraph& graph, dpf::nodeid_t source, dpf::nodeid_t sink, int rangeLowerBound, int rangeUpperBound, const std::string& perturbationMode, std::default_random_engine randomEngine) {
	//PICK AN EDGE IN THE OPTIMAL PATH

	std::vector<xyLoc> optimalPath;
	OutArc arc = const_cast<const AdjGraph&>(graph).getArc(source, sink);
	auto weightToAlter = graph.getWeightOfArc(source, sink);

	if (arc.hasBeenPerturbated()) {
		throw CpdException{"arc %ld->%ld has already been perturbated!", source, sink};
	}

	//ALTER SAID ARC
	std::uniform_int_distribution<int> weightDistribution{rangeLowerBound, rangeUpperBound};

	//we need to change the arc
	safe_int oldWeight = weightToAlter;
	safe_int newWeight = 0;

	if (perturbationMode == std::string{"ASSIGN"}) {
		newWeight = weightDistribution(randomEngine);
	} else if (perturbationMode == std::string{"SUM"}) {
		newWeight = oldWeight + weightDistribution(randomEngine);
	} else if (perturbationMode == std::string{"MULTIPLY"}) {
		int multiplier = weightDistribution(randomEngine);
		newWeight = oldWeight * multiplier;
	} else {
		throw std::invalid_argument{"invalid perturbateMode value. Have you set perturbate_mode?"};
	}

	if (newWeight < oldWeight) {
		error("the perturbation may only increase weights, not decrease them! we're tyring to change", source, "->", sink, "from", oldWeight, "to", newWeight);
		throw std::domain_error{"can't replace weight with a smaller value!"};
	}

	graph.changeWeightOfArc(static_cast<int>(source), sink, static_cast<int>(newWeight));
	//we have changed both edges
	edgeChanged += 2;
}

std::vector<xyLoc> AbstractPerturbator::getOptimalPath(const AdjGraph& graph, const Mapper& mapper, const xyLoc& start, const xyLoc& goal) const {
	EarlyStopDijkstra earlyStopDijkstra = EarlyStopDijkstra{graph, false};
	dpf::nodeid_t startId = mapper(start);
	dpf::nodeid_t goalId = mapper(goal);
	std::vector<dpf::nodeid_t> optimalMoves = earlyStopDijkstra.run(startId, goalId);
	return earlyStopDijkstra.getPathAsVector(startId, goalId, mapper, optimalMoves);
}

void AbstractPerturbator::reset() {
	this->edgeChanged = 0;
}

int AbstractPerturbator::getEdgeChanged() const {
	return this->edgeChanged;
}

}
