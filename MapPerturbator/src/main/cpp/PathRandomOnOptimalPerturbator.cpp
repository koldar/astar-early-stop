/*
 * PathRandomOnOptimalPerturbator.cpp
 *
 *  Created on: 13 nov 2018
 *      Author: massimobono
 */


#include "PathRandomOnOptimalPerturbator.h"
#include <DynamicPathFinding/ScenarioLoader.h>
#include <random>
#include "globals.h"
#include <DynamicPathFinding/earlystop_dijkstra.h>
#include <vector>

namespace map_perturbator {

PathRandomOnOptimalPerturbator::PathRandomOnOptimalPerturbator(range<int> sequentialPerturbationInPath, dpf::big_integer edgesToAlter, const range<int>& range, const std::string& mode, const std::string& scenarioFilename):
		edgesToAlter{edgesToAlter}, perturbationRange{range}, perturbationMode{mode},
	scenarioFilename{scenarioFilename}, sequentialPerturbationInPath{sequentialPerturbationInPath} {

}

AdjGraph PathRandomOnOptimalPerturbator::perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map) {
	AdjGraph result = AdjGraph{originalMap};
	ScenarioLoader scenarioLoader{this->scenarioFilename.c_str()};

	dpf::big_integer loop = 0;
	dpf::big_integer edgeTotal = originalMap.getTotalNumberOfArcs();

	while(true) {
		debug("starting new cycle", edgeChanged, this->edgesToAlter);
		if (edgeChanged >= this->edgesToAlter) {
			debug("found enough edges!");
			break;
		}

		//PICK A RANDOM QUERY IN THE SCENARIO
		std::uniform_int_distribution<int> experimentDistribution{0, scenarioLoader.GetNumExperiments()-1};
		int randomQueryId = experimentDistribution(global_randomEngine);
		debug("we have picked experiment #", randomQueryId, " on a grand total of", scenarioLoader.GetNumExperiments());
		Experiment randomQuery{scenarioLoader.GetNthExperiment(randomQueryId)};

		//GENERATE THE OPTIMAL PATH FROM THE ORIGINAL MAP
		EarlyStopDijkstra earlyStopDijkstra = EarlyStopDijkstra{originalMap, false};
		dpf::nodeid_t start = mapper(randomQuery.getStart());
		dpf::nodeid_t goal = mapper(randomQuery.getGoal());
		std::vector<dpf::nodeid_t> optimalMoves = earlyStopDijkstra.run(start, goal);
		std::vector<xyLoc> optimalPath = earlyStopDijkstra.getPathAsVector(start, goal, mapper, optimalMoves);


		//PICK ARCS IN THE OPTIMAL PATH
		std::uniform_int_distribution<int> sequentialDistribution{this->sequentialPerturbationInPath.getLB(), this->sequentialPerturbationInPath.getUB()};


		int sequenceLength = sequentialDistribution(global_randomEngine);
		std::vector<Arc> arcsToAlter = this->pickArcsInOptimalPath(result, mapper, randomQuery.getStart(), randomQuery.getGoal(), optimalPath, sequenceLength);

		//ALTER SAID ARCS

		for (Arc arc : arcsToAlter) {
			dpf::nodeid_t source = static_cast<dpf::nodeid_t>(arc.source);
			dpf::nodeid_t sink = static_cast<dpf::nodeid_t>(arc.target);

			dpf::big_integer previousChangePerturbationsInPath = 0;
			if (global_validateChanges) {
				previousChangePerturbationsInPath = result.getNumberOfArcsInPathSuchThat(mapper, optimalPath, [&](dpf::big_integer i, dpf::nodeid_t id, OutArc a) -> bool {
					//we count the number of perturbated arcs on the path
					return a.hasBeenPerturbated();
				});
			}

			this->perturbateArc(mapper, result, source, sink, perturbationRange.getLB(), perturbationRange.getUB(), this->perturbationMode, global_randomEngine);

			if (global_validateChanges) {
				//we check that the perturbations in the path have been increased by 1
				dpf::big_integer afterChangePerturbationsInPath = result.getNumberOfArcsInPathSuchThat(mapper, optimalPath, [&](dpf::big_integer i, dpf::nodeid_t id, OutArc a) -> bool {
					//we count the number of perturbated arcs on the path
					return a.hasBeenPerturbated();
				});

				if (afterChangePerturbationsInPath != (previousChangePerturbationsInPath + 1)) {
					error("we were supposed to alter arc", source, "->", sink, "in path", optimalPath, " but before changing it the perturbation on the path were", previousChangePerturbationsInPath, "while afterwards it were", afterChangePerturbationsInPath);
					throw std::domain_error{"validate change has failed!"};
				}
			}
		}

	}

	return result;

}

std::vector<Arc> PathRandomOnOptimalPerturbator::getArcsFromLocations(const AdjGraph& map, const Mapper& mapper, const std::vector<xyLoc>& locations) const {
	std::vector<Arc> result;

	xyLoc tmp = locations[0];
	for (auto i=1; i<locations.size(); ++i) {
		dpf::nodeid_t source = mapper(tmp);
		dpf::nodeid_t sink = mapper(locations[i]);
		result.push_back(Arc{static_cast<int>(source), static_cast<int>(sink), 0}); //i don't care about the weight
		tmp = locations[i];
	}

	return result;
}

std::vector<Arc> PathRandomOnOptimalPerturbator::pickArcsInOptimalPath(const AdjGraph& map, const Mapper& mapper, const xyLoc& start, const xyLoc& goal, const std::vector<xyLoc>& path, int sequenceLength) {
	std::vector<Arc> arcs = this->getArcsFromLocations(map, mapper, path);
	std::vector<Arc> result;

	if (arcs.size() <= sequenceLength) {
		//the path is smaller or equal to the arcs we want to perturbate. Returnthe whole path
		for (Arc a : arcs) {
			if (!map.hasArcBeenPerturbated(a.source, a.target)) {
				result.push_back(a);
			}
		}

		return result;
	}


	debug("asdgfhjgfdsa", arcs.size(), sequenceLength);
	std::uniform_int_distribution<int> experimentDistribution{0, static_cast<int>(arcs.size()) - sequenceLength};

	int randomLocationInPath = experimentDistribution(global_randomEngine);

	for (int i=randomLocationInPath; i<sequenceLength; ++i) {
		debug("i is", i);
		//the path is smaller or equal to the arcs we want to perturbate. Returnthe whole path
		Arc a = arcs[i];
		if (!map.hasArcBeenPerturbated(a.source, a.target)) {
			result.push_back(a);
		}
	}

    return result;

}

}


