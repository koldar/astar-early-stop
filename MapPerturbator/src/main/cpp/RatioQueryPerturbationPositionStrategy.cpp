/*
 * RatioQueryPerturbationPositionStrategy.cpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */


#include "RatioQueryPerturbationPositionStrategy.hpp"
#include "Utils.hpp"
#include <random>
#include <vector>
#include <PathTesterUtils/cliUtils.h>
#include "globals.h"
#include "exceptions.hpp"

namespace map_perturbator {

RatioQueryPerturbationPositionStrategy::RatioQueryPerturbationPositionStrategy(range<int> ratio, bool ensureLocationIsFarFromStartGoal, int numberOfCellsFarFromStartGoal, PathTooShortPolicy policy) :
	ratio{ratio},
	ensureLocationIsFarFromStartGoal{ensureLocationIsFarFromStartGoal},
	numberOfCellsFarFromStartGoal{numberOfCellsFarFromStartGoal},
	pathTooShortPolicy{policy} {

}

RatioQueryPerturbationPositionStrategy::~RatioQueryPerturbationPositionStrategy() {

}

dpf::nodeid_t RatioQueryPerturbationPositionStrategy::choosePosition(int queryId, const AdjGraph& graphToPerturbate, const Mapper& mapper, dpf::nodeid_t start, dpf::nodeid_t goal) {
	std::vector<dpf::nodeid_t> optimalPath = map_perturbator::getOptimalPathAsIds(graphToPerturbate, mapper, start, goal);

	//if ensureLocationIsFarFromStartGoal is enabled, we generate a subpath of it by removing the first and
	//the last numberOfCellsFarFromStartGoal cells
	if (this->ensureLocationIsFarFromStartGoal) {
		if (optimalPath.size() < (2 * this->numberOfCellsFarFromStartGoal)) {
			switch (this->pathTooShortPolicy) {
				case PathTooShortPolicy::AVOID_GENERATE_PERTURBATION: {
					throw PerturbationLocationNotFoundException{queryId};
				}
				case PathTooShortPolicy::THROW_ERROR: {
					throw std::domain_error{"optimal path is too short"};
				}
				default: {
					throw std::domain_error{"invalid path short policy!"};
				}
			}
		}
		int oldSize = optimalPath.size();
		debug("old path is", optimalPath);
		optimalPath.erase(optimalPath.begin(), optimalPath.begin() + this->numberOfCellsFarFromStartGoal);
		optimalPath.erase(optimalPath.end() - 3 - this->numberOfCellsFarFromStartGoal, optimalPath.end());
		debug("optimal path was ", oldSize, "long but now is ", optimalPath.size(), "number fo cells are ", this->numberOfCellsFarFromStartGoal);
		debug("new path is", optimalPath);
	}

	std::uniform_int_distribution<int> uniform(ratio.getLB(), ratio.getUB());
	int actualRatio = uniform(global_randomEngine);

	int position = static_cast<int>((actualRatio/100.) * static_cast<int>(optimalPath.size()));
	
	debug("QWERTY chosen ", position, " out of ", optimalPath.size(), " which is ", static_cast<float>((actualRatio/100.)));
	return optimalPath[position];
}



void RatioQueryPerturbationPositionStrategy::reset() {

}

}
