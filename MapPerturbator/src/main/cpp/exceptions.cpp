#include "exceptions.hpp"
#include <iostream> 
#include <string> 
#include <sstream>

namespace map_perturbator {

PerturbationLocationNotFoundException::PerturbationLocationNotFoundException(int queryId) : queryId{queryId} {

}

const char * PerturbationLocationNotFoundException::what () const throw () {
	std::stringstream ss;
	ss << "Couldn't generate perturbation for query " << this->queryId << "!";
	return ss.str().c_str();
}

}