/*
 * globals.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: koldar
 */

#include "globals.h"

namespace map_perturbator {

std::string global_mapFilename;
std::string global_scenarioFilename;
std::string global_perturbationModeStr;
std::string global_perturbationRangeStr;
std::string global_perturbationDensityStr;
int global_randomSeed;
std::string global_outputTemplate;
std::string global_mapPerturbationStr;
bool global_validateChanges;
std::string global_perturbationAreaRadiusStr;
std::string global_terrainToPerturbateSetStr;
int global_terranToPerturbateNumber;
std::string global_perturbationSequenceStr;
bool global_generatePerturbationsPerQuery;
std::string global_locationChooserStr;
std::string global_optimalPathRatioStr;

const std::string MAP_PERTURBATION_RANDOM = std::string{"RANDOM"};
const std::string MAP_PERTURBATION_RANDOM_ON_OPTIMAL = std::string{"RANDOM_ON_OPTIMAL"};
const std::string MAP_PERTURBATION_RANDOM_ON_MOSTSHAREDARCS = std::string{"RANDOM_ON_MOSTSHAREDARCS"};
const std::string MAP_PERTURBATION_RANDOM_ON_AREA = std::string{"RANDOM_ON_AREA"};
const std::string MAP_PERTURBATION_RANDOM_TERRAIN = std::string{"RANDOM_TERRAIN"};
const std::string MAP_PERTURBATION_PATH_RANDOM_ON_OPTIMAL = std::string{"PATH_RANDOM_ON_OPTIMAL"};

const std::string QUERY_PERTURBATION_RANDOM_ON_AREA = std::string{"QUERY_RANDOM_AREA"};
const std::string QUERY_PERTURBATION_RANDOM_ON_OPTIMALPATH = std::string{"QUERY_RANDOM_OPTIMAL_PATH"};

const std::string QUERY_LOCATION_RATIO_ON_OPTIMAL_PATH = std::string{"RATIO_ON_OPTIMAL_PATH"};

std::default_random_engine global_randomEngine;

}
