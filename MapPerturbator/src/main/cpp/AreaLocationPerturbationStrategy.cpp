/*
 * AreaLocationPerturbationStrategy.cpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */



#include "AreaLocationPerturbationStrategy.hpp"
#include <random>
#include <cmath>
#include <vector>
#include "globals.h"
#include <DynamicPathFinding/log.h>
#include <DynamicPathFinding/heap.h>
#include "exceptions.hpp"

namespace map_perturbator {

struct node_info {
	dpf::nodeid_t node;
	int depth;
};

AreaLocationPerturbationStrategy::AreaLocationPerturbationStrategy(range<int> radius, range<int> perturbationRange, const std::string& mode, bool ensureStartGoalAreNotAffected) :
		radius{radius}, perturbationRange{perturbationRange}, mode{mode}, ensureStartGoalAreNotAffected{ensureStartGoalAreNotAffected} {

}

AreaLocationPerturbationStrategy::~AreaLocationPerturbationStrategy() {

}

dpf::nodeid_t AreaLocationPerturbationStrategy::getCompliantEpicenter(int queryId, AdjGraph& graphToPerturbate, xyLoc location, dpf::nodeid_t locationId, const std::vector<dpf::nodeid_t>& paths, int areaRadius) {
	int result = 0;
	std::vector<node_info> nodeInfos;

	//ok first of all we find where "location" is in the optimal path
	auto it = std::find(paths.begin(), paths.end(), locationId);
	int index = std::distance(paths.begin(), it);

	//we keep generating the DFS up until it doesn't contain either start nor goal.
	//we fail when no positions can be candidate
	int acceptedPositionLb = 0;
	int acceptedPositionUb = paths.size() - 1;
	while (true) {

		if (acceptedPositionLb > acceptedPositionUb) {
			throw PerturbationLocationNotFoundException{queryId};
		}
		
		//force index to stay inside the bounds
		index = min(index, acceptedPositionUb);
		index = max(index, acceptedPositionLb);
		locationId = paths[index];

		nodeInfos.clear();

		debug("runnign DFS with lb=", acceptedPositionLb, " index=", index, "ub=", acceptedPositionUb);
		try {
			this->performDFS(graphToPerturbate, locationId, areaRadius, nodeInfos, paths[0], paths.back());
			//ok, now we alter all arcs involving these nodes
			if (nodeInfos.size() <= 1) {
				throw CpdException{"the DFS generated %ld nodes!", nodeInfos.size()};
			}
			return locationId;
		} catch (const StartNodeInterceptedException& e) {
			acceptedPositionLb += 1;
		} catch (const GoalNodeInterceptedException& e) {
			acceptedPositionUb -= 1;
		}
	
	}
}

int AreaLocationPerturbationStrategy::perturbateLocation(int queryId, AdjGraph& graphToPerturbate, xyLoc location, dpf::nodeid_t locationId, const std::vector<dpf::nodeid_t>& paths) {
	std::uniform_int_distribution<int> radiusDistribution(radius.getLB(), radius.getUB());
	int actualRadius = radiusDistribution(global_randomEngine);
	debug("actual radius is", actualRadius);

	locationId = this->getCompliantEpicenter(queryId, graphToPerturbate, location, locationId, paths, actualRadius);

	int result = 0;
	std::vector<node_info> nodeInfos;
	this->performDFS(graphToPerturbate, locationId, actualRadius, nodeInfos, paths[0], paths.back());



	//ok, now we alter all arcs involving these nodes
	if (nodeInfos.size() <= 1) {
		throw CpdException{"the DFS generated %ld nodes!", nodeInfos.size()};
	}


	for (int sourceId=0; sourceId<nodeInfos.size(); ++sourceId) {
		for (int sinkId=(sourceId+1); sinkId<nodeInfos.size(); ++sinkId) {
			dpf::nodeid_t source = nodeInfos[sourceId].node;
			dpf::nodeid_t sink = nodeInfos[sinkId].node;

			if (graphToPerturbate.containsArc(source, sink)) {
				if (graphToPerturbate.hasArcBeenPerturbated(source, sink)) {
					continue;
				}

				dpf::cost_t oldWeight = graphToPerturbate.getWeightOfArc(source, sink);

				int distance = nodeInfos[sourceId].depth;
				int lower = 1;
				int upper = this->perturbationRange.getUB();
				dpf::cost_t newWeight = 0;

				if (this->mode == std::string{"MULTIPLY"}) {
					//in multiply the edge cost is multiplying by a decaying factor
					// int newWeight = static_cast<int>(oldWeight * ((-((upper - lower)/(actualRadius))*distance)+upper));
					double multiplier = this->getNewWeightInArea(distance, upper, actualRadius, 1);
					newWeight = static_cast<dpf::cost_t>(static_cast<double>(oldWeight) * multiplier);

					debug("depth=", distance, "oldWeight=",oldWeight, "multiplier=", multiplier, "newWeight=", newWeight);
				} else if (this->mode == std::string{"ASSIGN"}) {
					//we assign to every edge the same value, which is the upperbound
					if (upper > 3000) {
						debug("upper has been reset to INFINITY!");
						//upper = 2147483647;
						//upper = 900000000;
						upper = static_cast<int>(dpf::cost_t::getUpperBound());
					}
					newWeight = upper;
				} else {
					throw CpdException{"invalid mode %s", this->mode.c_str()};
				}

				
				if (newWeight < oldWeight) {
					throw CpdException{"we want to perturbate the arcs with the new value %ld less than the old value %ld!", newWeight, oldWeight};
				}
				graphToPerturbate.changeWeightOfArc(source, sink, static_cast<int>(newWeight));
				result += 2;
			}
		}
	}

	return result;
}

double AreaLocationPerturbationStrategy::getNewWeightInArea(double x, double M, double stddev, double k, double lambda) const {
	//3*e^(-(1/(0.1*2))*(x/5)^2) + 1
	return M * exp(-lambda * 0.5*(x/stddev)*(x/stddev)) + k;
}

void AreaLocationPerturbationStrategy::performDFS(const AdjGraph& graph, dpf::nodeid_t start, int maxDepth, std::vector<node_info>& nodes, dpf::nodeid_t startPath, dpf::nodeid_t endPath) const {
	std::vector<bool> visited;
	for(int i=0; i<graph.node_count(); ++i) {
		visited.push_back(false);
	}


	std::vector<node_info> q;
	q.push_back(node_info{start, 0});

	while (!q.empty()) {
		node_info ni = q.front();
		q.erase(q.begin());
		dpf::nodeid_t n = ni.node;

		if (visited[n]) {
			continue;
		}
		visited[n] = true;
		if (this->ensureStartGoalAreNotAffected) {
			debug("we're affecting ", ni.node, "! start=", startPath, "end=", endPath, " depth=", ni.depth, "max depth= ", maxDepth);
			if (ni.node == startPath) {
				throw StartNodeInterceptedException{};
			}
			if (ni.node == endPath) {
				throw GoalNodeInterceptedException{};
			}
		}
		nodes.push_back(ni);

		debug("we're in node ", n, " the out degree is ", graph.out_deg(n));
		for (int i=0; i<graph.out_deg(n); ++i) {
			dpf::nodeid_t next = graph.getIthOutArc(n, i).target;
			if (visited[next]) {
				continue;
			}
			if ((ni.depth+1)>=maxDepth) {
				continue;
			}
			q.push_back(node_info{next, ni.depth+1});
		}

	}

//	this->_performDFS(graph, nullptr, start, 0, maxDepth, nodes, visited);
}

//void AreaLocationPerturbationStrategy::_performDFS(const AdjGraph& graph, dpf::nodeid_t* parent, dpf::nodeid_t n, int depth, int maxDepth, std::vector<node_info>& nodes, std::vector<bool>& visited) const {
//	if (depth > maxDepth) {
//		return;
//	}
//
//	visited[n] = true;
//	nodes.push_back(node_info{n, depth});
//
//	for (int i=0; i<graph.out_deg(n); ++i) {
//		dpf::nodeid_t next = graph.getIthOutArc(n, i).target;
//		this->_performDFS(graph, &n, next, depth + 1, maxDepth, nodes, visited);
//	}
//}

}
