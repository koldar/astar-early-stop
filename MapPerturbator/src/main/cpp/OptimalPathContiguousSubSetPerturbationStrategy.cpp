/*
 * OptimalPathContiguousSubSetPerturbationStrategy.cpp
 *
 *  Created on: 14 nov 2018
 *      Author: massimobono
 */

#ifndef OPTIMALPATHCONTIGUOUSSUBSETPERTURBATIONSTRATEGY_CPP_
#define OPTIMALPATHCONTIGUOUSSUBSETPERTURBATIONSTRATEGY_CPP_

#include "OptimalPathContiguousSubSetPerturbationStrategy.hpp"
#include "globals.h"
#include "Utils.hpp"

namespace map_perturbator {

OptimalPathContiguousSubSetPerturbationStrategy::OptimalPathContiguousSubSetPerturbationStrategy(const Mapper& mapper, range<int> sequenceLength, range<int> perturbationRange, const std::string& mode) :
sequenceLength{sequenceLength}, perturbationRange{perturbationRange}, mode{mode}, mapper{mapper} {

}

OptimalPathContiguousSubSetPerturbationStrategy::~OptimalPathContiguousSubSetPerturbationStrategy() {

}

int OptimalPathContiguousSubSetPerturbationStrategy::perturbateLocation(int quetyId, AdjGraph& graphToPerturbate, xyLoc location, dpf::nodeid_t locationId, const std::vector<dpf::nodeid_t>& path) {
	int result = 0;

	std::uniform_int_distribution<int> distribution(this->sequenceLength.getLB(), this->sequenceLength.getUB());
	int actualLength = distribution(global_randomEngine);

	std::uniform_int_distribution<int> perturbationDistribution(this->perturbationRange.getLB(), this->perturbationRange.getUB());

	std::vector<xyLoc> pathLocs = mapper.convert(path);
	std::vector<Arc> pathArcs = map_perturbator::getArcsFromLocations(graphToPerturbate, mapper, pathLocs);

	//look inside pathArcs the location
	//apparently there are some tests where the query ask to go from one place to another which are the same... we need to deal
	//with such queries as well. How? we simply do not generate perturbations
	if (path.size() == 1) {
		return 0;
	}


	int locationPosition = -1;
	for (int i=0; i<pathArcs.size(); ++i) {
		if (pathArcs[i].source == locationId || pathArcs[i].target == locationId) {
			locationPosition = i;
			break;
		}
	}
	if (locationPosition < 0) {
		error("location requested is", locationId, "or in xy is", location);
		error("path is", pathLocs);
		throw CpdException{"can't find location in path %s", pathArcs};
	}

	//we perturbate arcs before and after the fetched location
	int startPosition = (locationPosition-(actualLength/2));
	int endPosition = (locationPosition+(actualLength/2));

	if (startPosition < 0) {
		//if we go behind the start we translate the traffic
		endPosition += -startPosition;
		startPosition = 0;
	}

	debug("locationPosition=", locationPosition, "actualLength=", actualLength, "going from", startPosition, "to", endPosition);
	for (int i=startPosition; i<endPosition; ++i) {
		dpf::nodeid_t source;
		dpf::nodeid_t sink;

		if ((i<0) || (i>=pathArcs.size())) {
			continue;
		}

		source = pathArcs[i].source;
		sink = pathArcs[i].target;

		//perturb arc
		OutArc arc = const_cast<const AdjGraph&>(graphToPerturbate).getArc(source, sink);
		if (arc.hasBeenPerturbated()){
			warning("arc", pathArcs[i].source,"->", pathArcs[i].target, "has already been perturbated. Ignore");
			continue;
		}

		int oldWeight = graphToPerturbate.getWeightOfArc(static_cast<int>(source), static_cast<int>(sink));
		int newWeight = oldWeight * perturbationDistribution(global_randomEngine);

		if (this->mode != std::string{"MULTIPLY"}) {
			throw std::domain_error{"not implemented!"};
		}

		if (newWeight < oldWeight) {
			throw CpdException{"Trying to update the cost %ld with smaller value %ld", oldWeight, newWeight};
		}

		debug("perturbating", mapper(source), "->", mapper(sink));
		graphToPerturbate.changeWeightOfArc(source, sink, newWeight);
		result += 2;
	}


	return result;
}

}



#endif /* OPTIMALPATHCONTIGUOUSSUBSETPERTURBATIONSTRATEGY_CPP_ */
