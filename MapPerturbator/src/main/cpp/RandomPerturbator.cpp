/*
 * RandomPerturbator.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: koldar
 */


#include "RandomPerturbator.h"
#include "globals.h"
#include <DynamicPathFinding/types.h>
#include <DynamicPathFinding/log.h>
#include <iostream>
#include <fstream>
#include <sstream>

namespace map_perturbator {

AdjGraph RandomPerturbator::perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map) {
	AdjGraph result = AdjGraph{originalMap};

	//	std::string newMapPrint = std::string{global_outputTemplate};
	//	newMapPrint.append("newMap1");
	//
	//	result.print(mapper, newMapPrint);
	static std::uniform_real_distribution<double> coinDistribution{0.0, 1.0};
	std::uniform_int_distribution<dpf::nodeid_t> randomDistribution(0, originalMap.node_count() - 1);
	std::uniform_int_distribution<int> weightDistribution(this->perturbationRange.getLB(), this->perturbationRange.getUB());

	debug("graph is: ", originalMap);

	double totalEdges = 0;
	dpf::big_integer changedEdges=0;
	//we have changed both edges

	//every time we perturbe an edge we change w of them (a->b and b->a).
	//but on the frontend we required to change (say 50) edges we obviously mean 50 DIFFERENT edges.
	//hence the actual perturbated density will be twice the normal edges
	for (; changedEdges<this->edgesToAlter; changedEdges+=1) {

		do {
			dpf::nodeid_t source = randomDistribution(global_randomEngine);
			std::uniform_int_distribution<int> edgeDistribution{0, result.out_deg(static_cast<int>(source)) - 1};
			int ithArc = edgeDistribution(global_randomEngine);
			debug("source is", source, "(xy is ", mapper(source), "(max is ", result.size(), ") ithArc is ", ithArc, "outdeg is", result.out_deg(source));
			if (result.hasVertexNoSinks(source)) {
				//no possible outgoing edge. Continue
				continue;
			}
			dpf::nodeid_t sink = const_cast<const AdjGraph&>(result).getIthOutArc(source, ithArc).target;

			if (result.hasArcBeenPerturbated(source, sink)) {
				continue;
			}

			//we need to change the arc
			safe_int oldWeight =result.getWeightOfArc(static_cast<int>(source), sink);
			safe_int newWeight = 0;

			if (global_perturbationModeStr == std::string{"ASSIGN"}) {
				newWeight = weightDistribution(global_randomEngine);
				if (newWeight > 3000) {
					newWeight = dpf::cost_t::getUpperBound();
				}
			} else if (global_perturbationModeStr == std::string{"SUM"}) {
				newWeight = oldWeight + weightDistribution(global_randomEngine);
			} else if (global_perturbationModeStr == std::string{"MULTIPLY"}) {
				int multiplier = weightDistribution(global_randomEngine);
				//debug("multiplier is", multiplier, "oldweigth is", oldWeight);
				newWeight = oldWeight * multiplier;
			} else {
				throw std::invalid_argument{"invalid perturbateMode value. Have you set perturbate_mode?"};
			}

			if (newWeight < oldWeight) {
				error("the perturbation may only increase weights, not decrease them! we're tyring to change", source, "->", sink, "from", oldWeight, "to", newWeight);
				throw std::domain_error{"can't replace weight with a smaller value!"};
			}
			result.changeWeightOfArc(static_cast<int>(source), sink, static_cast<int>(newWeight));
			break;
		} while(true);
	}

	this->edgeChanged = changedEdges;
	return result;

//	for (dpf::nodeid_t v = 0; v < result.node_count(); ++v) {
//		for (auto it = result.arcBegin(v); it != result.arcEnd(v); ++it) {
//			totalEdges += 1;
//			//if the edge has been already been modified we avoid modifiying it again
//			if (it->hasBeenPerturbated()) {
//				continue;
//			}
//
//			if (changedEdges >= this->edgesToAlter) {
//				//TODO hack: using the normal distribution won't fetch us the expected probability, but always more.
//				//I know this will skew the perturbated edges on near the vrtex 0 node, but i prefer it over an overeastimate of the perturbation probability
//				continue;
//			}
//
//			//debug("lowerbound is", perturbationRange.getLB(), "upperbound is", perturbationRange.getUB());
//			std::uniform_int_distribution<int> weightDistribution{perturbationRange.getLB(), perturbationRange.getUB()};
//			if (coinDistribution(global_randomEngine) < ((edgesToAlter+0.0)/totalEdges)) {
//				//we need to change the arc
//				safe_int oldWeight =result.getWeightOfArc(static_cast<int>(v), it->target);
//				safe_int newWeight = 0;
//
//				if (global_perturbationModeStr == std::string{"ASSIGN"}) {
//					newWeight = weightDistribution(global_randomEngine);
//				} else if (global_perturbationModeStr == std::string{"SUM"}) {
//					newWeight = oldWeight + weightDistribution(global_randomEngine);
//				} else if (global_perturbationModeStr == std::string{"MULTIPLY"}) {
//					int multiplier = weightDistribution(global_randomEngine);
//					//debug("multiplier is", multiplier, "oldweigth is", oldWeight);
//					newWeight = oldWeight * multiplier;
//				} else {
//					throw std::invalid_argument{"invalid perturbateMode value. Have you set perturbate_mode?"};
//				}
//
//				if (newWeight < oldWeight) {
//					error("the perturbation may only increase weights, not decrease them! we're tyring to change", v, "->", it->target, "from", oldWeight, "to", newWeight);
//					throw std::domain_error{"can't replace weight with a smaller value!"};
//				}
//				result.changeWeightOfArc(static_cast<int>(v), it->target, static_cast<int>(newWeight));
//				changedEdges += 2; //we have changed both edges
//
//			}
//			//			debug("edge", v, "-->", it->target, "has weight", it->weight);
//		}
//	}
//
//	critical("changed", changedEdges, "of", totalEdges, "(aka ", (changedEdges/totalEdges)*100, "%)");
//
//	std::ofstream perturbationFile;
//	std::stringstream ss;
//	ss << global_outputTemplate << ".graph.info.txt";
//	perturbationFile.open(ss.str().c_str());
//
//	perturbationFile.precision(2);
//
//	perturbationFile << std::fixed;
//	perturbationFile << "changed edges: " << changedEdges << "\n";
//	perturbationFile << "total edges: " << totalEdges << "\n";
//	perturbationFile << "percentage edges: " << (changedEdges/totalEdges)*100 << "%\n";
//
//	perturbationFile.close();
//
//	//	newMapPrint.clear();
//	//	newMapPrint = std::string{global_outputTemplate};
//	//	newMapPrint.append("newMap2");
//	//	result.print(mapper, newMapPrint);
//
//
//	return result;
}

}
