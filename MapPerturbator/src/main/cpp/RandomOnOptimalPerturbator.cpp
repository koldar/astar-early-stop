/*
 * RandomOnOptimalPerturbator.cpp
 *
 *  Created on: Oct 25, 2018
 *      Author: koldar
 */

#include "RandomOnOptimalPerturbator.h"
#include <DynamicPathFinding/earlystop_dijkstra.h>
#include <DynamicPathFinding/ScenarioLoader.h>
#include <DynamicPathFinding/types.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include "globals.h"

namespace map_perturbator {

AdjGraph RandomOnOptimalPerturbator::perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map) {
	AdjGraph result = AdjGraph{originalMap};
	ScenarioLoader scenarioLoader{this->scenarioFilename.c_str()};

	dpf::big_integer loop = 0;
	dpf::big_integer edgeTotal = originalMap.getTotalNumberOfArcs();

	while(true) {
		debug("starting new cycle", edgeChanged, this->edgesToAlter);
		if (edgeChanged >= this->edgesToAlter) {
			debug("found enough edges!");
			break;
		}

		//PICK A RANDOM QUERY IN THE SCENARIO
		std::uniform_int_distribution<int> experimentDistribution{0, scenarioLoader.GetNumExperiments()-1};
		int randomQueryId = experimentDistribution(global_randomEngine);
		debug("we have picked experiment #", randomQueryId, " on a grand total of", scenarioLoader.GetNumExperiments());
		Experiment randomQuery{scenarioLoader.GetNthExperiment(randomQueryId)};

		//GENERATE THE OPTIMAL PATH
		EarlyStopDijkstra earlyStopDijkstra = EarlyStopDijkstra{result, false};
		dpf::nodeid_t start = mapper(randomQuery.getStart());
		dpf::nodeid_t goal = mapper(randomQuery.getGoal());
		std::vector<dpf::nodeid_t> optimalMoves = earlyStopDijkstra.run(start, goal);
		std::vector<xyLoc> optimalPath = earlyStopDijkstra.getPathAsVector(start, goal, mapper, optimalMoves);


		//PICK AN EDGE IN THE OPTIMAL PATH
		dpf::nodeid_t source;
		dpf::nodeid_t sink;
		this->pickArcInOptimalPath(result, mapper, randomQuery.getStart(), randomQuery.getGoal(), optimalPath, source, sink);
		auto weightToAlter = result.getWeightOfArc(source, sink);
		OutArc arc = const_cast<const AdjGraph&>(result).getArc(source, sink);

		if (arc.hasBeenPerturbated()) {
			warning("we have picked the arc", source, "->", sink, "which was already perturbated. Ignore it");
			continue;
		}


		//ALTER SAID ARC

		dpf::big_integer previousChangePerturbationsInPath = 0;
		if (global_validateChanges) {
			previousChangePerturbationsInPath = result.getNumberOfArcsInPathSuchThat(mapper, optimalPath, [&](dpf::big_integer i, dpf::nodeid_t id, OutArc a) -> bool {
				//we count the number of perturbated arcs on the path
				return a.hasBeenPerturbated();
			});
		}

		this->perturbateArc(mapper, result, source, sink, perturbationRange.getLB(), perturbationRange.getUB(), this->perturbationMode, global_randomEngine);

		if (global_validateChanges) {
			//we check that the perturbations in the path have been increased by 1
			dpf::big_integer afterChangePerturbationsInPath = result.getNumberOfArcsInPathSuchThat(mapper, optimalPath, [&](dpf::big_integer i, dpf::nodeid_t id, OutArc a) -> bool {
				//we count the number of perturbated arcs on the path
				return a.hasBeenPerturbated();
			});

			if (afterChangePerturbationsInPath != (previousChangePerturbationsInPath + 1)) {
				error("we were supposed to alter arc", source, "->", sink, "in path", optimalPath, " but before changing it the perturbation on the path were", previousChangePerturbationsInPath, "while afterwards it were", afterChangePerturbationsInPath);
				throw std::domain_error{"validate change has failed!"};
			}
		}

	}

	return result;

}

bool RandomOnOptimalPerturbator::pickArcInOptimalPath(const AdjGraph& map, const Mapper& mapper, const xyLoc& start, const xyLoc& goal, const std::vector<xyLoc>& path, dpf::nodeid_t& source, dpf::nodeid_t& sink) {
	//distrubtion genreate [a,b] but we want [a,b)
	std::uniform_int_distribution<int> experimentDistribution{0, static_cast<int>(path.size())-2};
	//we try 10 times before switching to a different path
	for (auto i=0; i<10; ++i) {
        int randomLocationInPath = experimentDistribution(global_randomEngine);

        source = mapper(path.at(randomLocationInPath));
        sink = mapper(path.at(randomLocationInPath + 1));

        if (!map.hasArcBeenPerturbated(source, sink)) {
            return true;
        }
	}
    return false;

}

}
