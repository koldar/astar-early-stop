/*
 * RandomTerrainPerturbator.cpp
 *
 *  Created on: Nov 10, 2018
 *      Author: koldar
 */


#include "RandomTerrainPerturbator.h"
#include <unordered_set>
#include <random>
#include "globals.h"

namespace map_perturbator {

RandomTerrainPerturbator::RandomTerrainPerturbator(const std::unordered_set<dpf::celltype_t>& possibleCellTypesToPerturbate, int numberOfCellTypeToPerturbate, const range<int>& range, const std::string& mode) :
perturbationRange{range}, perturbationMode{mode},
possibleCellTypesToPerturbate{possibleCellTypesToPerturbate}, numberOfCellTypeToPerturbate{numberOfCellTypeToPerturbate} {

}

RandomTerrainPerturbator::~RandomTerrainPerturbator() {

}

AdjGraph RandomTerrainPerturbator::perturbateMap(const AdjGraph& originalMap, const Mapper& mapper, const GridMap& map) {
	AdjGraph result{originalMap};

	//ok, first of all intersect all the possible types to perturbate to the available ones in the grid map
	std::unordered_set<dpf::celltype_t> mapCellTypes = map.getTraversableCellTypes();
	std::vector<dpf::celltype_t> intersection;

	critical("mapCellTypes is", mapCellTypes);
	critical("possible mcell rtypes is", this->possibleCellTypesToPerturbate);
	for(dpf::celltype_t el : this->possibleCellTypesToPerturbate) {

		if (mapCellTypes.find(el) != mapCellTypes.end()) {
			intersection.push_back(el);
		}

	}

	//ok, now we pick some random elements within intersection and perturbate all of them

	if (this->numberOfCellTypeToPerturbate > intersection.size()) {
		throw CpdException{"We want to perturbate %ld types fo cells, but we can perturbate only at most %ld!", this->numberOfCellTypeToPerturbate, intersection.size()};
	}

	for (int terrainToAlterId=0; terrainToAlterId<this->numberOfCellTypeToPerturbate; ++terrainToAlterId) {
		std::uniform_int_distribution<int> distribution(0, intersection.size() - 1);
		int id = distribution(global_randomEngine);
		dpf::celltype_t terrainToAlter = intersection.at(id);

		this->perturbateWholeTerrain(terrainToAlter, result, mapper, map);

		//remove the terrain jsut perturbated
		intersection.erase(intersection.begin() + id);
	}

	return result;

}


void RandomTerrainPerturbator::perturbateWholeTerrain(dpf::celltype_t terrainToAlter, AdjGraph& buildingMap, const Mapper& mapper, const GridMap& map) {
	for (dpf::nodeid_t source=0; source<buildingMap.node_count(); ++source) {
		for (int i=0; i<buildingMap.out_deg(source); ++i) {
			OutArc arc = const_cast<const AdjGraph&>(buildingMap).getIthOutArc(source, i);
			if (arc.hasBeenPerturbated()) {
				continue;
			}

			xyLoc sourceLoc = mapper(source);
			xyLoc sinkLoc = mapper(arc.target);
			if ((map.getValue(sourceLoc.x, sourceLoc.y) == terrainToAlter) && (map.getValue(sinkLoc.x, sinkLoc.y) == terrainToAlter)) {
				this->perturbateArc(mapper, buildingMap, source, static_cast<dpf::nodeid_t>(arc.target), this->perturbationRange.getLB(), this->perturbationRange.getUB(), this->perturbationMode, global_randomEngine);
			}

		}
	}
}

}
