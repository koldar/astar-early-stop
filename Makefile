
.PHONY: clean fetch_warthog warthog apply_patch DynamicPathFinding PathTesterUtils MapPerturbator DynamicPathFindingTester

all: warthog DynamicPathFinding MapPerturbator DynamicPathFindingTester

uninstall:
	cd DynamicPathFinding/build/Release; \
	sudo make uninstall
	
	cd PathTesterUtils/build/Release; \
	sudo make uninstall
	
	cd MapPerturbator/build/Release; \
	sudo make uninstall

	cd DynamicPathFindingTester/build/Release; \
	sudo make uninstall

test: DynamicPathFindingTest PathTesterUtilsTest MapPerturbatorTest DynamicPathFindingTesterTest
	cd DynamicPathFinding/build/Debug; ./DynamicPathFindingTest --abort

clean: 
	cd DynamicPathFinding/build/Release; \
	echo "pwd is "`pwd`; \
	make clean; \
	cd ..; \
	rm -rfv Release/; \
	mkdir -pv Release

	cd PathTesterUtils/build/Release; \
	echo "pwd is "`pwd`; \
	make clean; \
	cd ..; \
	rm -rfv Release/; \
	mkdir -pv Release

	cd MapPerturbator/build/Release; \
	echo "pwd is "`pwd`; \
	make clean; \
	cd ..; \
	rm -rfv Release/; \
	mkdir -pv Release

	cd DynamicPathFindingTester/build/Release; \
	echo "pwd is "`pwd`; \
	make clean; \
	cd ..; \
	rm -rfv Release/; \
	mkdir -pv Release

fetch_warthog:
	if [ ! -d "pathfinding" ]; then \
		git clone https://bitbucket.org/dharabor/pathfinding.git; \
	fi

apply_patch: fetch_warthog
	cp -v others/offline_jump_point_locator.cpp pathfinding/src/jps/
	cp -v others/offline_jump_point_locator2.cpp pathfinding/src/jps/
	cp -v others/Makefile pathfinding/src/

warthog: apply_patch
	cd pathfinding/src/; \
	make fast; \
	sudo make install; \
	sudo ldconfig

DynamicPathFinding:
	cd DynamicPathFinding; \
	mkdir -pv build/Release; \
	cd build/Release; \
	cmake ../..; \
	make; \
	sudo make install; \
	sudo ldconfig

PathTesterUtils: PathTesterUtils
	cd PathTesterUtils; \
	mkdir -pv build/Release; \
	cd build/Release; \
	cmake ../..; \
	make; \
	sudo make install; \
	sudo ldconfig

MapPerturbator: DynamicPathFinding PathTesterUtils
	cd MapPerturbator; \
	mkdir -pv build/Release; \
	cd build/Release; \
	cmake ../..; \
	make; \
	sudo make install; \
	sudo ldconfig

DynamicPathFindingTester: DynamicPathFinding PathTesterUtils
	cd DynamicPathFindingTester; \
	mkdir -pv build/Release; \
	cd build/Release; \
	cmake ../..; \
	make; \
	sudo make install; \
	sudo ldconfig

DynamicPathFindingTest:
	cd DynamicPathFinding; \
	mkdir -pv build/Debug; \
	cd build/Debug; \
	cmake ../..; \
	make

PathTesterUtilsTest: PathTesterUtilsTest
	cd PathTesterUtils; \
	mkdir -pv build/Debug; \
	cd build/Debug; \
	cmake ../..; \
	make;

MapPerturbatorTest: DynamicPathFindingTest PathTesterUtilsTest
	cd MapPerturbator; \
	mkdir -pv build/Debug; \
	cd build/Debug; \
	cmake ../..; \
	make

DynamicPathFindingTesterTest: DynamicPathFindingTest PathTesterUtilsTest
	cd DynamicPathFindingTester; \
	mkdir -pv build/Debug; \
	cd build/Debug; \
	cmake ../..; \
	make
	